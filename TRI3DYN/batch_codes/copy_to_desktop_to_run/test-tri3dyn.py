# basic system and numerical packages
import sys
import numpy as np
import matplotlib.pyplot as plt
import pycraters as pc

# build wrapper and params
exec_location = sys.argv[1]
wrapper = pc.create_wrapper("TRI3DYN", exec_location)
params  = wrapper.create_parameters()

#basic parameter setup
params.energy = 1000
params.angle = None
params.impacts = 1000
params.target = [["Al", 1.0]]
params.beam = "Xe"

## Iterable energy and angle
## do the simulations
# angles = np.linspace(0,88,89)
# energy = 300 #np.linspace(200, 1000, 41)
# for aa in angles:
#     for ee in energy:
#         print("running angle %02d." % (aa))
#         params.angle = aa
#         params.energy = ee
#         wrapper.go(params, save_raw_data=False)

# do the simulations
angles = np.linspace(0,89,90)
energy = 300 #np.linspace(200, 1000, 41)
for aa in angles:
    print("running angle %02d." % (aa))
    params.angle = aa
    params.energy = energy
    wrapper.go(params, save_raw_data=False)


