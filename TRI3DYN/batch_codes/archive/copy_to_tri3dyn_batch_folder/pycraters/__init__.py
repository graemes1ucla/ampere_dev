
import pycraters.wrappers
import pycraters.statistics
import pycraters.fits
import pycraters.schedulers

import pycraters.IO


def create_wrapper(codestring, execline, optsdict=None):
      
  if codestring == "TRI3DYN":
    return wrappers.TRI3DYN.TRI3DYN_Wrapper(execline)
  
  if codestring == "TRI3DST":
    return wrappers.TRI3DST.TRI3DST_Wrapper(execline)

  if codestring == "SDTRIMSP":
    return wrappers.SDTRIMSP.SDTRIMSP_Wrapper(execline)

  if codestring == "PARCAS":
    return wrappers.PARCAS.PARCAS_Wrapper(execline)
