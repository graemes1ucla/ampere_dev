from pathlib import Path
import numpy as np
import os
import matplotlib.pyplot as plt
from matplotlib import cm
import linecache
from scipy import interpolate, ndimage, optimize
from matplotlib.ticker import LinearLocator
import rotanimate
import time
import scipy as sp



#if dirs are in lower directory
# dir = os.path.dirname(os.path.realpath(__file__))

#if dirs are in some other location
dir = r'C:\Users\GEM\Desktop\run_tri3dyn_small'

maxEnergy = 1010 #eV
energyList = list(range(100, maxEnergy, 10))

angleList = list(range(-1, 90, 1))

for a, an in enumerate(angleList):
    print("Angle: {}".format(an))
    for e, en in enumerate(energyList):
        temp_dir = os.path.join(dir, 'Al-Xe-' + str(en).zfill(5) + 'eV-'+ str(an).zfill(2)+ 'deg-')
        if os.path.exists(temp_dir):
            for m in os.listdir(path=temp_dir):
                if m.endswith("out.dat"):
                    try:
                        line = linecache.getline(os.path.join(temp_dir, m), 91)
                    except:
                        print("coundn't read line")
                    new_row = [a, an, e, en, float(line.split()[-1])]
                    try:
                        if isinstance(sput_arr, np.ndarray):
                            sput_arr = np.vstack([sput_arr, new_row])
                    except:
                        sput_arr = np.array([new_row])                     
        else:
            pass
            # print("No dir at Energy:  {}, Angle: {}".format(en, an))

# np.savetxt("sput_arr", sput_arr, delimiter = ',')
    
fig = plt.figure()
ax = fig.add_subplot(projection='3d')

X = sput_arr[:, 1]
Y = sput_arr[:, 3]
Z = sput_arr[:, 4]

print(X)
print(Y)
print(Z)

Z = np.reshape(Z, (len(np.unique(X)), len(np.unique(Y))))
anSpacing = abs(np.unique(X)[1]-np.unique(X)[2])
enSpacing = abs(np.unique(Y)[1]-np.unique(Y)[2])

X, Y = np.meshgrid(np.arange(min(np.unique(X)), max(np.unique(X))+1, int(anSpacing)), \
                        np.arange(min(np.unique(Y)), max(np.unique(Y))+1, int(enSpacing)))
print(Z.shape, X.shape, Y.shape)

surf = ax.plot_surface(X.T, Y.T, Z, cmap=cm.plasma,
                       linewidth=0, rcount=200, ccount = 200, antialiased=True)
cset = ax.contourf(X.T, Y.T, Z, zdir='z', offset=-1, cmap=cm.plasma)
cset = ax.contourf(X.T, Y.T, Z, zdir='x', offset=-10, cmap=cm.plasma)
cset = ax.contourf(X.T, Y.T, Z, zdir='y', offset=-10, cmap=cm.plasma)


# Add a color bar which maps values to colors.
fig.colorbar(surf, shrink=0.5, aspect=5)

ax.set_xlabel('$Angle, degrees$', fontsize=10, rotation=150)
ax.set_ylabel('$Energy, eV$', fontsize = 10)
ax.set_zlabel(r'$Sputter Yield$', fontsize=10, rotation=150)

plt.xticks([0, 20, 40, 60, 80])
plt.yticks([200, 400, 600, 800, 1000])

# angles = np.linspace(0, 360, 200)[:-1] # Generate viewing angles
# rotanimate.rotanimate(ax, angles)

plt.show()

print('done')

# ----------------------------------------------------------- PDF section
# dir = os.path.dirname(os.path.realpath(__file__))
# print(dir)

# maxEnergy = 1010 #eV
# # energyList = list(range(100, maxEnergy, 10))
# energyList = 300


# angleList = list(range(-1, 90, 1))
# angleList = 46

# # for a, an in enumerate(angleList):
# print("Angle: {}".format(angleList))
# temp_dir = os.path.join(dir, 'Al-Xe-' + str(energyList).zfill(5) + 'eV-'+ str(angleList).zfill(2)+ 'deg-')
# print(temp_dir)
# if os.path.exists(temp_dir):
#     for m in os.listdir(path=temp_dir):
#         if m.endswith("splst.dat"):
#             print(os.path.join(temp_dir, m))
#             with open(os.path.join(temp_dir, m)) as f:
#                 for line in f:
#                     new_row = [float(line.split()[5]), float(line.split()[6]), float(line.split()[7])]
#                     print(new_row)
#                     try:
#                         if isinstance(sput_arr, np.ndarray):
#                             sput_arr = np.vstack([sput_arr, new_row])
#                     except:
#                         sput_arr = np.array([new_row])
               
# else:
#     pass

# polar_plotting = 1;
# fig = plt.figure()
# if polar_plotting:
#     ax = fig.add_subplot(111, projection='polar', xlim=(-90, 90))
# elif not polar_plotting:
#     ax = fig.add_subplot(111)
                    
# #Save csv
# # np.savetxt("sputter_data.csv", sput_arr, delimiter=",")

# # if there's enough sputtered atoms and this is an energy to be displayed
# # plot a PDF for this ion energy
# # convert euler angles to x, y and z components of sputtered atom vector
# Alpha_arr_comp = np.cos((sput_arr[:,0])*np.pi/180)
# Beta_arr_comp = np.cos(sput_arr[:,1]*np.pi/180)
# Gamma_arr_comp = np.cos(sput_arr[:,2]*np.pi/180)

# # Create PDFs of vectors in spherical coordinates (physics convention) 
# counts_phi = np.arctan(Beta_arr_comp / Alpha_arr_comp) #Spans -pi/2 to pi/2 (measured from azimuth, +x)
# counts_theta = np.arccos(Gamma_arr_comp) #Spans 0 to pi (measured from zenith, +z)
# # assert(size(counts_phi) == size(counts_theta)), "different number of phi/theta entries"

# # fig = plt.figure()
# # ax = fig.add_subplot(111, projection='polar', xlim=(-90, 90))

# # Create linspaces that span the limits of the dataset
# div = 361
# phi_lower_limit = -np.pi/2
# phi_upper_limit = np.pi/2
# theta_target = np.pi/2
# theta_threshold = (np.pi/180)                   
# dist_space_phi = np.linspace(phi_lower_limit, phi_upper_limit, div)

# points =  np.vstack((counts_phi,counts_theta)).T
# points_reduced = np.empty([1,2])

# #Approach 2
# app_2_tic = time.perf_counter()
# points_reduced = points[(points[:,1] > theta_target-theta_threshold) & (points[:,1] < theta_target+theta_threshold)]
# print(points_reduced)
# app_2_toc = time.perf_counter()
# app_2_time = time.strftime('%H:%M:%S', time.gmtime(app_2_toc-app_2_tic))
# print("App 2 time: ", app_2_time)    
# print("Total points in reduced dataset: ", np.shape(points_reduced)[0]) 

# points_reduced = np.sort(points_reduced[:,0], axis = 0)

# app_post_1_tic = time.perf_counter()
# #Approach 1
# hist, bin_edges = np.histogram(points_reduced, bins=div, range=(phi_lower_limit, phi_upper_limit))
# app_post_1_toc = time.perf_counter()
# app_post_1_time = time.strftime('%H:%M:%S', time.gmtime(app_post_1_toc-app_post_1_tic))
# print("App post 1 time: ", app_post_1_time)                        
            
# # Apply gaussian filter
# sigma = 1
# hist = sp.ndimage.filters.gaussian_filter(hist, sigma, mode='constant')
            
# #Normalize
# norm_start = 1
# f = lambda var_1: abs(np.trapz(hist/var_1, x=dist_space_phi)-1)
# norm_result = sp.optimize.minimize(f, norm_start, tol=1e-10)
# hist_norm = norm_result['x'][0]   

# text_file_name = str(tuple[0]) + r"_" + str(tuple[1]) + r"_"  + r"_" + str(time.time()) + "r_SMOOTHED_SIGMA_1" + r".csv"
# np.savetxt(text_file_name, hist, delimiter=",")            

# # Polar Plotting 
# if polar_plotting:
#     ax.set_thetamin(-90) # set the limits
#     ax.set_thetamax(90)
#     ax.set_theta_direction(-1)
#     ax.set_theta_offset(.5*np.pi) # point the origin towards the top
#     ax.set_thetagrids(range(-90, 91, 10)) # set the gridlines
#     ax.set_title('2D Sputterant Probability Distribution', pad=-50) # add title and relocate negative value lowers the location
#     ax.plot(dist_space_phi, hist/hist_norm, label= r"$E_i = {}~eV, \theta_i = {}\degree$".format(tuple[0],tuple[1]))
#     ax.set_rlabel_position(-22.5)  # Move radial labels away from plotted line
#     ax.grid(True)
#     plt.legend(loc="upper right", bbox_to_anchor=(1.5, 0.75))
#     # plot_file_name = str(tuple[0]) + r"_" + str(tuple[1]) + r"_" + tname + r"_" + str(time.time()) + r".png"
#     # plt.savefig(self.user_inputs.output_dir + r"//" + plot_file_name, dpi=300, bbox_inches='tight')

# # Cartesian Plotting
# elif not polar_plotting:
#     ax.set_title('2D Sputterant Probability Distribution', pad=-50) # add title and relocate negative value lowers the location
#     ax.plot(dist_space_phi, hist/hist_norm, label= r"$E_i = {}~eV, \theta_i = {}\degree$".format(tuple[0],tuple[1]))
#     ax.grid(True)
#     plt.legend(loc="upper right", bbox_to_anchor=(1.5, 0.75))
#     plot_file_name = str(tuple[0]) + r"_" + str(tuple[1]) + r"_" + tname + r"_" + str(time.time()) + r".png"
#     # plt.savefig(self.user_inputs.output_dir + r"//" + plot_file_name, dpi=300, bbox_inches='tight')
    

# # Update previous energy from angle/energy simulation combination 
# prev_nrg = tuple[0]
# # plt.show()



# plt.show()
