


from pathlib import Path
import numpy as np
import os
import matplotlib.pyplot as plt
from matplotlib import cm
import linecache
import scipy as sp
import scipy.ndimage
import scipy.optimize
import sys
from matplotlib.ticker import LinearLocator
import time
import fileinput
from numpy import size


# # ----------------------------------------------------------- PDF 3d surf plot section

#if dirs are in lower directory
# dir = os.path.dirname(os.path.realpath(__file__))

#if dirs are in some other location
dir = r'C:\Users\GEM\Desktop\run_tri3dyn_square_angles'#r'C:\Users\GEM\Desktop\run_tri3dyn_square_angles'

#Energy----------------------------
energyList = 300

#Angle-----------------------------
angleList = 60

# Create linspaces that span the limits of the dataset
div = 181
phi_lower_limit = -np.pi/2
phi_upper_limit = np.pi/2
theta_target = np.pi/2
theta_threshold = (np.pi/90)  # Alter to change the width about the midplane that trajectories are gathered                 
dist_space_phi = np.linspace(phi_lower_limit, phi_upper_limit, div)


sput_arr = np.empty([2000000, 3])
i = 0
sec_1_tic = time.perf_counter()    
print("Angle: {}".format(angleList))
temp_dir = os.path.join(dir, 'Al-Xe-' + str(energyList).zfill(5) + 'eV-'+ str(angleList).zfill(2)+ 'deg-')
print(temp_dir)
if os.path.exists(temp_dir):
    for m in os.listdir(path=temp_dir):
        if m.endswith("splst.dat"):
            for line in fileinput.input(files = os.path.join(temp_dir, m)):
                if (np.arccos(float(line.split()[7])) > theta_target-theta_threshold) & (np.arccos(float(line.split()[7])) < theta_target+theta_threshold):
                    new_row = [float(line.split()[5]), float(line.split()[6]), float(line.split()[7])]
                    if isinstance(sput_arr, np.ndarray):
                        sput_arr[i, :] = new_row
                        i = i+1                                
else:
    pass

print("done file analysis")
                    

# if there's enough sputtered atoms and this is an energy to be displayed
# plot a PDF for this ion energy
# convert euler angles to x, y and z components of sputtered atom vector
Alpha_arr_comp = ((sput_arr[:,0]))
Beta_arr_comp = ((sput_arr[:,1]))
Gamma_arr_comp = ((sput_arr[:,2]))

# Create PDFs of vectors in spherical coordinates (physics convention) 
counts_phi = np.arctan(Beta_arr_comp / Alpha_arr_comp) #Spans -pi/2 to pi/2 (measured from azimuth, +x)
counts_theta = np.arccos(Gamma_arr_comp) #Spans 0 to pi (measured from zenith, +z)

points =  np.vstack((counts_phi,counts_theta)).T
points_reduced = np.empty([1,2])
points_reduced = points #[(points[:,1] > theta_target-theta_threshold) & (points[:,1] < theta_target+theta_threshold)]
print("Total points in reduced dataset: ", np.shape(points_reduced)[0]) 
points_reduced = np.sort(points_reduced[:,0], axis = 0)
hist, bin_edges = np.histogram(points_reduced, bins=div, range=(phi_lower_limit, phi_upper_limit))               
            
# Apply gaussian filter
sigma = 1
hist = sp.ndimage.filters.gaussian_filter(hist, sigma, mode='constant')

#Normalize
norm_start = 1
f = lambda var_1: abs(np.trapz(hist/var_1, x=dist_space_phi)-1)
norm_result = sp.optimize.minimize(f, norm_start, tol=1e-10)
hist_norm = norm_result['x'][0]

Z = np.array([hist/hist_norm])   

sec_1_toc = time.perf_counter()
sec_1_time = time.strftime('%H:%M:%S', time.gmtime(sec_1_toc-sec_1_tic))
print("Sec 1 time: ", sec_1_toc-sec_1_tic)
time_data = np.array([sec_1_toc-sec_1_tic, np.shape(points_reduced)[0]])  

np.savetxt("Z.csv", Z, delimiter = ',')
np.savetxt("time_data.csv", time_data, delimiter = ',')

# Polar Plotting 
fig = plt.figure()
ax = fig.add_subplot(111, projection='polar', xlim=(-90, 90))
ax.set_thetamin(-90) # set the limits
ax.set_thetamax(90)
ax.set_theta_direction(-1)
ax.set_theta_offset(.5*np.pi) # point the origin towards the top
ax.set_thetagrids(range(-90, 91, 10)) # set the gridlines
ax.set_title('2D Sputterant Probability Distribution', pad=-50) # add title and relocate negative value lowers the location
ax.plot(dist_space_phi, hist/hist_norm, label= r"$E_i = {}~eV, \theta_i = {}\degree$".format(energyList,angleList))
ax.set_rlabel_position(-22.5)  # Move radial labels away from plotted line
ax.grid(True)
plt.legend(loc="upper right", bbox_to_anchor=(1.5, 0.75))
# plot_file_name = str(tuple[0]) + r"_" + str(tuple[1]) + r"_" + tname + r"_" + str(time.time()) + r".png"
# plt.savefig(self.user_inputs.output_dir + r"//" + plot_file_name, dpi=300, bbox_inches='tight')

print('done')
plt.show()