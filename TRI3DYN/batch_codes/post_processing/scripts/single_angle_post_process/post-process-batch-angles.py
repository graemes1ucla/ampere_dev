from pathlib import Path
import numpy as np
import os
import matplotlib.pyplot as plt
from matplotlib import cm
import linecache
import scipy as sp
import scipy.ndimage
import scipy.optimize
import sys
from matplotlib.ticker import LinearLocator
import time
import fileinput
from numpy import size


# # ----------------------------------------------------------- PDF 3d surf plot section

#if dirs are in lower directory
# dir = os.path.dirname(os.path.realpath(__file__))

#if dirs are in some other location
dir = r'C:\Users\GEM\Desktop\run_tri3dyn_square_angles'

#Energy----------------------------
energyList = 300

#Angle-----------------------------
angleList = 30

# Create linspaces that span the limits of the dataset
div = 721
phi_lower_limit = -np.pi/2
phi_upper_limit = np.pi/2
theta_target = np.pi/2
theta_threshold = (np.pi/36)  # Alter to change the width about the midplane that trajectories are gathered                 
dist_space_phi = np.linspace(phi_lower_limit, phi_upper_limit, div)

sput_arr = np.empty([500000, 3])
sec_1_tic = time.perf_counter()    
print("Angle: {}".format(angleList))
temp_dir = os.path.join(dir, 'Al-Xe-' + str(energyList).zfill(5) + 'eV-'+ str(angleList).zfill(2)+ 'deg-')
print(temp_dir)
if os.path.exists(temp_dir):
    for m in os.listdir(path=temp_dir):
        if m.endswith("splst.dat"):
            for line in fileinput.input(files = os.path.join(temp_dir, m)):
                if (np.arccos(float(line.split()[7])) > theta_target-theta_threshold) & (np.arccos(float(line.split()[7])) < theta_target+theta_threshold):
                    new_row = [float(line.split()[5]), float(line.split()[6]), float(line.split()[7])]
                    try:
                        if isinstance(sput_arr, np.ndarray):
                            sput_arr = np.vstack([sput_arr, new_row])
                            print(np.shape(sput_arr)[0])
                    except:
                        sput_arr = np.array([new_row])                                
else:
    pass

print("done file analysis")
                    

# if there's enough sputtered atoms and this is an energy to be displayed
# plot a PDF for this ion energy
# convert euler angles to x, y and z components of sputtered atom vector
Alpha_arr_comp = ((sput_arr[:,0]))
Beta_arr_comp = ((sput_arr[:,1]))
Gamma_arr_comp = ((sput_arr[:,2]))

# Create PDFs of vectors in spherical coordinates (physics convention) 
counts_phi = np.arctan(Beta_arr_comp / Alpha_arr_comp) #Spans -pi/2 to pi/2 (measured from azimuth, +x)
counts_theta = np.arccos(Gamma_arr_comp) #Spans 0 to pi (measured from zenith, +z)

points =  np.vstack((counts_phi,counts_theta)).T
points_reduced = np.empty([1,2])
points_reduced = points #[(points[:,1] > theta_target-theta_threshold) & (points[:,1] < theta_target+theta_threshold)]
print("Total points in reduced dataset: ", np.shape(points_reduced)[0]) 
points_reduced = np.sort(points_reduced[:,0], axis = 0)
hist, bin_edges = np.histogram(points_reduced, bins=div, range=(phi_lower_limit, phi_upper_limit))               
            
# Apply gaussian filter
sigma = 1
hist = sp.ndimage.filters.gaussian_filter(hist, sigma, mode='constant')
            
#Normalize
norm_start = 1
f = lambda var_1: abs(np.trapz(hist/var_1, x=dist_space_phi)-1)
norm_result = sp.optimize.minimize(f, norm_start, tol=1e-10)
hist_norm = norm_result['x'][0]

Z = np.array([hist/hist_norm])   

sec_1_toc = time.perf_counter()
sec_1_time = time.strftime('%H:%M:%S', time.gmtime(sec_1_toc-sec_1_tic))
print("Sec 1 time: ", sec_1_toc-sec_1_tic)
time_data = np.array([sec_1_toc-sec_1_tic, a[1], np.shape(points_reduced)[0]])  

np.savetxt("Z.csv", Z, delimiter = ',')
np.savetxt("time_data.csv", time_data, delimiter = ',')

# Polar Plotting 
ax = fig.add_subplot(111, projection='polar', xlim=(-90, 90))
ax.set_thetamin(-90) # set the limits
ax.set_thetamax(90)
ax.set_theta_direction(-1)
ax.set_theta_offset(.5*np.pi) # point the origin towards the top
ax.set_thetagrids(range(-90, 91, 10)) # set the gridlines
ax.set_title('2D Sputterant Probability Distribution', pad=-50) # add title and relocate negative value lowers the location
ax.plot(dist_space_phi, hist/hist_norm, label= r"$E_i = {}~eV, \theta_i = {}\degree$".format(energyList,angleList))
ax.set_rlabel_position(-22.5)  # Move radial labels away from plotted line
ax.grid(True)
plt.legend(loc="upper right", bbox_to_anchor=(1.5, 0.75))
# plot_file_name = str(tuple[0]) + r"_" + str(tuple[1]) + r"_" + tname + r"_" + str(time.time()) + r".png"
# plt.savefig(self.user_inputs.output_dir + r"//" + plot_file_name, dpi=300, bbox_inches='tight')

print('done')
plt.show()

# Single PDF plot --------------------------------------------------------------------------------------------------------------

#if dirs are in lower directory
# dir = os.path.dirname(os.path.realpath(__file__))

# #if dirs are in some other location
# dir = r'G:\My Drive\Projects\AMPERE\ampere_dev\TRI3DYN\batch_codes\post_processing\300ev_tests'
# polar_plotting = 1

# #Energy----------------------------
# # maxEnergy = 1010 #eV
# # energyList = list(range(100, maxEnergy, 10))
# energyList = 300

# #Angle-----------------------------
# angleList = list(range(-1, 90, 1))
# angleList = 0


# print("Angle: {}".format(angleList))
# temp_dir = os.path.join(dir, 'Al-Xe-' + str(energyList).zfill(5) + 'eV-'+ str(angleList).zfill(2)+ 'deg-')
# print(temp_dir)
# if os.path.exists(temp_dir):
#     for m in os.listdir(path=temp_dir):
#         if m.endswith("splst.dat"):
#             with open(os.path.join(temp_dir, m)) as f:
#                 for index, line in enumerate(f):
#                     new_row = ["200", "200", float(line.split()[5]), float(line.split()[6]), float(line.split()[7])]
#                     try:
#                         if isinstance(sput_arr, np.ndarray):
#                             sput_arr = np.vstack([sput_arr, new_row])
#                             if index%1000 == 0:
#                               print(index)
#                     except:
#                         sput_arr = np.array([new_row])
               
# else:
#     pass

# print("done file analysis")
# fig = plt.figure()
# if polar_plotting:
#     ax = fig.add_subplot(111, projection='polar', xlim=(-90, 90))
# elif not polar_plotting:
#     ax = fig.add_subplot(111)
                    

# # if there's enough sputtered atoms and this is an energy to be displayed
# # plot a PDF for this ion energy
# # convert euler angles to x, y and z components of sputtered atom vector
# Alpha_arr_comp = ((sput_arr[:,2]))
# Beta_arr_comp = ((sput_arr[:,3]))
# Gamma_arr_comp = ((sput_arr[:,4]))



# # Create PDFs of vectors in spherical coordinates (physics convention) 
# counts_phi = np.arctan(Beta_arr_comp / Alpha_arr_comp) #Spans -pi/2 to pi/2 (measured from azimuth, +x)
# counts_theta = np.arccos(Gamma_arr_comp) #Spans 0 to pi (measured from zenith, +z)
# # assert(size(counts_phi) == size(counts_theta)), "different number of phi/theta entries"
# np.savetxt("counts_phi.csv", counts_phi, delimiter = ',')
# np.savetxt("counts_theta.csv", counts_theta, delimiter = ',')


# # fig = plt.figure()
# # ax = fig.add_subplot(111, projection='polar', xlim=(-90, 90))

# # Create linspaces that span the limits of the dataset
# div = 361
# phi_lower_limit = -np.pi/2
# phi_upper_limit = np.pi/2
# theta_target = np.pi/2
# theta_threshold = (np.pi/90)                   
# dist_space_phi = np.linspace(phi_lower_limit, phi_upper_limit, div)

# points =  np.vstack((counts_phi,counts_theta)).T
# print(points)
# points_reduced = np.empty([1,2])

# #Approach 2
# app_2_tic = time.perf_counter()
# points_reduced = points[(points[:,1] > theta_target-theta_threshold) & (points[:,1] < theta_target+theta_threshold)]
# app_2_toc = time.perf_counter()
# app_2_time = time.strftime('%H:%M:%S', time.gmtime(app_2_toc-app_2_tic))
# print("App 2 time: ", app_2_time)    
# print("Total points in reduced dataset: ", np.shape(points_reduced)[0]) 
# points_reduced = np.sort(points_reduced[:,0], axis = 0)
# app_post_1_tic = time.perf_counter()

# #Approach 1
# hist, bin_edges = np.histogram(points_reduced, bins=div, range=(phi_lower_limit, phi_upper_limit))
# app_post_1_toc = time.perf_counter()
# app_post_1_time = time.strftime('%H:%M:%S', time.gmtime(app_post_1_toc-app_post_1_tic))
# print("App post 1 time: ", app_post_1_time)                        
            
# # Apply gaussian filter
# sigma = 1
# hist = sp.ndimage.filters.gaussian_filter(hist, sigma, mode='constant')
            
# #Normalize
# norm_start = 1
# f = lambda var_1: abs(np.trapz(hist/var_1, x=dist_space_phi)-1)
# norm_result = sp.optimize.minimize(f, norm_start, tol=1e-10)
# hist_norm = norm_result['x'][0]   

# # text_file_name = str(tuple[0]) + r"_" + str(tuple[1]) + r"_" + tname + r"_" + str(time.time()) + "r_SMOOTHED_SIGMA_1" + r".csv"
# # np.savetxt(text_file_name, hist, delimiter=",")            

# # Polar Plotting 
# if polar_plotting:
#     ax.set_thetamin(-90) # set the limits
#     ax.set_thetamax(90)
#     ax.set_theta_direction(-1)
#     ax.set_theta_offset(.5*np.pi) # point the origin towards the top
#     ax.set_thetagrids(range(-90, 91, 10)) # set the gridlines
#     ax.set_title('2D Sputterant Probability Distribution', pad=-50) # add title and relocate negative value lowers the location
#     ax.plot(dist_space_phi, hist/hist_norm, label= r"$E_i = {}~eV, \theta_i = {}\degree$".format(tuple[0],tuple[1]))
#     ax.set_rlabel_position(-22.5)  # Move radial labels away from plotted line
#     ax.grid(True)
#     plt.legend(loc="upper right", bbox_to_anchor=(1.5, 0.75))
#     # plot_file_name = str(tuple[0]) + r"_" + str(tuple[1]) + r"_" + tname + r"_" + str(time.time()) + r".png"
#     # plt.savefig(self.user_inputs.output_dir + r"//" + plot_file_name, dpi=300, bbox_inches='tight')

# # Cartesian Plotting
# elif not polar_plotting:
#     ax.set_title('2D Sputterant Probability Distribution', pad=-50) # add title and relocate negative value lowers the location
#     ax.plot(dist_space_phi, hist/hist_norm, label= r"$E_i = {}~eV, \theta_i = {}\degree$".format(tuple[0],tuple[1]))
#     ax.grid(True)
#     plt.legend(loc="upper right", bbox_to_anchor=(1.5, 0.75))
#     # plot_file_name = str(tuple[0]) + r"_" + str(tuple[1]) + r"_" + tname + r"_" + str(time.time()) + r".png"
#     # plt.savefig(self.user_inputs.output_dir + r"//" + plot_file_name, dpi=300, bbox_inches='tight')
    

# # Update previous energy from angle/energy simulation combination 
# # prev_nrg = tuple[0]
# # plt.show()



# plt.show()



# #3D plot ------------------
# dir = os.path.dirname(os.path.realpath(__file__))
# polar_plotting = 1
# maxEnergy = 1010 #eV
# # energyList = list(range(100, maxEnergy, 10))
# energyList = 300


# angleList = list(range(-1, 90, 1))
# angleList = 0

# # for a, an in enumerate(angleList):
# print("Angle: {}".format(angleList))
# temp_dir = os.path.join(dir, 'Al-Xe-' + str(energyList).zfill(5) + 'eV-'+ str(angleList).zfill(2)+ 'deg-')
# print(temp_dir)
# if os.path.exists(temp_dir):
#     for m in os.listdir(path=temp_dir):
#         if m.endswith("splst.dat"):
#             with open(os.path.join(temp_dir, m)) as f:
#                 for index, line in enumerate(f):
#                     new_row = [45, 45, 200, 200, float(line.split()[5]), float(line.split()[6]), float(line.split()[7])]
#                     try:
#                         if isinstance(sput_arr, np.ndarray):
#                             sput_arr = np.vstack([sput_arr, new_row])
#                             print(index)
#                     except:
#                         sput_arr = np.array([new_row])
               
# else:
#     pass



# # if there's enough sputtered atoms and this is an energy to be displayed
# # plot a PDF for this ion energy
# # convert euler angles to x, y and z components of sputtered atom vector
# Alpha_arr_comp = ((sput_arr[:,4]))
# Beta_arr_comp = ((sput_arr[:,5]))
# Gamma_arr_comp = ((sput_arr[:,6]))
# # np.savetxt("counts_alpha.csv", Alpha_arr_comp, delimiter = ',')
# # np.savetxt("counts_beta.csv", Beta_arr_comp, delimiter = ',')
# # np.savetxt("counts_gamma.csv", Gamma_arr_comp, delimiter = ',')



# # Create PDFs of vectors in spherical coordinates (physics convention) 
# counts_phi = np.arctan(Beta_arr_comp / Alpha_arr_comp) #Spans -pi/2 to pi/2 (measured from azimuth, +x)
# counts_theta = np.arccos(Gamma_arr_comp) #Spans 0 to pi (measured from zenith, +z)
# np.savetxt("counts_phi.csv", counts_phi, delimiter = ',')

# # Create linspaces that span the limits of the dataset
# div = 100
# phi_lower_limit = -np.pi/2
# phi_upper_limit = np.pi/2
# theta_lower_limit = 0
# theta_upper_limit = np.pi                    
# dist_space_phi = np.linspace(phi_lower_limit, phi_upper_limit, div)
# dist_space_theta = np.linspace(theta_lower_limit, theta_upper_limit, div)
# threshold = np.pi/div

# PHI, THETA = np.meshgrid(dist_space_phi, dist_space_theta)

# hist = np.zeros((size(dist_space_theta),size(dist_space_phi)))
# np.set_printoptions(threshold=sys.maxsize)

# for t in range(size(dist_space_theta)):
#     for p in range(size(dist_space_phi)):
#         for i in range(size(counts_phi)):
#             if counts_phi[i] < dist_space_phi[p] + threshold/2 and \
#                 counts_phi[i] > dist_space_phi[p] - threshold/2  and \
#                 counts_theta[i] < dist_space_theta[t] + threshold/2 and \
#                 counts_theta[i] >  dist_space_theta[t] - threshold/2:
#                 hist[t][p] = hist[t][p] + 1
#             else:
#                 print

# # Apply gaussian filter
# sigma_x = 5
# sigma_y = 5
# sigma = [sigma_y, sigma_x]
# hist = sp.ndimage.filters.gaussian_filter(hist, sigma, mode='constant')
# print(hist)

# # #Convert back to Cartesian
# X = (hist * np.sin(THETA) * np.cos(PHI))/sum(sum(hist))
# Y = (hist * np.sin(THETA) * np.sin(PHI))/sum(sum(hist))
# Z = (hist * np.cos(THETA))/sum(sum(hist))                              

# fig = plt.figure()

# # # Plot the surface.
# ax = fig.add_subplot(projection='3d')

# # Create cubic bounding box to simulate equal aspect ratio
# max_range = np.array([X.max()-0, Y.max()-Y.min(), Z.max()-Z.min()]).max()
# Xb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][0].flatten() + 0.5*(X.max()+0)
# Yb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][1].flatten() + 0.5*(Y.max()+Y.min())
# Zb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][2].flatten() + 0.5*(Z.max()+Z.min())

# # Comment or uncomment following both lines to test the fake bounding box:
# for xb, yb, zb in zip(Xb, Yb, Zb):
#     ax.plot([yb], [zb], [xb], 'w')
# s = ax.plot_surface(Y, Z, X, cmap=plt.get_cmap('jet'))
# ax.set_xlabel(r'Y')
# ax.set_ylabel(r'Z')
# ax.set_zlabel(r'X')

# angles = np.linspace(0,360, 180)[:-1] # Take 20 angles between 0 and 360

# name = str(i) + "-" +str("yeet") + ".mp4"
# rotanimate.rotanimate(ax, angles, name, fps=20, bitrate=1000)

# def NormFunction(self, var_1):
#     return abs(np.trapz(self.phi * 180/np.pi, self.hist/var_1)-1);           