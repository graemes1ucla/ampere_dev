clear
clc
close
opts = delimitedTextImportOptions("NumVariables", 7);

% Specify range and delimiter
opts.DataLines = [1, Inf];
opts.Delimiter = " ";

% Specify column names and types
opts.VariableNames = ["x", "y", "z", "VarName4", "density", "VarName6", "Var7"];
opts.SelectedVariableNames = ["x", "y", "z", "VarName4", "density", "VarName6"];
opts.VariableTypes = ["double", "double", "double", "double", "double", "double", "string"];

% Specify file level properties
opts.ExtraColumnsRule = "ignore";
opts.EmptyLineRule = "read";
opts.ConsecutiveDelimitersRule = "join";
opts.LeadingDelimitersRule = "ignore";

% Specify variable properties
opts = setvaropts(opts, "Var7", "WhitespaceRule", "preserve");
opts = setvaropts(opts, "Var7", "EmptyFieldRule", "auto");
opts = setvaropts(opts, "x", "TrimNonNumeric", true);
opts = setvaropts(opts, "x", "ThousandsSeparator", ",");

% Import the data
for i = 9
    file_path = "G:\My Drive\AMPERE\TRI3DYN\test\Matlab\SiO_NeSi23_sc00" + i + ".dat";
    SiONeSi23sc = readtable(file_path, opts);

    rho = zeros(100, 60, 60);

    line = [SiONeSi23sc.x, SiONeSi23sc.y, SiONeSi23sc.z, SiONeSi23sc.density];

    for j = 1:1:length(line)
        rho(line(j, 1),line(j, 2),line(j, 3)) = line(j, 4);
    end

    display_3D(rho, i);
    vtkwrite('density.vtk', 'structured_points', 'density', rho)
    clearvars -except i opts     %deletes all variables except X in workspace
    close

end
