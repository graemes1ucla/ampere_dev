@/afs/ipp/home/a/aam/public/archiv/TRIDYN/SDTrimSP/post/window.inc 
@/afs/ipp/home/a/aam/public/archiv/TRIDYN/SDTrimSP/post/level.inc 
@/afs/ipp/home/a/aam/public/archiv/TRIDYN/SDTrimSP/post/achsen.inc 
pro run
;input :E_distr.dat
;output:e_distr.ps    

  _start
  
  druck=0
  print,'druck:0/1    0...screen 1...postscript'
  read,druck
  form=0
  titlecase='' & text1='' & bez='' & text2=''& textdatum=''
  idep=1
  nx=1 & ny=1 & nz=1 & ll=1
  a=1.0d0 & b=1.0d0 & dx=1.0d0
  loadct,39
  if druck eq 0 then farbe=[255,250,150,80,100,200] else $
                     farbe=[0,250,150,80,100,200]
  close,1
  openr,1,'E_distr_all.dat'
   ;---header
   readf,1,text1
   readf,1,bez
   nh=long(0)
   readf,1,nh
   readf,1,nx,ny,nz
   print,'nx,ny,nx:',nx,ny,nz
   readf,1,dx
   dy=dx
   dz=dx
   print,'dx,dy,dx:',dx
   ener=dblarr(nx,ny,nz)
   
   hilf=dblarr(nz)
   
   xxh=dblarr(nx+1)
   yyh=dblarr(ny+1)
   zzh=dblarr(nz+1)

   readf,1,text1
   readf,1,xxh  
   readf,1,text1
   readf,1,yyh  
   readf,1,text1
   readf,1,zzh  
   
   xx=dblarr(nx)
   yy=dblarr(ny)
   zz=dblarr(nz)
   
   each=2
   for i=0,nx-1 do begin
     xx(i)=0.5*(xxh(i)+xxh(i+1))
   end  
   
   for i=0,ny-1 do begin
     yy(i)=0.5*(yyh(i)+yyh(i+1))
   end  

   for i=0,nz-1 do begin
     zz(i)=0.5*(zzh(i)+zzh(i+1))
   end  
   
   
   for i=0,nx-1 do begin
     readf,1,ll  
     for k=0,nz-1 do begin
        readf,1,hilf
        ener(i,*,k)=hilf(*)
     end
   end  
  close,1
  
  drname='e_distr.ps'
  print,'printname:',drname
     
     _fenster,druck,drname,form,nummer=10   
     !P.Multi=0 
     druckfak=1
     if druck eq 1 then druckfak=.8
     dd=1.0*druckfak
     gr=1.0*druckfak
     gd=1.0*druckfak
     
     xxx=[xxh(0),xxh(nx)]*3.0
     yyy=[yyh(0),yyh(ny)]*1.1
    
    ;30 Grad
     ;xxx=[xxh(0),xxh(nx)]*3.0
     ;yyy=[yyh(0),yyh(ny)]*1.1
    
     lmin=5 & lmax=7
     pos=[.08,.73,.95,.97]
     pos1=pos
     ytt='y   length of target in A' 
     xtt='x   thick of target in A' 
     xrichtung=0 & yrichtung=1
     ueber='!6line-cuts with one trajectory'
     sca=1
     
     ;---BILD 1 Trajec
     _bild,xxx(0),xxx(1),yyy(0),yyy(1),lmin,lmax,pos,xtt,ytt,ueber,unter,gr,gd,form $
        ,xrichtung,yrichtung,sca
        ;,xachsread=xachsread,yachsread=yachsread $
        ;,format_x=format_x,format_y=format_y

     text=''
     close,30
     openr,30,'trajec_all.dat'
     print,'---open trajec_all.dat' 
     readf,30,textdatum
     readf,30,bez2
     maxout2=long(1)
     readf,30,maxout2

     hilf=lonarr(7)
     readf,30,text ;& print,text 
     readf,30,hilf
     readf,30,text ;& print,text     
     print,'   trajec_all.dat:                  ihist:',hilf(1) 
 
     x_tra =dblarr(hilf(6))
     y_tra =dblarr(hilf(6))
     z_tra =dblarr(hilf(6))
     aa=1.0 & bb=1.0 & cc=1.0  
     i_hist=hilf(1)
      
     for i=0,hilf(6)-1 do begin 
       readf,30,j,aa,bb,cc ;& print,j,aa,bb,cc
       x_tra(i)=aa
       y_tra(i)=bb
       z_tra(i)=cc
       ;print,i,' x_tra(i),z_tra(i)',x_tra(i),z_tra(i)
     end

     ;---plot trajectory projectil  (letzts Bild alte Trajektorie)
      oplot,x_tra,z_tra,thick=1 ;,psym=-2 
       
     ;---plot trajectory recoil
     nextrecoil:
     readf,30,text ;& print,'text:',text 
     text=strtrim(text,1) 
     
     if text eq 'ende' then goto,otra2
     
     readf,30,hilf ;& print,'hilf:',hilf
     readf,30,text ;& print,text 
     x_tra =dblarr(hilf(6))
     y_tra =dblarr(hilf(6))
     z_tra =dblarr(hilf(6))
     aa=1.0 & bb=1.0 & cc=1.0  
     if i_hist eq hilf(1) then begin
       for i=0,hilf(6)-1 do begin 
        readf,30,j,aa,bb,cc ;& print,j,aa,bb,cc
        x_tra(i)=aa
        y_tra(i)=bb
        z_tra(i)=cc
       end
       ;---plot trajectory recoil
       oplot,x_tra,z_tra,thick=1 ;,color=50    ;,psym=-1 
       goto,nextrecoil
      end
      i_hist=hilf(1)     
otra2:
     
     
     ;---x line-cut
     
     for j=each-1,ny-1,each do begin
       oplot,[xx(0),xx(nx-1)],[yy(j),yy(j)]
     end
     ired=ny/2+each
     oplot,[xx(0),xx(nx-1)],[yy(ired),yy(ired)],color=250,thick=2
     
     ired2=ny/2+each*2
     oplot,[xx(0),xx(nx-1)],[yy(ired2),yy(ired2)],color=100,thick=2
     
     iyel=ny/2
     oplot,[xx(0),xx(nx-1)],[yy(iyel),yy(iyel)],color=200,thick=2
     oplot,[xx(0),xx(nx-1)],[yy(iyel),yy(iyel)],linestyle=2,thick=2
     
     ;---y line-cut
     for i=0,nx-1,each do begin
       oplot,[xx(i),xx(i)],[yy(0),yy(ny-1)]
     end
     
     iblue=each*1
     oplot,[xx(iblue),xx(iblue)],[yy(0),yy(ny-1)],color=80,thick=2
     
     igre=each*0
     oplot,[xx(igre),xx(igre)],[yy(0),yy(ny-1)],color=150,thick=2

    ;---BILD 2 
     sca=0
     pos2=pos
     pos2(1)=pos2(1)-.31
     pos2(3)=pos2(3)-.31
     yrichtung=0
     xtt='density of energy in eV/A!U3!N'    
     ueber='density of energy along a y cut-line'

     ;_bild,0,max(ener(*,*,nz/2)),yyy(0),yyy(1),lmin,lmax,pos2,xtt,ytt,ueber,unter,gr,gd,form $
     _bild,0,max(ener(*,*,nz/2))/5,min(yy),max(yy),lmin,lmax,pos2,xtt,ytt,ueber,unter,gr,gd,form $
        ,xrichtung,yrichtung,sca
        ;,xachsread=xachsread,yachsread=yachsread $
        ;,format_x=format_x,format_y=format_y
 
     ;plot,[.1,max(ener(*,*,nz/2))],[min(yy),max(yy)],/noerase $
     ;    ,xtitle=xtt $
     ;    ,ytitle=ytt $
     ;    ,position=pos2 $
     ;    ,xlog=1 $
     ;    ,title =ueber 
     
     for i=0,nx-1,each do begin
       eny=yy
       for j=0,ny-1 do begin
         eny(j)=ener(i,j,nz/2)
       end
       oplot,eny,yy
     end
     
     i=iblue
     eny=yy
     for j=0,ny-1 do begin
       eny(j)=ener(i,j,nz/2)
     end
     oplot,eny,yy,color=80,thick=2 
     
     i=igre
     eny=yy
     for j=0,ny-1 do begin
       eny(j)=ener(i,j,nz/2)
     end
     oplot,eny,yy,color=150,thick=2 
     
    ;---BILD 3 
     sca=0
     pos3=pos
     pos3(1)=pos2(1)-.31
     pos3(3)=pos2(3)-.31
     ;pos3(2)=pos3(0)+(pos3(3)-pos3(1))*27./18.
     yrichtung=0
     ytt='density of energy in eV/A!U3!N'
     xtt='x   thick of target in A' 
     ueber='density of energy along a depth cut-line'
    ;---BILD 2 
     ;_bild,xxx(0),xxx(1),0,max(ener(*,*,nz/2)),lmin,lmax,pos3,xtt,ytt,ueber,unter,gr,gd,form $
     _bild,min(xx),max(xx),0,max(ener(*,*,nz/2))/5,lmin,lmax,pos3,xtt,ytt,ueber,unter,gr,gd,form $
        ,xrichtung,yrichtung,sca
        ;,xachsread=xachsread,yachsread=yachsread $
        ;,format_x=format_x,format_y=format_y

     
     for j=each-1,ny-1,each do begin
       eny=xx
       for i=0,nx-1 do begin
         eny(i)=ener(i,j,nz/2)
       end
       oplot,xx,eny 
     end
     
       j=ired 
       enx=xx
       for i=0,nx-1 do begin
         eny(i)=ener(i,j,nz/2)
       end
       oplot,xx,eny,color=250,thick=2;,psym=-1 
       
       j=ired2 
       enx=xx
       for i=0,nx-1 do begin
         eny(i)=ener(i,j,nz/2)
       end
       oplot,xx,eny,color=100,thick=2;,psym=-1 
       
       j=iyel 
       enx=xx
       for i=0,nx-1 do begin
         eny(i)=ener(i,j,nz/2)
       end
       oplot,xx,eny,color=200,thick=2 
       oplot,xx,eny,linestyle=2,thick=2 
     
     
;  ener(0,*,*)=0.0
;  ener(1,*,*)=0.0
;  surface,reform(ener(*,ny/2,*)),az=120
     _int2string,nh,nh,s
     xyouts,.10,.05,'!6energy distribution ('+s+' trajectories)',charsize=gr*1.8,charthick=gd,/normal  
     xyouts,.10,.02,'!6'+bez                ,charsize=gr*1.8,charthick=gd,/normal  
     ;xyouts,.75,.00,'!6idl_edistr.pro:',charsize=gr*.4,charthick=gd,/normal  
  
  _druckende,druck,drname,form  
end
