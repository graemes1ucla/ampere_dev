@/afs/ipp/home/a/aam/public/archiv/TRIDYN/SDTrimSP/post/window.inc 
@/afs/ipp/home/a/aam/public/archiv/TRIDYN/SDTrimSP/post/level.inc 
@/afs/ipp/home/a/aam/public/archiv/TRIDYN/SDTrimSP/post/achsen.inc 
pro run
;input :E0_31_target.dat           (einzel=0)
;depth_recoil.dat
;depth_proj.dat


;   or :dsurf_f_31.dat      (einzel=1)
;       backscatt_f_31.dat
;       backsputt_f_31.dat
;       comp_surf_f_31.dat
;       density_d_f_31.dat
;       comp_steady_state_d_31.dat
;output:E031_dyn.ps

;go to words
;      picture

  _start
  druck=0
  print,'druck:0/1    0...screen 1...postscript'
  read,druck
  drname='E031_dyn.ps'
  form=0
  text='' & text1='' & bez=''& textdatum=''
  jflu=1
  idep=1
  ncp=1
  a=1.0d0 & b=1.0d0 & c=1.0d0
  loadct,39
  close,1
  if druck eq 0 then farbe=[255,250,150,80,100,200,40,215,180,255,250,150] else $
                     farbe=[0  ,250,150,80,100,200,40,215,180,0  ,250,150]
  
  ;--- 0... read 'E0_31_target.dat'  1...read  *_f_31.dat...
  einzel=0
;for kkk=2,10 do begin
  if einzel eq 0 then begin
  print,'------------------------'
  print,'INPUT: read E0_31_target.dat'
  close,31
  openr,31,'E0_31_target.dat'
   readf,31,textdatum
   readf,31,bez
   readf,31,text
   readf,31,text
   nh=long(1) & idep=1 & ncp=1 & idout=1 & nqxm=1 & npro=1 & idrel=1  
   readf,31, nh,idep,ncp,idout, nqxm , npro, idrel
   print, '  ncp=',ncp
   print, '  NH =',nh,' max_idep=',idep,' ncp=',ncp, ' idepth=',idout

   jflu=nh/idout+1
;print,jflu
;read,jflu
;jflu=kkk
   flu =dblarr(jflu) 
   surf=dblarr(jflu)
   flic=dblarr(jflu)
   nproj  =dblarr(ncp,jflu)
   sumnproj=dblarr(jflu)
   ard=dblarr(ncp,jflu)
   iback_p=dblarr(ncp,jflu)
   eback_p=dblarr(ncp,jflu)
   itran_p=dblarr(ncp,jflu)
   etran_p=dblarr(ncp,jflu)
   iback_r=dblarr(ncp,jflu)
   eback_r=dblarr(ncp,jflu)
   itran_r=dblarr(ncp,jflu)
   etran_r=dblarr(ncp,jflu)
   e_tot_p=dblarr(ncp,jflu)
   reem  =dblarr(ncp,jflu) 
   irec  =dblarr(ncp,jflu) 
   backscatt=dblarr(ncp,jflu)
   backsputt=dblarr(ncp,jflu)
   defects=dblarr(ncp,jflu)
   comp_surf=dblarr(ncp,jflu)
   comp_dep =dblarr(ncp,idep)
   help=dblarr(ncp)
   symbol=strarr(ncp)
   text1=strarr(ncp+1)
   quxi=dblarr(idep,jflu,ncp)
   dep=dblarr(idep) 
   dens=dblarr(jflu,idep) 
   
   for i=0,jflu-1 do begin

     if (i eq 0)then begin
             ; (31, '(10a10)') symbol(1:ncp)
        readf,31,symbol,format='(20a10)' & symbol(*)=strtrim(symbol(*),1)
        text1(1:ncp)=symbol(0:ncp-1)  
        text2=text1
        text2(1)='[-]'
        ;for j=1,16 do begin ;with irec
        for j=1,15 do begin
          readf,31,text ;(31, '(a)') text
          ;print,text
        end 
     end
    if i eq 0 then iii=1 else iii=5
    iii=1
    for ii=1,iii do begin
      readf,31,a,b;(31,*)fluc(i),depd(i)
;print,a
      flu(i)=a
      surf(i)=b
      readf,31,text ;(31,*)csf(i,1:ncp) surf composition
;print,text
       readf,31,text;(31,*)flib(i,1:5,1:npro)
;print,text
      readf,31,help;(31,*) ard
      ard(*,i)=help(*)
print,help     
      readf,31,help;(31,*)
;print,help(0)
      nproj(*,i)=help(*)
      sumnproj(i)=total(help)
      readf,31,help;(31,*)
;print,help(0)
      iback_p(*,i)=help(*)
      readf,31,help;(31,*)
;print,help(0)
      eback_p(*,i)=help(*)
      readf,31,help;(31,*)
;print,help(0)
      itran_p(*,i)=help(*)
      readf,31,help;(31,*)
;print,help(0)
      etran_p(*,i)=help(*)
      readf,31,help;(31,*)
;print,help(0)
      iback_r(*,i)=help(*)
      readf,31,help;(31,*)
;print,help(0)
      eback_r(*,i)=help(*)
      readf,31,help;(31,*)
;print,help(0)
      itran_r(*,i)=help(*)
      readf,31,help;(31,*)
;print,help(0)
      etran_r(*,i)=help(*)
      readf,31,help;(31,*)
;print,help(0)
      e_tot_p(*,i)=help(*)
      readf,31,help;(31,*)
;print,help(0)
      reem(*,i)=help(*) 

      ;readf,31,help;(31,*)
      ;irec(*,i)=help(*) 
      
      if (i eq 0) then begin
        readf,31,text ;(31, '(a)') text
        readf,31,text ;(31, '(a)') text
      end
      for mm = 0, idep-1 do begin
        readf,31,a,b,help ;(31,*) xxx(mm), dns(i,mm), (quxi(mm,i,jp),jp=1,ncp)
        dep(mm)=a
        dens(i,mm)=b
        quxi(mm,i,*)=help(*)
        if mm eq      0 then comp_surf(*,i)=help(*)
        if i  eq jflu-1 then comp_dep(*,mm)=help(*)
      end
    end ;---iii
    end ;---i
  close,31

  xytext=['fluence [atoms/A**2]','surface [A]']
  end
  ;if idrel eq 0 then drname='E031_stat.ps
  ;---prepare 6 picture
  _fenster,druck,drname,form,nummer=9  
   spa=2 & zei=3
   !P.Multi=[0,spa,zei] 
   if form eq 0 then pos=[0.00, 0.05 , .99, .96]
   if form eq 1 then pos=[0.01, 0.1, .98, .97]
   ric=0
   dspa=25 ;% Zwischenraum
   dzei=25 ;% Zwischenraum
   dx=(pos(2)-pos(0))/spa
   dy=(pos(3)-pos(1))/zei
   isp=0
   jze=0
   if ric eq 0 then jze=1 else isp=1
   druckfak=1
   if druck eq 1 then druckfak=.8
   dd=1.5*druckfak
   gr=2.3*druckfak
   gd=1.5*druckfak
  
  if einzel eq 1 then begin
    openr,1,'dsurf_f_31.dat'
    ;---header
     readf,1,bez,format='(a50)' & bez=strtrim(bez,1)
     bezall=bez
     print,'Calculation:',bezall
     readf,1,text
     readf,1,jflu,ncp
      text1=strarr(ncp+1)
      text2=text1
      readf,1,text1,format='(20a12)' & text1(*)=strtrim(text1(*),1)
      readf,1,text2,format='(20a12)' & text2(*)=strtrim(text2(*),1)
      xytext=text1+' '+text2
    ;---values
     flu=dblarr(jflu)
     surf=flu
     for i=0,jflu-1 do begin
       readf,1,a,b
       flu (i)=a
       surf(i)=-b
     print,i,a,b
     end
    close,1
  end

  ;---surface or depth picture 1
  _posit,ric,isp,jze,spa,zei,dspa,dzei,dx,dy,pos,pos1
  if idrel eq 0 then begin
    xx=[min(flu) ,max(flu)]
    yy=[min(surf),max(surf)]
    ;xx=[0 ,10]
    ;yy=[-80,40]
    
    
    if abs(yy(0)-yy(1)) lt 1.0e-20 then yy=[-.1, .1]
    plot,xx,yy,xtitle=xytext(0),ytitle=xytext(1),title='surface' $
     ,yrange=[max(yy),min(yy)] $
     ,thick=dd,charsize=gr,charthick=gd,position=pos1,/nodata
    oplot,flu,surf
  end else begin
    maxidep=0.0d0
    maxdep=0.0d0
    nnn=1
    nprojb=1.d0
    idepb=1
    maxwert=0.0d0
    print,'------------------------'
    print,'INPUT: read depth_proj.dat'
    print,'INPUT: read depth_recoil.dat'
    for jjj=1,2 do begin
      if jjj eq 1 then openr,1,'depth_proj.dat'
      if jjj eq 2 then openr,1,'depth_recoil.dat'
      ;---header
      readf,1,text
      readf,1,text
      readf,1,nnn
      for anz=1,nnn do begin
       readf,1,text
       readf,1,idepb,nprojb
       text2b=strarr(8)
       text3b=strarr(8)
       readf,1,text2b,format='(20a12)' & text2b(*)=strtrim(text2b(*),1)
       readf,1,text3b,format='(20a12)' & text3b(*)=strtrim(text3b(*),1)
       text3b(0)='depth [A]'
       text3b(1)='stop position'
       text2b(1)='particels (normalized to: nproj)'
       ;---values
       wert=dblarr(8,idepb)
       hilf=dblarr(8)
       for i=0,idepb-1 do begin
        readf,1,hilf
        wert(*,i)=hilf(*)
       end   
       readf,1,hilf ;sum
       ihilf=idepb-1
       for i=idepb-1,0,-1 do begin
         if  wert(1,i) eq 0.0 and wert(3,i) eq 0. and wert(4,i) eq 0. $
         and wert(5,i) eq 0.0 and wert(5,i) eq 0. then ihilf=i
       end

       if ihilf gt maxidep then begin
          maxidep=ihilf
          maxdep=wert(0,maxidep-1)
       end
       k=1 ;STOP-POSITION
       if nprojb > 0 then $
         if max(wert(k,*))/nprojb gt maxwert then maxwert=max(wert(k,*))/nprojb
      end ;anz 
      close,1  
    end;jjj 
    print,' -------'
    print,' Picture 1: stop position: 
    print,'  maxdepth of stopped (y):',maxdep
    print,'  maxcoef. of stopped (x):',maxwert
    
    k=1 ;STOP-POSITION
    xx=[0.0,maxwert]
    yy=[maxdep,0.0]
    lmin=3 & lmax=4
    _achslevels,xx(0),xx(1),lmin,lmax,ndx,idx,strdx ;,forma=format_y
    _achslevels,yy(0),yy(1),lmin,lmax,ndy,idy,strdy ;,forma=format_y
    
    plot,xx,yy,xtitle=text2b(k),ytitle=text3b(0),title=text3b(k),/nodata $
       ,Xticks=ndx, Xtickv=idx ,Xtickname=strdx $
       ,Yticks=ndy, Ytickv=idy ,Ytickname=strdy $
       ,xrange=xx ,yrange=yy $
      ,thick=dd,charsize=gr,charthick=gd,position=pos1
    for jjj=1,2 do begin
    lll=0
      if jjj eq 1 then openr,1,'depth_proj.dat'
      if jjj eq 2 then openr,1,'depth_recoil.dat'
      ;---header
      readf,1,text
      readf,1,text
      readf,1,nnn
      for anz=1,nnn do begin
       readf,1,text
       readf,1,idepb,nprojb
       text2b=strarr(8)
       text3b=strarr(8)
       readf,1,text2b,format='(20a12)' & text2b(*)=strtrim(text2b(*),1)
       readf,1,text3b,format='(20a12)' & text3b(*)=strtrim(text3b(*),1)
       ;---values
       ;print,'idepb,nprojb',idepb
       wert=dblarr(8,idepb)
       hilf=dblarr(8)
       for i=0,idepb-1 do begin
        readf,1,hilf
        wert(*,i)=hilf(*)
       end       
       readf,1,hilf ;sum
       ihilf=idepb-1
       ;print,'ihilf',ihilf
       for i=idepb-1,0,-1 do begin
         if  wert(1,i) eq 0.0 and wert(3,i) eq 0. and wert(4,i) eq 0. $
         and wert(5,i) eq 0.0 and wert(5,i) eq 0. then ihilf=i
       end
       if ihilf eq 0 then ihilf=1
   
       k=1 ;STOP-POSITION  (ihilf only to last stop level)
       if nprojb > 0 then begin
        oplot,wert(k,0:ihilf-1)/nprojb,wert(0,0:ihilf-1),color=farbe(lll);,psym=-1
        xyouts,.001,pos1(3)-(pos1(3)-pos1(1))/8*(lll+1) $
        ,text1(lll+1),charsize=gr/2,charthick=1.5,/normal,color=farbe(lll)  
        lll=lll+1
       end   
      end ;anz 
     close,1  
    end ;jjj
    print,'  NR*NH or nproj         :',nprojb,'   (values of normalized)'
  
  end ;stop pos.  idrel=1
  
  if einzel eq 1 then begin
  openr,1,'backscatt_f_31.dat'
  ;---Bezeichnung
   readf,1,bez,format='(a50)' & bez=strtrim(bez,1)
   if bez ne bezall then begin
     print,'WARNING not the same calculation: backscatt_f_31.dat' 
     print,'Calculation:',bez
   end
   readf,1,text
   readf,1,jflu,ncp,npro
    text1=strarr(ncp+1)
    text2=text1
    readf,1,text1,format='(20a12)' & text1(*)=strtrim(text1(*),1)
    readf,1,text2,format='(20a12)' & text2(*)=strtrim(text2(*),1)
    xytext=text1+' '+text2
  ;---values
   flu=dblarr(jflu)
   backscatt=dblarr(ncp,jflu)
   help=dblarr(ncp)
   for i=0,jflu-1 do begin
     readf,1,a,help
     flu (i)=a
     backscatt(*,i)=help(*)
   end
  close,1
  end  
  
  ;---particle reflection coefficient   picture 2
  backscatt(*,0)=0
  for i=1,jflu-1 do begin
   for j=0,ncp-1 do begin  
    if (nproj(j,i)-nproj(j,i-1)) gt 0 then begin
     backscatt(j,i)=(iback_p(j,i)-iback_p(j,i-1))/(nproj(j,i)-nproj(j,i-1))
    end else begin
     backscatt(j,i)=0
    end
   end
  end ;i
  
  _posit,ric,isp,jze,spa,zei,dspa,dzei,dx,dy,pos,pos1    
  xx=[0.0 ,max(flu)]
  hilf=backscatt(0:npro-1,*)
  if min(hilf) eq 0 then hilf(where(hilf eq 0 ))=+1000
  yy=[min(backscatt(0:npro-1,*)) ,max(backscatt(0:npro-1,*))]
  yy=[min(hilf                 ) ,max(backscatt(0:npro-1,*))]
  yy(1)=yy(1)+0.005
  ;yy(1)=0.013
  
  lmin=3 & lmax=4
  ;_achslevels,xx(0),xx(1),lmin,lmax,ndx,idx,strdx ;,forma=format_y
  fff='(f6.3)'
  if yy(1) lt 0.0011 then fff='(f6.4)'
  if yy(1) lt 0.00011 then fff='(f7.5)'
  if yy(1) lt 0.000011 then fff='(f7.6)'
  _achslevels,yy(0),yy(1),lmin,lmax,ndy,idy,strdy,forma=fff

  plot,xx,yy,xtitle=xytext(0),ytitle=' ',title='part. refl. coeff.',/nodata $
     ;,Xticks=ndx, Xtickv=idx ,Xtickname=strdx $
     ,Yticks=ndy, Ytickv=idy ,Ytickname=strdy $
     ,xrange=xx ,yrange=yy $
     ,thick=dd,charsize=gr,charthick=gd,position=pos1
  oplot,flu,backscatt(0,1:jflu-1) 

  j=1
  for i=0,npro-1 do begin
    oplot,flu,backscatt(i,1:jflu-1),color=farbe(i)
    if idrel eq 1 then begin
      bmittel=dblarr(jflu)
      bmittel(0)=0
      for jj=1,jflu-1 do begin
        bmittel(jj)=(bmittel(jj-1)*(jj-1)+backscatt(i,jj))/jj
      end
      oplot,flu(1:jflu-1),bmittel(1:jflu-1),color=farbe(i)
      ;print,bmittel(1:jflu-1)
    end
    xyouts,.501,pos1(3)-(pos1(3)-pos1(1))/8*(j) $
    ,text1(i+1),charsize=gr/2,charthick=gd,/normal,color=farbe(i)  
    j=j+1
  end
  xyouts,.501,pos1(3)-(pos1(3)-pos1(1))/8*(j) $
  ,text2(1),charsize=gr/2,charthick=gd,/normal  
 
 if einzel eq 1 then begin
  openr,1,'backsputt_f_31.dat'
  ;---Bezeichnung
   readf,1,bez,format='(a50)' & bez=strtrim(bez,1)
   if bez ne bezall then begin
     print,'WARNING not the same calculation: backscatt_f_31.dat' 
     print,'Calculation:',bez
   end
   readf,1,text
   readf,1,jflu,ncp,npro
    text1=strarr(ncp+1)
    text2=text1
    readf,1,text1,format='(20a12)' & text1(*)=strtrim(text1(*),1)
    readf,1,text2,format='(20a12)' & text2(*)=strtrim(text2(*),1)
    xytext=text1+' '+text2
  ;---values
   flu=dblarr(jflu)
   backsputt=dblarr(ncp,jflu)
   help=dblarr(ncp)
   for i=0,jflu-1 do begin
     readf,1,a,help
     flu (i)=a
     backsputt(*,i)=help(*)
   end
  close,1
  end  
  ;---backsputter picture 3

  backsputt(*,0)=0
  bball=backsputt
  for i=1,jflu-1 do begin
   for j=0,ncp-1 do begin  
    if ( sumnproj(i)-sumnproj(i-1) ) gt 0 then begin
     ;---backsputtering calculation with sum of projectiles (all)
     backsputt(j,i)=(iback_r(j,i)-iback_r(j,i-1))/(sumnproj(i)-sumnproj(i-1))

     ;---backsputtering calculation with projectiles of species
     ;backsputt(j,i)=(iback_r(j,i)-iback_r(j,i-1))/(nproj(j,i)-nproj(j,i-1))
     ;bball(j,i)    =(iback_r(j,i)-iback_r(j,i-1)+iback_p(j,i)-iback_p(j,i-1)  )/(nproj(j,i)-nproj(j,i-1))
     ;if j eq ncp-1 then backsputt(j,i)=(iback_r(j,i)-iback_r(j,i-1))/(sumnproj(i)-sumnproj(i-1))
    end else begin
     backsputt(j,i)=0.0
    end
   end
  end
;print,'            ',text1(0:ncp-1)
;if jflu ge 5 then begin
;print,'  Sputt.(',string(flu(jflu-5),format='(f5.2)'),'):',backsputt(0:ncp-1,jflu-5)
;print,'  Sputt.(',string(flu(jflu-4),format='(f5.2)'),'):',backsputt(0:ncp-1,jflu-4)
;print,'  Sputt.(',string(flu(jflu-3),format='(f5.2)'),'):',backsputt(0:ncp-1,jflu-3)
;print,'  Sputt.(',string(flu(jflu-2),format='(f5.2)'),'):',backsputt(0:ncp-1,jflu-2)
;print,'  Sputt.(',string(flu(jflu-1),format='(f5.2)'),'):',backsputt(0:ncp-1,jflu-1)
;hilf=  $
;      backsputt(0:ncp-1,jflu-5) $
;     +backsputt(0:ncp-1,jflu-4) $
;     +backsputt(0:ncp-1,jflu-3) $
;     +backsputt(0:ncp-1,jflu-2) $
;     +backsputt(0:ncp-1,jflu-1) 
;print,'  SptSumm:      ',hilf(0:ncp-1)/5.0
;print,' areal density [10**16 atoms/cm**2]'
;print,'  ard.(',string(flu(jflu-3),format='(f5.2)'),'):',ard(0:ncp-1,jflu-3)
;print,'  ard.(',string(flu(jflu-2),format='(f5.2)'),'):',ard(0:ncp-1,jflu-2)
;print,'  ard.(',string(flu(jflu-1),format='(f5.2)'),'):',ard(0:ncp-1,jflu-1)
;end else begin
;print,'  SptSumm:      ',backsputt(0:ncp-1,jflu-1)
;print,'  ard.(',string(flu(jflu-1),format='(f5.2)'),'):',ard(0:ncp-1,jflu-1)
;end

;print,min(backsputt),max(backsputt) 
;print,min(iback_r),max(iback_r) 

   _posit,ric,isp,jze,spa,zei,dspa,dzei,dx,dy,pos,pos1
   plot,flu,backsputt(1,*),xtitle=xytext(0) $
   ,yrange=[0,max(backsputt(0:ncp-1,*))],title='part. sputt. coeff.' $
;   ,yrange=[0,1.0],title='part. sputt. coeff.' $
   ,thick=dd,charsize=gr,charthick=gd,position=pos1,/nodata
   j=1
    bmittel2=dblarr(jflu)
    bmittel2(0)=0
   for i=0,ncp-1 do begin
     if i eq 0 then fff=1. else fff=1.
     oplot,flu,backsputt(i,1:jflu-1)*fff,color=farbe(i)
     ;oplot,flu,bball    (i,1:jflu-1)*fff,color=farbe(i),linestyle=2
      ;print,backsputt(i,1:jflu-1),backsputt(0,1:jflu-1)+backsputt(1,1:jflu-1)
     if idrel eq 1 then begin
       bmittel=dblarr(jflu)
       bmittel(0)=0
       for jj=1,jflu-1 do begin
         bmittel(jj)=(bmittel(jj-1)*(jj-1)+backsputt(i,jj))/jj
         bmittel2(jj)=bmittel2(jj)+backsputt(i,jj)
       end
       oplot,flu(1:jflu-1),bmittel(1:jflu-1)*fff,color=farbe(i)
       ;if i eq 1 then oplot,flu,bmittel2(1:jflu-1),color=farbe(i+1)
     end
     xyouts,.001,pos1(3)-(pos1(3)-pos1(1))/8*j $
     ,text1(i+1),charsize=gr/2,charthick=gd,/normal,color=farbe(i)  
      j=j+1
   end
   ;---unit
   xyouts,.001,pos1(3)-(pos1(3)-pos1(1))/8*(j) $
   ,text2(1),charsize=gr/2,charthick=gd,/normal  

 comp=comp_surf
 if einzel eq 1 then begin
  openr,1,'comp_surf_f_31.dat'
  ;---Bezeichnung
   readf,1,bez,format='(a50)' & bez=strtrim(bez,1)
   if bez ne bezall then begin
     print,'WARNING not the same calculation: backscatt_f_31.dat' 
     print,'Calculation:',bez
   end
   readf,1,text
   readf,1,jflu,ncp
    text1=strarr(ncp+1)
    text2=text1
    readf,1,text1,format='(20a12)' & text1(*)=strtrim(text1(*),1)
    readf,1,text2,format='(20a12)' & text2(*)=strtrim(text2(*),1)
    xytext=text1+' '+text2
  ;---values
   flu=dblarr(jflu)
   comp=dblarr(ncp,jflu)
   help=dblarr(ncp)
   for i=0,jflu-1 do begin
     readf,1,a,help
     flu (i)=a
     comp(*,i)=help(*)
   end
  close,1
  end  
  ihelp=where(comp gt 1)
  if ihelp(0) ge 0 then comp(where(comp gt 1))=1.0
  if ihelp(0) ge 0 then print,'atomic fraction > 1 --> =1
  ihelp=where(comp lt 0)
  if ihelp(0) ge 0 then comp(where(comp lt 0))=0.0
  if ihelp(0) ge 0 then print,'atomic fraction < 0 --> =0
   
   ;picture 4
   ;---atomic fraction at surface or 
   ;   max. penetration depth of backscatt.part. 
   _posit,ric,isp,jze,spa,zei,dspa,dzei,dx,dy,pos,pos1
  if idrel eq 0 then begin

   plot,flu,comp(0,*),xtitle=xytext(0) $
   ,yrange=[min(comp)-.001,max(comp)+.001],title='atomic fraction at surface' $
   ,thick=dd,charsize=gr,charthick=gd,position=pos1
   j=1
   for i=0,ncp-1 do begin
     oplot,flu,comp(i,*),color=farbe(i)
     xyouts,.501,pos1(3)-(pos1(3)-pos1(1))/8*(j) $
     ,text1(i+1),charsize=gr/2,charthick=gd,/normal,color=farbe(i)  
     j=j+1
   end
   xyouts,.501,pos1(3)-(pos1(3)-pos1(1))/8*(j) $
   ,text2(1),charsize=gr/2,charthick=gd,/normal  
   end else begin
     openr,1,'mpe_ex_p.dat'
      ;---header
      readf,1,text
      readf,1,text
      readf,1,nnn,nh
      nproj=1.0d0
      idepb=1
      maxtief=0.0d0
      maxwert=0.0d0
      for anz=1,nnn do begin
        readf,1,nproj
        readf,1,text
        readf,1,text
        nenerg=1
        readf,1,nenerg,idepb
        readf,1,text
        text2b=strarr(8)
        text3b=strarr(8)
         text3b(0)='depth [A]'
         text3b(1)='penetration depth of refl. part.'
         text2b(1)='particels (normalized to: nproj)'
        ;---values
        a=1.0d0
        wert=dblarr(idepb)
        tief=dblarr(idepb)
        hilf=dblarr(idepb)
        readf,1,a,tief
        for i=0,nenerg-1 do begin
          readf,1,a,hilf
          wert(*)=wert(*)+hilf(*)
       end   
        readf,1,a,wert ;sum
      end
      maxtief=max([maxtief,max(tief)])
      if nproj > 0 then maxwert=max([maxwert,max(wert/nproj)])
     close,1
     
     print,' -------'
     print,' Picture 3: penetration depth of refl. part.: 
     print,'  max depth     :',maxtief
     print,'  max particles :',maxwert
     print,'  NR*NH or nproj:',nprojb,'   (values of normalized)'
   
     if maxwert eq 0 then maxwert=1
     xx=[0.0,maxwert]
     yy=[maxtief,0.0]
     lmin=3 & lmax=4
     _achslevels,xx(0),xx(1),lmin,lmax,ndx,idx,strdx ;,forma=format_y
     _achslevels,yy(0),yy(1),lmin,lmax,ndy,idy,strdy ;,forma=format_y
    
     plot,xx,yy,xtitle=text2b(k),ytitle=text3b(0),title=text3b(k),/nodata $
      ,Xticks=ndx, Xtickv=idx ,Xtickname=strdx $
      ,Yticks=ndy, Ytickv=idy ,Ytickname=strdy $
      ,xrange=xx ,yrange=yy $
      ,thick=dd,charsize=gr,charthick=gd,position=pos1
    openr,1,'mpe_ex_p.dat'
     ;---header
     readf,1,text
     readf,1,text
     readf,1,nnn,nh
     nproj=1.0d0
     maxtief=0.0d0
     maxwert=0.0d0
     lll=0
     for anz=1,nnn do begin
       readf,1,nproj
       text5=strarr(2)
       readf,1,text5,format='(a64,a5)' & text5(*)=strtrim(text5(*),1)
       ;print,text5(0)
       ;print,text5(1)
       readf,1,text
       nenerg=1
       readf,1,nenerg,idepb
       readf,1,text
       ;---values
       wert=dblarr(idepb)
       tief=dblarr(idepb)
       hilf=dblarr(idepb)
       readf,1,a,tief
       for i=0,nenerg-1 do begin
         readf,1,a,hilf
         wert(*)=wert(*)+hilf(*)
       end   
       readf,1,a,wert ;sum
       if nproj > 0 then begin
         oplot,wert/nproj,tief,color=farbe(lll)
         xyouts,.501,pos1(3)-(pos1(3)-pos1(1))/8*(lll+1) $
         ,text5(1),charsize=gr/2,charthick=1.5,/normal,color=farbe(lll)  
         lll=lll+1
       end     
     end       
    close,1       
  end

 if einzel eq 1 then begin
  openr,1,'density_d_f_31.dat'
  ;---Bezeichnung
   readf,1,bez,format='(a50)' & bez=strtrim(bez,1)
   if bez ne bezall then begin
     print,'WARNING not the same calculation: backscatt_f_31.dat' 
     print,'Calculation:',bez
   end
    readf,1,text
   readf,1,idep,jflu
   dep=dblarr(idep)
   flu=dblarr(jflu)
   dens=dblarr(jflu,idep)
    readf,1,text
   readf,1,flu 
    text1=strarr(min([10,idep]))
    text2=text1
    readf,1,text1,format='(20a12)' & text1(*)=strtrim(text1(*),1)
    readf,1,text2,format='(20a12)' & text2(*)=strtrim(text2(*),1)
    xytext=text1+' '+text2
  ;---values
   help=dblarr(jflu)
   for i=0,idep-1 do begin
     readf,1,a,help
     dep (i)=a
     dens(*,i)=help(*)
   end
  close,1
  end  
  
  ;--------------Picture 5: density / (number recoils / NH)
   _posit,ric,isp,jze,spa,zei,dspa,dzei,dx,dy,pos,pos1
   densconst=dens(jflu-1,idep-1)
   print,'-------'
   print,' Picture 5: density: 
   print,'  density:',densconst,' layer:',idep-1 ;,' fluence:',flu(jflu-1) 
   ii=0
   iiq=0
   for j=jflu-1,jflu-2,-1 do begin ;j=jflu-1,0,-1
     iiqh=0
     for i=idep-1,1,-1 do begin
      if dens(j,i) ne densconst and ii lt i then begin
        ii=i
      end
      if    comp_dep(0,i) ne comp_dep(0,i-1) and iiqh eq 0 then iiqh=i
      if ncp gt 0 then begin
         if comp_dep(1,i) ne comp_dep(1,i-1) and iiqh eq 0 then iiqh=i
      end
     end
     iiq=max([iiq,iiqh])
   end
   ;---ii...index from this layer density is constant
   if ii ne idep-1 then ii=ii+1
   if ii ne idep-1 then print,'  density until layer',ii,' =',dep(ii),' A constant'
  
  if idrel eq 10 then begin
   ;---no used
   ;--------------Picture 5 number recoils / NH)
   defects(*,0)=0
   for i=1,jflu-1 do begin
    for j=0,ncp-1 do begin  
     if ( sumnproj(i)-sumnproj(i-1) ) gt 0 then begin
      defects(j,i)=(irec(j,i)-irec(j,i-1))/(sumnproj(i)-sumnproj(i-1))
     end else begin
      defects(j,i)=0.0
     end
    end
   end
   plot,flu,defects(1,*),xtitle=xytext(0) $
   ,yrange=[0,max(defects(0:ncp-1,*))],title='number recoils / NH' $
   ,thick=dd,charsize=gr,charthick=gd,position=pos1,/nodata
   j=1
    bmittel2=dblarr(jflu)
    bmittel2(0)=0
    for i=0,ncp-1 do begin
     if i eq 0 then fff=1. else fff=1.
     oplot,flu,defects(i,1:jflu-1)*fff,color=farbe(i)
     ;print,defects(i,1:jflu-1),defects(0,1:jflu-1)+defects(1,1:jflu-1)
     if idrel eq 1 then begin
       bmittel=dblarr(jflu)
       bmittel(0)=0
       for jj=1,jflu-1 do begin
         bmittel(jj)=(bmittel(jj-1)*(jj-1)+defects(i,jj))/jj
         bmittel2(jj)=bmittel2(jj)+defects(i,jj)
       end
       oplot,flu(1:jflu-1),bmittel(1:jflu-1)*fff,color=farbe(i)
       ;if i eq 1 then oplot,flu,bmittel2(1:jflu-1),color=farbe(i+1)
     end
     xyouts,.001,pos1(3)-(pos1(3)-pos1(1))/8*j $
     ,text1(i+1),charsize=gr/2,charthick=gd,/normal,color=farbe(i)  
      j=j+1
    end
    ;---unit
    xyouts,.001,pos1(3)-(pos1(3)-pos1(1))/8*(j),text2(1),charsize=gr/2,charthick=gd,/normal  
  
  end else begin
     
    ;---density picture 5
     if min(dens)*100 eq max(dens)*100 then begin
       lev=[dens(0)*.9,dens(0),dens(0)*1.1] 
       texta='density constant'
       _real2string,dens(0),dens(0),s
       textb=s+' [atoms/A**2]'
     end else begin
       texta=''
       textb=''
     _achsenwerte1,[min(dens)*100,max(dens)*100],lev,j,8,15
     end
     
     lab=lev
     lab(*)=1
     
     contour,dens*100,flu,dep $
     ,xtitle='fluence [atoms/A**2]',ytitle='depth [A]' $
     ,xrange=[0,max(flu)] $
     ,yrange=[dep(ii),0] $
     ,title='density [100*atoms/A**3]',levels=lev,c_labels=lab $
     ,thick=dd,charsize=gr,charthick=gd,position=pos1
     
    xyouts,pos1(0)+(pos1(2)-pos1(0))/10.,pos1(1)+(pos1(3)-pos1(1))*2./3.,texta $
      ,charsize=gr/2,charthick=gd,/normal  
    xyouts,pos1(0)+(pos1(2)-pos1(0))/10.,pos1(1)+(pos1(3)-pos1(1))/3. ,textb $
      ,charsize=gr/2,charthick=gd,/normal   
  end
  comp=comp_dep
  if einzel eq 1 then begin
   openr,1,'comp_steady_state_d_31.dat'
    ;---Bezeichnung
    readf,1,bez,format='(a50)' & bez=strtrim(bez,1)
    if bez ne bezall then begin
      print,'WARNING not the same calculation: backscatt_f_31.dat' 
      print,'Calculation:',bez
    end
    readf,1,text
    readf,1,idep,ncp
    dep=dblarr(idep)
    comp=dblarr(ncp,idep)
    text1=strarr(min([10,idep]))
    text2=text1
    readf,1,text1,format='(20a12)' & text1(*)=strtrim(text1(*),1)
    readf,1,text2,format='(20a12)' & text2(*)=strtrim(text2(*),1)
    xytext=text1+' '+text2
    ;---values
    help=dblarr(ncp)
    for i=0,idep-1 do begin
     readf,1,a,help
     dep (i)=a
     comp(*,i)=help(*)
    end
   close,1
  end  ;einzel eq 1
 
  ihelp=where(comp gt 1)
  if ihelp(0) ge 0 then comp(where(comp gt 1))=1.0d0
  ;if ihelp(0) ge 0 then print,'atomic fraction > 1 --> =1
  ihelp=where(comp lt 0)
  if ihelp(0) ge 0 then comp(where(comp lt 0))=0.0d0
  ;if ihelp(0) ge 0 then print,'atomic fraction < 0 --> =0

  ;---atomic fraction picture 6
  _posit,ric,isp,jze,spa,zei,dspa,dzei,dx,dy,pos,pos1

   ii=max([ii,iiq])
   plot,comp(0,*),dep,ytitle='depth [A]' $
   ,xrange=[min(comp)-.001,max(comp)+.001]  $ 
   ,yrange=[dep(ii),0] $
   ,title='atomic fraction (end)' $
   ,thick=dd,charsize=gr,charthick=gd,position=pos1,/nodata
   j=1

   for i=0,ncp-1 do begin 
     c=dblarr(idep)
     d=dblarr(idep)
     for j2=0,idep-1 do begin
       c(idep-1-j2)=comp(i,j2)
       d(idep-1-j2)=dep(j2)
     end
     oplot,c,d,color=farbe(i)
     xyouts,pos1(0)-.022*ncp+(pos1(2)-pos1(0))/5.9*(j) ,pos1(1)*.68 $
     ,text1(i+1),charsize=gr/2,charthick=gd,/normal,color=farbe(i)  
     j=j+1
     print,'  qmax(end):',text1(i+1),max(c)
   end

   xyouts,pos1(0)+(pos1(2)-pos1(0))/8*(j),pos1(1)*.68 $
   ,text2(1),charsize=gr/2,charthick=gd,/normal  
   
   xyouts,.10,.05,'!6ANALYSIS OF DYNAMICS   '+bez,charsize=gr*.7,charthick=gd,/normal  
;   xyouts,.10,.02,'!6'+strmid(titlecase(0),0,k),charsize=gr*.6,charthick=gd,/normal
   xyouts,.65,.00,'!6'+textdatum,charsize=gr*.4,charthick=gd,/normal  

     _druckende,druck,drname,form
;end ;kkk
ende: 

end
