@/afs/ipp/home/a/aam/public/archiv/TRIDYN/SDTrimSP/post/window.inc 
@/afs/ipp/home/a/aam/public/archiv/TRIDYN/SDTrimSP/post/level.inc 
@/afs/ipp/home/a/aam/public/archiv/TRIDYN/SDTrimSP/post/achsen.inc 
pro run
;---only if idrel==0---
;input;   E0_33_sputt.dat
;aoutput: EO33_sputt.ps
  _start
  druck=0
  print,'druck:0/1    0...screen 1...postscript'
  read,druck
  drname='EO33_sputt.ps'
  form=0
  text='' & text1='' & bez=''& textdatum=''
  jflu=1
  idep=1
  ncp=1
  a=1.0d0 & b=1.0d0 & c=1.0d0
  loadct,39
  close,1
  if druck eq 0 then farbe=[255,250,150,80,100,200] else $
                     farbe=[0,250,150,80,100,200]
  
  openr,1,'E0_33_sputt.dat'
   readf,1,textdatum
   readf,1,bez
   readf,1,text
   fluenc=1.0
   nh=long(1) & idout=1 & ncp=1 & flumax=1.0 
   readf,1, nh,idout,ncp,flumax 
   symbol=strarr(ncp)
   text1=strarr(ncp+1)
   readf,1,symbol,format='(20a12)' & symbol(*)=strtrim(symbol(*),2)
        text1(1:ncp)=symbol(0:ncp-1)  
        text1(0)='[-]'  
   jflu=nh/idout+1
   print,'Fluence step',jflu,' max Fluence:',flumax,' atoms/A**2'
   print,'kind of atoms:',symbol
   help=dblarr(ncp)
   
   flu =dblarr(jflu) 
   backsputt=dblarr(jflu,ncp,ncp)
   help=dblarr(ncp)
    
   for i=0,jflu-1 do begin
     if (i eq 0)then begin
       readf,1,text
       readf,1,a
       flu(i)=a
       readf,1,text
       for j=0,ncp-1 do begin
         readf,1,text
         readf,1,help
         backsputt(i,j,*)=help(*)  ;by j 
       end
     end else begin
       readf,1,a
       flu(i)=a
       for j=0,ncp-1 do begin
         readf,1,help
         backsputt(i,j,*)=help(*)     
       end
     end
   end
    close,1
  xytext=['fluence [atoms/A**2]','surface [A]']
  
  ;---prepare 6 picture
  _fenster,druck,drname,form   
   spa=2 & zei=3
   !P.Multi=[0,spa,zei] 
   if form eq 0 then pos=[0.02, 0.05 , .97, .96]
   if form eq 1 then pos=[0.01, 0.1, .98, .97]
   ric=0
   dspa=25 ;% Zwischenraum
   dzei=25 ;% Zwischenraum
   dx=(pos(2)-pos(0))/spa
   dy=(pos(3)-pos(1))/zei
   isp=0
   jze=0
   if ric eq 0 then jze=1 else isp=1
   druckfak=1
   if druck eq 1 then druckfak=.8
   dd=1.5*druckfak
   gr=2.5*druckfak
   gd=1.5*druckfak
  
  ;bey j
  for k=0,ncp-1 do begin
  _posit,ric,isp,jze,spa,zei,dspa,dzei,dx,dy,pos,pos1
   tt=strtrim('backsputtering by '+symbol(k),2)
   plot,flu,backsputt(*,k,0),xtitle=xytext(0) $
   ,yrange=[0,max(backsputt(*,*,*))],title=tt $
   ,xrange=[0,max(flu)] $
   ,thick=dd,charsize=gr,charthick=gd,position=pos1,/nodata
   
   print,'min/max:',min(backsputt(*,k,*)),max(backsputt(*,k,*)),' by: ',symbol(k)
   for i=0,ncp-1 do begin
     oplot,flu(1:jflu-1),backsputt(1:jflu-1,k,i),color=farbe(i)
     xyouts,pos1(0)-.1,pos1(3)-(pos1(3)-pos1(1))/6*(i+1) $
     ,text1(i+1),charsize=gr/2,charthick=gd,/normal,color=farbe(i)  
   end
   xyouts,pos1(0)-.1,pos1(3)-(pos1(3)-pos1(1))/6*(ncp+1) $
     ,text1(0),charsize=gr/2,charthick=gd,/normal,color=farbe(0)  
   
   ;xyouts,.52,pos1(3)-(pos1(3)-pos1(1))/6*(j) $
   ;,text2(1),charsize=gr/2,charthick=gd,/normal  
  end
  
   
   xyouts,.10,.05,'!6ANALYSIS OF SPUTTERING  '+bez,charsize=gr*.7,charthick=gd,/normal  
;   xyouts,.10,.02,'!6'+strmid(titlecase(0),0,k),charsize=gr*.6,charthick=gd,/normal
   xyouts,.65,.00,'!6'+textdatum,charsize=gr*.4,charthick=gd,/normal  
     _druckende,druck,drname,form

end
