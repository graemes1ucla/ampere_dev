pro begin_window_inc__________
end
;---alle von vis.inc
;siehe auch   /afs/ipp-garching.mpg.de/home/a/aam/MAGCOOR/PROGRAMME/up.p

;@/afs/ipp/home/a/aam/IDL/TEST/window.inc 
;@/afs/ipp/home/a/aam/IDL/TEST/mathe.inc 
;@/afs/ipp/home/a/aam/IDL/TEST/level.inc 
;@/afs/ipp/home/a/aam/IDL/TEST/achsen.inc 

; 1pro _anfwidget,hilfvar,base
; 2pro widget2_event,ev
; 3pro widget3_event,ev
; 4pro widget4_event,ev
; 5pro widget5_event,ev
; 6pro _select,text,anz,s,i
; 7pro _clic3,n,beschr,auswahl,zeilen=zeilen
; 8pro _clic4,text,beschr,auswahl
; 9pro _intein,i,gtext
;10pro _fltein,a,gtext
;11pro _strein,textaus,gtext
;12pro _eingabe4,s
;13pro _texteingabe,textu,text,gr,di,xcoord,ycoord
;14pro _eing_char   ,uber,text,gr,di
;15pro _eing_Spalten_Zeilen,xyanz,spa,zei,dspa,dzei,ric
;16pro _from_input,testtext
;17pro _hauptmenu,form,druck,drname,auswahl,textplot=textplot
;alle mit  common widhilf,hilfvar
;
;18pro _start
;19pro _fenster,druck,drname,form,nummer=nummer,xxsize=xxsize,yysize=yysize
;20pro _druckende,druck,drname,form
;21pro _druckende_a3,druck,drname,form
;22pro _posit,ric,isp,jze,spa,zei,dspa,dzei,dx,dy,pos,pos1
;23pro _real2string,r1,r2,s,forma=forma
;24pro _int2string,i1,i2,s

;25pro _load_farbe_kugel
;=================================================================
pro _start
  s=!version
  print,'machine:',s.os
  if s.os eq 'AIX'   then device,pseudo_color=8,retain=2,set_font='Helvetica'
  if s.os eq 'linux' then device,true_color=24,retain=2,set_font='Helvetica',decompose=0
  set_plot,'x'
  hilf=size(!D.window)
  for i=1,hilf(1)-1 do begin
      if !D.window(0) ne -1 then wdelete
  endfor
end
;================================================================
pro _anfwidget,hilfvar,base
  hilfvar='0'
  base =widget_base(/column)
end
;=================================================================
pro widget2_event,ev
  common widhilf,hilfvar
  widget_control, ev.top, get_uvalue=textwid
  widget_control, ev.id , get_uvalue=uval
  hilfvar=uval
  if uval ne '0' then widget_control,ev.top,/destroy
end
;================================================================
pro widget3_event,ev
  common widhilf,hilfvar
  widget_control, ev.top, get_uvalue=textwid
  widget_control, ev.id , get_uvalue=uval
  if uval eq '1' then begin
     widget_control, ev.id , get_value=t
     hilfvar=t
     widget_control,ev.top,/destroy
  end
  if uval eq '2' then widget_control,ev.top,/destroy
end
;===============================================================
pro widget4_event,ev
  common widhilf,hilfvar
  widget_control, ev.id , get_uvalue=uval
  if uval eq '0' then widget_control,ev.top,/destroy
end
;=================================================================
pro widget5_event,ev
  common widhilf,hilfvar
  widget_control, ev.id , get_uvalue=uval
  i=fix(uval)
  if uval eq '100' then widget_control,ev.top,/destroy
  if i ne 100 then hilfvar(i)=1-hilfvar(i)
end
;=================================================================
pro _select,text,anz,s,i
  beschr=strarr(anz+1)
  beschr(0)=text
  for l=0,anz-1 do begin  
    beschr(l+1)=s(l)
  end
  auswahl=0
  _clic3,anz,beschr,auswahl
  i=auswahl-1
  print,'It will be shown: ',s(i)
end
;===============================================================
pro _clic3,n,beschr,auswahl,zeilen=zeilen
  common widhilf,hilfvar
  if n_elements(zeilen) eq 0 then zeilen=15
  ;print,'zeilen:',zeilen,'n:',n
  hilfvar='0'
  lll=1
  if n lt zeilen then base =widget_base(/column) $
      else begin
        lll = fix((n)/zeilen)+1
        if lll le 5 then base =widget_base(column=lll)
        if lll gt 5 then base =widget_base(column=lll,/scroll,scr_xsize=5*220,scr_ysize=700)
      end
  for i=0,n do button=widget_button(base,value=beschr(i),uvalue=i)

  ;print,'lll:',lll
  if lll gt 1 then begin
      hilf=lll*zeilen-n-1
      for i=0,hilf-1 do l=widget_label(base,value=' ')
  end
  widget_control,base,/realize,tlb_set_xoffset=50,tlb_set_yoffset=50
  xmanager,'widget2',base
  auswahl=fix(hilfvar)
end
;=================================================================
pro _clic4,text,beschr,auswahl
  common widhilf,hilfvar
  hilf=size(auswahl)
  hilfvar=auswahl
  if hilf(1)-1 lt 15 then base =widget_base(/column) $
      else begin
        lll = fix((hilf(1)-1) /15) +1
        if lll le 5 then base =widget_base(column=lll)
        if lll gt 5 then base =widget_base(column=lll,/scroll,scr_xsize=5*220,scr_ysize=700)
      end
  bgroup1=CW_BGROUP(base,beschr(0),/nonexclusive,uvalue=0,set_value=hilfvar(0),label_top=text,/frame)
  for i=1,hilf(1)-1 do $
      bgroup1=CW_BGROUP(base,beschr(i),/nonexclusive,uvalue=i,set_value=hilfvar(i),/frame)
  button=widget_button(base,value='OK'  ,uvalue=100)
  widget_control,base,/realize,tlb_set_xoffset=50,tlb_set_yoffset=50;
  xmanager,'widget5',base
  auswahl=hilfvar
 end
;=================================================================
pro _intein,i,gtext
  common widhilf,hilfvar
  hilfvar=string(i)
  if abs(i) lt 1000 then hilfvar=string(i,format="(i4)")
  if abs(i) lt 100 then hilfvar=string(i,format="(i3)")
  if abs(i) lt 10 then hilfvar=string(i,format="(i2)")
  base =widget_base(/column)
  button0=widget_button(base,value=gtext,uvalue='0')
  text=widget_text(base,/editable,value=hilfvar,uvalue='1',xsize=20)
  button1=widget_button(base,value='cancel',uvalue='2')
  widget_control,base,set_uvalue=text
  widget_control,base,/realize,tlb_set_xoffset=50,tlb_set_yoffset=50
  xmanager,'widget3',base
  ;---umwandeln von i bzw. hilfvar in ein Skalar
   i=fix(hilfvar)
   hilfvar=string(i(0))
   i=fix(hilfvar)
end
;=================================================================
pro _fltein,a,gtext
  common widhilf,hilfvar
  ;_real2string,a,a,hilfvar
  hilfvar=string(a)
  base =widget_base(/column)
  button0=widget_button(base,value=gtext,uvalue='0')
  text=widget_text(base,/editable,value=hilfvar,uvalue='1',xsize=50)
  button1=widget_button(base,value='cancel',uvalue='2')
  widget_control,base,set_uvalue=text
  widget_control,base,/realize,tlb_set_xoffset=50,tlb_set_yoffset=50
  xmanager,'widget3',base
  ;---umwandeln von a bzw. hilfvar in ein Skalar
   a=float(hilfvar)
   hilfvar=string(a(0))
   a=float(hilfvar)
end
;=================================================================
pro _strein,textaus,gtext
  common widhilf,hilfvar
  hilfvar=textaus
  base =widget_base(/column)
   button0=widget_button(base,value=gtext,uvalue='0')
   text=widget_text(base,/editable,value=hilfvar,uvalue='1',xsize=50)
   button1=widget_button(base,value='cancel',uvalue='2')
  widget_control,base,set_uvalue=text
  widget_control,base,/realize,tlb_set_xoffset=50,tlb_set_yoffset=50
  xmanager,'widget3',base
  close,1
  openw,1,'vis_temp.dat'
    printf,1,hilfvar
  close,1
  openr,1,'vis_temp.dat'
   textaus=''
   readf,1,textaus
  close,1
end
;================================================================
 pro _eingabe4,s
   common widhilf,hilfvar
   a0=s(0)
   a1=s(1)
   a2=s(2)
   a3=s(3)
   clicmal:
   _anfwidget,hilfvar,base
   button=widget_button(base,value='Position of plot window',uvalue=0)
   button=widget_button(base,value='X_0 =' +string(a0)      ,uvalue=1)
   button=widget_button(base,value='Y_0 =' +string(a1)      ,uvalue=2)
   button=widget_button(base,value='X_1 =' +string(a2)      ,uvalue=3)
   button=widget_button(base,value='Y_1 =' +string(a3)      ,uvalue=4)
   button=widget_button(base,value='return'                 ,uvalue=5)
   widget_control,base,/realize,tlb_set_xoffset=50,tlb_set_yoffset=50
   xmanager,'widget2',base
   auswahl=fix(hilfvar)
   if auswahl eq 1 then _fltein,a0,'X_0 (0..1)=' +string(a0)
   if auswahl eq 2 then _fltein,a1,'Y_0 (0..1)=' +string(a1)
   if auswahl eq 3 then _fltein,a2,'X_1 (0..1)=' +string(a2)
   if auswahl eq 4 then _fltein,a3,'Y_1 (0..1)=' +string(a3)
   if auswahl ne 5 then goto,clicmal
   s(0)=a0
   s(1)=a1
   s(2)=a2
   s(3)=a3
end
;================================================================
pro _fenster,druck,drname,form,nummer=nummer,xxsize=xxsize,yysize=yysize
  ;---druck == 0 'Screen'
  ;         == 1 'postscrpt'
  ;         == 2 'PCL'
  ;         == 3 'bmp'
  ;         == 4 'jpeg'

  ;---form=0 ...'portrait  A4'
  ;---form=1 ...'landscape A4' 
  ;---form=2 ...'variable'
  
  if n_elements(nummer) eq 0 then nummer=9
  if n_elements(xxsize) eq 0 then xxsize=10
  if n_elements(yysize) eq 0 then yysize=10
  
  ;set_plot,'x'
  ;device,pseudo_color=8,retain=2,set_font='Helvetica',decompose=0

  if druck eq 2 then begin
    set_plot,'PCL'
    ;Eingabe
    if form eq 0 then device,filename=drname,optimize=1,/portrait,$
                                        xsize=18,    ysize=27, $
                                        xoffset=1.5, yoffset=1.5
    if form eq 1 then device,filename=drname,optimize=1,/landscape,$
                                        xsize=27,     ysize=18,$
                                        xoffset=.3,   yoffset=1.0
  endif

  if druck eq 1 then begin
    set_plot,'ps'
    print,'print-file: ',drname
    if form eq 0 then device,filename=drname $
                ,/encapsulated $
                ,/Color,bits=8 $
                ,/portrait  $
                ,xsize=18. $
                ,ysize=27.
    if form eq 1 then device,filename=drname $
                ,/encapsulated $
                ,/Color,bits=8 $
                ,/landscape $
                ,xsize=27. $
                ,ysize=18.
    if form eq 2 then device,filename=drname $
                ,/encapsulated $
                ,/Color,bits=8 $
                ,xsize=xxsize $
                ,ysize=yysize
  endif
  
  ;---> Fenster A4 hoch
  if( (druck eq 0) and (form eq 0) )then $
      window,nummer, title='IDL'+string(nummer),retain=2 $
                                 ,xpos=600,ypos=100, xsize=18*33, ysize=27*33
  ;---> Fenster quer
  if( (druck eq 0) and (form eq 1) )then $
      window,nummer,title='IDL'+string(nummer), retain=2 $
                           ,xpos=60,ypos=70, ysize=18*42, xsize=27*42
  ;---> Fenster variable
  if( (druck eq 0) and (form eq 2) )then $
      window,nummer,title='IDL'+string(nummer), retain=2 $
          ,xpos=1000-xxsize*10,ypos=500-yysize*10, xsize=xxsize*42, ysize=yysize*42
end
;================================================================
pro _druckende,druck,drname,form
  if (druck eq 1) or (druck eq 2) then begin
    ;---druck == 1 'postscrpt' / == 2 'PCL'
    device,/close_file
    set_plot,'x'
    device,pseudo_color=8,retain=2,set_font='Helvetica'

    if (druck eq 1) then begin
      find=FINDFILE(drname,COUNT=exist)
      if(exist ne 0) then begin
        t=' '
        close,2
        close,3
        openr,2,drname
        openw,3,'vis_temp.dat'
        les:
        if not EOF(2) then readf,2,t
        printf,3,t
        if t eq '%%EndPageSetup' then begin
          if form eq 0 then  printf,3,' 3000 3000 translate'
          if form eq 1 then  printf,3,'-2000 1000 translate' ;quer
          printf,3,'0.90 0.90 scale'
           ;printf,3,'90 rotate'
        end
        if t eq '%%Trailer' then begin
          printf,3,'showpage'
        end
        if not EOF(2) then goto,les
      end
      close,2
      close,3
      spawn,'/bin/mv vis_temp.dat '+drname
    end
  end

  ;---druck == 3 'bmp'
  if druck eq 3 then begin
    bild1=tvrd(channel=1)
    bild2=tvrd(channel=2)
    bild3=tvrd(channel=3)
    s=size(bild1)
    bild=bytarr(3,s[1],s[2])
    bild(0,*,*)=bild3
    bild(1,*,*)=bild2
    bild(2,*,*)=bild1
    write_bmp,drname,bild
   end

   ;---druck == 4 'jpg'
   if druck eq 4 then begin
    bild1=tvrd(channel=1)
    bild2=tvrd(channel=2)
    bild3=tvrd(channel=3)
    s=size(bild1)
    bild=bytarr(3,s(1),s(2))
    bild(0,*,*)=bild1(*,*)
    bild(1,*,*)=bild2(*,*)
    bild(2,*,*)=bild3(*,*)
    write_jpeg,drname,bild,true=1
   end
end
;================================================================
pro _druckende_a3,druck,drname,form
  if (druck eq 1) or (druck eq 2) then begin
    ;---druck == 1 'postscrpt' / == 2 'PCL'
    device,/close_file
    set_plot,'x'
    device,pseudo_color=8,retain=2,set_font='Helvetica'
    
    if (druck eq 1) then begin
      find=FINDFILE(drname,COUNT=exist)
      if(exist ne 0) then begin
        t=' '
        close,2
        close,3
        openr,2,drname
        openw,3,'vis_temp.dat'
        les:
        if not EOF(2) then readf,2,t
        printf,3,t
        if t eq '%%EndPageSetup' then begin
          if form eq 0 then  printf,3,' 3000 3000 translate'
          ;if form eq 1 then  printf,3,'-2000 1000 translate' ;quer
          if form eq 1 then  printf,3,'-14000 1000 translate' ;quer
          ;---A3
          printf,3,'1.4 1.403 scale'
           ;printf,3,'90 rotate'
        end
        if t eq '%%Trailer' then begin
          printf,3,'showpage'
        end
        if not EOF(2) then goto,les
      end
      close,2
      close,3
      spawn,'/bin/mv vis_temp.dat '+drname
    end
  end
end
;================================================================
pro _posit,ric,isp,jze,spa,zei,dspa,dzei,dx,dy,pos,pos1
       if ric eq 0 then begin 
         ; zeilenweise
         if isp eq spa then jze=jze+1
         if isp eq spa then isp=1 else isp=isp+1 
       end else begin
         ; spaltenweise
         if jze eq zei then isp=isp+1
         if jze eq zei then jze=1 else jze=jze+1 
       end
       xxx=pos(0)+(isp-1)*dx
       yyy=pos(1)+(zei-jze)*dy
       pos1=[xxx+dx*dspa/100,yyy+dy*dzei/100,xxx+dx,yyy+dy] 
end 
;================================================================
pro _texteingabe,textu,text,gr,di,xcoord,ycoord
   common widhilf,hilfvar
   clicmal:
   _anfwidget,hilfvar,base
   button=widget_button(base,value=textu                   ,uvalue=0)
   button=widget_button(base,value=text                    ,uvalue=1)
   button=widget_button(base,value='Charsize' +string(gr)  ,uvalue=2)
   button=widget_button(base,value='Charthick'+string(di)  ,uvalue=3)
   button=widget_button(base,value='x-coord'+string(xcoord),uvalue=4)
   button=widget_button(base,value='y-coord'+string(ycoord),uvalue=5)
   button=widget_button(base,value='return'                ,uvalue=6)
   widget_control,base,/realize,tlb_set_xoffset=50,tlb_set_yoffset=50
   xmanager,'widget2',base
   auswahl=fix(hilfvar)
   if auswahl eq 1 then _strein,text,text
   if auswahl eq 2 then _fltein,gr,'Charsize' +string(gr)
   if auswahl eq 3 then _fltein,di,'Charthick'+string(di)
   if auswahl eq 4 then _fltein,xcoord,'x-coord'+string(xcoord)
   if auswahl eq 5 then _fltein,ycoord,'y-coord'+string(ycoord)
   if auswahl ne 6 then goto,clicmal
end
;================================================================
pro _eing_char,uber,text,gr,di
   ;'---change of text,gr,di
   ;'---if text==0 text=uber
   common widhilf,hilfvar
   if strlen(text) eq 0 then text=uber
  clicmal30:
   _anfwidget,hilfvar,base
   button=widget_button(base,value=uber                    ,uvalue=0)
   button=widget_button(base,value=text                    ,uvalue=1)
   button=widget_button(base,value='Charsize' +string(gr)  ,uvalue=2)
   button=widget_button(base,value='Charthick'+string(di)  ,uvalue=3)
   button=widget_button(base,value='return'                ,uvalue=4)
   widget_control,base,/realize,tlb_set_xoffset=50,tlb_set_yoffset=50
   xmanager,'widget2',base
   auswahl2=fix(hilfvar)
   if auswahl2 eq 1 then _strein,text,text
   if auswahl2 eq 2 then _fltein,gr,'Charsize'  +string(gr)
   if auswahl2 eq 3 then _fltein,di,'Charthick' +string(di)
   if auswahl2 ne 4 then goto,clicmal30
end
;================================================================
pro _eing_Spalten_Zeilen,xyanz,spa,zei,dspa,dzei,ric
   common widhilf,hilfvar
   clicmal50:
   _int2string,xyanz,xyanz,hilf5
   if ric eq 0 then hilf0='rows major' else hilf0='column major'
   _int2string,spa,spa,hilf1
   _int2string,zei,zei,hilf2
   _int2string,dspa,dspa,hilf3
   _int2string,dzei,dzei,hilf4
   _anfwidget,hilfvar,base
   button=widget_button(base,value=hilf5+' Figure',uvalue=0)
   button=widget_button(base,value=hilf0                   ,uvalue=1)
   button=widget_button(base,value='Number of columns '+ hilf1 ,uvalue=2)
   button=widget_button(base,value='Number of rows '+ hilf2 ,uvalue=3)
   button=widget_button(base,value='Distance of columns '+ hilf3+'% of draw frame',uvalue=4)
   button=widget_button(base,value='Distance of rows '+ hilf4+'% of draw frame',uvalue=5)
   button=widget_button(base,value='return'        ,uvalue=6)
   widget_control,base,/realize,tlb_set_xoffset=50,tlb_set_yoffset=50
   xmanager,'widget2',base
   auswahl3=fix(hilfvar)
   if auswahl3 eq 1 then ric=1-ric
   if auswahl3 eq 2 then _intein,spa ,'Number of columns '+hilf1
   if auswahl3 eq 3 then _intein,zei ,'Number of rows '+hilf2
   if auswahl3 eq 4 then _intein,dspa ,'Distance of columns '+ hilf3+'% of draw frame'
   if auswahl3 eq 5 then _intein,dzei ,'Distance of rows '+ hilf4+'% of draw frame'
   if auswahl3 ne 6 then goto,clicmal50
end
;================================================================
pro _from_input,testtext
    common widhilf,hilfvar
    _anfwidget,hilfvar,base
    button  =widget_button(base,value='TEXT SELECT ',uvalue=0)
    button  =widget_button(base,value='Text from input file',uvalue=1)
    button  =widget_button(base,value='Text from config file',uvalue=2)
    widget_control,base,/realize,tlb_set_xoffset=50,tlb_set_yoffset=50
    xmanager,'widget2',base
    if fix(hilfvar) eq 1 then testtext=1 else testtext=0
end
;;================================================================
pro _hauptmenu,form,druck,drname,auswahl,textplot=textplot
   common widhilf,hilfvar
   if n_elements(textplot) eq 0 then textplot=''
   beschr=strarr(7)
   beschr(0)='SELECT '
   if form eq 0 then beschr(1)='Portrait  (Landscape)' $
                else beschr(1)='Landscape  (Portrait)'
   if druck eq 0 then beschr(2)='Screen  (Postscript)' $
                 else beschr(2)='Postscript  (Screen)'
   beschr(3)='Output file: '+drname
   beschr(4)='Plot parameter -->'
   if druck eq 0 then beschr(5)='Plot'+textplot  $
                 else beschr(5)='Print to file'+textplot
   beschr(6)='Return'
   _anfwidget,hilfvar,base
   for i=0,6 do button=widget_button(base,value=beschr(i)  ,uvalue=i)
   widget_control,base,/realize,tlb_set_xoffset=50,tlb_set_yoffset=50
   xmanager,'widget2',base
   auswahl=fix(hilfvar)
   if auswahl eq 1 then form =1-form
   if auswahl eq 2 then druck=1-druck
   if auswahl eq 3 then begin
      gtext='Output file'
      _strein,drname,gtext
   end
end
;================================================================
pro _real2string,r1,r2,s,forma=forma
  if n_elements(forma) eq 0 then begin
   ;'---forma nicht definiert
   www:
   if r2 ge 0 then begin
    if abs(r1) ge 1000000 then s=string(r2                )
    if abs(r1) lt 1000000 then s=string(long(r2+.1),format="(i6)")
    if abs(r1) lt  100000 then s=string(long(r2+.1),format="(i5)")
    if abs(r1) lt   10000 then s=string(fix(r2+.1),format="(i4)")
    if abs(r1) lt    1000 then s=string(fix(r2+.1),format="(i3)")
    if abs(r1) lt     100 then s=string(fix(r2+.1),format="(i2)")
    if abs(r1) lt      10 then s=string(r2,format="(f5.2)")
    if abs(r1) lt      .1 then s=string(r2,format="(f6.3)")
    if abs(r1) lt     .01 then s=string(r2,format="(f7.4)")
    if abs(r1) lt    .001 then s=string(r2,format="(f8.5)")
    if abs(r1) lt   .0001 then s=string(r2                )
   end else begin
    if abs(r1) ge 1000000 then s=string(r2                )
    if abs(r1) lt 1000000 then s=string(long(r2-.1),format="(i7)"  )
    if abs(r1) lt  100000 then s=string(long(r2-.1),format="(i6)"  )
    if abs(r1) lt   10000 then s=string(fix(r2-.1),format="(i5)"  )
    if abs(r1) lt    1000 then s=string(fix(r2-.1),format="(i4)"  )
    if abs(r1) lt     100 then s=string(fix(r2-.1),format="(i3)"  )
    if abs(r1) lt      10 then s=string(r2,format="(f6.2)")
    if abs(r1) lt      .1 then s=string(r2,format="(f6.3)")
    if abs(r1) lt     .01 then s=string(r2,format="(f7.4)")
    if abs(r1) lt    .001 then s=string(r2,format="(f8.5)")
    if abs(r1) lt   .0001 then s=string(r2                )
   end
  end else begin
   ;'---forma definiert, aber leer
   if forma eq '' then goto,www
   s=string(r2,format=forma  )
  end
  ;print,'_real2string r1,r2,s',r1,r2,' ->',s
end
;================================================================
pro _int2string,i1,i2,s
  if i2 ge 0 then begin
   if abs(i1) ge 1000000 then s=string(i2                )
   if abs(i1) lt 1000000 then s=string(i2,format="(i6)"  )
   if abs(i1) lt  100000 then s=string(i2,format="(i5)"  )
   if abs(i1) lt   10000 then s=string(i2,format="(i4)"  )
   if abs(i1) lt    1000 then s=string(i2,format="(i3)"  )
   if abs(i1) lt     100 then s=string(i2,format="(i2)"  )
   if abs(i1) lt      10 then s=string(i2,format="(i1)")
  end else begin
   if abs(i1) ge 1000000 then s=string(i2                )   
   if abs(i1) lt 1000000 then s=string(i2,format="(i7)"  )   
   if abs(i1) lt  100000 then s=string(i2,format="(i6)"  )   
   if abs(i1) lt   10000 then s=string(i2,format="(i5)"  )   
   if abs(i1) lt    1000 then s=string(i2,format="(i4)"  )   
   if abs(i1) lt     100 then s=string(i2,format="(i3)"  )   
   if abs(i1) lt      10 then s=string(i2,format="(i2)")
  end
end
;================================================================
;pro _load_farbe_kugel
;  set_plot,'x'
;  loadct,5
;  r=bytarr(256)
;  g=bytarr(256)
;  b=bytarr(256)
;  r(255)=255 & g(255)=255 & b(255)=255 ;weiss
;
;  jf=0
;  jd=24
;  ;---blau
;  jf=jf+(jd+1) ;25
;  for i=jf-jd,jf-1 do begin
;    r(i)=0
;    g(i)=0
;    b(i)=10*(i-(jf-jd)+1)
;  end
;  b(jf)=255
;  for i=jf+1,jf+jd+1 do begin
;    r(i)=10*(i-jf)
;    g(i)=10*(i-jf)
;    b(i)=255
;  end
;  print,'blue:        ',jf-jd,jf+jd+1
;
;  ;---rot
;  jf=jf+2*(jd+1) ;75
;  for i=jf-jd,jf-1 do begin
;    r(i)=10*(i-(jf-jd)+1)
;    g(i)=0
;    b(i)=0
;  end
;  r(jf)=255
;  for i=jf+1,jf+jd+1 do begin
;    r(i)=255
;    g(i)=10*(i-jf)
;    b(i)=10*(i-jf)
;  end
;  print,'red:         ',jf-jd,jf+jd+1
;
;  ;---gelb
;  jf=jf+2*(jd+1) ;125
;  for i=jf-jd,jf-1 do begin
;    r(i)=10*(i-(jf-jd)+1)
;    g(i)=10*(i-(jf-jd)+1)
;    b(i)=0
;  end
;  r(jf)=255
;  g(jf)=255
;  for i=jf+1,jf+jd+1 do begin
;    r(i)=255
;    g(i)=255
;    b(i)=10*(i-jf)
;  end
;  print,'yellow:        ',jf-jd,jf+jd+1
;
;  ;goto,ohnegreen
;  ;---gruen
;  jf=jf+2*(jd+1) ;175
;  for i=jf-jd,jf-1 do begin
;    r(i)=0
;    g(i)=10*(i-(jf-jd)+1)
;    b(i)=0
;  end
;  g(jf)=255
;  for i=jf+1,jf+jd+1 do begin
;    r(i)=10*(i-jf)
;    g(i)=255
;    b(i)=10*(i-jf)
;  end
;  print,'green:       ',jf-jd,jf+jd+1
;  ohnegreen:
;
;  ;---gelb -->blau
;  jf=jf+2*(jd+1)  ;!!! 2*   beim 1.mal   jd/2
;  for i=jf-jd,jf-1 do begin
;    r(i)=255-10*(i-(jf-jd)+1)
;    g(i)=255-10*(i-(jf-jd)+1)
;    b(i)=10*(i-(jf-jd)+1)
;  end
;  b(jf)=255
;  print,'yellow -->blue:',jf-jd,jf
;
;  ;---gelb -->rot
;  jf=jf+(jd+1)
;  for i=jf-jd,jf-1 do begin
;    r(i)=255
;    g(i)=255-10*(i-(jf-jd)+1)
;    b(i)=0
;  end
;  r(jf)=255
;  print,'yellow -->red: ',jf-jd,jf
;
;  ;---violett
;  ;100 0 255 lila
;  jf=jf+(jd+1)
;  for i=jf-jd,254 do begin
;    r(i)=100
;    g(i)=0
;    b(i)=255-50*(i-(jf-jd)+1)
;  end
;  print,'violet:     ',jf-jd,254
;
;  tvlct,r,g,b
;end
;;------------------------------------------------
pro end_window_inc__________
end
