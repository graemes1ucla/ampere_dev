!---program recalculate the distribution with help of moments (E0_34_moments.dat)
!   The distribution can draw with mom2dis.pro
!   input: 
!         E0_34_moments.dat
!   output:
!         E0_34_distribut.dat

module work_moment
  implicit none
  integer  , parameter :: iwp      = 8  !---double precision    
  integer  , parameter :: io_out   = 6  !---standard output   
  
  !---constant real
  real(iwp), parameter  :: pi    = 3.141592653589793238d0
  real(iwp), parameter  :: pi2   = 3.141592653589793238d0*2.0d0
  
  integer   :: ndeg,io06
  real(iwp) :: yscale,xscale,xl(10)

end module work_moment

program plmaxent
  use work_moment
  implicit none
  !---number of sampling points of calculated distribution
  !   value is arbitary from user (have an influence of table in E0_34_distribut.dat)
  integer,parameter   :: vvi=50  
  
  !---number of sampling points of given distribution (option: DEBUGMOM in SDTrimSP)
  !   value is fix 
  integer,parameter:: vvj=40
  
  real(iwp), dimension(:,:,:), pointer :: vvv !---vvv...distribution (counts from SDTrimSP)
  real(iwp) :: vvx                            !---vvx...sampling pointscenter 
  
  real(iwp), parameter:: eps  = 1.0d-10
  
  real(iwp) :: x1s,x2s,x3s,x4s,x5s,x6s 
  real(iwp) :: xmina,xmaxa,xdif,xminal,xmaxal,xdifl
  real(iwp) :: udt ,fctexpl,fctexp3 
  integer   :: i,k,np

  character :: title*40,version*40
  character :: text*80
  character :: file_in*100  !---inputname
  character :: file_out*100 !---outputname
  integer   :: nh,ncp,nr,idout,ncp_proj,idrel,nh_nr
  character(len=5), dimension(:), pointer :: symbol,einheit
  character(len=29):: bez(8)
  real(iwp), dimension(:),   pointer :: e0_beam
  real(iwp), dimension(:),   pointer :: ipart
  real(iwp), dimension(:,:), pointer :: mom_part
  real(iwp) :: nr_momente,flu,dx_lin,dx_log
  real(iwp) :: xaa,xee
  integer   :: lin(8),incp(8)
  integer   :: im,mh,debugmom,io31,io20
 
  io06=6
  io20=20
  io31=31 
   
   file_in='E0_34_moments.dat'
   file_out ='E0_34_distribut.dat'
   open(io20, file=file_out)
   open(io31, file=file_in)
   
   read(io31, '(a)') version
   read(io31, '(a)') title
   write(io06,*)' '
   write(io06,*)'----------/post/mom2dis.exe--------------------------------'
   write(io06,*)' read file:',file_in
   write(io06,*)' version: ',version
   write(io06,*)' title  : ',title
   write(io20,'(a)')version
   write(io20,'(a)')title
   
   read( io31,'(a)')text
   write(io20,'(a)')text
   
   read( io31,'(a)')text
   write(io20,'(a)')text 
   
   read(io31, *) nh,nr,ncp,idout,ncp_proj,idrel,debugmom

   if (idrel==0)then
     nh_nr=nh/idout
     write(io20,13) nh,nh_nr,ncp,idout,ncp_proj,idrel,debugmom
   else
     !---static case output only the end of calculation
     nh_nr=nh*nr
     write(io20,13) nh,nh_nr,ncp,nh   ,ncp_proj,idrel,debugmom
   endif
   !write(io06,*)'nh,ncp,idout,ncp_proj,idrel:',nh,ncp,idout,ncp_proj,idrel

   allocate(symbol(ncp),einheit(ncp))
   read( io31,'(10a10)') symbol(1:ncp)
   write(io20,'(10a10)') symbol(1:ncp)
   write(io06,*        )' elements: ',symbol(1:ncp)
   
   read( io31,'(a)')text
   write(io20,'(a)')text

   allocate (e0_beam(ncp))
   read( io31, *) e0_beam(1:ncp)
   write(io20,12) e0_beam(1:ncp)
   write(io06,*)' E0 (eV): ',e0_beam(1:ncp)
   
   
   
   do i=1,12
      read(io31, '(a)') text
      !print*,text
   enddo    
   
   !---value from E0_34_moments.dat     '
   bez(1)='Energy refl. coeff. (lin)    '  !---lin
   bez(2)='Energy refl. coeff. (log)    '  !---log
   bez(3)='Energy transm. coeff. (lin)  '  !---lin
   bez(4)='Energy transm. coeff. (log)  '  !---log
   bez(5)='Energy sputt. yield (log)    '  !---log
   bez(6)='Energy transm.sp. yield (log)'  !---log 
   bez(7)='Depth  impl. proj. (lin)     '  !---lin
   bez(8)='Radius impl. proj. (lin)     '  !---lin
   lin(1)=1  !---lin
   lin(2)=0  !---log
   lin(3)=1  !---lin
   lin(4)=0  !---log
   lin(5)=0  !---log
   lin(6)=0  !---log
   lin(7)=1  !---lin
   lin(8)=1  !---lin
   incp(1)=ncp_proj  !1 i_back_p(1:ncp_proj)    lin_moment_energy_refl.     
   incp(2)=ncp_proj  !2 i_back_plog(1:ncp_proj) log_moment_energy_refl.     
   incp(3)=ncp_proj  !3 i_tran_p(1:ncp_proj)    lin_moment_energy_transm.   
   incp(4)=ncp_proj  !4 i_tran_plog(1:ncp_proj) log_moment_energy_transm.   
   incp(5)=ncp       !5 i_back_rlog(1:ncp)      log_moment_energy_sputt.(r) 
   incp(6)=ncp       !6 i_tran_rlog(1:ncp)      log_moment_energy_transm.sp.
   incp(7)=ncp_proj  !7 i_stop_p(1:ncp_proj)    lin_moment_depth_impl._proj.
   incp(8)=ncp_proj  !8 i_stop_p(1:ncp_proj)    lin_moment_r_impl._proj.(p) 
   
   
   
   allocate (ipart   (  ncp))
   allocate (mom_part(8,ncp))
   
   if (debugmom==1)then
    allocate (vvv(8,ncp,vvj))  
   endif   
     
   !---Number moments input  
   nr_momente=nh/idout

   !nr_momente=1
   do mh=1,nr_momente
     read(io31,*) flu
     if (debugmom==1) then
      !---only check moments and distributions
      !---distribution example 
      do im=1,8 
       if( im==2 .or. im==4 )cycle
       do np=1,incp(im)
         read(io31,*)ipart(np),vvv(im,np,1:vvj)   !read given distribution
         if (idrel==0 .or. mh == nr_momente) then
           !---write given distribution
           write(io20,*)'---------- FLU:',mh,' MOM:',im,' NP:',np
           write(io20,*)'----------------------------------------'
           write(io20,*) im,np,vvj,' nr-moment nr-element nr-sampling-points'
           write(io20,*) 'GIVEN DISTRIBUTION:',bez(im),symbol(np),', fluence: ',flu
           xaa=0.0d0
           select case (im)
           case (1,2,3,4,5,6)
             !write(io06,11) 'energy'  ,'counts_old/de'
             write(io20,11) 'energy'  ,'counts_old/de'
             xee=e0_beam(1)
           case (7)
             !write(io06,11) 'x(depth)','counts_old/dx'
             write(io20,11) 'x(depth)','counts_old/dx'
             xee=500 !like projectile
           case (8)
             !write(io06,11) 'radius'  ,'counts_old/dr'
             write(io20,11) 'radius'  ,'counts_old/dr'
             xee=1000 !like projectile
           end select  
           dx_lin=(xee-xaa)/vvj
           do k=1,vvj 
             vvx= 0.5*(xaa+(k-1)*((xee-xaa)/(vvj)) + xaa+(k)*((xee-xaa)/(vvj)))
             !---output t = counts(i) / dx / nh                 !sum(counts)
             !write(io06,10) vvx, vvv(im,np,k)/dx/ipart(np)
             if( abs(ipart(np)) < eps )ipart(np)=1
             write(io20,10) vvx, vvv(im,np,k)/dx_lin/nh_nr       !/ipart(np)
           enddo
           write(io06,*) 'sum',sum(vvv(im,np,:)/((xee-xaa)/(vvj)))
         endif
       enddo
      enddo
     endif !(debugmom==1)    
    
     do im=1,8 
       !---input moments
       read(io31,*) ipart(1:incp(im)),(mom_part(i,1:incp(im)),i=1,8)
       if (idrel==0 .or. mh == nr_momente) then
       do np=1,incp(im)
         write(io06,*)'--------------------------------------------------------'
         write(io06,*)'--- Fluence-step:',mh,' MOMENT:',im,' element:',np
         write(io20,*)'----------------------------------------'
         write(io20,*)bez(im)
         write(io20,*)'  ',symbol(np),'  (symbol)'
         write(io20,'(f10.5,a11)')flu,'  (fluence)'
         if (abs(ipart(np)) > 1.0e-10) then
           
           write(io06,*)bez(im)
           write(io06,*)symbol(np),'  (symbol)'
           write(io06,'(f10.5,a11)')flu,'  (fluence)'
           write(io06,*)'  ipart:',ipart(np)
           write(io06,*)'  mom 1  :',mom_part(1,np)!/ipart(np)
           write(io06,*)'  mom 2  :',mom_part(2,np)!/ipart(np)
           write(io06,*)'  mom 3  :',mom_part(3,np)!/ipart(np)
           write(io06,*)'  mom 4  :',mom_part(4,np)!/ipart(np)
           write(io06,*)'  mom 5  :',mom_part(5,np)!/ipart(np)
           write(io06,*)'  mom 6  :',mom_part(6,np)!/ipart(np)
           write(io06,*)'  min    :',mom_part(7,np)
           write(io06,*)'  max    :',mom_part(8,np)
         
           xmina=mom_part(7,np)
           xmaxa=mom_part(8,np)
           x1s=mom_part(1,np)/ipart(np)
           x2s=mom_part(2,np)/ipart(np)
           x3s=mom_part(3,np)/ipart(np)
           x4s=mom_part(4,np)/ipart(np)
           x5s=mom_part(5,np)/ipart(np)
           x6s=mom_part(6,np)/ipart(np)
           xdif=xmaxa-xmina
           
           xminal=log10(mom_part(7,np))
           xmaxal=log10(mom_part(8,np))
           xdifl=xmaxal-xminal
           
           if(lin(im)==1) write(io06,*)'  Rechnung  lin  '
           if(lin(im)==0) write(io06,*)'  Rechnung  log  '
           
           !---distribution with help of linear moments
           if(lin(im)==1)call moments(xdif,x1s,x2s,x3s,x4s,x5s,x6s,xmina,xmaxa)  
           
           !---distribution with help of logarithm moments
           if (lin(im)==0)then
             if( abs(xmina) < eps .or. xmaxa < eps) goto 1000
             if( abs(xmaxa-xmina) < eps ) goto 1000
             !call moments(xdif,x1s,x2s,x3s,x4s,x5s,x6s,log10(xmina),log10(xmaxa))  
             call moments(xdifl,x1s,x2s,x3s,x4s,x5s,x6s,xminal,xmaxal)  
           endif
           
           
           !---write calculation distribution
           !---vvi  ... number of depth-intervall between Xa and Xe  (-->dx)
           write(io20,*) im,np,vvi,' nr-moment nr-element nr-sampling-points'
           select case (im)
           case (1,2,3,4,5,6)
             write(io06,11) 'energy','udt','counts_neu/de'
             write(io20,11) 'energy','udt','counts_neu/de'
           case (7)
             write(io06,11) 'x(depth)','udt','counts_neu/dx'
             write(io20,11) 'x(depth)','udt','counts_neu/dx'
           case (8)
             write(io06,11) 'radius','udt','counts_neu/dr'
             write(io20,11) 'radius','udt','counts_neu/dr'
           end select  
           
           !---range of distribution
           if ( im <=6 ) then
             xaa=0.0d0
             xee=e0_beam(1)
           else
             xaa=0
             xee=xmaxa
           endif
           
           if (lin(im)==1) then
             !---recalculation from linear moments
             do k=1,vvi 
               !---vv1 ...number center_Stuetzstellen
               vvx= 0.5*( (xaa+(k-1)*(xee-xaa)/vvi) + (xaa+k*(xee-xaa)/vvi) )
               if ( xmina <= vvx .and. vvx <=xmaxa ) then
                !---distribution with help of linear moments
                udt=fctexp3(vvx) 
               else
                udt=0.0d0
               endif
               !---counts               =   t     * sum(counts) * dx
               !---counts/dx/sum(counts)=   t     
               !---counts/dx/sum(counts)=udt/xdif   !--udt normalize with xdiff_calc
               if(k==1 .or. k==2 .or. k==vvi) write(io06,10) vvx, udt ,udt/xdif*ipart(np)/nh_nr
               if(k==3 ) write(io06,11) ':',':',':'
               write(io20,10) vvx, udt ,udt/xdif*ipart(np)/nh_nr
             enddo !--k
           else
             !---recalculation from logarithmic moments
             !---linear x-achs  for log-moments
             !write(io06,11) 'energy','udt','counts_neu/dr','vvx_log','dx_log','dx_lin'
             xaa=log10(1.0d0)
             xee=log10(e0_beam(1))
             do k=1,vvi 
               !---vv1 ...number center_Stuetzstellen
               vvx= 0.5*( (xaa+(k-1)*(xee-xaa)/vvi) + (xaa+k*(xee-xaa)/vvi) )
               if ( xminal <= vvx .and. vvx <=xmaxal ) then
                 udt=fctexp3(vvx) 
               else
                udt=0.0d0
               endif
               !---counts                    =   t       * sum(counts) * dx_log
               !---counts/dx_lin/sum(counts) =   t       * dx_log / dx_lin
               !---counts/dx_lin/sum(counts) = udt/xdif  * dx_log / dx_lin  !--udt normalize with xdiff_calc
               dx_log=(xee-xaa)/vvi
               dx_lin=10**(xaa+k*(xee-xaa)/vvi) - 10**(xaa+(k-1)*(xee-xaa)/vvi)
               if(k==1 .or. k==2 .or. k==vvi)write(io06,10) 10**(vvx), udt ,udt/xdifl*dx_log/dx_lin*ipart(np)/nh_nr !,vvx, dx_log,dx_lin
               if(k==3 ) write(io06,11) ':',':',':'
               write(io20,10) 10**(vvx), udt ,udt/xdifl*dx_log/dx_lin*ipart(np)/nh_nr !,vvx,dx_log,dx_lin
             enddo !--k
            
             !!---logarithmic x-achs log-moments
             !xaa=1.0d0
             !xee=e0_beam(1)
             !do k=1,vvi 
             !  !---vv1 ...number center_Stuetzstellen
             !  vvx= 0.5*( (xaa+(k-1)*(xee-xaa)/vvi) + (xaa+k*(xee-xaa)/vvi) )
             ! if ( xminal <= log10(vvx) .and. log10(vvx) <=xmaxal ) then
             !    udt=fctexp3(log10(vvx)) 
             !  else
             !   udt=0.0d0
             !  endif
             !  !---counts                    =    t     * sum(counts) * dx_log
             !  !---counts/dx_lin/sum(counts) =    t     * dx_log / dx_lin
             !  !---counts/dx_lin/sum(counts) = udt/xdif * dx_log / dx_lin  !--udt normalize with xdiff_calc
             !    dx_log=log10(xaa+k*(xee-xaa)/vvi) - log10(xaa+(k-1)*(xee-xaa)/vvi)
             !    dx_lin=(xee-xaa)/vvi
             !  write(io06,10) vvx, udt ,udt/xdifl*dx_log/dx_lin !,log10(vvx),dx_log,dx_lin
             !  write(io20,10) vvx, udt ,udt/xdifl*dx_log/dx_lin !,log10(vvx),dx_log,dx_lin
             !enddo !--k
           endif
         else
           1000 continue
           !---write calculation distribution
           write(io06,11) '   no calculation, moments=0'
           write(io20,*) im,np,0
           write(io20,11) '  no distribution all momens zero'
         endif
       enddo !---np        
       endif
     enddo !---im        
   enddo !---mh  
   close(io20)
   close(io31)  
10    format(50f20.6)
11    format(50a20)
12    format(50e16.6)
13    format(50i10)
   
END

!--------------------------------------------------------------
subroutine moments(x0s,x1s,x2s,x3s,x4s,x5s,x6s,x0h,x1h)
  use work_moment
  implicit none

  integer, parameter :: ndst = 1000
  integer, parameter :: nd   = 10
  real(iwp)  :: x0s,x1s,x2s,x3s,x4s,x5s,x6s,x0h,x1h
  real(iwp)  :: ww(ndst),xx(ndst),r(2*nd)
  real(iwp)  :: eta(2*nd)
  real(iwp)  :: a(nd,nd),b(nd,nd),v(nd),epscon,epsmax,diffold,diff
  integer    :: indx(nd),nst0,nloopmax,ndeg0,neq,j,isw,i
  character*70 filename
  real(iwp)  :: x0,x1
  logical    :: loutput
  
  loutput=.false.
  
  ndeg = 6
  ndeg = ndeg + 1

  !--skalierung
  !xscale    =  x1s
  xscale    =  sqrt(x2s)
  yscale    =  x0s
  x0=x0h/xscale
  x1=x1h/xscale

  eta(1)   =  1.d0
  eta(2)   =  x1s/xscale
  eta(3)   =  x2s/xscale**2
  eta(4)   =  x3s/xscale**3
  eta(5)   =  x4s/xscale**4
  eta(6)   =  x5s/xscale**5
  eta(7)   =  x6s/xscale**6
  !... # of integration points in simpson
  !         (the actual number is 2*nst0+1)
  nst0    = 100

  !... max. # of iterations
  nloopmax  =1000

  !... initial values
  !... mode = 0: degree gradually increase
  !... else xl(i) read and max. moment used

  !if(loutput) write(10,'(1x,"ndeg  :",i5)')ndeg
  ! write(io06,'(1x,"eta(i) :")')
  ! do j = 1, ndeg
  !   write(io06,'(1x,i5,f16.8)')j,eta(j)
  ! enddo

   epscon    = 1.0d-8
   epsmax    = 1.0d+20

   ndeg0 = ndeg
   neq   = 1
   do j = 1, ndeg0
     xl(j) = 0.0d0
   enddo

   diffold = 1.0d30
   isw  = 1

!111  write(io06,'(1x,"maximum moment ",i5)')ndeg

      do j = 1, ndeg0
        b(j,1) = 0.0d0
      enddo
      do j = ndeg+1, ndeg0
        xl(j) = 0.0d0
      enddo

      do i = 1, nloopmax
        if(loutput) write(10,'(1x,"xl :")')
        do j = 1, ndeg
          xl(j)  = xl(j) + .1d0*b(j,1) !---faktor 0.1*b()
          if(loutput) write(10,'(1x,i5,g16.8)')j,xl(j)
        enddo
        call defmat(a,ww,xx,ndst,nst0,x0,x1,isw,r,xl,ndeg,nd)
        isw = 0
        do j = 1, ndeg
          b(j,1) = (r(j)-eta(j))
        enddo
        diff = 0.0d0
        do j = 1, ndeg
          diff = diff + abs(b(j,1))
        enddo
        if(loutput) write(10,'(1x,"diff = ",f20.10)')diff
        !write(io06,'(1x,"diff = ",f20.10)')diff
        
        if (diff < epscon) goto 999
        if (diff > epsmax) goto 888
        diffold = diff
        
        !---output in file
        if(loutput) then
          write(10,'(1x,"phi :")')
          do j = 1, ndeg
            write(10,'(1x,i5,3f18.10)')j,b(j,1),r(j),eta(j)
          enddo
        endif
        
        call gauss(a,v,indx,b,nd,ndeg,neq)
      enddo

!     if (ndeg == ndeg0) goto 999
!     ndeg = ndeg+1
!     goto 111

888   print*,'not converged'
      xl(:)=0.50+20
      pause
999   continue

      !write((io06,'(1x,"xl(i) :")')
      !do j = 1, ndeg
      !  write(io06,'(1x,i5,f16.8)')j,xl(j)
      !enddo
end
!*=====================================================================
subroutine defmat(a,ww,xx,ndst,nst0,x0,x1,isw,r,xl,ndeg,nd)
!
!     simpson integration
!     the actual number of integration points is   2*nst0 + 1
!
  use work_moment, only: iwp !,xscale,yscale,ndeg,xl
  implicit none
  real(iwp)  :: xx(*),factor,xl(10),r(2*nd),a(nd,nd),ww(*),x0,x1
  integer :: i,nst,nst0,isw,ndst,ndeg,nd,is,j,j1,j2    
  real(iwp)  :: dx,x,ee,fctexp2

      nst = 2*nst0+1
      if (isw == 1) then
        if (nst > ndst) then
           print*,'ndst should be .ge.',nst
           stop 1111
        endif
        factor = (x1-x0)/(6*nst0)
        is = 1
        do i = 2, nst-1
          ww(i) = (3 + is)*factor
          is    = -is
        enddo
        ww(1)   = factor
        ww(nst) = factor
        dx     = (x1-x0)/(nst-1)
        do i = 1, nst
          xx(i) = x0 + dx*(i-1)
        enddo
      endif

      do j = 1, 2*ndeg
        r(j) = 0.0d0
      enddo
      do i = 1, nst
      x   = xx(i)
      !---ee .. wahrscheinlichkeit von x
      ee  = fctexp2(x) !log

      !---bildung der momente r(1..12) r=f(x)
      do j = 1, 2*ndeg
        r(j) = r(j)  + ww(i)*ee
        ee   = ee*x
      enddo
      
      enddo
      do j1 = 1, ndeg
        do j2 = j1, ndeg
          a(j1,j2) = r(j1+j2-1)
          !... original indices are j0 +1 = j
          !... hence: (j01+j02) + 1-> j1+j2-2 + 1 = j1+j2-1
          a(j2,j1) = a(j1,j2)
        enddo
      enddo

end

!============================
function fctexp2(x)
  !---only call from defmat
  use work_moment, only: iwp,ndeg,xl
  implicit none
  real(iwp)  :: arg,factor,fctexp2,e0,x
  integer :: i    
  arg    = 0.0d0
  factor = 1.0d0
  do i = 1, ndeg
    arg    = arg + xl(i) * factor
    factor = factor*x
  enddo
  arg = -arg
  if (abs(arg) < 82.0) then
      e0 = exp(arg)
  else if (arg < 0.0) then
      e0 = 1.d0-20
  else if (arg > 0.0) then
      e0 = 1.d0+20
  endif
  fctexp2 = e0
end
!============================
function fctexpl(xx)
  use work_moment, only: iwp,xscale,yscale,ndeg,xl
  implicit none
  real(iwp)  :: fctexpl,xx,arg,factor,e0,x
  integer :: i    
  arg    = 0.0d0
  factor = 1.0d0
  x      = log10(xx)/xscale
  do i = 1, ndeg
    arg    = arg + xl(i) * factor
    factor = factor*x
  enddo
  arg = -arg
  if (abs(arg) < 82.0) then
      e0 = exp(arg)
  else if (arg < 0.0) then
      e0 = 1.d0-20
  else if (arg > 0.0) then
      e0 = 1.d0+20
  endif
  
  e0 = exp(arg)
  
  fctexpl = yscale*e0/xscale/xx/alog(10.)
  fctexpl = max(fctexpl,1.d-8)
end
!============================
function fctexp3(xx)
  use work_moment, only: iwp,xscale,yscale,ndeg,xl
  implicit none
  real(iwp)  :: xx,arg,factor,e0,fctexp3,x
  integer :: i    
  arg    = 0.0d0
  factor = 1.0d0
  x      = xx/xscale
  do i = 1, ndeg
    arg    = arg + xl(i) * factor
    factor = factor*x
  enddo
  arg = -arg
  !if (abs(arg) < 82.0) then
  !  e0 = exp(arg)
  !else if (arg < 0.0) then
  !    e0 = 1.0d-20
  !else if (arg > 0.0) then
  !    e0 = 1.d0+20
  !endif
  
 ! if (arg <= 0.0) then 
 !   e0=0.0 
 ! else 
    e0=exp(arg)
 ! endif

  fctexp3 = yscale*e0/xscale
end

!=====================================
subroutine gauss(a,v,indx,b,nd,n,neq)
use work_moment ,only: iwp
  implicit none
  ! ---compute invers of a in b 
  ! ---destroying a
  real(iwp)  :: a(nd,nd),b(nd,nd),v(nd),disi0
  integer:: indx(nd),n,neq,nd,j
  call dludcp(a,v,n,nd,indx,disi0)
  do j = 1, neq
    call dluksb(a,n,nd,indx,b(1,j))
  enddo
end subroutine gauss

!*======================================
subroutine dludcp(a,vv,n,np,indx,d)
use work_moment ,only: iwp,io06
  implicit none
  real(iwp),parameter  :: tiny=1.0d-20
  real(iwp)  :: a(np,np),vv(np),d
  integer    :: indx(n),n,np
  real(iwp)  :: summe,aamax,dum
  integer    :: i,j,k,imax
  d=1.d0
  do i=1,n
    aamax=0.0d0
    do j=1,n
      if (abs(a(i,j)).gt.aamax) aamax=abs(a(i,j))
    enddo
    if (aamax == 0.0d0) then
      write(io06,*)'singular matrix.'
      stop
    endif
    vv(i)=1./aamax
  enddo
  do j=1,n
    if (j > 1) then
      do i=1,j-1
        summe=a(i,j)
        if (i > 1)then
          do k=1,i-1
            summe=summe-a(i,k)*a(k,j)
          enddo
          a(i,j)=summe
        endif
      enddo
    endif
    aamax=0.0d0
    do i=j,n
      summe=a(i,j)
      if (j > 1)then
        do k=1,j-1
          summe=summe-a(i,k)*a(k,j)
        enddo
        a(i,j)=summe
      endif
      dum=vv(i)*abs(summe)
      if (dum.ge.aamax) then
        imax=i
        aamax=dum
      endif
    enddo
    if (j.ne.imax) then
      do k=1,n
        dum=a(imax,k)
        a(imax,k)=a(j,k)
        a(j,k)=dum
      enddo
      d=-d
      vv(imax)=vv(j)
    endif
    indx(j)=imax
    if (j.ne.n)then
      if(a(j,j)==0.0d0)a(j,j)=tiny
      dum=1./a(j,j)
      do i=j+1,n
        a(i,j)=a(i,j)*dum
      enddo
    endif
  enddo
  if(a(n,n) == 0.0d0)a(n,n)=tiny
end subroutine dludcp

!*======================================
subroutine dluksb(a,n,np,indx,b)
use work_moment ,only: iwp
  implicit none
  real(iwp)  :: a(np,np),b(n)
  integer    :: indx(n)
  integer    :: ii,ll,i,j,n,np
  real(iwp)  :: summe
  ii=0
  do i=1,n
    ll=indx(i)
    summe=b(ll)
    b(ll)=b(i)
    if (ii .ne. 0) then
      do j=ii,i-1
        summe=summe-a(i,j)*b(j)
      enddo
    else if (summe .ne. 0.0d0) then
      ii=i
    endif
    b(i)=summe
  enddo
  do i=n,1,-1
     summe=b(i)
     if (i < n)then
       do j=i+1,n
         summe=summe-a(i,j)*b(j)
       enddo
     endif
     b(i)=summe/a(i,i)
   enddo
end subroutine dluksb

