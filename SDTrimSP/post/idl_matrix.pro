;/afs/ipp-garching.mpg.de/home/a/aam/TRIDYN/SDTrimSP/post/idl_matrix.pro
@/afs/ipp/home/a/aam/public/archiv/TRIDYN/SDTrimSP/post/window.inc 
@/afs/ipp/home/a/aam/public/archiv/TRIDYN/SDTrimSP/post/level.inc 
@/afs/ipp/home/a/aam/public/archiv/TRIDYN/SDTrimSP/post/achsen.inc 
pro run
;input :meagb.dat
;       meagbt.dat
;       meags.dat
;       meagst.dat
;output:meagb.dat.ps
;       meagbt.dat.ps
;       meags.dat.ps
;       meagst.dat.ps
;       meagb.dat2.ps (color contour more as 1 component)
;       meagbt.dat2.ps
;       meags.dat2.ps
;       meagst.dat2.ps

;  goto,Worter
;  Bild1
;  Bild2
;  Bild3

  _start
  druck=0
  print,'druck:0/1    0...screen 1...postscript'
  read,druck
  form=0
  text='' & text1='' & bez='' & text2=''& textdatum=''
  ke=1 & na=1 & np=1 & iout_pol=1 & nr=long(1) & nh=long(1)
  lmatout_cos_angle=0 
  idep=1
  ncp=1
  a=1.0d0 & b=1.0d0
  loadct,39
  if druck eq 0 then farbe=[255,250,150,80,100,200,40,215,180,255,250,150] else $
                     farbe=[0  ,250,150,80,100,200,40,215,180,0  ,250,150]

  for anz=1,4 do begin  ;4
   if(anz eq 1) then name='meagb_p.dat' 
   if(anz eq 2) then name='meagb_s.dat' 
   if(anz eq 3) then name='meagt_p.dat' 
   if(anz eq 4) then name='meagt_s.dat' 

   
   close,1
   print,'  '
   print,'-----------INPUT:',name
   openr,1,name
   readf,1,textdatum
   readf,1,bez  & bez=strtrim(bez,1)
   print,bez
   readf,1,text
   readf,1,ncp,nh,nr,lmatout_cos_angle
   print,ncp,nh,nr,lmatout_cos_angle   
   iout_pol=0   ;---lmatout_cos_angle ==false
   if lmatout_cos_angle then begin
     print,'polar angle -->cosinus
     print,'---graphic only for polar angle in degree---'
     stop
   end
   titlecase=strarr(ncp) 
   nnnsum=0
   
   for logg=0,0 do begin
     ma=dblarr(ncp)
     for nnn=0,ncp-1 do begin
       readf,1,text
       titlecase(nnn)=text
       readf,1,text
       readf,1,ke,na,np ;,iout_pol
       print,'number energy values :',ke
       print,'number azimuth values:',na
       print,'number polar values  :',np
       if nnn eq 0 then matrix=dblarr(ncp,ke,na,np)
       if nnn eq 0 then ener  =dblarr(ke)
       if nnn eq 0 then azim  =dblarr(na)
       if nnn eq 0 then polar =dblarr(np)
       if nnn eq 0 then mat_energ=dblarr(ncp,na,np)
       texte='' & texta='' & textp='' &
       readf,1,texte
       readf,1,ener
       readf,1,texta
       readf,1,azim
       readf,1,textp
       readf,1,polar
       hilf =dblarr(np)
       hilfr=dblarr(np)
       test1=0
       for k=0,ke-1 do begin   ; energy
         readf,1,text          ; title angle
         for  i=0,na-1 do begin
           readf,1,hilf
           matrix(nnn,k,i,*)=hilf(*)
           ma(nnn)=ma(nnn)+total(hilf)
         end
       end
       readf,1,text
       for  i=0,na-1 do begin
         readf,1,hilfr
         mat_energ(nnn,i,*)=hilfr(*)
       end
       nnnsum=nnnsum+ma(nnn)
       print,titlecase(nnn),' sum:',ma(nnn),'sum of matrix /nr:',fix(.5+ma(nnn)/nr)
     end ;nnn
   end ;logg
   close,1
   
   summa=ma
   if nnnsum eq 0 then print,'--------no draw------------'
   if nnnsum eq 0 then goto,next_bitte
   ;-----------------neues Blatt-------------------------
   drname=name+'.ps'
   _fenster,druck,drname,form,nummer=anz   
   spa=1 & zei=3
   !P.Multi=[0,spa,zei] 
   if form eq 0 then pos=[0.02, 0.05 , .97, .96]
   if form eq 1 then pos=[0.01, 0.1, .98, .97]
   ric=0
   dspa=25 ;% Zwischenraum
   dzei=25 ;% Zwischenraum
   dx=(pos(2)-pos(0))/spa
   dy=(pos(3)-pos(1))/zei
   isp=0
   jze=0
   if ric eq 0 then jze=1 else isp=1
   druckfak=1
   if druck eq 1 then druckfak=.8
   dd=1.5*druckfak
   gr=2.5*druckfak
   gd=1.5*druckfak
   
   
   ;-------------------BILD1---azimuthal energy-----------------------------
   print,'---azimuthal energy distribution ------------'

   ;winkel,energy  wegen zeichnung 2*na
   ma=dblarr(ncp,na*2) ;number of Particle
   wi=dblarr(2*na)
   en=dblarr(ncp,2*na)
   en2=dblarr(ncp,2*na)

   for nnn=0,ncp-1 do begin  
     ii=0
     for i=0,na-1 do begin
      ;sum anzahl ueber energy and polarwinkel 
      for k=0,ke-1 do begin     ;energy
         for j=0,np-1 do begin  ;polarwinkel 
           ma(nnn,ii)  =ma(nnn,ii)  +matrix(nnn,k,i,j) 
           ma(nnn,ii+1)=ma(nnn,ii+1)+matrix(nnn,k,i,j) 
          end
      end
      ii=ii+2
     end 
   end

   for nnn=0,ncp-1 do begin  
     ii=0
     wi(ii)=0.0
     for i=0,na-1 do begin
                        wi(ii+1)=azim(i)
      if i ne na-1 then wi(ii+2)=azim(i)
      ;sum energy ueber polarwinkel 
      for j=0,np-1 do begin
        ;---matrix_energ / sum(matrix_number) 
        en(nnn,ii)  =en(nnn,ii)  +mat_energ(nnn,i,j) 
        en(nnn,ii+1)=en(nnn,ii+1)+mat_energ(nnn,i,j) 
        ;---only sum(matrix_number(k,i,j)*ener(k)) / sum(matrix_number)
        for k=0,ke-1 do begin
          en2(nnn,ii)  = en2(nnn,ii  )+matrix(nnn,k,i,j)*ener(k)
          en2(nnn,ii+1)= en2(nnn,ii+1)+matrix(nnn,k,i,j)*ener(k)
        end
      end
      ii=ii+2
     end 
   end
  
  i=where(ma lt 1)
  if i(0) ge 0 then ma(where(ma lt 1))=1.e+20
  en=en/ma ;average sum E(*) /number(*)
  en2=en2/ma ;average sum E(*) /number(*)
  
  enmax=max([max(en),.001])
  for nnn=0,ncp-1 do begin      
     k=strpos(titlecase(nnn),':')+2
     l=max([strlen(titlecase(nnn)),4])
     text=strmid(titlecase(nnn),k,l)
     print,'1.max(energy) ',text,':',max(en(nnn,*))
  end
  xa=-enmax & xe=enmax & ya=0 & ye=enmax
  lmin=3 & lmax=5
  ueber='AZIMUTHAL ENERGY DISTRIBUTION'
  xtt=texte+' per particle' 
  ytt=' '
  unter=' '
  xrichtung=0 & yrichtung=0 & sca=1 
   
  _posit,ric,isp,jze,spa,zei,dspa,dzei,dx,dy,pos,pos1
  _bild,xa,xe,ya,ye,lmin,lmax,pos1,xtt,ytt,ueber,unter,gr,di,form $
         ,xrichtung,yrichtung,sca $
        ; ,neg_achs_pos=1 $ 
         ;,xachsread=xachsread,yachsread=yachsread $
         ;,format_x=format_x
         ,format_y='(f0.0)'
  
   
   for nnn=0,ncp-1 do begin
       oplot,en(nnn,*),wi*!dpi/180.d0,/polar,color=farbe(nnn)
       ;---the same
       ;oplot,en2(nnn,*),wi*!dpi/180.d0,/polar,color=250
   end
   oplot,[0,1.e6],[0, 30*!dpi/180.d0],/polar,linestyle=1
   oplot,[0,1.e6],[0, 60*!dpi/180.d0],/polar,linestyle=1
   oplot,[0,1.e6],[0, 90*!dpi/180.d0],/polar,linestyle=1
   oplot,[0,1.e6],[0,120*!dpi/180.d0],/polar,linestyle=1
   oplot,[0,1.e6],[0,150*!dpi/180.d0],/polar,linestyle=1
   hr=fltarr(181)
   hw=fltarr(181)   
   
   _achslevels,xa,xe,lmin,lmax,ndx,xdx,strdx ;same as bild
   i=where(xdx  gt 0)
   for k=i(0),ndx-1 do begin
    for j=0,180 do begin
     hr(j)=xdx(k)
     hw(j)=j*!dpi/180.
    end
    oplot,hr,hw,/polar,linestyle=1
   end
   rhilf=enmax/2.
   if ndx-1-i(0) gt 1 then rhilf=xdx(i(0)+(ndx-1-i(0))/2)

   for j=0,180 do begin
     hr(j)=rhilf
     hw(j)=j*!dpi/180.
   end
   oplot,hr,hw,/polar,linestyle=1
   for j=30,150,30 do begin
     _int2string,j,j,s
     xyouts,hr(j)*1.05*cos(hw(j)),hr(j)*1.05*sin(hw(j)),s+'!9%!6',/data
   end
   j=1
   for i=0,ncp-1 do begin
     k=strpos(titlecase(i),':')+2
     l=max([strlen(titlecase(i)),4])
     text=strmid(titlecase(i),k,l)
     if summa(i) gt 0 then begin
       xyouts,pos1(0)-.1,pos1(3)-(pos1(3)-pos1(1))/6*(j) $
        ,text,charsize=gr/2,charthick=gd,/normal,color=farbe(i)  
       j=j+1
     end
   end
   ;-------------------BILD2--polar energy------------------------------
   print,'---polar energy distribution ------------'

   ma=dblarr(ncp,2*np)
   en=dblarr(ncp,2*np)
   wi=dblarr(2*np)
   for nnn=0,ncp-1 do begin  
     jj=0
     for  j=0,np-1 do begin                        
       ;sum anzahl ueber energy, azimutalwinkel 
       for k=0,ke-1 do begin
         for  i=0,na-1 do begin
           ma(nnn,jj)  =ma(nnn,jj)  +matrix(nnn,k,i,j)
           ma(nnn,jj+1)=ma(nnn,jj+1)+matrix(nnn,k,i,j)
         end
       end
       jj=jj+2
     end 
   end
   for nnn=0,ncp-1 do begin  
     jj=0
     if iout_pol eq 0 then wi(0)=0 else wi(0)=90
     for  j=0,np-1 do begin                        
       if iout_pol eq 0 then begin
                           wi(jj+1)=polar(j)
         if j ne np-1 then wi(jj+2)=polar(j)
       end else begin
                           wi(jj+1)=acos(polar(j))*180./!dpi
         if j ne np-1 then wi(jj+2)=acos(polar(j))*180./!dpi
       end
       ;sum energy ueber azimutalwinkel 
       for  i=0,na-1 do begin
         en(nnn,jj)  =en(nnn,jj)  +mat_energ(nnn,i,j)
         en(nnn,jj+1)=en(nnn,jj+1)+mat_energ(nnn,i,j)
       end
       jj=jj+2
     end 
   end
  
  i=where(ma lt 1)
  if i(0) ge 0 then ma(where(ma lt 1))=1.e20
  en=en/ma ;average sum E /number

;print,en(where(ma lt 1.e20))
;print,ma(where(ma lt 1.e20))

  enmax=max([max(en),.000001])
  for nnn=0,ncp-1 do begin  
     k=strpos(titlecase(nnn),':')+2
     l=max([strlen(titlecase(nnn)),4])
     text=strmid(titlecase(nnn),k,l)
     print,'2.max(energy) ',text,':',max(en(nnn,*))
  end
  xa=0.0 & xe=enmax & ya=0 & ye=enmax
  lmin=3 & lmax=5
  ueber='POLAR ENERGY DISTRIBUTION'
  if iout_pol eq 1 then ueber='POLAR ENERGY DISTRIBUTION (COS)'
  xtt=texte+' per particle'
  ytt=' '
  unter=' '
  xrichtung=0 & yrichtung=0 & sca=1 

  _posit,ric,isp,jze,spa,zei,dspa,dzei,dx,dy,pos,pos1
   pos1(2)=pos1(2)-.2
  _bild,xa,xe,ya,ye,lmin,lmax,pos1,xtt,ytt,ueber,unter,gr,di,form $
         ,xrichtung,yrichtung,sca $ 
         ;,xachsread=xachsread,yachsread=yachsread $
         ;,format_x=format_x
         ,format_y='(f0.0)'

   for nnn=0,ncp-1 do begin  
     if iout_pol eq 0 then $
       oplot,en(nnn,*),(90-wi)*!dpi/180.d0,/polar,color=farbe(nnn) $
     else $  
       oplot,en(nnn,*),(90-wi)*!dpi/180.d0,/polar,color=farbe(nnn)
   end
   oplot,[0,1.e6],[0, 30*!dpi/180.d0],/polar,linestyle=1
   oplot,[0,1.e6],[0, 60*!dpi/180.d0],/polar,linestyle=1
   hr=fltarr(91)
   hw=fltarr(91)   
   _achslevels,xa,xe,lmin,lmax,ndx,xdx,strdx ;same as bild
   i=where(xdx  gt 0)
   for k=i(0),ndx-1 do begin
    for j=0,90 do begin
     hr(j)=xdx(k)
     hw(j)=j*!dpi/180.
    end
    oplot,hr,hw,/polar,linestyle=1
   end
   rhilf=enmax/2.
   if ndx-1-i(0) gt 1 then rhilf=xdx(i(0)+(ndx-1-i(0))/2)
   for j=0,90 do begin
     hr(j)=rhilf
     hw(j)=j*!dpi/180.
   end
   oplot,hr,hw,/polar,linestyle=1
   for j=30,60,30 do begin
     _int2string,90-j,90-j,s
     xyouts,hr(j)*1.05*cos(hw(j)),hr(j)*1.05*sin(hw(j)),s+'!9%!6',/data
   end
   j=1
   for i=0,ncp-1 do begin
     k=strpos(titlecase(i),':')+2
     l=max([strlen(titlecase(i)),4])
     text=strmid(titlecase(i),k,l)
     if summa(i) gt 0 then begin
       xyouts,pos1(0)-.1,pos1(3)-(pos1(3)-pos1(1))/6*(j) $
       ,text,charsize=gr/2,charthick=gd,/normal,color=farbe(i)  
       j=j+1
     end
   end
   ;-------------------BILD3-polat azimut-------------------------------
   print,'---polar-azimut energy distribution ------------'
   
   ma=dblarr(ncp,na,np)
   
   ;da=dblarr(na)
   ;pa=dblarr(np)
   ;da(0)=azim(0)
   ;for  i=1,na-1 do begin
   ;  da(i)=azim(i)-azim(i-1)
   ;end
   
   ;pa(0)=polar(0)
   ;for  i=1,np-1 do begin
   ;  pa(i)=polar(i)-polar(i-1)
   ;end
   ;if iout_pol eq 1 then begin
   ;  pa(0)=!dpi/2.0-acos(polar(0))
   ;  for  i=1,np-1 do begin
   ;    pa(i)=abs(acos(polar(i))-acos(polar(i-1)))
   ;  end
   ;  pa=pa*180./!dpi
   ;end
   
   for nnn=0,ncp-1 do begin  
     for i=0,na-1 do begin ;azi
       for j=0,np-1 do begin ;polar
         ;sum anzahl uber energy  
         for k=0,ke-1 do begin ;energy
           ma(nnn,i,j)=ma(nnn,i,j)+matrix(nnn,k,i,j)
         end
       end
     end 
   end

  i=where(ma lt 1)
  if i(0) ge 0 then ma(where(ma lt 1))=1.0e201
  en=mat_energ/ma ;average sum(E)/number

  enmax=max([max(en),.001])
  for nnn=0,ncp-1 do begin  
     k=strpos(titlecase(nnn),':')+2
     l=max([strlen(titlecase(nnn)),4])
     text=strmid(titlecase(nnn),k,l)
     print,'3.max(energy) ',text,':',max(en(nnn,*,*))
  end
  xa=0.0 & xe=180 & ya=0 & ye=90
  lmin=3 & lmax=5
  ueber='DISTRIBUTION '+texte+' per particle'
  xtt=texta 
  ytt=textp
  unter=' '
  xrichtung=0 & yrichtung=0 & sca=0 

  _posit,ric,isp,jze,spa,zei,dspa,dzei,dx,dy,pos,pos1
  pos1(2)=pos1(2)-.1
  _bild,xa,xe,ya,ye,lmin,lmax,pos1,xtt,ytt,ueber,unter,gr,di,form $
         ,xrichtung,yrichtung,sca  
         ;,xachsread=xachsread,yachsread=yachsread $
         ;,format_x=format_x
         ;,format_y='(f0.0)'
   
   z=[0,max(en)]
   lmin=7 & lmax=11
   _achsenwerte1,z,zhilf,levanz,lmin,lmax
   levanz=levanz+1
   lev=fltarr(levanz)
   lev(0:levanz-2)=zhilf(0:levanz-2)
   lev(levanz-1)=lev(levanz-2)+(lev(levanz-2)-lev(levanz-3))
   print,levanz,' levels(all):',lev(0),lev(1),lev(2),'...',lev(levanz-2),lev(levanz-1)
   lab=lev
   lab(*)=1
   itest=0
   inn=0
   for nnn=0,ncp-1 do begin
     hilf=polar     
     if iout_pol eq 1 then hilf=acos(polar)*180./!pi
     
     contour,en(nnn,*,*),azim,hilf $
      ,color=farbe(nnn),/overplot,levels=lev,c_labels=lab,min_value=.01
     if summa(nnn) gt 0 then itest=itest+1
     if itest eq 1 then inn=nnn 
   end
   if itest eq 1 then begin
     ;---contour with fill
     col=lev
     for jj=0,levanz-1 do begin
       col(jj)=250*lev(jj)/max(lev)
     end
     contour,en(inn,*,*),azim,hilf $
      ,c_color=col,/overplot,levels=lev,c_labels=lab,/cell_fill,min_value=.01
     contour,en(inn,*,*),azim,hilf $
      ,/overplot,levels=lev,c_labels=lab,min_value=.01
     balkq=0
     xa=pos1(2)+.01
     ya=pos1(1)
     dxb=.01
     dyb=(pos1(3)-pos1(1))/levanz
     balkint=1
     grb=gr/2
     
     legend_balk=0        ;each level == level legende
     anz_leg_balk=levanz  ;no influence if legend_balk=0 
     leg_balk    =lev     ;no influence if legend_balk=0 
     levanf=lev(0)        ;no influence if legend_balk=0 
     levend=lev(levanz-1) ;no influence if legend_balk=0 

     _balken2,lev,balkq,xa,dxb,ya,dyb,col,balkint,grb,di $
     ;,skalfak=skalfak,forma=forma
      ,legend_balk,anz_leg_balk,leg_balk,levanf,levend
   end
   
   j=1
   for i=0,ncp-1 do begin
     k=strpos(titlecase(i),':')+2
     l=max([strlen(titlecase(i)),4])
     text=strmid(titlecase(i),k,l)
     if summa(i) gt 0 then begin
       xyouts,pos1(0)-.15,pos1(3)-(pos1(3)-pos1(1))/6*(j) $
         ,text,charsize=gr/2,charthick=gd,/normal,color=farbe(i)  
       j=j+1
     end  
   end
   k=strpos(titlecase(0),',')
   xyouts,.10,.05,'!6ANALYSIS OF SCATTERS   '+bez,charsize=gr*.7,charthick=gd,/normal  
   xyouts,.10,.02,'!6'+strmid(titlecase(0),0,k),charsize=gr*.6,charthick=gd,/normal  
   xyouts,.65,.00,'!6'+textdatum,charsize=gr*.4,charthick=gd,/normal  
   _druckende,druck,drname,form  
   open_window=0
   if itest gt 1  then begin
    ;---contour with fill ,more as one, less as 3, new window/page
    j=1
    open_window=0
    for inn=0,ncp-1 do begin
     if summa(inn) gt 0 then begin
      if j eq 1 or j eq 4 or j eq 7 then begin
        if j eq 1 then drname=name+'2.ps'
        if j eq 4 then drname=name+'3.ps'
        if j eq 7 then drname=name+'4.ps'
        _fenster,druck,drname,form,nummer=anz*10+j
        open_window=1   
        isp=0
        jze=0
        if ric eq 0 then jze=1 else isp=1
      end
      _posit,ric,isp,jze,spa,zei,dspa,dzei,dx,dy,pos,pos1
      pos1(2)=pos1(2)-.1
      _bild,xa,xe,ya,ye,lmin,lmax,pos1,xtt,ytt,ueber,unter,gr,di,form $
          ,xrichtung,yrichtung,sca  
      z=[0,max(en)]
      lmin=7 & lmax=11
      _achsenwerte1,z,zhilf,levanz,lmin,lmax
      levanz=levanz+1
      lev=fltarr(levanz)
      lev(0:levanz-2)=zhilf(0:levanz-2)
      lev(levanz-1)=lev(levanz-2)+(lev(levanz-2)-lev(levanz-3))
      s=string(inn+1,format='(i3)')
      print,levanz,' levels(',s,'):',lev(0),lev(1),lev(2),'...',lev(levanz-2),lev(levanz-1)
      lab=lev
      lab(*)=1
      ;---contour with fill
      col=lev
      for jj=0,levanz-1 do begin
        col(jj)=250*lev(jj)/max(lev)
      end
      contour,en(inn,*,*),azim,hilf $
       ,c_color=col,/overplot,levels=lev,c_labels=lab,/cell_fill,min_value=.01
      contour,en(inn,*,*),azim,hilf $
       ,/overplot,levels=lev,c_labels=lab,min_value=.01
      balkq=0
      xa=pos1(2)+.01
      ya=pos1(1)
      dxb=.01
      dyb=(pos1(3)-pos1(1))/levanz
      balkint=1
      grb=gr/2
      
      legend_balk=0        ;each level == level legende
      anz_leg_balk=levanz  ;no influence if legend_balk=0 
      leg_balk    =lev     ;no influence if legend_balk=0 
      levanf=lev(0)        ;no influence if legend_balk=0 
      levend=lev(levanz-1) ;no influence if legend_balk=0 
      
      _balken2,lev,balkq,xa,dxb,ya,dyb,col,balkint,grb,di $ 
              ,legend_balk,anz_leg_balk,leg_balk,levanf,levend

      k=strpos(titlecase(inn),':')+2
      l=max([strlen(titlecase(inn)),4])
      text=strmid(titlecase(inn),k,l)
      xyouts,pos1(0)-.15,pos1(3)-(pos1(3)-pos1(1))/6*(j) $
      ,text,charsize=gr/2,charthick=gd,/normal,color=farbe(inn)
      
      if j eq 3 or j eq 6 or j eq 9 then begin
        k=strpos(titlecase(0),',')
        xyouts,.10,.05,'!6ANALYSIS OF SCATTERS   '+bez,charsize=gr*.7,charthick=gd,/normal  
        xyouts,.10,.02,'!6'+strmid(titlecase(0),0,k),charsize=gr*.6,charthick=gd,/normal  
        xyouts,.65,.00,'!6'+textdatum,charsize=gr*.4,charthick=gd,/normal  
        _druckende,druck,drname,form         
        open_window=0   
       end
      j=j+1  
     end
    end
   end   
   if open_window eq 1 then begin
   k=strpos(titlecase(0),',')
   xyouts,.10,.05,'!6ANALYSIS OF SCATTERS   '+bez,charsize=gr*.7,charthick=gd,/normal  
   xyouts,.10,.02,'!6'+strmid(titlecase(0),0,k),charsize=gr*.6,charthick=gd,/normal  
   xyouts,.65,.00,'!6'+textdatum,charsize=gr*.4,charthick=gd,/normal  
   _druckende,druck,drname,form  
  end
   
   next_bitte:

  end;anz
end  
