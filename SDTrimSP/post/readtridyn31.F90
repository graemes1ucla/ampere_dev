   program readE031
  !---program  input E0_31.dat 
  !---program output: 
  !                    (like in version 4.00)                
  !---surface thickness versus fluence
  !   open(21, file='dsurf_f_31.dat')
  !---sputtering yields versus fluence
  !   open(22, file='backsputt_f_31.dat')
  !---particle reflection coefficient  versus fluence
  !   open(23, file='backscatt_f_31.dat')
  !---surface composition versus fluence
  !   open(24, file='comp_surf_f_31.dat')
  !---atomic fraction depend on depth  for 5 fluences (example:2,6,11,21,nout)
  !   open(25, file='comp_5f_d_31.dat')
  !---average of atomic fraction at last 10 fluence step depend on deph (steady state)------- 
  !   open(26, file='comp_steady_state_d_31.dat')
  !---average last 10 fluence step or steady state
  !   open(27, file='all_steady_state_31.dat')
  !---atomic fraction of component one = f(depth,fluence)-------------------------------
  !   open(28, file='comp_one_d_f_31.dat')
  !   open(28, file='comp_two_d_f_31.dat')
  !   open(28, file='comp_three_d_f_31.dat')
  !---density=f(fluence)
  !   open(29, file='density_5layer_f_31.dat')
  !---areal density=f(depth) 5.fluence step
  !   open(30, file='areal_density_5_d_31.dat')
  !---depth distribution of density for several fluences  
  !   open(31, file='density_d_f_31.dat')
  !--- atomic fraction of component one = f(depth,fluence)-------------------------------

  implicit none
  character :: title*40
  character :: titlecase*80
  character :: text*10
  integer :: ncp,nqxm,nqx,nout,i_ncp
  integer :: nh, idout, nqout, i, mm, jp, ifl, j, nn, mi,p_ncp
  integer, dimension(5) :: nh_step
  real :: flc, flstep, retf_ss,sum_incidence
  real, dimension(:), pointer  :: fluc, depd, retf, retp, flibd, flib1, flib0
  real, dimension(:), pointer  :: backscat_ss, backsput_ss, quxi_ss,xxx,dx
  real, dimension(:,:), pointer :: dns, adns,   rangeq ,flib_ss
  real, dimension(:,:), pointer :: csf, backsput, ard, backscat, erfl, refl, sfcn
  real, dimension(:,:), pointer :: nproj,e_tot_p,iback_p,eback_p
  real, dimension(:,:), pointer :: itran_p,etran_p,iback_r,eback_r,itran_r,etran_r,reem,irec
  
  real, dimension(:,:,:), pointer :: quxi, range, flib
  character(len=5), dimension(:), pointer :: symbol,einheit
  
  write(6,*)'---'
  write(6,*)'read: E0_31_target.dat'
  open (31, file='E0_31_target.dat')
    read(31, '(a)') title
    write(6,*)'  title: ',title
    read(31, '(a)') titlecase
    write(6,'(a10,a70)')'  case : ',titlecase
    read(31, '(a)') text
    read(31, '(a)') text     
    read(31,*) nh,nqx,ncp, idout, nqxm,p_ncp
    write( 6,*) '  nh=',nh,' nqx=',nqx,' ncp=',ncp, &
                ' idout=',idout,' nqxm=',nqxm,' ncp_proj',p_ncp
    write(6,*)'  output fluence step: ',nh/idout+1
    write(6,*)'  output depth   step: ',nqx
    nout=nh/idout+1
    allocate (fluc(nout),depd(nout),retf(nout),retp(nout) &
            ,flibd(nout),flib1(nout),flib0(nout)) 
    allocate (backscat_ss(ncp), backsput_ss(ncp), quxi_ss(ncp))      
    allocate (xxx(nqx),dx(nqx) )
    allocate (flib   (nout,5,p_ncp))
    allocate (flib_ss(     5,p_ncp))
    allocate (csf(nout,ncp),backsput(nout,ncp),ard(nout,ncp),backscat(nout,ncp) &
              ,erfl(nout,ncp),refl(nout,ncp),sfcn(nout,ncp))
    allocate (dns(nout,nqx), adns(nout,nqx))
    allocate (rangeq(nqx,ncp))
    allocate (quxi(nqx,nout,ncp), range(nqx,nout,ncp)) 
    allocate (symbol(ncp),einheit(ncp))
    allocate (symbol(ncp),einheit(ncp))
    allocate (nproj(nout,ncp),e_tot_p(nout,ncp),iback_p(nout,ncp),eback_p(nout,ncp))
    allocate (itran_p(nout,ncp),etran_p(nout,ncp),iback_r(nout,ncp) &
             ,eback_r(nout,ncp),itran_r(nout,ncp),etran_r(nout,ncp),reem(nout,ncp),irec(nout,ncp))
     !deallocate(gitr)
    
    do i=1,nout
     if (i==1)then
        read (31, '(10a10)') symbol(1:ncp)
        write(6,*)'elements: ',symbol(1:ncp)
        !do j=1,16  !with irec
        do j=1,15
          read (31, '(a)') text
        enddo
      endif
      read(31,*)fluc(i),depd(i)     !fluence-step,dsurface                  : fluc,srrc
      read(31,*)csf(i,1:ncp)        !surface atomic composition             : csf(ncp)
      read(31,*)(flib(i,1:5,1:p_ncp))           !Momente                                 : flib(5,qbeam)
      read(31,*)ard(i,1:ncp)     !areal densities (atoms/A^2)             : ard(ncp)
      read(31,*)nproj  (i,1:ncp) !number projectils                       : nproj           
      read(31,*)iback_p(i,1:ncp) !number particle backscattered           : iback_p(1:ncp)
      read(31,*)eback_p(i,1:ncp) !energy particle backscattered           : eback_p(1:ncp)
      read(31,*)itran_p(i,1:ncp) !number particle transmission            : itran_p(1:ncp)
      read(31,*)etran_p(i,1:ncp) !energy particle transmission            : etran_p(1:ncp)
      read(31,*)iback_r(i,1:ncp) !number particle backsputtering          : iback_r(1:ncp)
      read(31,*)eback_r(i,1:ncp) !energy particle backsputtering          : eback_r(1:ncp)
      read(31,*)itran_r(i,1:ncp) !number particle transmission sputtering : itran_r(1:ncp)
      read(31,*)etran_r(i,1:ncp) !energy particle transmission sputtering : etran_r(1:ncp)
      read(31,*)e_tot_p(i,1:ncp) !energy all projectils                   : e_tot_p(1:ncp)
      read(31,*)reem   (i,1:ncp) !number particle reemission              : reem   (1:ncp)
      !read(31,*)irec   (i,1:ncp)!number of recoil                        : i_rec   (1:ncp)
      if (i==1)then
        read (31, '(a)') text
        read (31, '(a)') text
      endif
      do mm = 1, nqx
        read (31,*) xxx(mm), dns(i,mm), (quxi(mm,i,jp),jp=1,ncp)
      enddo
    enddo
  close(31)
  write(6,*)'file: E0_31_target.dat was read'
  write(6,*)'---'
  !write(6,*)titlecase
  write(text,'(f5.4)') fluc(nout)/nh
  titlecase(len_trim(titlecase)+1:len_trim(titlecase)+10)='  fl_step:'
  titlecase(len_trim(titlecase)+1:len_trim(titlecase)+5)=text(1:5)
  !write(6,*)titlecase
  !===============================================21================================
  !---surface thickness versus fluence
  open(21, file='dsurf_f_31.dat')
#ifndef XMGR   
   write (21,*)titlecase
   write (21,*)'fluence ; absolut surface change (nout,layer)'
   write (21,20) nout,1
   write (21,10)'fluence', 'surface'
   write (21,10)'[atoms/A**2]', '[A]'
#endif
   do i=1,nout
      write (21, 30) fluc(i), depd(i)
   enddo
  close(21)
  write(6,*)'file: dsurf_31.dat was written'
      
  !===============================================22================================
  !---particle backsputtering coefficient versus fluence
  einheit(1:ncp)='[-]'
  open(22, file='backsputt_f_31.dat')
#ifndef XMGR   
   write (22,*)titlecase
   write (22,*)'fluence ;particle backsputtering coefficient (between output interval),(nout,ncp,p_ncp)'
   write (22,20) nout ,ncp, p_ncp
   write (22,10)'fluence',symbol(1:ncp)
   write (22,10)'[atoms/A**2]',einheit(1:ncp)
#endif
   backsput(1,1:ncp)=0.0
   write (22,30) fluc(1), backsput(1,1:ncp)
   do i=2,nout
     sum_incidence=sum(nproj(i,1:p_ncp)-nproj(i-1,1:p_ncp))
     do jp=1,ncp
      if (sum_incidence > 0 ) then
        backsput(i,jp)=(iback_r(i,jp)-iback_r(i-1,jp))/sum_incidence
      else
        backsput(i,jp)=0.0
      endif
     enddo
     write (22,30) fluc(i), backsput(i,1:ncp)
   enddo
  close(22)
  write(6,*)'file: backsputt_31.dat was written'
      
  !===============================================23================================
  !---particle reflection coefficient  versus fluence
  open(23, file='backscatt_f_31.dat')
#ifndef XMGR   
   write (23,*)titlecase
   write (23,*)'fluence ;particle reflection coefficient [-] (between output interval),(nout,ncp,p_ncp)'
   write (23,20) nout ,ncp, p_ncp
   write (23,10)'fluence'     ,symbol(1:ncp)
   write (23,10)'[atoms/A**2]',einheit(1:ncp)
#endif
   backscat(1,1:ncp)=0.0
   write (23,30) fluc(1), backscat(1,1:ncp)
   do i=2,nout
     sum_incidence=sum(nproj(i,1:p_ncp)-nproj(i-1,1:p_ncp))
     do jp=1,ncp
      if (sum_incidence > 0 ) then
        backscat(i,jp)=(iback_p(i,jp)-iback_p(i-1,jp))/sum_incidence
      else
        backscat(i,jp)=0.0
      endif
     enddo
     write (23,30) fluc(i), backscat(i,1:ncp)
   enddo
  close(23)
  write(6,*)'file: backscatt_31.dat was written'
     
  !===============================================24================================
  !---surface composition versus fluence
  open(24, file='comp_surf_f_31.dat')
#ifndef XMGR   
   write (24,*)titlecase
   write (24,*)'fluence ;atomic fraction (quxi) at surface (nout,ncp)'
   write (24,20) nout ,ncp
   write (24,10)'fluence',symbol(1:ncp)
   write (24,10)'[atoms/A**2]',einheit(1:ncp)
#endif
   do i=1,nout
     write(24,30) fluc(i), quxi(1,i,1:ncp)
   enddo
  close(24)
  write(6,*)'file: comp_surf_31.dat was written'
  
  !===============================================25================================
  !---atomic fraction depend on depth  for 5 fluences (example:2,6,11,21,nout)
  open(25, file='comp_5f_d_31.dat')
   nh_step(1)=2
   nh_step(2)=6
   nh_step(3)=11
   nh_step(4)=21
   nh_step(5)=nout
   do i=1,5
    if( nh_step(i) > nout-5+i)  nh_step(i)= nout-5+i
   enddo    
#ifndef XMGR   
   write (25,*)titlecase
   write (25,*)'center layer ;atomic fraction(quxi) depend on depth at 5 fluence,(nqx,ncp,step)'
   write (25,20) nqx ,ncp,5
   write (25,30) (fluc(nh_step(j)),j=1,5)
   write (25,10)'cent.-layer',(symbol(1:ncp),i=1,5)
   write (25,10)'[A]',(einheit(1:ncp),i=1,5)
#endif
   do mm=1,nqx
     write (25,30) xxx(mm),quxi(mm,nh_step(1),1:ncp),quxi(mm,nh_step(2),1:ncp) &
                          ,quxi(mm,nh_step(3),1:ncp),quxi(mm,nh_step(4),1:ncp) &
                          ,quxi(mm,nh_step(5),1:ncp)
   enddo
  close(25)
  write(6,*)'file: comp_5f_d_31.dat was written'
          
  !===============================================26================================
  !---average of atomic fraction at last 10 fluence step depend on deph (steady state)------- 
  open(26, file='comp_steady_state_d_31.dat')
   ifl = min(10,nout) - 1 
   do jp = 1, ncp
    do mm = 1, nqx
       rangeq(mm,jp) = sum(quxi(mm,nout-ifl:nout,jp))/float(ifl+1)
    enddo
   enddo
#ifndef XMGR   
   write (26,*)titlecase
   write (26,*)'center layer ;average atomic fraction(quxi) depend on depth last 10 fluence,(nqx,ncp)'
   write (26,20) nqx ,ncp
   write (26,10)'cent.-layer',symbol(1:ncp)
   write (26,10)'[A]',einheit(1:ncp)
#endif
   do mm=1,nqx
     write (26,30) xxx(mm),rangeq(mm,1:ncp)
    enddo
  close(26)
  write(6,*)'file: comp_steady_state_31.dat'
      
!??????????????????????????? Physik der Ausgabe nicht verstanden
!      !---retained fluence  
!      !---retained fluence and retained fluence (per fluence step) versus fluence
!      open(28, file='retf')
!      retp(1) = 0.
!      flc = fluc(nout)
!      flstep = flc/float(nout)
!        write(6,*) ' flc=',flc,' nout=',nout,' flstep=',flstep
!      do i = 2, nout
!         !                               - backsput      - part.reflection(backscatterring) 
!         retp(i) = retp(i-1) + idout*(1. - backsput(i,1) - backscat(i,1))
!      enddo
!      do i = 2, nout
!         retf(i) = retp(i) * flstep
!      enddo
!      do i = 2, nout
!         write (28, 1030) fluc(i), retf(i), retp(i)
!      enddo
! 1030 format(1e11.4, 2e12.4)
!      close(28)

  !===============================================27================================
  !---average last 10 fluence step or steady state
  !---average of surface-atomic-fraction,backsputterin and backscattering 
  open(27, file='all_steady_state_31.dat')
   ifl = min(10,nout) - 1 
   do jp = 1, ncp
     !---average of surface-atomic-fraction,backsputterin and backscattering 
     backsput_ss(jp) = sum(backsput(nout-ifl:nout, jp))/float(ifl+1)
     backscat_ss(jp) = sum(backscat(nout-ifl:nout, jp))/float(ifl+1)
     quxi_ss    (jp) = sum(quxi  (1,nout-ifl:nout, jp))/float(ifl+1)
   enddo
   !  ---average of moments (2-5) of range distribution
   do jp = 1, p_ncp
    do j = 2, 5
     flib_ss(j,jp) = sum(flib(nout-ifl:nout,j,jp))  /float(ifl+1)
    enddo
   enddo
   
   !??????????????????????????? Physik der Ausgabe nicht verstanden
   !    retf_ss = 0.
   !     do i = nout-9, nout
   !       retf_ss = retf_ss + retf(i)
   !    enddo
   !    retf_ss = retf_ss/10.
#ifndef XMGR   
   write (27,*)titlecase
   write (27,*)'average last 10 fluence step or steady state, moment for:',ncp,'component,(ncp,?) '
   write (27,20) ncp
   write (27,10)'backscat','backsput','atomic-frac','symbol'
   write (27,10)'[-]','[-]','[-]','[-]'
   do jp = 1, ncp
     write (27,'(3(e12.4),a12)')backscat_ss(jp), backsput_ss(jp), quxi_ss(jp), symbol(jp)
   enddo
#else
   do jp = 1, ncp
     write (27,'(3(e12.4),a12)')backscat_ss(jp), backsput_ss(jp), quxi_ss(jp)
   enddo
#endif
#ifndef XMGR   
   write (27,10)'2.moment','3.moment','4.moment','5.moment'
   write (27,10)'[A]','[A**2]','[A**3]','[A**4]'
#endif
   do jp = 1, ncp
     write (27,30) (flib_ss(jp,j), j = 2, 5)
   enddo
   !?write (29, 1070) retf_ss
  close(27)
  write(6,*)'file: all_steady_state_31.dat was written'

  !===============================================28================================
  !---atomic fraction of component one = f(depth,fluence)-------------------------------
  i_ncp=1
  if(i_ncp ==1)open(28, file='comp_one_d_f_31.dat')
  if(i_ncp ==2)open(28, file='comp_two_d_f_31.dat')
  if(i_ncp ==3)open(28, file='comp_three_d_f_31.dat')
#ifndef XMGR   
   write (28,*)titlecase
   write (28,*)'center layer ;atomic fraction(quxi) depend on fluence,(nqx,nout)'
   write (28,20) nqx,nout
   write (28,10)'cent.-layer',('quxi',nn=1,min(20,nout)),'...'
   write (28,10)'[A]',('[-]',nn=1,min(20,nout)),'...'
#endif
   do mm = 1, nqx
     if(i_ncp ==1) write (28, 30) xxx(mm),(quxi(mm,nn,1),nn=1,nout)
     if(i_ncp ==2) write (28, 30) xxx(mm),(quxi(mm,nn,2),nn=1,nout)
     if(i_ncp ==3) write (28, 30) xxx(mm),(quxi(mm,nn,3),nn=1,nout)
  enddo
  close(28)
  if(i_ncp ==1)write(6,*)'file: comp_one_d_f_31.dat was written'
  if(i_ncp ==2)write(6,*)'file: comp_two_d_f_31.dat was written'
  if(i_ncp ==3)write(6,*)'file: comp_three_d_f_31.dat was written'

  !===============================================29================================
  !---density=f(fluence)
  open(29, file='density_5layer_f_31.dat')
#ifndef XMGR   
   write (29,*)titlecase
   write (29,*)'fluence ;density,(nout,layer) '
   write (29,20) nout,5
   write (29,10)'fluence  ', 'density_surf','density_l2','density_l3','density_l4','density_l5'
   write (29,10)'[atoms/A**2]','[a/A^3]','[a/A^3]','[a/A^3]','[a/A^3]','[a/A^3]'
#endif             
   do i = 1, nout
     write (29, 30) fluc(i), (dns(i, j),j=1,5) 
   enddo
  close(29)
  write(6,*)'file: density-5layer_f_31.dat was written'

  !===============================================30================================
  !---areal density=f(depth) 5.fluence step
  open(30, file='areal_density_5_d_31.dat')
#ifndef XMGR   
   write (30,*)titlecase
   write (30,*)'areal density=f(depth) 5.fluence step,(nqx)'
   write (30,20) nqx
   write (30,10)'cum._thick ', 'area_density','1.at.-frac'
   write (30,10)'[A]','[a/A^2]','[-]'
#endif             
   dx(1) = 2*xxx(1)
   do mm = 2, nqx
     dx(mm)=2*( xxx(mm)-xxx(mm-1)-dx(mm-1)/2.0  )
   enddo
   do i = 1, nout
     do mm = 1, nqx
        adns(i, mm) = 0.
     enddo
   enddo
   do i = 2, nout
     adns(i, 1) = dns(i, 1) * dx(i) * 0.5
     do mm = 2, nqx
       adns(i, mm) = adns(i, mm-1) + dns(i, mm) * dx(i) 
     enddo
   enddo

   do mm = 1, nqx
     write (30,30) sum(dx(1:mm)),adns(5, mm), quxi(mm, 5, 1)
   enddo
  close(30)
  write(6,*)'file: areal_density_5_d_31.dat was written'

  !===============================================31================================
  !---depth distribution of density for several fluences  
  open(31, file='density_d_f_31.dat')
  
#ifndef XMGR  
   write (31,*)titlecase
   write (31,*)'density=f(depth) depend on fluence,(nqx,nout)'
   write (31,20) nqx,nout
   write (31,*)'fluence [atoms/A**2]'
   write (31, 30) (fluc(i),i=1,nout)
   write (31,10)'cent.-layer ',('dens',nn=1,min(20,nout)),'...'
   write (31,10)'[A]',('[a/A^3]',nn=1,min(20,nout)),'...'
#endif             
   do mm = 1, nqx
     write (31, 30) xxx(mm), (dns(i, mm),i=1,nout) 
   enddo
  close(31)
  write(6,*)'file: density_d_f_31.dat was written'

10 format( 30(a12  ) )
20 format( 30(i12  ) )
30 format( 30(e12.4) )
end program readE031
