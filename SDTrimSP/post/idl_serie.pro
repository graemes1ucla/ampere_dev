@/afs/ipp/home/a/aam/public/archiv/TRIDYN/SDTrimSP/post/window.inc 
@/afs/ipp/home/a/aam/public/archiv/TRIDYN/SDTrimSP/post/level.inc 
@/afs/ipp/home/a/aam/public/archiv/TRIDYN/SDTrimSP/post/achsen.inc 

pro run
  _start
  druck=0
  print,'druck:'
  ;read,druck
  drname='plot.ps'
  form=0
  text='' & bez='' & bez=''& textdatum=''
  jflu=1
  idep=1
  ncp=1
  a=1.0d0 & b=1.0d0 & c=1.0d0 & d=1.0d0&  e=1.0d0
  loadct,39
  if druck eq 0 then farbe=[255,250,150,80,100,200,40,215,180,255,250,150,80,100,200,40,215] else $
                     farbe=[0  ,250,150,80,100,200,40,215,180,0  ,250,150,80,100,200,40,215]
  
  gr=1.5
  gd=1.0
  
  ;name='serie.00001'
  name='serie.dat'
  eingabe,name,bez,textdatum,ka,ke,ncp1,ncp2,s1,s2,ee,al,rf,sp

  
  drname=name+'.ps' 
  !P.Multi=0
  _fenster,druck,drname,form,nummer=ll  
    
   mm1=max(rf) 
   mm2=max(sp) 
   mm=max([mm1,mm2])
   ;mm=.2
   if ke gt 1 then begin 
     if ka gt 1 then pos=[.1,.59,.95,.99] else pos=[.1,.19,.95,.99]
     plot,[min(ee),max(ee)*1.01],[0,mm],/nodata,position=pos $
      ,xtitle='energy [eV]' $
      ,ytitle='coefficient',charsize=1.5
     fa=0 
      for i=0,ka-1 do begin
       for k=0,ncp1*2-1,2 do begin
         fa=k/2
         oplot,ee(*),rf(k,*,i),color=farbe(k),psym=-1
         xyouts,ee(ke-1)*1.02,rf(k,ke-1,i),'a='+string(al(i),format='(i2)'),/data,color=farbe(k),charsize=gr
       
       end
       for k=0,ncp2*2-1,2 do begin
         fa=(k+2*ncp1)/2
         oplot,ee(*),sp(k,*,i),color=farbe(fa),psym=-1
         xyouts,ee(ke-1)*1.02,sp(k,ke-1,i),'a='+string(al(i),format='(i2)'),/data,color=farbe(fa),charsize=gr
         for ii=0,ke-1 do begin
         print,ee(ii),sp(k,ii,i)
         end
       end
     end
     yy=.96
     for k=0,ncp1*2-1,2 do begin
       fa=k/2
       plots,[.15,.18],[yy-fa*.02,yy-fa*.02]                   ,/normal,color=farbe(fa) 
       xyouts,.19,           yy-.005-fa*.02,'refl.coeff '+s1(k),/normal,color=farbe(fa),charsize=1.5
     end
     for k=0,ncp2*2-1,2 do begin
       fa=(k+2*ncp1)/2
       plots,[.15,.18],[yy-fa*.02,yy-fa*.02]                    ,/normal,color=farbe(fa) 
       xyouts,.19,           yy-.005-fa*.02,'sputt.coeff '+s2(k),/normal,color=farbe(fa),charsize=1.5
     end
   end

   if ka gt 1 then begin 
     plot,[0,100],[0,mm*1.05],/nodata,position=[.1,.12,.95,.52],/noerase $
      ,xtitle='alpha [degree]' $
      ,ytitle='coefficient',charsize=1.5
      fa=0 
      for j=0,ke-1 do begin
       for k=0,ncp1*2-1,2 do begin
         fa=k
         oplot,al(*),rf(k,j,*),color=farbe(fa),psym=-1
         xyouts,al(ka-1)*1.02,rf(k,j,ka-1),'e='+string(ee(j),format='(i4)'),/data,color=farbe(fa),charsize=gr
       end
       for k=0,ncp2*2-1,2 do begin
         fa=k+ncp1
         oplot,al(*),sp(k,j,*),color=farbe(fa),psym=-1
         xyouts,al(ka-1)*1.02,sp(k,j,ka-1),'e='+string(ee(j),format='(i4)'),/data,color=farbe(fa),charsize=gr
       end
     end
     yy=.49
     for k=0,ncp1-1 do begin
         fa=k
      plots,[.15,.18],[yy-fa*.02,yy-fa*.02]                ,/normal,color=farbe(fa) 
      xyouts,.19,           yy-.005-fa*.02,'refl.coeff '+s1(k)  ,/normal,color=farbe(fa),charsize=1.5
     end
     for k=0,ncp2-1 do begin
        fa=k+ncp1
        plots,[.15,.18],[yy-fa*.02,yy-fa*.02]              ,/normal,color=farbe(fa) 
        xyouts,.19,           yy-.005-fa*.02,'sputt.coeff '+s2(k),/normal,color=farbe(fa),charsize=1.5
     end
    end
    xyouts,.1, .05,'!6'+bez  ,/normal,charsize=2.0,charthick=1.5*gd
    xyouts,.6, .01,'!6'+textdatum  ,/normal,charsize=1.0


  ;---eventuell Vergleich mit weiterer Datei------
  ;name='serie.001'
  ;eingabe,name,bez,textdatum,ka,ke,ncp1,ncp2,s1,s2,ee,al,rf,sp
  ;fa=0 
  ; for i=0,ka-1 do begin
  ;  for k=0,ncp1*2-1,2 do begin
  ;    fa=k/2
  ;    oplot,ee(*),rf(k,*,i),psym=-1,color=250 ;color=farbe(k),linestyle=2
  ;  end
  ;  for k=0,ncp2*2-1,2 do begin
  ;    fa=(k+2*ncp1)/2
  ;    oplot,ee(*),sp(k,*,i),psym=-1,color=250; color=farbe(fa),linestyle=2
  ;  end
  ;end
  ;
  
  ;---eventuell Vergleich mit weiterer Datei------
  ;name='serie.dat'
  ;eingabe,name,bez,textdatum,ka,ke,ncp1,ncp2,s1,s2,ee,al,rf,sp
  ;fa=0 
  ; for i=0,ka-1 do begin
  ;  for k=0,ncp1*2-1,2 do begin
  ;    fa=k/2
  ;    oplot,ee(*),rf(k,*,i),psym=-1,color=farbe(k),linestyle=2
  ;  end
  ;  for k=0,ncp2*2-1,2 do begin
  ;    fa=(k+2*ncp1)/2
  ;    oplot,ee(*),sp(k,*,i),psym=-1,color=farbe(fa),linestyle=2
  ;  end
  ;end
  
  
  _druckende,druck,drname,form
 
end
pro eingabe,name,bez,textdatum,ka,ke,ncp1,ncp2,s1,s2,ee,al,rf,sp
  close,1
  openr,1,name
   ncp1=0 & ncp2=0 & ka=0 & ke=0 & text=''
   
   readf,1,bez & print,bez
   readf,1,textdatum & print,textdatum
   readf,1,text & print,text
   readf,1,text,format='(a50)' 
  
   readf,1,text,format='(a3)' & if strtrim(text,1) eq '1' then ncp=1
   readf,1,text,format='(a3)' & if strtrim(text,1) eq '2' then ncp=2
   readf,1,text,format='(a3)' & if strtrim(text,1) eq '3' then ncp=3
   readf,1,text,format='(a3)' & if strtrim(text,1) eq '4' then ncp=4
   print,'ncp=',ncp
   readf,1,ke
   readf,1,ka
   readf,1,ndep
   readf,1,ncp1
   readf,1,ncp2
   s1=strarr(ncp1*2)
   s2=strarr(ncp2*2)
   s=''
   readf,1,s,s,s,s1,s2,format='(20a13)'
   s1(*)=strtrim(s1(*),1)
   s2(*)=strtrim(s2(*),1)
   print,s,s,s1,s2,format='(10a13)'
   readf,1,text
  
   print,'number refl.coeff :',ncp1
   print,'number sputt.coeff:',ncp2
   print,'number energy     :',ke
   print,'number alpha      :',ka  
  al=dblarr(ka) 
  ee=dblarr(ke)
  dd=1.0d0
  hilf1=dblarr(ncp1*2)
  hilf2=dblarr(ncp2*2)
  rf=dblarr(ncp1*2,ke,ka)
  sp=dblarr(ncp2*2,ke,ka)
  i=0
  j=0
  readf,1,a,b,d,hilf1,hilf2
  ;print,a,b,hilf1,hilf2,format='(10f10.5)'
  ee(j)=a
  al(i)=b
  rf(*,j,i)=hilf1(*) ;- 0.15 ;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  sp(*,j,i)=hilf2(*)
  aold=a
  bold=b
  for l=1,(ka)*(ke)-1 do begin 
      readf,1,a,b,d,hilf1,hilf2
      if aold ne a  and bold eq b then j=j+1 
      if aold eq a  and bold ne b then i=i+1 
      if aold ne a  and bold ne b then begin 
        i=i+1
        j=j+1 
        if i gt ka-1 then i=0
        if j gt ke-1 then j=0
      end
      ;print,l,i,j
      ;print,a,b,hilf1,hilf2,format='(10f10.5)'
      ee(j)=a
      al(i)=b
      rf(*,j,i)=hilf1(*) ;- 0.15 ;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      sp(*,j,i)=hilf2(*)
      aold=a
      bold=b
  end
  close,1
end