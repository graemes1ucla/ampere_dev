module zb_stopping_calc
    ! Contains functions to calculate the stopping of a projectile
    ! in a target material based on Ziegler Biersack theory
    ! All this has been ransacked from the SimNRA source code files uElement.pas 
    ! and uStopping.pas generously supplied by M. Mayer
    
    use zb_stopping_tabledb
    implicit none
    contains
    
    ! Public member functions
    !_______________________________________________________________________________________
    
    ! Calculates electronic stopping in units keV/10^15/cm^2
    function StoppingKeVPerADens(z1, z2, e0) result (stopping)
    
        integer, intent(in) :: z1, z2 ! Atomic number of projectile and target
        real, intent(in) :: e0 ! Energy in E0 in keV/amu!
        real :: stopping ! The resulting stopping in units eV/10^15/cm^2
        
        stopping = 0.0
        
        select case (z1)
            
        case (0)
            write(stdio,*) 'ZBStoppingCalc.StoppingeVPerADens->invalid projectile Z = 0'            
        case (1)
            stopping = p__Stopping_H(z2, e0)
        case (2)
            stopping = p__Stopping_He(z2, e0)
        case default
            if (z2 .le. numelements) then
                stopping = p__Stopping_Heavy(z1, z2, e0)
            else
                write(stdio, *) 'ZBStoppingCalc.StoppingKeVPerADens->Invalid atomic number encountered'
                stop 'ERROR IN MODULE ZBStoppingCalc'
            end if ! z2 .leq. numelements
        end select 
        
    end function StoppingKeVPerADens
    
    ! Calculates electronic stopping power in eV/A
    function Stopping_eVperA(z1, z2, e0) result (stopping)
    
        integer, intent(in) :: z1, z2 ! Atomic number of projectile and target
        real, intent(in) :: e0 ! Energy in E0 inkeV/amu!
        real :: stopping ! The resulting stopping in units eV/10^15/cm^2
        
        ! Locals
        real :: dens = 0
        
        ! Number density in #/cm^3
        dens = numdens_table(z2)  
        
        ! Calculate in units of keV/10^15/cm^2
        stopping = StoppingKeVPerADens(z1, z2, e0)
        
        ! Convert to keV/A
        stopping = stopping * dens * 1.0E-20! 1000.0 * (stopping / (1.0E15/dens)) * 1.0E-8        
        
    end function Stopping_eVperA
    
    ! Calculates nuclear stopping in units keV/10^15/cm^2
    function Nuclear_StoppingKeVPerADens(z1, z2, e0, M1) result(stopping)
    
        integer, intent(in) :: z1, z2 ! Atomic number of projectile and target
        real, intent(in) :: e0 ! Energy in E0 in keV!
        real, intent(in) :: M1 ! Ion  mass in AMU
        real :: stopping ! The resulting stopping in units eV/10^15/cm^2
        
        ! Locals
        real :: M2
        real :: epsilon ! reduced energy
        real :: leftx, lefty, rightx, righty
    
        ! Get the mass of the target atom
        M2 = mass_table(z2)
        
        !  reduced energy
        epsilon = 32.53*M2*e0/(z1*z2*(M1+M2)*((z1**0.23)+(z2**0.23)))
        
        if ((epsilon .le. 27.0) .or. (epsilon .ge. 33)) then
            stopping = p__reduced_nucl_stop(epsilon)
        else
            ! Peform some form of interpolation for epsilon around the jump point
            ! of epsilon .eq. 30
            ! Currently just interpolate linearely (Crude but helps)
            leftx = 27.0
            lefty = p__reduced_nucl_stop(leftx)
            rightx = 33.0
            righty = p__reduced_nucl_stop(rightx)
            stopping = lefty + epsilon * (righty - lefty)/(rightx - leftx)
        end if ! (epsilon .le. 27.0) .or. (epsilon .ge. 33)
        
        stopping = 0.001*8.462*z1*z2*M1*stopping/((M1+M2)*((z1**0.23)+(z2**0.23)))
        
    end function Nuclear_StoppingKeVPerADens

    ! Calculates electronic stopping power in eV/A
    function Nuclear_Stopping_eVperA(z1, z2, e0, M1) result(stopping)
    
        integer, intent(in) :: z1, z2 ! Atomic number of projectile and target
        real, intent(in) :: e0 ! Energy in E0 in keV!
        real, intent(in) :: M1 ! Ion  mass in AMU
        real :: stopping ! The resulting stopping in units eV/10^15/cm^2
        
        ! Locals
        real :: dens = 0
        
        ! Number density in #/cm^3
        dens = numdens_table(z2)  
        
        ! Calculate in units of keV/10^15/cm^2
        stopping = Nuclear_StoppingKeVPerADens(z1, z2, e0, M1)
        
        ! Convert to keV/A
        stopping = stopping * dens * 1.0E-20! 1000.0 * (stopping / (1.0E15/dens)) * 1.0E-8        
        
    end function Nuclear_Stopping_eVperA
    
    ! Private member functions
    !_______________________________________________________________________________________
    
    ! Calculates reduced nuclear stopping
    function p__reduced_nucl_stop(epsilon) result(stopping)
    
        real, intent(in) ::epsilon
        real :: stopping
        
        if (epsilon <= 30) then
            stopping = log(1.0 + 1.1383*epsilon)/(2.0*(epsilon + 0.01321*(epsilon**0.21226) + 0.19593*sqrt(epsilon)))
        else
            stopping= log(epsilon)/(2.0*epsilon)
        end if
        
    end function p__reduced_nucl_stop
    
    ! Calculates ZB hydrogen stopping
    function p__Stopping_H(z2, e0) result (stopping)
    
        integer, intent(in) :: z2 ! Atomic number of projectile and target
        real, intent(in) :: e0 ! Energy in E0 in keV/amu!    
        real :: stopping ! The resulting stopping in units eV/10^15/cm^2
        
        ! Locals
        real :: lne0toe0
        real :: slow, shigh, le0
        
        stopping = 0.0
        
        if (e0 .gt. 0.0) then
            
            if (e0 .gt. 10000.0) then
                ! High energy stopping for E > 10 MeV/AMU
                lne0toe0 = log(e0)/e0
                stopping = prot_stop_table(z2, 9) + (prot_stop_table(z2, 10) * lne0toe0) &
                + (prot_stop_table(z2, 11) * (lne0toe0*lne0toe0)) + (prot_stop_table(z2, 12) / lne0toe0)
            else
                ! Low energy stopping for E < 10 MeV/AMU
                le0 = max(10.0, e0)  !--?
                !---Eckstein,91 S.70  gl(5.2.10)
                !---Eckstein,91 S.70  e0<25 keV/amu
                slow = prot_stop_table(z2, 1) * le0**prot_stop_table(z2, 2) &
                      + prot_stop_table(z2, 3) * le0**prot_stop_table(z2, 4)
                !---Eckstein,91 S.70  e0>>25 keV/amu
                shigh = (prot_stop_table(z2, 5) / (le0**prot_stop_table(z2, 6))) &
                       * log((prot_stop_table(z2, 7) / le0) + (prot_stop_table(z2, 8) * le0))                
                stopping = (slow * shigh) / (slow + shigh)
                
                 ! Low energy stopping correction
                if (e0 .lt. 10) then
                    if (z2 .le. 6) then
                        stopping = stopping * ((e0/10.0)**0.35)
                    else 
                        stopping = stopping * ((e0/10.0)**0.45)
                    end if ! z2 <= 6
                end if ! e0 < 10      
                
            end if ! e0 .gt. 10000.0                               
        end if  ! e0 .gt. 0.0          
        
        stopping = stopping * 0.001 ! Konvert from eV/10^15/cm^2 to keV/10^15/cm^2  
        
    end function p__Stopping_H
    
    ! Calculates ZB helium stopping
    function p__Stopping_He(z2, e0) result (stopping)
    
        integer, intent(in) :: z2 ! Atomic number of projectile and target
        real, intent(in) :: e0 ! Energy in E0 inkeV/amu! 
        real :: stopping ! The resulting stopping in units eV/10^15/cm^2
        
        ! Locals        
        real :: le0
        real :: lne0, AFact, HeToH, CFact
        
        stopping = 0.0
        
        if (e0 .gt. 0.0) then
        
            ! Use 1 keV/amu for E0 < 1 keV/amu
            le0 = max(1.0, e0)
        
            lne0 = log(le0)        
            AFact =  min(30.0, 0.2865 + 0.1266*lne0 - 0.001429*(lne0**2) &
                   + 0.02402*(lne0**3) - 0.01135*(lne0**4) + 0.001475*(lne0**5))
        
            ! Stopping power ratio He/H
            HeToH = 1 - exp(-AFact)
        
            ! Add Z1^3 effect to He/H stopping power ratio HeH 
            CFact =  1.0 + (0.007 + 0.00005*Z2)*exp(-((7.6-lne0)**2))
            HeToH = HeToH * (CFact**2)
      
           ! Stopping of He is based on stopping of H 
           stopping = 4000.0 * HeToH * p__Stopping_H(z2, le0)
              
           ! Velocity proportional stopping below 1 keV/amu 
           if (E0 < 1.0) then
               stopping = stopping * sqrt(e0)
           end if ! (E0 < 1.0)
       end if ! e0 .gt. 0.0
       
       stopping = 0.001 * stopping ! Konvert from eV/10^15/cm^2 to keV/10^15/cm^2  
       
    end function p__Stopping_He
    
    ! Calculates ZB heavy ion stopping
    function p__Stopping_Heavy(z1, z2, e0) result (stopping)
    
        integer, intent(in) :: z1, z2 ! Atomic number of projectile and target
        real, intent(in) :: e0 ! Energy in E0 in keV/amu! 
        real :: stopping ! The resulting stopping in units eV/10^15/cm^2
        
        ! Locals        
        real :: vrmin
        real, parameter :: yrmin = 0.13
        real :: vfermi, vrel, vmin
        real :: le0, lne0, evfermcorr
        real :: yr, zfact, AFact, QFact, lambda, Zeta
        real :: hipower
        
        vrmin = 1.0
        stopping = 0.0
                        
        if (e0 .gt. 0.0) then      
            
            ! relative velocity to Fermi velocity 
            vfermi = vfermi_table(z2)
            vrel = sqrt(e0/25.0) / vfermi             
            
            if (vrel .lt. 1.0) then
                vrel = 0.75 * vfermi * (1.0 + 0.666666666666666667*(vrel**2) - 0.066666666666666667*(vrel**4))
            else
                vrel = vrel * vfermi * (1.0 + 1.0/(5.0 * (vrel**2)))
            end if ! vrel .lt. 1.0
  
            ! set yr = Maximum of (vr/Z1^.67),(vrmin/Z1^.67) or yrmin 
            zfact = 1.0 / (z1**0.6667)
            yr = max(vrel * zfact, yrmin)
            yr = max(yr, vrmin * zfact)
        
            ! Ionization level QFact
            AFact = min(50.0, -0.803 * (yr**0.3) + 1.3167*(yr**0.6) + 0.38157*yr + 0.008983*(yr**2))
            QFact = 1 - exp(-AFact)
            QFact = max(0.0, QFact)
            QFact = min(1.0, QFact)
        
            ! Convert QFact to effective charge
            ! Screening distance of ion Lambda
            lambda = getlambda(z1, QFact) / (z1**0.33333)
   
            Zeta = QFact + (1.0/(2.0*(vfermi**2)))*(1.0 - QFact)*Log(1.0 + ((4.0*lambda*vfermi/1.919)**2))
        
            ! Add Z1^3 effect
            lne0 = max(0.0, log(e0))
            Zeta = Zeta * (1.0 + (1.0/(z1**2))*(0.08 + 0.0015*z2)*exp(-((7.6-lne0)**2)))
        
            AFact = max(yrmin, vrmin * zfact)
        
            if (yr .gt. AFact) then
                stopping = 1000.0 * ((Zeta * z1)**2) * p__Stopping_H(z2, e0)
            
                ! Add Fermi velocity correction
                evfermcorr = e0
                If (evfermcorr .gt. 9999) then 
                    evfermcorr = 9999  ! Not valid > 10 MeV/amu
                end if ! le0 .gt. 9999
                stopping = stopping * getvfermi_corr(z2, evfermcorr)
            else ! yr <= AFact
                AFact = yrmin * (z1**0.6667); 
                vrmin = max(vrmin, Afact)            
            
                AFact = max(0.0, vrmin**2 - 0.8*(vfermi**2))
                vmin = 0.5*(vrmin + sqrt(AFact))
            
                le0 = 25.0*(vmin**2)
                stopping = 1000.0 * p__Stopping_H(z2, le0)
            
                ! Add Fermi velocity correcion
                evfermcorr = le0
                If (evfermcorr .gt. 9999) then 
                    evfermcorr = 9999  ! Not valid > 10 MeV/amu
                end if ! le0 .gt. 9999
                stopping = stopping * getvfermi_corr(z2, evfermcorr)
            
                ! Correction for low energy stopping, where little data exists
                ! Traditionally, this is velocity-proportional stopping, however,
                ! for light ions, light targets and semiconductors a large variation exists
                hipower = 0.47
                if (z1 .eq. 3) then 
                    hipower = 0.55
                else
                    if (z2 .lt. 7) hipower = 0.375 
                    if ((z1 .lt. 18) .and. ((z2 .eq. 14) .or. (z2 .eq. 32))) hipower = 0.375 
                end if ! z1 .eq. 3
                stopping = stopping * ((Zeta*z1)**2) * ((e0/le0)**hipower)
            end if ! yr .gt. AFact
        
        end if ! e0 .gt. 0.0
                
        stopping = 0.001 * stopping ! Konvert from eV/10^15/cm^2 to keV/10^15/cm^2  
        
    end function p__Stopping_Heavy    
    
end module zb_stopping_calc