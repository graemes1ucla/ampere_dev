module alloc_types
   implicit none

interface alloc
   module procedure alloc_field, alloc_int_vector
end interface

interface
   function assign_address_1d(a,n1) result (p)
      integer, intent(in)      :: n1
      integer, target, intent(in) :: a(*)
      integer, pointer :: p(:)
   end function

   function assign_address_2d(a,n1,n2) result (p)
      integer, intent(in)      :: n1, n2
      real, target, intent(in) :: a(*)
      real, pointer :: p(:,:)
   end function
end interface

   integer :: pv

contains

#ifdef SEQ
subroutine alloc_field(f, n1, n2)
   real, pointer, dimension(:,:) :: f
   integer :: n1, n2, err = 0

   allocate (f(n1,n2), stat=err)
   if (err /= 0) then
      write(*,*) "Allocation failed in routine alloc_field."
      write(*,*) "Program stopped."
      stop
   endif

end subroutine

subroutine alloc_int_vector(f, n1)
   integer, pointer, dimension(:) :: f
   integer :: n1, err = 0

   allocate (f(n1))
   if (err /= 0) then
      write(*,*) "Allocation failed in routine alloc_int_vector."
      write(*,*) "Program stopped."
      stop
   endif

end subroutine

#else /* SEQ */

subroutine alloc_field(f, n1, n2)
   real, pointer, dimension(:,:) :: f
   integer :: n1, n2
   integer :: rc 
   integer :: err = 0
   real, allocatable, dimension(:,:) :: v

   allocate(v(n1,n2), stat=err)
   if (err /= 0) then
      write(*,*) "Allocation failed in routine alloc_field"
      write(*,*) "Progam stopped."
      stop
   endif

   f => assign_address_2d(v,n1,n2)
end subroutine

subroutine alloc_int_vector(f, n1)
   integer, pointer, dimension(:) :: f
   integer :: n1
   integer :: rc 
   integer :: err = 0

   integer, allocatable, dimension(:) :: v

   allocate(v(n1), stat=err)
   if (err /= 0) then
      write(*,*) "Allocation failed in routine alloc_int_vector"
      write(*,*) "Progam stopped."
      stop
   endif
   f => assign_address_1d(v,n1)
end subroutine

#endif /* SEQ */

end module
