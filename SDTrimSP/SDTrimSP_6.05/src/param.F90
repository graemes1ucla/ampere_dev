module parameters
  !---number of elements
   integer, parameter :: ncpm = 3         
  
  !---nqxm...max. number of layer
   integer, parameter :: nqxm = 5000

#if defined ART_RAND || defined RAND2    /* ART_RAND or RAND2 */
   integer, parameter :: iwpi      = 4     !---long    
#else
   integer, parameter :: iwpi      = 8     !---double long   
   !integer, parameter :: iwpi = selected_int_kind(8)  !bei GCC und INTEL 
   !integer, parameter :: iwpi = selected_int_kind(16) !nur bei GCC   
#endif  
  
end module parameters
