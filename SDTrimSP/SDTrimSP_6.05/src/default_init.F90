!subroutine default_init_0
!subroutine default_init

subroutine default_init_0
  use work
  !--- = 22 -> tableread
  io   = 6  !6  Standard output
  io7  = 7  !6   time output
  io29 = 29 !--- general output
  io30 = 30 !--- tri.inp
  io31 = 31 !            output
  io33 = 33 !--- idrel=0 output
  io34 = 34 !--- idrel=0 output
  io35 = 35 !--- if (case_e0 > 0 ) then output
  io100=100 !--- time_run.dat
  
  ncp   = -1       ! no default, control of Inputs
  flc   = -1.0     ! no default, control of Inputs
  nh    = -1       ! no default, control of Inputs
  ipot  = -1       ! no default, control of Inputs
  isbv  = -1       ! no default, control of Inputs
  nqx   = -1       ! no default, control of Inputs
  qubeam(1:ncpm) = -1.0 ! no default, control of Inputs
  
  number_calc = 1
  icase_e0    = 0
  icase_alpha = 0
  case_alpha  = 0 !alpha_case...0,1,2,3,4,5
  case_e0     = 0 !case_e0   ...0,1,2,3,  5
  e0(1:ncpm)  = 0.

  alpha0(1:ncpm) = 0.0 !may be default
  qumax (1:ncpm) = 1.0 !may be default  
  nr_pproj   = 10     

end subroutine default_init_0

subroutine default_init
!-----------------------------------------------
!   M o d u l e s
!-----------------------------------------------
  use work
  use parameters, only:ncpm
  implicit none

  !---ncpm...number of elements -> param.F90
  e_bulkb(1:ncpm)       =  0.  !bulk binding energy
  ck_elec(1:ncpm,1:ncpm) = 1.  !correction factor for inelastic energy loss
  ca_scre(1:ncpm,1:ncpm) = 1.  !correction factor for the screening length
                               !in the interaction potential
  iwc = 2       !Number of ring cylinders for weak simultaneous collisions 
                !(projectiles)
  iwcr = 2      !Number of ring cylinders for weak simultaneous collisions 
                !(recoils)
  iintegral = 2 !integration by 0..MAGIC 1: Gauss-Mehler 2:Gaus-Legendre)

  idout = -1    ! = nh / 100  ...data output after each idout'th history

  idrel = 1     ! static case 

  sfin = 0      ! default=0 ...->hlm=0   or  1  ...->hlm =*
  
  ipivot = 8     !number of pivots in the Gauss_Mehler integration
  ttemp  = 300.  !target temperature [k]

  !---composition of target
  iq0 = 0 !--- iq0... iq0 < 0...composition of target <-layer.inp
          !--- iq0... iq0 ==0...Rechn:composition of target homogeneous

  inel0(1:ncpm) = 3 !inelastic loss model 1: Lindhard-Scharff,
                    !                     2: Oen-Robinson,
                    !                     3: equipartition of 1 and 2
                    !4: high energy hydrogen,table 3 over 20KeV / 25KeV
                    !5: high energy helium  ,table 4 over 50 KeV / 100 KeV
                    !6: General high energy stopping based on ziegler biersack 

  x0(1:ncpm) = 0.   !--- x0...x0(ncp) defaults=0 <-tri.inp moeglich
                    !starting position of projectile (<=0. means
                    !      starting outside the surface at x=xc=-su)

  irc0 = 1          !subthreshold recoil atoms free
  charge(:)=0       !charge of projectiles (not 0 if case_e0>=2)
  shth = 0.         !sheath potential (in eV); only of interest in
                    !    connection with a Maxwellian distribution (case_e0>=2)
  imcp = 0          !1...flib-moments calculated 
  dsf  = 5.         !minimum of averaging depth (in A) for surface composition


  deltahf    = -1.    ! heat of formation              ! no default
  deltahd(:) = -1.    ! heat of dissociation           ! no default
  nm         = -1     ! number target atom (2 atoms molecul)
  nm1        = -1     ! number target atom (1. atom)
  nm2        = -1     ! number target atom (2. atom)
  rhom       = -1.    ! density of 2-compound target   ! no default 
  two_comp=' ??? '    ! formel of 2-compound target example: Ta2O5 

  dns0(:)    = -1.    ! no default, control of Inputs
  a_rho(:)   = -1.    ! no default, density atom in g/cm*3 
  e_surfb(:) = -1.    ! no default, control of Inputs
  e_displ(:) = -1.    ! no default, control of Inputs
  e_cutoff(:)= -1     ! no default, control of Inputs
  ttarget    = -1.    ! no default, control of Inputs
  qubeam(:)  = -1.0   ! no default, control of Inputs


  ltableread = .true. !read all values from table

  isot=0              !no consider isotop      

  ltraj_p =.false.          !output trajectories projectiles
  ltraj_r =.false.          !output trajectories recoils
  numb_hist =20             !number of output all trajectories
  ioutput_hist(:) = 10      !number of output 6 kind trajectories

  lparticle_p=.false.      !output particles projectiles
  lparticle_r=.false.      !output particles recoils
  ioutput_part(:) = 10     !number of output 6 kind particles

  irand  =1
  iseed2 =1
  seed_g =1

  layerinp ='./' 
  energyinp='./' 
  angleinp ='./'
  tableinp ='../../tables'

  !---memory of particle information
  maxpart =500  
  dmaxpart=500
  maxt    =500
  dmaxt   =500   

  !---memory of  tasdescriptor(dbl)
  ntqmax =100000
  dntqmax=100000

  lmoments = .false.

  q_one_particle(:)=0.0

  lmatrices=.false.            !output matrices
  lmatout_log_energ=.false.    !output in matrix false...linear true..log
  lmatout_cos_angle=.false.    !output in matrix false...degree true..cos
  matrix_e_min  =-1.0          ! no default, control of Inputs
  matrix_e_max  =-1.0
  lrestart = .false.

  error99  = 0   !--- Error value
  i_two_comp=1   !---Method to compile dns0 for two-component-target
 
  !---energy distribution 
  dist_nx=60
  dist_ny=60
  dist_nz=60
  dist_delta=2.0
  lenergy_distr=.false.
  
  !---qux interpolation
  qu_int=.false.
  
  !---outgasing
  loutgas=.false.
  diff_koeff1(:)=0.0
  diff_koeff2(:)=0.0
  diff_koeff3(:)=0.0
  it_diff       =200
  !---Andock model
  !i_dock1       =0
  !i_dock2       =0
  
  !---chemical erosion C-H,T,D
  lchem_ch=.false.
  chem_jph =-1
  chem_jpd =-1
  chem_sp2 =-1
  chem_sp3 =-1
  chem_sp3h=-1   
  chem_dy=1.0
  chem_dt=0.0

  !---lchem_ga  ---chemical erosion Ga/As with Oxygen
  lchem_ga=.false.

  flux    = 1 !atoms/A**2/s    flux*1e+20 atoms/m**2/s
  
  !---automaticle parameter to reduce calculation time:
  lauto     =.false.

 !ltime_red=.false.
  
  nx_mat=1000
  
  case_layer_thick=0
  
  
  lpart_r_ed=.true.
  
  lterm_dif=.false.
  !---1...explicit (lambda=0) 2...implicit (lambda=1) 3...Crank-Nicolson (lambda=1/2) 4...Crank-Nicolson with lambda=lambda_cn=[0,1]
  i_diff_algo=2 
  !---Generalized Crank-Nicolsen lambda: algorithm for solution of diffusion equation (0...explicit 1...fully implicit)
  lambda_cn=0.95     
  a_0  (:,:)=0.0
  e_act(:,:)=0.0
  
  !---surface potential thick  true...suu=f(dns) false...suu=f(dns,iwc,iwcr)
  l_pot_thick=.false.
  
  !---i_cg  ---varable e_surfb of C_g
  i_cg=0 

  !---isb==7
  e_neg(:)=-1.0
  r_cov(:)=-1.0
  e_cov(:)=-1.0

  !---correction dlang fuer E_elt   version 5.07:false   version 6.05: true
  l_kor_lang=.true.
  
#if defined VAC    /* VAC */
  !---vac/stop
  e_cut_min(:)= -1     ! no default, control of Inputs
  e_cut_max(:)= -1     ! no default, control of Inputs
  delta_l  (:)=  5     ! 5 Angstrom
  delta_e     = 10     ! eV
  e_max       =1000    ! eV
#endif
end subroutine
