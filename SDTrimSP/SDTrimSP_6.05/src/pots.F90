!===volle Uebergabe von SDTrimSP
!subroutine newton_krc(r_a, eps, p_a, phi_a, phi_v1, ex1)
!subroutine newton_krc_old(r_a, eps, p_a, phi_a, phi_v1, ex1)
!subroutine newton_moliere(r_a, eps, p_a, phi_a, phi_v1, ex1)
!subroutine newton_zbl(r_a, eps, p_a, phi_a, phi_v1, ex1)
!subroutine newton_si(r_a, eps, p_a, phi_a, phi_v1, ex1)
!subroutine krc(vc,bcof)
!subroutine moliere(vc,bcof)
!subroutine zbl(vc,bcof)
!subroutine power(vc,bcof)
!subroutine Si_Si(vc,bcof)
!subroutine OR_loss

subroutine newton_krc(r_a, eps, p_a, phi_a, phi_v1, ex1)
   implicit none
   real   , intent(out)   :: r_a    !radius / screening length a
   real   , intent(in)    :: eps    !reduced energy
   real   , intent(in)    :: p_a    !impact parameter /screening length a
   real   , intent(out)   :: phi_a  !potential/r_a
   real   , intent(out)   :: phi_v1 !sum of potential and its derivative
   real   , intent(out)   :: ex1    !exponentials in the screening function
   real   :: ex2,ex3,d1,d2,d3,c1,c2,c3
   real   :: d1_c1,d2_c2,d3_c3,phi,phi_s,p_a2,fr, fr1, q
   integer:: i
    real   ::rr,rrr
   
   !---KRYPTON-CARBON POTENTIAL
   !--- (-d_i),(c_i) Eckstein,91 S.40 gl(4.1.2) ,table S.41 tab(4.1)
   DATA d1,d2,d3 / -0.278544, -0.637174, -1.919249 /
   DATA c1,c2,c3 /   .190945,   .473674,   .335381 /
   DATA d1_c1,d2_c2,d3_c3 /  -5.3186584080000002E-02 ,-0.3018127572760000 &
                            ,-0.6436796488690000 / 
   !---Newton method to find min(r)
   r_a=p_a !---start value old
   p_a2=p_a*p_a
   
   
   do   
     !!---reduce run time r_a=25/11
     !if (r_a > 25.) then
     ! ex1 = exp(d1*r_a)
     ! phi   =    c1*ex1  !phi
     ! phi_s = d1_c1*ex1  !d(phi)/d(r_a)
     !else
     ! if (r_a > 11.) then
     !  ex1 = exp(d1*r_a)
     !  ex2 = exp(d2*r_a)
     !  phi   =    c1*ex1 +    c2*ex2  !phi
     !  phi_s = d1_c1*ex1 + d2_c2*ex2  !d(phi)/d(r_a)
     ! else 
       ex1 = exp(d1*r_a)
       ex2 = exp(d2*r_a)
       ex3 = exp(d3*r_a)
       !---Eckstein,91 S.40 gl(4.1.2) 
       !---phi=sum( c_i*e**(-d_i*r/a) )  a...screening length,
       phi   =    c1*ex1 +    c2*ex2 +    c3*ex3 !phi
       phi_s = d1_c1*ex1 + d2_c2*ex2 + d3_c3*ex3 !d(phi)/d(r_a)
      !endif
     !endif

      !---  dr/dt= +- Vo * g(r) !---Eckstein,91 S.12 gl(2.5.4)-gl(2.5.6)
      !--- 0=g(r=R)  
      fr    =   p_a2/r_a      + phi  /eps - r_a !g=f(r_a)
      fr1   =  -p_a2/r_a/r_a  + phi_s/eps - 1.  !d(g)/d(r_a)
      q     = fr/fr1
      r_a   = r_a - q
      !print*,'Nullstelle:',r_a,fr
      if (abs(q/r_a) <= .001) exit
   enddo
   !write(*,'(a15,2g15.5)')'end:Nullstelle:',r_a,fr
   !if( r_a<0 .or. r_a > 50 )write(*,'(a15,2g15.5)')'end:Nullstelle:',r_a,fr
   
   !if (r_a > 25.) then
   ! ex1 = exp(d1*r_a)  !---out
   ! phi   =    c1*ex1  !phi
   ! phi_s = d1_c1*ex1  !d(phi)/d(r_a)
   !else
   ! if (r_a > 11.) then
   !  ex1 = exp(d1*r_a)              !---out
   !  ex2 = exp(d2*r_a)
   !  phi   =    c1*ex1 +    c2*ex2  !phi
   !  phi_s = d1_c1*ex1 + d2_c2*ex2  !d(phi)/d(r_a)
   ! else
     ex1 = exp(d1*r_a)                         !---out
     ex2 = exp(d2*r_a)
     ex3 = exp(d3*r_a)
     phi   =    c1*ex1 +    c2*ex2 +    c3*ex3 !phi
     phi_s = d1_c1*ex1 + d2_c2*ex2 + d3_c3*ex3 !d(phi)/d(r_a)
   ! endif
   !endif

   phi_a  =  phi/r_a                 !---out
   phi_v1 = -phi_a/r_a + phi_s/r_a   !---out

   !write(*,'(a15,2f15.10)')'Nullstelle(end):', p_a2/r_a      + phi  /eps - r_a,r_a
end subroutine

subroutine newton_moliere(r_a, eps, p_a, phi_a, phi_v1, ex1)
   implicit none
   real   , intent(out)   :: r_a
   real   , intent(in)    :: eps, p_a
   real   , intent(out)   :: phi_a, phi_v1, ex1
   real   :: ex2,ex3,d1,d2,d3,c1,c2,c3
   real   :: d1_c1,d2_c2,d3_c3,phi,phi_s,p_a2,fr, fr1, q
   !---  MOLIERE POTENTIAL
   !--- (-d_i),(c_i) Eckstein,91 S.40 gl(4.1.2) ,table S.41 tab(4.1)
   DATA d1,d2,d3 /-0.3, -1.2, -6.0  /
   DATA c1,c2,c3 /  .35,  .55,  .10 /
   DATA d1_c1,d2_c2,d3_c3 /  -0.105 ,-0.66 ,-0.6 /
      
   !---Newton method to find min(r)
   r_a=p_a !---start value
   p_a2=p_a*p_a
   do 
     ex1 = exp(d1*r_a)
     ex2 = exp(d2*r_a)
     ex3 = exp(d3*r_a)
     !---Eckstein,91 S.40 gl(4.1.2) 
     !phi=sum( c_i*e**(-d_i*r/a) )  a...screening length,
     phi   =    c1*ex1 +    c2*ex2 +    c3*ex3 !phi
     phi_s = d1_c1*ex1 + d2_c2*ex2 + d3_c3*ex3 !d(phi)/d(r_a)
     !---  dr/dt= +- Vo * g(r) !---Eckstein,91 S.12 gl(2.5.4)-gl(2.5.6)
     !--- 0=g(r=R)  
     fr    =   p_a2/r_a      + phi  /eps - r_a  !g=f(r_a)
     fr1   =  -p_a2/r_a/r_a  + phi_s/eps - 1.   !d(g)/d(r_a)
     q     = fr/fr1
     r_a   = r_a - q
     if (abs(q/r_a) <= .001) exit
   enddo   
   !if ( abs(fr) > 1) then
   !  write(*,'(a15,2g15.5)')'end:Nullstelle:',fr
   !  stop
   !endif
 
   ex1    = exp(d1*r_a)
   ex2    = exp(d2*r_a)
   ex3    = exp(d3*r_a)
   phi    =    c1*ex1 +    c2*ex2 +    c3*ex3 !phi
   phi_s  = d1_c1*ex1 + d2_c2*ex2 + d3_c3*ex3 !d(phi)/d(r_a)
   phi_a  =  phi/r_a
   phi_v1 = -phi_a/r_a + phi_s/r_a

   !print*,'Nullstelle(end):', p_a2/r_a      + phi  /eps - r_a
end subroutine                                                    

subroutine newton_zbl(r_a, eps, p_a, phi_a, phi_v1, ex1)
   implicit none
   real   , intent(out)   :: r_a
   real   , intent(in)    :: eps, p_a
   real   , intent(out)   :: phi_a, phi_v1, ex1
   real   :: ex2,ex3,ex4,d1,d2,d3,d4,c1,c2,c3,c4
   real   :: d1_c1,d2_c2,d3_c3,d4_c4,phi,phi_s,p_a2,fr, fr1, q
   !--- ZBL POTENTIAL
   !--- (-d_i),(c_i) Eckstein,91 S.40 gl(4.1.2) ,table S.41 tab(4.1)
   DATA d1,d2,d3,d4 / -0.20162, -0.40290, -0.94229, -3.19980 /
   DATA c1,c2,c3,c4 /   .02817,   .28022,   .50986,   .18175 /
   DATA d1_c1,d2_c2,d3_c3,d4_c4  &
        /-5.6796354000E-03,-0.1129006380,-0.4804359794,-0.5815636500 /
   
   !d1_c1=d1*c1
   !d2_c2=d2*c2
   !d3_c3=d3*c3
   !d4_c4=d4*c4

   !---Newton method to find min(r)
   r_a=p_a !---start value
   p_a2=p_a*p_a
   do 
     ex1 = exp(d1*r_a)
     ex2 = exp(d2*r_a)
     ex3 = exp(d3*r_a)
     ex4 = exp(d4*r_a)
     !---Eckstein,91 S.40 gl(4.1.2) 
     !---phi=sum( c_i*e**(-d_i*r/a) )  a...screening length,
     phi   =    c1*ex1 +    c2*ex2 +    c3*ex3 +    c4*ex4!phi
     phi_s = d1_c1*ex1 + d2_c2*ex2 + d3_c3*ex3 + d4_c4*ex4!d(phi)/d(r_a)
     !---  dr/dt= +- Vo * g(r) !---Eckstein,91 S.12 gl(2.5.4)-gl(2.5.6)
     !--- 0=g(r=R)  
     fr    =   p_a2/r_a      + phi  /eps - r_a !g=f(r_a)
     fr1   =  -p_a2/r_a/r_a  + phi_s/eps - 1.  !d(g)/d(r_a)
     q     = fr/fr1
     r_a   = r_a - q
     if (abs(q/r_a) <= .001) exit
   enddo   
   !if ( abs(fr) > 1) then
   !  write(*,'(a15,2g15.5)')'end:Nullstelle:',fr
   !  stop
   !endif
   ex1   = exp(d1*r_a)
   ex2   = exp(d2*r_a)
   ex3   = exp(d3*r_a)
   ex4   = exp(d4*r_a)
   phi   =    c1*ex1 +    c2*ex2 +    c3*ex3 +    c4*ex4 !phi
   phi_s = d1_c1*ex1 + d2_c2*ex2 + d3_c3*ex3 + d4_c4*ex4 !d(phi)/d(r_a)
   phi_a  =  phi/r_a
   phi_v1 = -phi_a/r_a + phi_s/r_a

end subroutine

subroutine newton_naya(r_a, eps, p_a, phi_a, phi_v1, ex1,i,j)
   use work, only:potcoef4_b,potcoef4_c
   implicit none
   real   , intent(out)   :: r_a
   real   , intent(in)    :: eps, p_a
   integer, intent(in)    :: i,j  !=species of collision partner
   real   , intent(out)   :: phi_a, phi_v1, ex1
   real    :: potcoef4_a,phi,phi_s,p_a2,fr, fr1, q
   DATA potcoef4_a /1.51 /
   !---Nakagawa-Yamamura  (IPOT=4)
   !---Eckstein,91 S.43 (4.1.14) 

   !---Newton method to find min(r)
   r_a=p_a !---start value
   p_a2=p_a*p_a
   do 
     !---Eckstein,91 S.43 (4.1.14) 
     !---phi=-aa*r/a + bb*(r/a)**1.5 -cc * (r/a)**1)  a...screening length,
     !phi  = exp(    (-potcoef4_a * r_a + potcoef4_b(i,j) * r_a**1.5 - potcoef4_c(i,j) * r_a * r_a)) !phi
     phi   = exp(r_a*(-potcoef4_a       + potcoef4_b(i,j) * sqrt(r_a) - potcoef4_c(i,j) * r_a      )) !phi
     phi_s = phi*    (-potcoef4_a  +1.5 * potcoef4_b(i,j) * sqrt(r_a) - 2.* potcoef4_c(i,j) * r_a)    !d(phi)/d(r_a)

     !---  dr/dt= +- Vo * g(r) !---Eckstein,91 S.12 gl(2.5.4)-gl(2.5.6)
     !--- 0=g(r=R)  
     fr    =   p_a2/r_a      + phi  /eps - r_a !g=f(r_a)
     fr1   =  -p_a2/r_a/r_a  + phi_s/eps - 1.  !d(g)/d(r_a)
     q     = fr/fr1
     r_a   = r_a - q
     if (abs(q/r_a) <= .001) exit
   enddo   
!   !if ( abs(fr) > 1) then
!   !  write(*,'(a15,2g15.5)')'end:Nullstelle:',fr
!   !  stop
!   !endif
    phi   = exp(r_a*(-potcoef4_a       + potcoef4_b(i,j) * sqrt(r_a) - potcoef4_c(i,j) * r_a      )) !phi
    phi_s = phi*    (-potcoef4_a  +1.5 * potcoef4_b(i,j) * sqrt(r_a) - 2.* potcoef4_c(i,j) * r_a)    !d(phi)/d(r_a)
    phi_a  =  phi/r_a
    phi_v1 = -phi_a/r_a + phi_s/r_a
    ex1=phi
end subroutine

subroutine newton_si(r_a, eps, p_a, phi_a, phi_v1, ex1)
   implicit none
   real   , intent(out)   :: r_a
   real   , intent(inout) :: p_a
   real   , intent(in)    :: eps
   real   , intent(out)   :: phi_a, phi_v1, ex1
   real   :: ex2,ex3,ex4,d1,d2,d3,d4,d5,c1,c2,c3,c4,c5
   real   :: d1_c1,d2_c2,d3_c3,d4_c4,p_a2,fr, fr1, q, fr_old
   real   :: phi,phi1,phi2,phi_s,phi_s1,phi_s2 
   real   :: x
   integer:: j
   !integer:: itest
   
   !--- Si POTENTIAL
   !--- Eckstein,91 S.55 gl(4.4.3) korrectur
   DATA d1,d2,d3,d4,d5 /-0.28, -1.20, -6.00 ,-.15    , -5.757   /
   DATA c1,c2,c3,c4,c5 /.35  ,   .55,   .10 ,-.002327, -0.005713/

   d1_c1=d1*c1
   d2_c2=d2*c2
   d3_c3=d3*c3
   d4_c4=d4*c4 
   p_a2=p_a*p_a
   
   r_a=p_a !---start value
   
   !---find start-value
   fr_old=10000
   do j=1,100
     r_a=j*.5
     ex1 = exp(d1*r_a)
     ex2 = exp(d2*r_a)
     ex3 = exp(d3*r_a)
     ex4 = exp(d4*(r_a+d5)**2)
     !---Eckstein,91 S.55 gl(4.4.3) 
     phi1 = (c1*ex1 + c2*ex2 + c3*ex3 + c4*r_a*ex4) !phi_1
     phi2 = (1+c5*r_a**2)                           !phi1_2
     phi  = phi1 * phi2                             !phi
     
     !---  dr/dt= +- Vo * g(r) !---Eckstein,91 S.12 gl(2.5.4)-gl(2.5.6)
     !--- 0=g(r=R)  
     fr    =   p_a2/r_a      + phi  /eps - r_a !g=f(r_a)
     
     !---Vorzeichenwechsel
     if(fr*fr_old <0) exit
     fr_old=fr
   enddo  
   fr_old=r_a
   
   !---Newton method to find min(r)
   j=0
   do 
     j=j+1
     ex1 = exp(d1*r_a)
     ex2 = exp(d2*r_a)
     ex3 = exp(d3*r_a)
     ex4 = exp(d4*(r_a+d5)**2)
     !---Eckstein,91 S.55 gl(4.4.3) 
     phi1 = (c1*ex1 + c2*ex2 + c3*ex3 + c4*r_a*ex4) !phi_1
     phi2 = (1+c5*r_a**2)                           !phi1_2
     phi  = phi1 * phi2                             !phi
     
     phi_s1=d1_c1*ex1 + d2_c2*ex2 + d3_c3*ex3 + c4*ex4 + 2.*d4_c4*r_a*ex4*(r_a+d5)
     phi_s2=2.*c5*r_a
     phi_s =phi1 * phi_s2 + phi_s1* phi2            !d(phi)/d(r_a)
     
     !--- dr/dt= +- Vo * g(r) !---Eckstein,91 S.12 gl(2.5.4)-gl(2.5.6)
     !--- 0=g(r=R)  
     fr    =   p_a2/r_a      + phi  /eps - r_a !g=f(r_a)
     fr1   =  -p_a2/r_a/r_a  + phi_s/eps - 1.  !d(g)/d(r_a)
     q     = fr/fr1
     
     if(abs(q) > 1                     ) q=q/abs(q)
     if(abs(p_a2/r_a+phi/eps - r_a) > 1) q=q/10.0
     
     r_a   = r_a - q
     
     if (abs(q/r_a) <= .001) exit
       
     if (j>100) then
       open (unit=100 ,file='time_run.dat',action="write",position="append",status="old")
       write(100,*)' WARNING: pots.F90 end-values p_a,r_a,q:',p_a,r_a,q
       write(100,*)'          set r_a=20'
       close(100)
       write(*,*)' WARNING: pots.F90 end-values p_a,r_a,q:',p_a,r_a,q
       write(*,*)'          set r_a=20'
       r_a=20
       exit
     endif   
   enddo   
      
   ex1 = exp(d1*r_a)
   ex2 = exp(d2*r_a)
   ex3 = exp(d3*r_a)
   ex4 = exp(d4*(r_a+d5)**2)
   phi1 = (c1*ex1 + c2*ex2 + c3*ex3 + c4*r_a*ex4) !phi_1
   phi2 = (1+c5*r_a**2)                           !phi1_2
   phi  = phi1 * phi2                             !phi
   phi_s1=d1_c1*ex1 + d2_c2*ex2 + d3_c3*ex3 + c4*ex4 + 2*d4_c4*r_a*ex4*(r_a+d5)
   phi_s2=2.*c5*r_a
   phi_s =phi1 * phi_s2 + phi_s1* phi2            !d(phi)/d(r_a)
   phi_a  =  phi/r_a
   phi_v1 = -phi_a/r_a + phi_s/r_a
   
   !if (itest==1)print*,'r_a start-value:',fr_old,'r_a end-value:',r_a,'Nullstelle:',p_a2/r_a+phi/eps - r_a
end subroutine
                                                    
subroutine krc(vc,bcof)
   !  KR-C POTENTIAL (IPOT=1)
   implicit none
   real, intent(in)  :: bcof !=r/a
   real, intent(out) :: vc   !=phi(r/a)/(r/a)
   real :: ex1, ex2, ex3
   !--- (-d_i),(c_i) Eckstein,91 S.40 gl(4.1.2) ,table S.41 tab(4.1)
   
   !ex1 = exp((-0.278544*bcof))
   !ex2 = exp((-0.637174*bcof))
   !ex3 = exp((-1.919249*bcof))
   !vc  = (.190945*ex1 + .473674*ex2 + .335381*ex3)/bcof
   
   !---reduce run time r_a=25/11
   !if (bcof > 25.)then
   !   ex1 = exp((-0.278544*bcof))
   !   vc  = (.190945*ex1)/bcof
   !else
   !  if (bcof > 11.)then
   !    ex1 = exp((-0.278544*bcof))
   !    ex2 = exp((-0.637174*bcof))
   !    vc  = (.190945*ex1 + .473674*ex2)/bcof
   !  else
       ex1 = exp((-0.278544*bcof))
       ex2 = exp((-0.637174*bcof))
       ex3 = exp((-1.919249*bcof))
       vc  = (.190945*ex1 + .473674*ex2 + .335381*ex3)/bcof
   !  endif
   !endif
end subroutine

subroutine moliere(vc,bcof)
   !  MOLIERE POTENTIAL (IPOT=2)
   implicit none
   real, intent(in)  :: bcof !=r/a
   real, intent(out) :: vc   !=phi(r/a)/(r/a)
   real :: ex1, ex2, ex3
   !--- (-d_i),(c_i) Eckstein,91 S.40 gl(4.1.2) ,table S.41 tab(4.1)
   ex1 = exp((-0.3*bcof))
   ex2 = exp((-1.2*bcof))
   ex3 = exp((-6.0*bcof))
   vc  = (.35*ex1 + .55*ex2 + .10*ex3)/bcof
end subroutine

subroutine zbl(vc,bcof)
   !  ZBL POTENTIAL  (IPOT=3)
   implicit none
   real, intent(in)  :: bcof !=r/a
   real, intent(out) :: vc   !=phi(r/a)/(r/a)
   real :: ex1, ex2, ex3, ex4
   !--- (-d_i),(c_i) Eckstein,91 S.40 gl(4.1.2) ,table S.41 tab(4.1)
   ex1 = exp((-0.20162*bcof))
   ex2 = exp((-0.40290*bcof))
   ex3 = exp((-0.94229*bcof))
   ex4 = exp((-3.19980*bcof))
   vc  = (.02817*ex1+.28022*ex2+.50986*ex3+.18175*ex4)/bcof
end subroutine

subroutine naya(vc,bcof,i,j)
   !---Nakagawa-Yamamura  (IPOT=4)
   use work, only:potcoef4_b,potcoef4_c
   implicit none
   real   , intent(in)  :: bcof !=r/a
   integer, intent(in)  :: i,j  !=species of collision partner
   real, intent(out)    :: vc   !=phi(r/a_yn) / (r/a_yn)
   real :: ex1,potcoef4_a
   !---Eckstein,91 S.43 (4.1.14) 
   potcoef4_a=1.51
   !---b=0.763 *   (i_num_z(i)**0.169  + i_num_z(j)**0.169 )/(i_num_z(i)**0.307 + i_num_z(j)**0.307)
   !---c=0.191 * ( (i_num_z(i)**0.0481 + i_num_z(j)**0.0481)/(i_num_z(i)**0.307 + i_num_z(j)**0.307))**4./3.
   ex1 = exp((-potcoef4_a*bcof + potcoef4_b(i,j)*bcof**1.5 - potcoef4_c(i,j)*bcof*bcof))
   vc  = (ex1)/bcof
end subroutine

subroutine power(vc,bcof)
  implicit none
   real, intent(in)  :: bcof !=r
   real, intent(out) :: vc   !=PHI(r)
   integer, parameter :: ex = -2
   !---Eckstein,91 S.22 (n=-2) 
   vc = bcof**ex 
end subroutine

subroutine Si_Si(vc,bcof)
   !  Si POTENTIAL(PHI)  
   implicit none
   real, intent(in)  :: bcof !=r/a
   real, intent(out) :: vc   !=PHI(r) / (r/a)
   real :: ex1, ex2, ex3, ex4
   !---Eckstein,91 S.55 gl(4.4.3)
   !---a_screen=a_s=0.1224 A    Eckstein,91 S.S47 
   ex1 = exp((-0.28*bcof))
   ex2 = exp((-1.20*bcof))
   ex3 = exp((-6.00*bcof))
   !a_s=0.122456709214463
   !ex4= exp((-0.10*a_s*a_s*(bcof-0.705)**2)) wrong or Error  not the same as next: Eckstein 4.4.3
   ex4 = exp((-0.15*(bcof-5.757)**2))         !---Eckstein 4.4.3 correction-page
   !vc= ((.35*ex1+.55*ex2+.10*ex3-.019*a_s*bcof*ex4)*(1-0.381*a_s*a_sc*bcof**2))/bcof
   vc = ((.35*ex1+.55*ex2+.10*ex3-.002327*bcof*ex4) *(1-0.005713*bcof**2))/bcof
end subroutine
!-----------------------------------------------------------
subroutine OR_loss
  use work
  !use MPP_functions
  implicit none
  real,parameter :: ab_bohr    = 0.52917725  ! Bohr radius [A] Eckstein,91 S.239
  real,parameter :: fp_const   = 0.885341377 ! ((9*pi**2)/128)**1/3 
  real,parameter :: e2_const   = 14.399651      ! e**2  [eV*A] Eckstein,91 S.239
  real,parameter :: c_light    = 2.99792458e+03 !       [A/fs] Eckstein,91 S.239
 !real,parameter :: c_light_si = 2.99792458e+08 !       [m/s]    Eckstein,91 S.239
  real,parameter :: mass_unit_c2 = 9.3149432e+8 !m_u*c**2 [eV] Eckstein,91 S.239
  real,parameter :: eec_const = mass_unit_c2/(2*c_light*c_light) !m_u/2 eV*fs**2/A**2
  integer        :: i,j
  real           :: abc
  real, dimension(ncp) :: a_num_z 
   
   a_num_z(1:ncp)=i_num_z(1:ncp) !--- integer -> real 

   !ktemp = boltzm * ttemp   !old: ttemp=0   standart version 4.00 
   ktemp=0.0
   !---thermal vibration
   !if(ttemp > 300.01 ) ktemp = boltzm * ttemp   !old: ttemp=300 standart version 5.00 
   !ktemp = boltzm * ttemp
   
   !---ab...Bohr radius (Eckstein,91 S.40 unten) (siehe constanten)
   !---fp_const                (Eckstein,91 S.40 unten) (siehe constanten)
   abc = ab_bohr*fp_const
  
   !!Lindhard or Bohr
   !i=1
   !j=2
   !a_screen(i,j)=ca_scre(i,j)*abc/( a_num_z(i)**(2./3.) + a_num_z(j)**(2./3.) )**(1./2.)
   !!Thomas-Fermi  wenig kleiner etwas mehr  bei großen Energien
   !!a_screen(i,j)=ca_scre(i,j)*abc/( a_num_z(i)          + a_num_z(j)          )**(1./3.)
   !
   !!print*,'pots: a(Li)',a_screen(i,j)
   !eps_d_e(i,j) = a_screen(i,j)*a_mass(j)/(a_num_z(i)*a_num_z(j)*e2_const*(a_mass(i)+a_mass(j)))
   !!!---E=   (eps=0.015)
   !e0(1)=0.015/eps_d_e(i,j)
   !if(pid==0) print*,'pots: E(Li)',0.015/eps_d_e(i,j)
   !e0(3)=0.015/eps_d_e(i,j)
   
   !!!e0(1)=0.08/eps_d_e(i,j)
   !!!print*,'E(Li)',0.080/eps_d_e(i,j)
    
    do i = 1, ncp
    do j = 1, ncp
      !---=1/A=m1/m2...   Eckstein,91 S.8 gl(2.3.4)
      mass_ratio(i,j) = a_mass(i)/a_mass(j)
      !---=4*A/(1+A)**2   Eckstein,91 S.11 gl(2.4.3)
      mass_trans(i,j) = 4.*mass_ratio(i,j)/(1. + mass_ratio(i,j))**2
      !---a...screening length
      select case (ipot)
       case (1,2,5,6)  !---potential of KrC,Molier,Si, power(?) need Firsov  17.12.2014
        !---a_screen=a_F (Firsov)
        !---Eckstein,91 S.41 gl(4.1.5) or S.23 gl(2.7.27) 
        a_screen(i,j)=ca_scre(i,j)*abc/(sqrt(a_num_z(i)) + sqrt(a_num_z(j))  )**(2./3.)
       case (3)
        !---a_screen=a_U (Ziegler,Biersack,Littmark:ZBL)
        !---Eckstein,91 S.42 gl(4.1.8) 
        a_screen(i,j)=ca_scre(i,j)*abc/(a_num_z(i)**0.23 + a_num_z(j)**0.23  )
       case (4)
        !---a_screen=a_U (Nakagawa-Yamamura:NaYa)
        !---Eckstein,91 S.45 gl(4.1.15) 
        a_screen(i,j)=ca_scre(i,j)*abc/(a_num_z(i)**0.307 + a_num_z(j)**0.307)**(2./3.)
        
        !---Eckstein,91 S.43 (4.1.14) 
        potcoef4_b(i,j)=0.763 *   (a_num_z(i)**0.169  + a_num_z(j)**0.169 )/(a_num_z(i)**0.307 + a_num_z(j)**0.307)
        potcoef4_c(i,j)=0.191 * ( (a_num_z(i)**0.0481 + a_num_z(j)**0.0481)/(a_num_z(i)**0.307 + a_num_z(j)**0.307))**(4./3.)
      end select


      !---eps=Er/C = Er*a/(Z1*Z1*e2) Eckstein,91 S.25 gl(2.7.31) 
      !          Er=Eo*m2/(m1+m2)    Eckstein,91 S.7 gl(2.2.6)
      !---eps   =a*Er/(Z1*Z2) 
      !   eps/Eo=a*m2/(Z1*Z2*e2*(m1+m2)) 
      eps_d_e(i,j) = a_screen(i,j)*a_mass(j)/(a_num_z(i)*a_num_z(j)*e2_const*(a_mass(i)+a_mass(j)))
     
      !---inelastic energy loss Eckstein,91 S.67 gl(5.2.1) (Lindhard/Scharf)
      kl_fac(i,j) = 1.212*a_num_z(i)**(7./6.)*a_num_z(j) &
                /((a_num_z(i)**(2./3.)+a_num_z(j)**(2./3.))**1.5*sqrt(a_mass(i)))
    enddo
   enddo

   do i = 1, ncp
     !---tmpr=sqrt(2*Mu*m(amu))
     tmpr(i) = sqrt(4.*eec_const*a_mass(i))
   enddo
   
   select case (ipot)
   !---kor --> Eckstein,91 S.65 gl(5.1.5) 
   !---coeff.:--> d_1^2/2 Eckstein,91 S.41 Tab 4.1
   case (1,7)
   !  KR-C POTENTIAL (IPOT=1, IPOT=7)
      kor(1:ncp,1:ncp) = 0.0389205*kl_fac(1:ncp,1:ncp)/(pi*a_screen(1:ncp,1:ncp)**2)
   case (2)
   !  MOLIERE POTENTIAL (IPOT=2)
      kor(1:ncp,1:ncp) = 0.0450000*kl_fac(1:ncp,1:ncp)/(pi*a_screen(1:ncp,1:ncp)**2)
   case (3)
   !  ZBL POTENTIAL  (IPOT=3)
      kor(1:ncp,1:ncp) = 0.0203253*kl_fac(1:ncp,1:ncp)/(pi*a_screen(1:ncp,1:ncp)**2)
   case (4)
   !---Nakagawa-Yamamura   (IPOT=4) same as IPOT=2
      kor(1:ncp,1:ncp) = 0.0389205*kl_fac(1:ncp,1:ncp)/(pi*a_screen(1:ncp,1:ncp)**2)
   case (5)
   !  Si POTENTIAL  (IPOT=5)
     !kor(1:ncp,1:ncp) = 0.0450000*kl_fac(1:ncp,1:ncp)/(pi*a_screen(1:ncp,1:ncp)**2)
     !                  d1**2/2= 0.28**2/2=0.0392    24.03.2015 S.55 4.4.3
      kor(1:ncp,1:ncp) = 0.0392000*kl_fac(1:ncp,1:ncp)/(pi*a_screen(1:ncp,1:ncp)**2)
   case (6)
   !  POWER POTENTIAL  (IPOT=6) same as IPOT=1
      kor(1:ncp,1:ncp) = 0.0389205*kl_fac(1:ncp,1:ncp)/(pi*a_screen(1:ncp,1:ncp)**2)
   end select

end subroutine

