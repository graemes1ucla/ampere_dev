!subroutine openfile_output_start
!subroutine openfile_output_end

subroutine openfile_output_start(datum)
  !---OPEN NEW OUTPUT FILES 80-86, 90-96, io31, io33, io34,
  use work
  implicit none
  character(len= 8) :: datum     
  integer   :: j,i,idebugmom  
  character(len=30), dimension(6) :: texth  
  character(len=4), dimension(8) :: stern  
  real   , dimension(ncp,ncp) :: spydj !---only for output io33
  character(len=32) :: xfmtcar
  Character(len=50) :: filename
   
  !---prepare formats
  xfmt2035 = vfmt('(3e12.4,/#e12.4,#(/5e12.4),5(/#e12.4))',ncp,ncp_proj,ncp)
  xfmt2036 = vfmt('(1e11.4,#(/#e11.4))',ncp,ncp)
  xfmt2038 = vfmt('(2i10,i5,e11.4,#e11.4)',ncp)
! xfmt2045 = vfmt('(e12.4,e15.6,4x,#f8.4,#f8.4)',ncp,ncp)
  xfmt2045 = vfmt('(e12.6,e15.6,4x,#f9.5,#f9.5)',ncp,ncp)
  xfmt4391 = vfmt('(/" SBV:   CPT.",#i6)',ncp)
  xfmt4393 = vfmt('(1x,i9,4x,#f6.2)',ncp)
   xfmtcar = vfmt('(#e16.8,a20)',ncp)
  
  texth(1)='(stopped-projectile)      '
  texth(2)='(backscattered-projectile)'
  texth(3)='(transmitted-projectile)  '
  texth(4)='(stopped-recoil)          '
  if(lpart_r_ed)texth(4)='(stopped-recoil E(start)>ED)          '
  texth(5)='(back_sputtered-recoil)   '
  texth(6)='(transm_sputtered-recoil) '
      
  if (ltraj_p .or. ltraj_r) then 
    open(80, FILE="trajec_all.dat"  ,ACTION="write")
    open(81, FILE="trajec_stop_p.dat",ACTION="write")
    open(82, FILE="trajec_back_p.dat",ACTION="write")
    open(83, FILE="trajec_tran_p.dat",ACTION="write")
    open(84, FILE="trajec_stop_r.dat",ACTION="write")
    open(85, FILE="trajec_back_r.dat",ACTION="write")
    open(86, FILE="trajec_tran_r.dat",ACTION="write")
    write(80, 2025)avs0,datum(7:8),datum(5:6),datum(1:4) 
    write(80,101)title
    if (ltraj_r) then 
      write(80,*)numb_hist,1,'  maxnumber history, 1...projectiles + recoil'
    else
      write(80,*)numb_hist,0,'  maxnumber history, 0...only projectiles'
    endif
    close(80)
    do i=1,6
      write(80+i, 2025)avs0,datum(7:8),datum(5:6),datum(1:4) 
      write(80+i,101)title
      write(80+i,*)ioutput_hist(i),'  maxnumber history ',texth(i) 
      close(80+i)
    enddo
  endif
      
  if (lparticle_p .or. lparticle_r ) then
    open(91, FILE="partic_stop_p.dat",ACTION="write")
    open(92, FILE="partic_back_p.dat",ACTION="write")
    open(93, FILE="partic_tran_p.dat",ACTION="write")
    open(94, FILE="partic_stop_r.dat",ACTION="write")
    open(95, FILE="partic_back_r.dat",ACTION="write")
    open(96, FILE="partic_tran_r.dat",ACTION="write")
    do i=1,6
      write(90+i, 2025)avs0,datum(7:8),datum(5:6),datum(1:4) 
      write(90+i,101)title
      write(90+i,*)ioutput_part(i),'  maxnumber ',texth(i)
      write(90+i,*)nh,nr_pproj,'  NH NR'
      if (i==4) then
      write(90+i,113)'species','gen/coll','fluence','start-energy' &
            ,'x_start','y_start','z_start'  &
            ,'x_end'  ,'y_end'  ,'z_end'    &
            ,'x_max'  ,'y_max'  ,'z_max'    &
            ,'cosp','cosa','pathlength'     &
            ,'e_transf_sum','e_transf','e_con_elec','e_transf>ED','time'
      else
      write(90+i,113)'species','gen/coll','fluence','end-energy' &
            ,'x_start','y_start','z_start'  &
            ,'x_end'  ,'y_end'  ,'z_end'    &
            ,'x_max'  ,'y_max'  ,'z_max'    &
            ,'cosp','cosa','pathlength'     &
            ,'e_transf_sum','e_transf','e_con_elec','e_transf>ED','time'
      endif
      close(90+i)
    enddo
  endif
  if (number_calc == 1 ) then    
    !-----OUTPUT----- io35
    if (case_e0 > 0) then
     idebugmom=0
#ifdef DEBUGMOM
     idebugmom=1
#endif
     open (unit=io35, file='E0_35_moments.dat', action="write")
     write(io35, 2025)avs0,datum(7:8),datum(5:6),datum(1:4) 
     write(io35,101)title
     write(io35,100)'!flu.-step','atoms','each-out','n-projec','stat./dyn'
     write(io35,100)'!       nh','nr'     ,'ncp'  ,'idout'   ,'ncp_proj','idrel','debugmom'
     write(io35,200) nh         , nr_pproj, ncp   , idout    , ncp_proj , idrel, idebugmom 
     write(io35,100)symbol(1:ncp)
     write(io35, 100) '!e0(1:ncp)'
     write(io35, 312) e0(1:ncp)
     write(io35,'(a100)')'!1 nproj(1:ncp_proj)    lin_moment_energy_inp.  :  e_beam_p(1:ncp_proj,1:6)  min(e),max(e)'
     write(io35,'(a100)')'!1 nproj(1:ncp_proj)    lin_moment_alpha_inp.   :  alpha   (1:ncp_proj,1:6)  min(a),max(a)'
     stern(1)=')**1'
     stern(2)=')**2'
     stern(3)=')**3'
     stern(4)=')**4'
     stern(5)=')**5'
     stern(6)=')**6'
     stern(7)=')MIN'
     stern(8)=')MAX'
     write(io35,313)'!','(1/2)',('i_p(',i,')   ',i=1,ncp_proj),(('E/a_p(',i,stern(j),i=1,ncp_proj),j=1,8) 
    close(io35)
    endif

    !-----OUTPUT----- io34
    if (lmoments) then
     idebugmom=0
#ifdef DEBUGMOM
     idebugmom=1
#endif
     open (unit=io34, file='E0_34_moments.dat', action="write")
     write(io34, 2025)avs0,datum(7:8),datum(5:6),datum(1:4) 
     write(io34,101)title
     write(io34,100)'!flu.-step','atoms','each-out','n-projec','stat./dyn'
     write(io34,100)'!       nh','nr'     ,'ncp'  ,'idout'   ,'ncp_proj','idrel','debugmom'
     write(io34,200) nh         , nr_pproj, ncp   , idout    , ncp_proj , idrel, idebugmom 
     write(io34,100)symbol(1:ncp)
     write(io34, 100) '!e0(1:ncp)'
     write(io34, 312) e0(1:ncp)
     write(io34,'(a100)')'!1 i_back_p(1:ncp_proj)    lin_moment_energy_refl.     :     e_back_p(1:ncp_proj,1:6)  min(e),max(e)'
     write(io34,'(a100)')'!2 i_back_plog(1:ncp_proj) log_moment_energy_refl.     : log e_back_p(1:ncp_proj,1:6)  min(e),max(e)'
     write(io34,'(a100)')'!3 i_tran_p(1:ncp_proj)    lin_moment_energy_transm.   :     e_tran_p(1:ncp_proj,1:6)  min(e),max(e)'
     write(io34,'(a100)')'!4 i_tran_plog(1:ncp_proj) log_moment_energy_transm.   : log e_tran_p(1:ncp_proj,1:6)  min(e),max(e)'
     write(io34,'(a100)')'!5 i_back_rlog(1:ncp)      log_moment_energy_sputt.(r) : log e_back_r(1:ncp,1:6)       min(e),max(e)'
     write(io34,'(a100)')'!6 i_tran_rlog(1:ncp)      log_moment_energy_transm.sp.: log e_tran_r(1:ncp,1:6)       min(e),max(e)'
     write(io34,'(a100)')'!7 i_stop_p(1:ncp_proj)    lin_moment_depth_impl._proj.: x...x**6(1:ncp_proj,1:6)      min(x),max(x)'
     write(io34,'(a100)')'!8 i_stop_p(1:ncp_proj)    lin_moment_r_impl._proj.(p) : r...r**6(1:ncp_proj,1:6)      min(r),max(r)'
     stern(1)=')**1'
     stern(2)=')**2'
     stern(3)=')**3'
     stern(4)=')**4'
     stern(5)=')**5'
     stern(6)=')**6'
     stern(7)=')MIN'
     stern(8)=')MAX'
     write(io34,313)'!','(1..4)',('i_p(',i,')   ',i=1,ncp_proj),(('E_p(',i,stern(j),i=1,ncp_proj),j=1,8) 
     write(io34,313)'!','(5..6)',('i_r(',i,')   ',i=1,ncp     ),(('E_r(',i,stern(j),i=1,ncp     ),j=1,8) 
     write(io34,313)'!','   (7)',('i_p(',i,')   ',i=1,ncp_proj),(('X_p(',i,stern(j),i=1,ncp_proj),j=1,8) 
     write(io34,313)'!','   (8)',('i_p(',i,')   ',i=1,ncp_proj),(('R_p(',i,stern(j),i=1,ncp_proj),j=1,8) 
     close(io34)
    endif
    if (idrel == 0 .and. lmoments) then
      !-----OUTPUT----- io33 
      !-----dynamic
      open (unit=io33, file='E0_33_sputt.dat', action="write")
       write(io33, 2025)avs0,datum(7:8),datum(5:6),datum(1:4) 
       write(io33,101)title
       write(io33,100)'nh','idout'  ,'ncp'  ,'fluence'  
       write(io33,'(3I10,g16.8)') nh, idout, ncp, flc      
       write(io33,100)symbol(1:ncp)
       write(io33,100)'flu step'  
       write(io33,312) fluc 
       write(io33,*)'back. sputtered yield by generation [-]'
       spydj(:ncp,:ncp) = 0.
       do jin=1,ncp
        write(io33,*)'by ',jin
        write(io33, 312)(spydj(jp,jin),jp=1,ncp)
       enddo
      close(io33)
    endif !---(idrel == 0 )
  endif !---(number_calc == 1)

    !-----OUTPUT----- io31 
    !xfmtcar = vfmt('(#e16.8,a20)',ncp)
    filename='E0_31_target.dat'
    if (number_calc >1) then  
     filename='E0_31_target'
     write(filename(13:15),'(i3.3)') inumber_calc
     filename(16:19)='.dat'
    endif
    open (unit=31,file=filename, action="write")
    write(31, 2025)avs0,datum(7:8),datum(5:6),datum(1:4) 
    write(31,101)title
    write(31,100)'flue.-step','out-layer','atoms','each-out','max-layer',' calc_mom.','n-projec','stat./dyn'
    write(31,100)'nh','nqx','ncp','idout','nqxm','ncp_proj','idrel'
    write(31,200) nh , nqx , ncp ,  idout, nqxm , ncp_proj , idrel
    write(31,100) symbol(1:ncp)
    write(31,*)' fluence-step,dsurface                   : fluc,srrc' 
    write(31,*)' surface atomic fraction                 : csf(ncp)'
    write(31,*)' Momente                                 : flib(5,qbeam)' 
    write(31,*)' areal densities (atoms/A^2)             : ard(1:ncp)' 
    write(31,*)' number projectils                       : nproj'  
    write(31,*)' number particle backscattered           : iback_p(1:ncp)'
    write(31,*)' energy particle backscattered           : eback_p(1:ncp)'
    write(31,*)' number particle transmission            : itran_p(1:ncp)'
    write(31,*)' energy particle transmission            : etran_p(1:ncp)'
    write(31,*)' number particle backsputtering          : iback_r(1:ncp)'
    write(31,*)' energy particle backsputtering          : eback_r(1:ncp)'
    write(31,*)' number particle transmission sputtering : itran_r(1:ncp)'
    write(31,*)' energy particle transmission sputtering : etran_r(1:ncp)'
    write(31,*)' energy all projectils                   : e_tot_p(1:ncp)'
    write(31,*)' number particle reemission              : reem   (1:ncp)'
    write(31,*)' number particle chemical erosion        : d_chem (1:ncp)'

    write(31,312)fluc,srrc
    write(31,xfmtcar)csf(1:ncp),' !--- csf(ncp)      '
    write(31,312)flib(1:5,1:ncp_proj)   
    write(31,xfmtcar)ard(1:ncp),' !--- ard(1:ncp)    ' 
    write(31,312)nproj  (1:ncp)
    write(31,312)iback_p(1:ncp) 
    write(31,312)eback_p(1:ncp) 
    write(31,312)itran_p(1:ncp) 
    write(31,312)etran_p(1:ncp) 
    write(31,312)iback_r(1:ncp) 
    write(31,312)eback_r(1:ncp) 
    write(31,312)itran_r(1:ncp) 
    write(31,312)etran_r(1:ncp) 
    write(31,312)e_tot_p(1:ncp) 
    write(31,312)reem   (1:ncp)       
    write(31,312)d_chem (1:ncp)       
           
    !---INITIAL OUTPUT OF PROFILES  idout<--tri.inp   
    write (31, *) '  center[A]  density[a/A^3]  atomic fraction  '
    write (31, *) '  xxx(*)       dns(*)       (qux(*,jp),jp=1,ncp)'
    
    do mm = 1, nqx
     write (31, xfmt2045) xxc(mm), dns(mm), (qux(mm,jp),jp=1,ncp) ,kdepth_all(mm),kdepth_all(mm)
    enddo
    close(31)
2025  format('SDTrimSP: VERSION ',a4,'    ',a2,'.',a2,'.',a4)
101   format(a)
100   format(50a10)
113   format(50a13)
200   format(50I10)
312   format(50e16.8)
313   format(a1,a6,(a5,i2,a4,3x),100(a5,i2,a4,5x))
end subroutine

!-------------------------------------------------------------------------
subroutine openfile_output_end(datum)
  !---OPEN OUTPUT FILES 80-86, 90-96
  use work
  implicit none
  character(len= 8) :: datum     
  integer   :: j,i
  
  if (ltraj_p .or. ltraj_r) then 
    if (inumb_hist(7) == 0) then
      open(80, FILE="trajec_all.dat"  ,ACTION="write")
       write(80,2025)avs0,datum(7:8),datum(5:6),datum(1:4)
       write(80,101 )title
       write(80,*   )0,0,'  number history'
      close(80)
    endif
    open(80, FILE="trajec_all.dat"   ,POSITION="append",ACTION="write")
     write(80,*)'ende'
     write(80,*)min(inumb_hist(7),numb_hist),'  number history from SDTrimSP'
    close(80)
    
    do i=1,6 
      if (inumb_hist(i) == 0) then
        if(i ==1)open(81, FILE="trajec_stop_p.dat",ACTION="write")
        if(i ==2)open(82, FILE="trajec_back_p.dat",ACTION="write")
        if(i ==3)open(83, FILE="trajec_tran_p.dat",ACTION="write")
        if(i ==4)open(84, FILE="trajec_stop_r.dat",ACTION="write")
        if(i ==5)open(85, FILE="trajec_back_r.dat",ACTION="write")
        if(i ==6)open(86, FILE="trajec_tran_r.dat",ACTION="write")
         write(80+i,2025)avs0,datum(7:8),datum(5:6),datum(1:4)
         write(80+i,101 )title
         write(80+i,*   )0,'  number history'
        close(80+i)
      endif
      if(i ==1)open(81, FILE="trajec_stop_p.dat",POSITION="append",ACTION="write")
      if(i ==2)open(82, FILE="trajec_back_p.dat",POSITION="append",ACTION="write")
      if(i ==3)open(83, FILE="trajec_tran_p.dat",POSITION="append",ACTION="write")
      if(i ==4)open(84, FILE="trajec_stop_r.dat",POSITION="append",ACTION="write")
      if(i ==5)open(85, FILE="trajec_back_r.dat",POSITION="append",ACTION="write")
      if(i ==6)open(86, FILE="trajec_tran_r.dat",POSITION="append",ACTION="write")
       write(80+i,*)'ende'
       write(80+i,*)min(inumb_hist(i),ioutput_hist(i)),'  number history from SDTrimSP'
      close(80+i)
    enddo 
  endif
   
  if (lparticle_p .or. lparticle_r) then
     do i=1,6
      if (inumb_part(i) == 0) then
        if(i ==1)open(91, FILE="partic_stop_p.dat" ,ACTION="write")
        if(i ==2)open(92, FILE="partic_back_p.dat" ,ACTION="write")
        if(i ==3)open(93, FILE="partic_tran_p.dat" ,ACTION="write")
        if(i ==4)open(94, FILE="partic_stop_r.dat",ACTION="write")
        if(i ==5)open(95, FILE="partic_back_r.dat",ACTION="write")
        if(i ==6)open(96, FILE="partic_tran_r.dat",ACTION="write")
         write(90+i,2025)avs0,datum(7:8),datum(5:6),datum(1:4)
         write(90+i,101 )title
         write(90+i,*   )0,'  number particle'
         write(90+i,*)nh,nr_pproj,'  NH NR'
        close(90+i)
      endif
      if(i ==1)open(91, FILE="partic_stop_p.dat",POSITION="append",ACTION="write")
      if(i ==2)open(92, FILE="partic_back_p.dat",POSITION="append",ACTION="write")
      if(i ==3)open(93, FILE="partic_tran_p.dat",POSITION="append",ACTION="write")
      if(i ==4)open(94, FILE="partic_stop_r.dat",POSITION="append",ACTION="write")
      if(i ==5)open(95, FILE="partic_back_r.dat",POSITION="append",ACTION="write")
      if(i ==6)open(96, FILE="partic_tran_r.dat",POSITION="append",ACTION="write")
       write(90+i,*)'ende'
       write(90+i,*)min(inumb_part(i),ioutput_part(i)),'  number particle from SDTrimSP'
      close(90+i)
    enddo
  endif
 
2025  format('SDTrimSP: VERSION ',a4,'    ',a2,'.',a2,'.',a4)
101   format(a)
end
