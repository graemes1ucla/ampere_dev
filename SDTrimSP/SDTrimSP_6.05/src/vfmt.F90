      function vfmt(fmt,i1,i2,i3,i4,i5) result (varfmt)
!->insert up to 5 integers into format string at marked places
      character(*), intent (in)        :: fmt
      integer             :: i1
      integer, optional   :: i2, i3, i4, i5
      integer             :: n
      character(LEN(fmt)) :: varfmt

      character   :: mark="#"
      integer     :: m(5), i, ii, j, k, limit
      !logical     :: markfound = .true.


      m(:) = 0
      m(1) = i1
      if (present(i2)) m(2)=i2
      if (present(i3)) m(3)=i3
      if (present(i4)) m(4)=i4
      if (present(i5)) m(5)=i5
      
      varfmt = fmt
      i = 0
      k = 0
      do while (i<LEN(fmt))
         i = i + 1
         if (varfmt(i:i)==mark) then
            ii = i
            !print*,mark,'find1 at',ii
            do while (varfmt(i+1:i+1)==mark)
               i = i + 1
            enddo
            !print*,mark,'find2 at',i
            k = k+1
            n = m(k)
            !print*,'find3 at',n
            limit = 10**(i-ii+1)
            !print*,mark,'find3 limit',limit
            if (n<limit) then
               do  j=i,ii,-1
                  varfmt(j:j) = CHAR(ICHAR("0") + modulo(n,10))
                  n = n / 10
               end do
            end if
         end if
      end do
      end function vfmt
