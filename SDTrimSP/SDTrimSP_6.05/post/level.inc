;pro _einlev,levanf,levend,levlog,levanz,minlevel=minlevel,maxlevel=maxlevel
;    need  /afs/ipp/home/a/aam/IDL/TEST/window.inc
;alle mit  common widhilf,hilfvar

;pro _levberech ,levanf,levend,levlog,levanz,lev
;pro _levberech2,levanf,levend,levlog,levanz,lev,col,lab,levelfile=levelfile
;pro _farben2,fill,form,levanz,levanf,levend,levlog,colanf,colsch $
;            ,balkq,dx,dy,xa,ya,balkint,gr,di,format_balk,skalfak=skalfak
;pro _balken2,lev,balkq,xa,dx,ya,dy,col,balkint,gr,di,skalfak=skalfak,forma=forma
;            ,legend_balk,anz_leg_balk,leg_balk,levanf,levend

;pro _farbtabelle,farbtab


pro _einlev,levanf,levend,levlog,levanz,minlevel=minlevel,maxlevel=maxlevel $
             ,autolevel=autolevel
  common widhilf,hilfvar
  if n_elements(minlevel) eq 0 then minlevel=0.0
  if n_elements(maxlevel) eq 0 then maxlevel=0.0
  if n_elements(autolevel) eq 0 then autolevel=0
  if autolevel eq 0 then begin
   clicmal:
   _real2string,levanf,levanf,s1
   _real2string,levend,levend,s2
   _int2string,levanz,levanz,s3
   _anfwidget,hilfvar,base
   button  =widget_button(base,value='Level of plot'        ,uvalue=0)
   button  =widget_button(base,value='value of begin  =' +s1,uvalue=1)
   button  =widget_button(base,value='value of end    =' +s2,uvalue=2)
   if levlog eq 0 then $
     button=widget_button(base,value='linear   (log)'       ,uvalue=3) $
     else $
     button=widget_button(base,value='log   (linear)'       ,uvalue=3)
   button  =widget_button(base,value='number of level =' +s3,uvalue=4)
   if minlevel ne maxlevel then $
   button  =widget_button(base,value='standard of level'    ,uvalue=5)
   button  =widget_button(base,value='print level'          ,uvalue=6)
   button  =widget_button(base,value='return'               ,uvalue=7)
   widget_control,base,/realize,tlb_set_xoffset=50,tlb_set_yoffset=50
   xmanager,'widget2',base
   auswahl=fix(hilfvar)
   if auswahl eq 1 then _fltein,levanf,'value of begin =' +s1
   if auswahl eq 2 then _fltein,levend,'value of end   =' +s2
   if auswahl eq 3 then levlog=1-levlog
   if auswahl eq 4 then _intein,levanz,'number of level=' +s3
   if auswahl eq 5 then begin
      levanf=minlevel
      levend=maxlevel
      levlog=0
      levanz=50
   end
   if auswahl eq 6 then begin
     _levberech,levanf,levend,levlog,levanz,lev
     print,'level:'
     print,lev
   end
   if auswahl ne 7 then goto,clicmal
  end else begin
      levanf=minlevel
      levend=maxlevel
      levlog=0
      levanz=50
  endelse
end
;================================================================
pro _levberech,levanf,levend,levlog,levanz,lev
     lev=fltarr(levanz)
     if levlog eq 0 then begin
       sch=(levend-levanf)/(levanz-1)
       lev(0)=levanf
       for i=1,levanz-2 do lev(i)=lev(i-1)+sch
       lev(levanz-1)=levend
     end
     if levlog eq 1 then begin
       sch=(alog(levend)-alog(levanf))/(levanz-1)
       lev(0)=levanf
       for i=1,levanz-2 do lev(i)=lev(i-1)*exp(sch)
       lev(levanz-1)=levend
     end
end
;================================================================
pro _levberech2,levanf,levend,levlog,levanz,colanf,colsch,labsch $
                ,lev,col,lab,levelfile=levelfile
    ;---need  UP _levberech
    ;---input levanf ... level-begin
    ;         levend ... level-end
    ;         levlog ... 0...linear / 1...logarithmic
    ;         levanz ... number of level
    ;         colanf ... color-begin
    ;         colsch ... color-step
    ;         labsch ... label-step
    ;--output lev    ... field of levels
    ;         col    ... field of colors
    ;         lab    ... 0...no label / 1...labels are writed
     if n_elements(levelfile) eq 0 then levelfile=0


     if levelfile eq 0 then begin
       _levberech,levanf,levend,levlog,levanz,lev
       lab=fltarr(levanz)
       col=fltarr(levanz)
       col(0)=colanf
       for i=1,levanz-1        do col(i)=col(i-1)+colsch
       for i=1,levanz-1,labsch do lab(i)=1
     end else begin
       fname='level.dat'
       find=FINDFILE(fname,COUNT=exist)
       if(exist eq 0) then begin
         close,1
         openw,1,fname
          _levberech,levanf,levend,levlog,levanz,lev
          lab=intarr(levanz)
          col=intarr(levanz)
          col(0)=colanf
          for i=1,levanz-1         do col(i)=col(i-1)+colsch
          for i=1,levanz-1,labsch  do lab(i)=1
          printf,1,levanz,'  number of level'
          printf,1,'    nr.         level          color       write(0..no / 1..yes)'
          for i=0,levanz-1 do begin
             printf,1,i,lev(i),col(i),lab(i)
          end
         close,1
       end

       close,1
       openr,1,fname
        readf,1,levanz
        ttt='' & a=1 & b=1. & c=1 & d=1
        readf,1,ttt
        lev=fltarr(levanz)
        lab=intarr(levanz)
        col=intarr(levanz)
        for i=0,levanz-1 do begin
          readf,1,a,b,c,d
          lev(i)=b
          col(i)=c
          lab(i)=d
        endfor
       close,1
     end
end
;================================================================
pro _farben2,fill,form,levanz,levanf,levend,levlog,colanf,colsch $
           ,balkq,dx,dy,xa,ya,balkint,gr,di,format_balk,skalfak=skalfak $
           ,legend_balk,anz_leg_balk,leg_balk
   if n_elements(skalfak) eq 0 then skalfak=1.0
   clicmal:
   n=15
   beschr=strarr(n+1)
   beschr(0)='SELECT COLORS'
   beschr(1)=   'begin color step ('+string(colanf)+')'
   beschr(2)='interval color step ('+string(colsch)+')'
   if balkq eq 0 then beschr(3)='Portrait bar  (Landscape)' $
                 else beschr(3)='Landscape bar (Portrait)'
   beschr(4) ='width dx      ('+string(dx)+')'
   beschr(5) ='begin of xa ('+string(xa)+')'
   beschr(6) ='width dy      ('+string(dy)+')'
   beschr(7) ='begin of ya ('+string(ya)+')'
   beschr(8) ='interval of descriptions ('+string(balkint)+')'
   beschr(9) ='scal factor ('+string(skalfak)+')'
   beschr(10)='format of legend:'+format_balk
   
   if legend_balk eq 0 then beschr(11)='standard value of legend (special)' $
                       else beschr(11)='special  value of legend, consider scalfactor (standard)'
   if legend_balk eq 0 then beschr(12)='' $
                       else beschr(12)='change special  value of legend'
   beschr(13)='Plot all color-numbers'
   beschr(14)='Plot color bar'
   beschr(n)='     return     '
   _clic3,n,beschr,auswahl,zeilen=16
   
   if  auswahl eq  1 then _intein,colanf,beschr(1)
   if  auswahl eq  2 then _intein,colsch,beschr(2)
   if  auswahl eq  3 then balkq=1-balkq
   if  auswahl eq  4 then _fltein,dx,beschr(4)
   if  auswahl eq  5 then _fltein,xa,beschr(5)
   if  auswahl eq  6 then _fltein,dy,beschr(6)
   if  auswahl eq  7 then _fltein,ya,beschr(7)
   if  auswahl eq  8 then _intein,balkint,beschr(8)
   if  auswahl eq  9 then _fltein,skalfak,beschr(9)
   if  auswahl eq 10 then _strein,format_balk,'e.g. (in brackets !!): (f10.5)'
   if  auswahl eq 11 then legend_balk=1-legend_balk
   if (auswahl eq 12) and  (legend_balk eq 1 ) then $
                          _legend_balk_ein,anz_leg_balk,leg_balk,levlog,levanf,levend,skalfak
   if  auswahl eq 13 then begin
     ;-----Dimension
     lang=5
     z=fltarr(lang,lang)
     zx=fltarr(lang*lang)
     zy=fltarr(lang*lang)
     x=fltarr(lang)
     y=fltarr(lang)
     x(0)=0 & y(0)=0
     x(1)=0 & y(1)=0.1
     x(2)=0.1 & y(2)=0.1
     x(3)=0.1 & y(3)=0
     x(4)=0 & y(4)=0
     window,5,Title='Colors',XSize=880,Ysize=870
        k=0
        for i=0,14 do begin
        for j=0,17 do begin
           x(0)=i/19.       & y(0)=j/19.
           x(1)=x(0)        & y(1)=y(0)+1/20.
           x(2)=x(0)+1/20.  & y(2)=y(0)+1/20.
           x(3)=x(0)+1/20.  & y(3)=y(0)
           x(4)=x(0)        & y(4)=y(0)
           polyfill,x,y,/normal,color=k
           strx=string(k,format="(i3)")
           m=256
           if k gt 100 then m=0
           xyouts,x(0),y(0),strx,charsize=1.5,color=m,/normal
           k=k+1
        endfor
        endfor
   end
   if auswahl eq 14 then begin
     ;---Darstellen
     druck=0 & drname='bar.ps'
     _fenster,druck,drname,form
     _levberech,levanf,levend,levlog,levanz,lev
     col=fltarr(levanz)
     col(0)=colanf
     for i=1,levanz-1 do col(i)=col(i-1)+colsch
    _balken2,lev,balkq,xa,dx,ya,dy,col,balkint,gr,di,skalfak=skalfak,forma=format_balk $
            ,legend_balk,anz_leg_balk,leg_balk,levanf,levend
    _druckende,druck,drname1,form3
   end ;auswahl 9
   if auswahl ne n then goto,clicmal
end
;=================================================================
pro _legend_balk_ein,number_val,feld_val,levlog,levanf,levend,skalfak
      ;---'number_val   ... Anzahl der Legendenwerte
      ;---'feld_val     ... Werte 
      ;---'levlog,levanf,levend,skalfak  ...level Werte     
      
      nochmal:   
      bes=['Values special legend' $
          ,'Number of values  ('+string(number_val)+')' $
          ,'change values'  $
          ,'standard'  $
          ,'return']
      _clic3,4,bes,auswahl2

      if auswahl2 eq 1 then begin
         ihilf=0
         if number_val gt 0 then begin
            ihilf=number_val
            hilf=feld_val
         end
         _intein,number_val,bes(1)
         if number_val gt 0 then begin
            feld_val=fltarr(number_val)
            if ihilf gt 0 then begin
              for i=0, min([ihilf,number_val])-1 do feld_val(i)=hilf(i)
            end else begin
              auswahl2=3
            end 
         end
         ;print,'======1======',feld_val
      end
      if auswahl2 eq 2 then begin
        nochmal2:
        bes=strarr(number_val+2)
        bes(0)='select and change'
        for mm=0,number_val-1 do bes(mm+1)=string(feld_val(mm))
        bes(number_val+1)='return'
        _clic3,number_val+1,bes,auswahl3
        if auswahl3 lt number_val+1 then begin
          a=feld_val(auswahl3-1)
          _fltein,a,'input legend value'
          feld_val(auswahl3-1)=a
        end
        if auswahl3 ne number_val+1 then goto,nochmal2
      end
      ;'----auswahl2==3 standard
      if auswahl2 eq 3 then begin
        if levlog eq 0 then begin
          sch=(levend-levanf)/(number_val-1)
          feld_val(0)=levanf
          for i=1,number_val-2 do feld_val(i)=feld_val(i-1)+sch
          feld_val(number_val-1)=levend
        end
        if levlog eq 1 then begin
          sch=(alog(levend)-alog(levanf))/(number_val-1)
          feld_val(0)=levanf
          for i=1,number_val-2 do feld_val(i)=feld_val(i-1)*exp(sch)
          feld_val(number_val-1)=levend
        end
        feld_val=feld_val*skalfak
        ;print,'===stand=========',feld_val
      end      
;      ;'----auswahl2==4 min/max of standard
;      if auswahl2 eq 4 then begin
;        einschnitt4:
;        bes=strarr(5)
;        bes(0)='Number:'+string(number_val)+' min:'+string(hilfmin(schnitt))+' max:'+string(hilfmax(schnitt))  
;        bes(1)='min. value:'+string(wertschnitt(0))      
;        bes(2)='max. value:'+string(wertschnitt(number_val-1))        
;        bes(3)='ok'        
;        _clic3,3,bes,auswahl4
;        hilf0=wertschnitt(0)
;        hilf1=wertschnitt(number_val-1)
;        if auswahl4 eq 1 then begin
;           _fltein,hilf0,'min:'+string(hilfmin(schnitt))
;           wertschnitt(0)=hilf0
;        end   
;        if auswahl4 eq 2 then begin
;           _fltein,hilf1,'max:'+string(hilfmax(schnitt))
;           wertschnitt(number_val-1)=hilf1
;        end          
;        if auswahl4 ne 3 then goto,einschnitt4
;        xyanz=1
;        ks=intarr(xyanz)
;        wertschnitt=fltarr(number_val)
;        wertschnitt(0)=hilf0
;        wertschnitt(number_val-1)=hilf1
;        for i=1,number_val-2 do wertschnitt(i) $
;          =wertschnitt(0)+i*(wertschnitt(number_val-1)-wertschnitt(0))/(number_val-1.0) 
;        wertschnitt=wertschnitt(sort(wertschnitt)) 
;      end
;      ;'----auswahl2==5 Schnitte'
;      if auswahl2 eq 5 then begin
;        bes=strarr(number_val)
;        aus=intarr(number_val)
;        for mm=0,number_val-1 do bes(mm)=string(wertschnitt(mm))
;        for mm=0,xyanz-1 do aus(ks(mm))=1
;        text='SELECT'
;        _clic4,text,bes,aus
;        ks=where(aus eq 1)
;        hilf=size(ks) 
;        if hilf(0) ge 1 then xyanz=hilf(1) else xyanz=0
;      end

      ;'----auswahl2==4 return'
      if auswahl2 ne 4 then goto,nochmal 

end
;=================================================================
pro _balken2,lev,balkq,xa,dx,ya,dy,col,balkint,gr,di,skalfak=skalfak,forma=forma $
             ,legend_balk,anz_leg_balk,leg_balk,levanf,levend
     
     if n_elements(skalfak) eq 0 then skalfak=1.0
     
     levneu=lev*skalfak
     leva  =levanf*skalfak
     leve  =levend*skalfak
     maxlev=max(levneu)
     if abs( min(levneu) ) gt maxlev then maxlev=min(levneu)

     hilf=size(levneu)
     x=fltarr(5) & y=fltarr(5)
     bes=strarr(hilf(1))
     k=balkint-1
     if balkq eq 0 then begin
      for i=0,hilf(1)-1 do begin
        k=k+1
        if k gt balkint then k=1
        _real2string,maxlev,levneu(i),s,forma=forma
        bes(i)=s
        x(0)=xa              & y(0)=ya+i*dy
        x(1)=x(0)            & y(1)=y(0)+dy
        x(2)=x(0)+dx         & y(2)=y(1)
        x(3)=x(2)            & y(3)=y(0)
        x(4)=x(0)            & y(4)=y(0)
        polyfill,x,y,/normal,color=col(i)
        plots,x,y,/normal,thick=di
        if legend_balk eq 0 and k eq balkint then xyouts,x(0)+1.5*dx,y(0)+0.2*dy,bes(i),/normal $
                                    ,charsize=gr,charthick=di
        ;if                       k eq balkint then xyouts,x(0)+1.5*dx,y(0)+0.2*dy,bes(i),/normal $
        ;                            ,charsize=gr,charthick=di
        ;print,y(0)+0.2*dy,bes(i)
      end
      if legend_balk eq 1 then begin
        ddlev=leve-leva
        maxlev=max(leg_balk)
        if abs( min(leg_balk) ) gt maxlev then maxlev=min(leg_balk) 
        for i=0,anz_leg_balk-1 do begin
         _real2string,maxlev,leg_balk(i),s,forma=forma
         ddyy=(hilf(1)-1)*dy/ddlev
         xyouts,xa+1.5*dx,ya+(leg_balk(i)-leva)*ddyy +0.2*dy,s,/normal,charsize=gr,charthick=di
         ;print,ya+(leg_balk(i)-leva)*ddyy +0.2*dy,s,(hilf(1)-1),(hilf(1)-1)*dy,ddlev        
        end
      end
     
     end
     if balkq eq 1 then begin
      for i=0,hilf(1)-1 do begin
        k=k+1
        if k gt balkint then k=1
        _real2string,maxlev,levneu(i),s,forma=forma
        bes(i)=s
        x(0)=xa+i*dx         & y(0)=ya
        x(1)=x(0)            & y(1)=y(0)+dy
        x(2)=x(0)+dx         & y(2)=y(1)
        x(3)=x(2)            & y(3)=y(0)
        x(4)=x(0)            & y(4)=y(0)
        polyfill,x,y,/normal,color=col(i)
        plots,x,y,/normal
        if legend_balk eq 0 and k eq balkint then xyouts,x(0)+dx/2,y(0)-1.5*dy,bes(i),/normal $
                                    ,charsize=gr,charthick=di
        ;if                       k eq balkint then xyouts,x(0)+dx/2,y(0)-1.5*dy,bes(i),/normal $
        ;                            ,charsize=gr,charthick=di
      end
      if legend_balk eq 1 then begin
        ddlev=leve-leva
        maxlev=max(leg_balk)
        if abs( min(leg_balk) ) gt maxlev then maxlev=min(leg_balk) 
        for i=0,anz_leg_balk-1 do begin
         _real2string,maxlev,leg_balk(i),s,forma=forma
         ddxx=(hilf(1)-1)*dx/ddlev
         xyouts,xa+(leg_balk(i)-leva)*ddxx  +dx/2 ,ya-1.5*dy,s,/normal,charsize=gr,charthick=di        
        end
      end
     end
end
;================================================================
pro _farbtabelle,farbtab
    ;---Nr. Farbtabelle eingeben
    ;---Auswahl Farbtabelle
    print,' 0-       B-W LINEAR  14-        STEPS    28-  Hardcandy'
    print,' 1-       BLUE/WHITE  15-STERN SPECIAL    29-  Nature'
    print,' 2-  GRN-RED-BLU-WHT  16-         Haze    30-     Ocean'
    print,' 3-  RED TEMPERATURE  17-Blue-Pastel-R    31-  Peppermint'
    print,' 4-BLUE/GREEN/RED/YE  18        Pastels   32-  Plasma'
    print,' 5-     STD GAMMA-II  19-Hue Sat Light    33-  Blue-Red'
    print,' 6-            PRISM  20-Hue Sat Light    34-  Rainbow'
    print,' 7-       RED-PURPLE  21-Hue Sat Value 1  35-  Blue Waves'
    print,' 8-GREEN/WHITE LINEA  22-Hue Sat Value 2  36-  Volcano'
    print,' 9-GRN/WHT EXPONENTI  23-Purple-Red+Stri  37-     Waves'
    print,'10-       GREEN-PINK  24-          Beach  38-  Rainbow18'
    print,'11-         BLUE-RED  25-      Mac Style  39-  Rainbow + white'
    print,'12-         16 LEVEL  26-          Eos A  40-  Rainbow + black'
    print,'13-          RAINBOW  27-          Eos B
    gtext='Eingabe der Farb=Nr.: 0..40'
    _intein,farbtab,gtext
    loadct,farbtab
end
;===============================================================
