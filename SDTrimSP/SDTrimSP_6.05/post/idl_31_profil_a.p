@/afs/ipp/home/a/aam/public/archiv/TRIDYN/SDTrimSP/post/window.inc 
@/afs/ipp/home/a/aam/public/archiv/TRIDYN/SDTrimSP/post/level.inc 
@/afs/ipp/home/a/aam/public/archiv/TRIDYN/SDTrimSP/post/achsen.inc 
pro run
;input :E0_31_target.dat           (einzel=0)
;depth_recoil.dat
;depth_proj.dat


;   or :dsurf_f_31.dat      (einzel=1)
;       backscatt_f_31.dat
;       backsputt_f_31.dat
;       comp_surf_f_31.dat
;       density_d_f_31.dat
;       comp_steady_state_d_31.dat
;output:E031_dyn.ps

;go to words
;      picture

  _start
  druck=0
  print,'druck:0/1    0...screen 1...postscript'
  ;read,druck
  drname='E031a_profil.ps'
  form=0
  text='' & text1='' & bez=''& textdatum=''
  jflu=1
  idep=1
  ncp=1
  a=1.0d0 & b=1.0d0 & c=1.0d0
  loadct,39
  close,1
  if druck eq 0 then farbe=[255,250,80,150,100,200,40,215,180,255,250,150] else $
                     farbe=[0  ,250,80,150,100,200,40,215,180,0  ,250,150]
   xx=[0,1.0]   
   yy=[2000,0] 
   flux        = 0.001
 
  ;--- 0... read 'E0_31_target.dat'  1...read  *_f_31.dat...
  einzel=0
  print,'------------------------'
  print,'INPUT: read E0_31_target.dat'
  close,31
  openr,31,'E0_31_target.dat'
   readf,31,textdatum
   readf,31,bez
   readf,31,text
   readf,31,text
   nh=long(1) & idep=1 & ncp=1 & idout=1 & nqxm=1 & npro=1 & idrel=1  
   readf,31, nh,idep,ncp,idout, nqxm , npro, idrel
   print, '  ncp=',ncp
   print, '  NH =',nh,' max_idep=',idep,' ncp=',ncp, ' idepth=',idout

   jflu=nh/idout+1
;print,jflu
;read,jflu
;jflu=kkk
   flu =dblarr(jflu) 
   surf=dblarr(jflu)
   flic=dblarr(jflu)
   nproj  =dblarr(ncp,jflu)
   sumnproj=dblarr(jflu)
   iback_p=dblarr(ncp,jflu)
   eback_p=dblarr(ncp,jflu)
   itran_p=dblarr(ncp,jflu)
   etran_p=dblarr(ncp,jflu)
   iback_r=dblarr(ncp,jflu)
   eback_r=dblarr(ncp,jflu)
   itran_r=dblarr(ncp,jflu)
   etran_r=dblarr(ncp,jflu)
   e_tot_p=dblarr(ncp,jflu)
   reem  =dblarr(ncp,jflu) 
   irec  =dblarr(ncp,jflu) 
   ichem =dblarr(ncp,jflu)
   backscatt=dblarr(ncp,jflu)
   backsputt=dblarr(ncp,jflu)
   defects=dblarr(ncp,jflu)
   comp_surf=dblarr(ncp,jflu)
   comp_dep =dblarr(ncp,idep)
   help=dblarr(ncp)
   symbol=strarr(ncp)
   text1=strarr(ncp+1)
   quxi=dblarr(idep,jflu,ncp)
   dep=dblarr(idep) 
   dens=dblarr(jflu,idep) 
   bild=0
   for i=0,jflu-1 do begin

     if (i eq 0)then begin
             ; (31, '(10a10)') symbol(1:ncp)
        readf,31,symbol,format='(20a10)' & symbol(*)=strtrim(symbol(*),1)
        text1(1:ncp)=symbol(0:ncp-1)  
        text2=text1
        text2(1)='[-]'
        ;for j=1,16 do begin ;with irec
        for j=1,16 do begin
          readf,31,text ;(31, '(a)') text
          ;print,text
        end 
     end
    if i eq 0 then iii=1 else iii=5
    iii=1
    for ii=1,iii do begin
      readf,31,a,b;(31,*)fluc(i),depd(i)
      flu(i)=a
      surf(i)=b
      readf,31,text ;(31,*)csf(i,1:ncp) surf composition
;print,text
       readf,31,text;(31,*)flib(i,1:5,1:npro)
;print,text
      readf,31,text;(31,*) ard
;print,text      
      readf,31,help;(31,*)
;print,help(0)
      nproj(*,i)=help(*)
      sumnproj(i)=total(help)
      readf,31,help;(31,*)
;print,help(0)
      iback_p(*,i)=help(*)
      readf,31,help;(31,*)
;print,help(0)
      eback_p(*,i)=help(*)
      readf,31,help;(31,*)
;print,help(0)
      itran_p(*,i)=help(*)
      readf,31,help;(31,*)
;print,help(0)
      etran_p(*,i)=help(*)
      readf,31,help;(31,*)
;print,help(0)
      iback_r(*,i)=help(*)
      readf,31,help;(31,*)
;print,help(0)
      eback_r(*,i)=help(*)
      readf,31,help;(31,*)
;print,help(0)
      itran_r(*,i)=help(*)
      readf,31,help;(31,*)
;print,help(0)
      etran_r(*,i)=help(*)
      readf,31,help;(31,*)
;print,help(0)
      e_tot_p(*,i)=help(*)
      readf,31,help;(31,*)
;print,help(0)
      reem(*,i)=help(*) 
      readf,31,help;(31,*)
      ichem  (*,i)=help(*)

      ;readf,31,help;(31,*)
      ;irec(*,i)=help(*) 
      
      if (i eq 0) then begin
        readf,31,text ;(31, '(a)') text
        readf,31,text ;(31, '(a)') text
      end
      for mm = 0, idep-1 do begin
        readf,31,a,b,help ;(31,*) xxx(mm), dns(i,mm), (quxi(mm,i,jp),jp=1,ncp)
        dep(mm)=a
        dens(i,mm)=b
        comp_dep(*,mm)=help(*)
      end
    end ;---iii
    

  xytext=['fluence [atoms/A**2]','surface [A]']
  
  ;---prepare 6 picture
  if i eq 0 then begin
    _fenster,druck,drname,form,nummer=9  
    spa=2 & zei=3
    !P.Multi=[0,spa,zei] 
    if form eq 0 then pos=[0.00, 0.05 , .99, .96]
    if form eq 1 then pos=[0.01, 0.1, .98, .97]
    ric=0
    dspa=25 ;% Zwischenraum
    dzei=25 ;% Zwischenraum
    dx=(pos(2)-pos(0))/spa
    dy=(pos(3)-pos(1))/zei
    isp=0
    jze=0
    if ric eq 0 then jze=1 else isp=1
    druckfak=1
    if druck eq 1 then druckfak=.8
    dd=1.5*druckfak
    gr=2.3*druckfak
    gd=1.5*druckfak
  end
   
   ;if i mod 1 eq 0 then begin
  ;if i eq 0  or i eq 10 or i eq 20 or i eq 30 or i eq 40 or i eq 100 then begin
   if i eq 0  or i eq 20 or i eq 40 or i eq 60 or i eq 80 or i eq 100 then begin
   
    print,i,' flu= ', flu(i)
    bild=bild+1
    
    ;---atomic fraction picture 6
    _posit,ric,isp,jze,spa,zei,dspa,dzei,dx,dy,pos,pos1
    plot,comp_dep(0,*),dep,ytitle='depth [A]' $
    ,xrange=xx  $ 
    ,yrange=yy $
    ,title='atomic fraction time='+string(flu(i)/flux+0.001 ,format='(i5)')+'s' $
    ,thick=dd,charsize=gr,charthick=gd,position=pos1,/nodata
    
    j=1
    for k=0,ncp-1 do begin  ;,ncp-1 do begin
      c=dblarr(idep)
      d=dblarr(idep)
      for j2=0,idep-1 do begin
        c(idep-1-j2)=comp_dep(k,j2)
        d(idep-1-j2)=dep(j2)
      end
      oplot,c,d,color=farbe(k),thick=dd
      if bild eq 5 then xyouts,pos1(0)-.022*ncp+(pos1(2)-pos1(0))/5.9*(j) ,pos1(1)*.68 $
      ,text1(k+1),charsize=gr/2,charthick=gd,/normal,color=farbe(k)  
      j=j+1
    end
   
   end ;if
   if bild eq 6 then goto,ende
  end ;---i
  close,31
   ende:
   xyouts,.10,.05,bez,charsize=gr*.7,charthick=gd,/normal   
     _druckende,druck,drname,form

end
