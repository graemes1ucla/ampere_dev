@/afs/ipp/home/a/aam/public/archiv/TRIDYN/SDTrimSP/post/window.inc 
@/afs/ipp/home/a/aam/public/archiv/TRIDYN/SDTrimSP/post/level.inc 
@/afs/ipp/home/a/aam/public/archiv/TRIDYN/SDTrimSP/post/achsen.inc 
pro run

;input 

;   E0_31_target.dat          

;picture 1 static idrel=1
;   depth_proj.dat
;   depth_recoil.dat

;picture 4 static idrel=1
;   mpe_ex_p.dat


;picture 5 static idrel=1
;   morigin_ex_bs.dat

;go to words
;      picture

  _start
  
  ;---all values
  
  druck=-1 ;  1 -1 0


  ;print,'druck:0/1/2    0...screen  1...postscript   2...Serie'
  ;aaa=' '
  ;read,aaa
  ;if aaa eq '1' then druck=1

  ;---X-Achse
  ixxaa=1  ;0... x-achse auto  1..._achslevels 
  
  ;;--- H        Sp2       Sp3      Sp3H   auch fuer IAEA
  ;---sp3=sp3(2)+sp3H(3)  if ncp=4,5  4...z.B. H->c  5...z.B.H -> CW
  mitsp=0     ;0...sp2,sp3  1...sp2+sp3
  
  drname='E031_dyn.ps'
  form=0
  text='' & text1='' & bez=''& textdatum=''
  jflu=1
  idep=1
  ncp=1
  a=1.0d0 & b=1.0d0 & c=1.0d0
  loadct,39
  close,1
  if druck le 0 then farbe=[255,215,250,80,150,40,180,255,250,150] else $
                     farbe=[0  ,215,250,80,150,40,180,0  ,250,150]
  if druck le 0 then farbe=[255,150,250,80,215,40,180,255,250,150] else $
                     farbe=[0  ,150,250,80,215,40,180,0  ,250,150]
 
 
 iyr1=0 & yr1=[-500,500]   ;idrel=0 [25,0]   
 iyr2=0 & yr2=[0,0.52] 
 iyr3=0 & yr3=[0,0.1d0] 
 iyr4=0 & yr4=[500,0] 
 iyr5=0 & yr5=[1000,0] 
 iyr6=0 & yr6=[20,0] ;yr6=[50,0]

 ;iyr1=0 & yr1=[25,0]   ;idrel=0 [25,0]   
 ;iyr4=0 & yr4=[25,0] 
 

  ;---idrel=0
  ;iyr5=1 & yr5=[50.,0.0] 

;achtung    sp3h=sprh+sp3+sp2

 eee=10000  ;druck=2 
 if druck ge 0 then eee=2

  kkk=1
  ;kkk=1000 ;gleich bis ende
  
  ;for kkk=2,eee,101 do begin
  while kkk lt eee do begin
  if kkk eq 1 then begin
     kkk=kkk+1
  end else begin
     kkk=kkk+1    ;kkk+10 
  end
  print,'------------------------',kkk,eee

  nn   ='./E0_31_target.dat'      & ttnn='diff'
  nn2  ='./E0_31_target.gas'      & ttn2='outgas' 

  ;nn2 ='~/TRIDYN/SDTrimSP_5.08/case/DO_WCrY/E0_31_target.ohne'  & ttn2='+O' 
  ;nn  ='~/TRIDYN/SDTrimSP_5.08/case/D_WCrY/E0_31_target.ohne'   & ttnn='red Es of W' 

 nn ='./E0_31_target.dat'  & ttnn='diff' 
 nn2='../Z_Cu_serie_dyn_200/E0_31_target001.dat' & ttn2='200'
 nn2='./E0_31_target.old' & ttn2='old'
 nn2=' '
 
  if nn2 eq ' ' then nn2=nn  
  ;---nn2 ne nn 
  close,31
  print,'---read: ',nn2
  openr,31,nn2
   readf,31,textdatum
   readf,31,bez
   readf,31,text
   readf,31,text
   nh=long(1) & idep=1 & ncp=1 & idout=long(1) & nqxm=1 & npro=1 & idrel=1  
   readf,31, nh,idep,ncp,idout, nqxm , npro, idrel
;nh=100000
   jflu=nh/idout+1 
;print,'jflu',jflu

  if druck eq -1 then begin
       ;---serie
      eee=jflu
      jflu=kkk  ;else kkk=jflu
   end
   flu =dblarr(jflu) 
   surf2=dblarr(jflu)
   flic=dblarr(jflu)
   nproj2  =dblarr(ncp,jflu)
   sumnproj2=dblarr(jflu)
   ard=dblarr(ncp,jflu)
   iback_p2=dblarr(ncp,jflu)
   eback_p=dblarr(ncp,jflu)
   itran_p=dblarr(ncp,jflu)
   etran_p=dblarr(ncp,jflu)
   iback_r2=dblarr(ncp,jflu)
   eback_r=dblarr(ncp,jflu)
   itran_r=dblarr(ncp,jflu)
   etran_r=dblarr(ncp,jflu)
   e_tot_p=dblarr(ncp,jflu)
   reem2  =dblarr(ncp,jflu) 
   irec  =dblarr(ncp,jflu) 
   ichem2 =dblarr(ncp,jflu)
   backscatt2=dblarr(ncp,jflu)
   backsputt2=dblarr(ncp,jflu)
   defects=dblarr(ncp,jflu)
   comp_surf2=dblarr(ncp,jflu)
   comp_dep2 =dblarr(ncp,idep)
   help=dblarr(ncp)
   symbol=strarr(ncp)
   text1=strarr(ncp+1)
   quxi=dblarr(idep,jflu,ncp)
   dep2=dblarr(idep) 
   dens=dblarr(jflu,idep) 
   
   for i=0,jflu-1 do begin
    if (i eq 0)then begin
       readf,31,symbol,format='(20a10)' & symbol(*)=strtrim(symbol(*),1)
       text1(1:ncp)=symbol(0:ncp-1)  
       text2=text1
       text2(1)='[-]'
       for j=1,16 do begin
         readf,31,text ;(31, '(a)') text
       end 
       if ncp ge 4 and mitsp eq 1 then begin
         ;---sp3=sp3(2)+sp3H(3)
         for iii=4,ncp-1 do text1(iii)=text1(iii+1)
         for iii=4,ncp-1 do text2(iii)=text2(iii+1)
       end
    end
    if i eq 0 then iii=1 else iii=5
    iii=1
    for ii=1,iii do begin
      while not eof(31) do begin
        readf,31,a,b;(31,*)fluc(i),depd(i)
        goto,weof2
      end 
      print,'EOF2 jflu2',i 
      ;stop
      weof2:
      flu(i)=a 
     
      surf2(i)=b
      readf,31,text ;(31,*)csf(i,1:ncp) surf composition
       readf,31,text;(31,*)flib(i,1:5,1:npro)
      readf,31,help;(31,*) ard
      ard(*,i)=help(*)
      readf,31,help;(31,*)
      nproj2(*,i)=help(*)
      sumnproj2(i)=total(help)
      readf,31,help;(31,*)
      iback_p2(*,i)=help(*)
      readf,31,help;(31,*)
      eback_p(*,i)=help(*)
      readf,31,help;(31,*)
      itran_p(*,i)=help(*)
      readf,31,help;(31,*)
      etran_p(*,i)=help(*)
      readf,31,help;(31,*)
      iback_r2(*,i)=help(*)
      readf,31,help;(31,*)
      eback_r(*,i)=help(*)
      readf,31,help;(31,*)
      itran_r(*,i)=help(*)
      readf,31,help;(31,*)
      etran_r(*,i)=help(*)
      readf,31,help;(31,*)
      e_tot_p(*,i)=help(*)
      readf,31,help;(31,*)
      reem2(*,i)=help(*) 
      readf,31,help;(31,*)
      ichem2  (*,i)=help(*)

      ;========== sp3=sp3(2)+sp3H(3) ===================
      if ncp ge 4 and mitsp eq 1 then begin
        ;---sp3=sp3(2)+sp3H(3)
        iback_r2(2,i)=iback_r2(2,i)+iback_r2(3,i)
        ichem2  (2,i)=ichem2  (2,i)+ichem2  (3,i)
        reem2   (2,i)=reem2   (2,i)+reem2   (3,i)
        iback_r2(3,i)=0.0
        ichem2  (3,i)=0.0
        reem2   (3,i)=0.0
        if ncp eq 5 then iback_r2(3,i)=iback_r2(4,i)
        if ncp eq 5 then ichem2  (3,i)=ichem2  (4,i)
        if ncp eq 5 then ichem2  (3,i)=ichem2  (4,i)
        if ncp eq 5 then reem2   (3,i)=reem2   (4,i)
        if ncp eq 5 then farbe(3)=farbe(4)
      endif
      if (i eq 0) then begin
        readf,31,text ;(31, '(a)') text
        readf,31,text ;(31, '(a)') text
      end
      kk=0 ;---fuer output profile---
      for mm = 0, idep-1 do begin
        readf,31,a,b,help ;(31,*) xxx(mm), dns(i,mm), (quxi(mm,i,jp),jp=1,ncp)
        dep2(mm)=a
        dens(i,mm)=b
        quxi(mm,i,*)=help(*)
        if ncp ge 4 and mitsp eq 1 then begin
         ;---sp3=sp3+sp3H
         quxi(mm,i,2)=quxi(mm,i,2)+quxi(mm,i,3)
         quxi(mm,i,3)=0.0
         if ncp eq 5 then quxi(mm,i,3)=quxi(mm,i,4)
        end
        if mm eq      0 then comp_surf2(*,i)=help(*)
        if i  eq jflu-1 then comp_dep2(*,mm)=help(*)
       if ncp ge 4 and mitsp eq 1 then begin
        ;---sp3=sp3+sp3H
        comp_dep2(2,mm)=comp_dep2(2,mm)+comp_dep2(3,mm) 
        comp_dep2(3,mm)=0.0
        if ncp eq 5 then comp_dep2(3,mm)=comp_dep2(4,mm)
       end
      end ;mm
      if ncp ge 4 and mitsp eq 1 then begin
       ;---sp3=sp3+sp3H
       comp_surf2(2,i)= comp_surf2(2,i)+comp_surf2(3,i) 
       comp_surf2(3,i)=0.0    
       if ncp eq 5 then comp_surf2(3,i) =comp_surf2(4,i)
      end
    end ;---iii
    end ;---i
  close,31


;======================================================================  
  print,'INPUT: read E0_31_target.dat'
  close,31
  ;nn='E0_31_target.dat'
  

  openr,31,nn
   readf,31,textdatum
   readf,31,bez
   readf,31,text
   readf,31,text
   nh=long(1) & idep=1 & ncp=1 & idout=long(1) & nqxm=1 & npro=1 & idrel=1  
   readf,31, nh,idep,ncp,idout, nqxm , npro, idrel
   ;npro=ncp   
   print, '  ncp=',ncp,'  npro=',npro
   print, '  NH =',nh,' max_idep=',idep,' ncp=',ncp, ' idepth=',idout
;nh=100000
   jflu=nh/idout+1 

;jflu=min([jflu,51])

;jflu=251
;print,jflu
;read,jflu
 
   if druck eq -1 then begin
       ;---serie
      eee=jflu
      jflu=kkk  ;else kkk=jflu
   end
   flu =dblarr(jflu) 
   surf=dblarr(jflu)
   flic=dblarr(jflu)
   nproj   =dblarr(ncp,jflu)
   sumnproj=dblarr(jflu)
   ard=dblarr(ncp,jflu)
   iback_p=dblarr(ncp,jflu)
   eback_p=dblarr(ncp,jflu)
   itran_p=dblarr(ncp,jflu)
   etran_p=dblarr(ncp,jflu)
   iback_r=dblarr(ncp,jflu)
   eback_r=dblarr(ncp,jflu)
   itran_r=dblarr(ncp,jflu)
   etran_r=dblarr(ncp,jflu)
   e_tot_p=dblarr(ncp,jflu)
   reem  =dblarr(ncp,jflu) 
   irec  =dblarr(ncp,jflu) 
   ichem =dblarr(ncp,jflu)
   backscatt=dblarr(ncp,jflu)
   backsputt=dblarr(ncp,jflu)
   defects=dblarr(ncp,jflu)
   comp_surf=dblarr(ncp,jflu)
   comp_dep =dblarr(ncp,idep)
   help=dblarr(ncp)
   symbol=strarr(ncp)
   text1=strarr(ncp+1)
   quxi=dblarr(idep,jflu,ncp)
   dep=dblarr(idep) 
   dens=dblarr(jflu,idep) 
   
   for i=0,jflu-1 do begin

    if (i eq 0)then begin
       readf,31,symbol,format='(20a10)' & symbol(*)=strtrim(symbol(*),1)
       text1(1:ncp)=symbol(0:ncp-1)  
       text2=text1
       text2(1)='[-]'
       for j=1,16 do begin
         readf,31,text ;(31, '(a)') text
         ;print,text
       end 
       if ncp ge 4 and mitsp eq 1 then begin
         ;---sp3=sp3(2)+sp3H(3)
         for iii=4,ncp-1 do text1(iii)=text1(iii+1)
         for iii=4,ncp-1 do text2(iii)=text2(iii+1)
       end
    end
    if i eq 0 then iii=1 else iii=5
    iii=1
    for ii=1,iii do begin
      while not eof(31) do begin
        readf,31,a,b;(31,*)fluc(i),depd(i)
        goto,weof
      end 
      print,'EOF jflu',i 
      stop
      
      weof:
;print,a
      flu(i)=a
      surf(i)=b
      readf,31,text ;(31,*)csf(i,1:ncp) surf composition
;print,text
       readf,31,text;(31,*)flib(i,1:5,1:npro)
;print,text
      readf,31,help;(31,*) ard
      ard(*,i)=help(*)
;print,help     
      readf,31,help;(31,*)
;print,help(0)
      nproj(*,i)=help(*)
      sumnproj(i)=total(help)
      ;print,nproj(*,i)/sumnproj(i)
      readf,31,help;(31,*)
;print,help(0)
      iback_p(*,i)=help(*)
      readf,31,help;(31,*)
;print,help(0)
      eback_p(*,i)=help(*)
      readf,31,help;(31,*)
;print,help(0)
      itran_p(*,i)=help(*)
      readf,31,help;(31,*)
;print,help(0)
      etran_p(*,i)=help(*)
      readf,31,help;(31,*)
;print,help(0)
      iback_r(*,i)=help(*)
      readf,31,help;(31,*)
;print,help(0)
      eback_r(*,i)=help(*)
      readf,31,help;(31,*)
;print,help(0)
      itran_r(*,i)=help(*)
      readf,31,help;(31,*)
;print,help(0)
      etran_r(*,i)=help(*)
      readf,31,help;(31,*)
;print,help(0)
      e_tot_p(*,i)=help(*)
      readf,31,help;(31,*)
;print,help(0)
      reem(*,i)=help(*) 
      
      readf,31,help;(31,*)
      ichem  (*,i)=help(*)

      ;readf,31,help;(31,*)
      ;irec(*,i)=help(*) 

      ;========== sp3=sp3(2)+sp3H(3) ===================
      if ncp ge 4 and mitsp eq 1 then begin
        ;---sp3=sp3(2)+sp3H(3)
        iback_r(2,i)=iback_r(2,i)+iback_r(3,i)
        ichem  (2,i)=ichem  (2,i)+ichem  (3,i)
        reem   (2,i)=reem   (2,i)+reem   (3,i)
        iback_r(3,i)=0.0
        ichem  (3,i)=0.0
        reem   (3,i)=0.0
        if ncp eq 5 then iback_r(3,i)=iback_r(4,i)
        if ncp eq 5 then ichem  (3,i)=ichem  (4,i)
        if ncp eq 5 then ichem  (3,i)=ichem  (4,i)
        if ncp eq 5 then reem   (3,i)=reem   (4,i)
        if ncp eq 5 then farbe(3)=farbe(4)
      endif

      if (i eq 0) then begin
        readf,31,text ;(31, '(a)') text
        readf,31,text ;(31, '(a)') text
      end
      
      kk=0 ;---fuer output profile---
      
      for mm = 0, idep-1 do begin
        readf,31,a,b,help ;(31,*) xxx(mm), dns(i,mm), (quxi(mm,i,jp),jp=1,ncp)

        dep(mm)=a
        dens(i,mm)=b
        quxi(mm,i,*)=help(*)

        if ncp ge 4 and mitsp eq 1 then begin
         ;---sp3=sp3+sp3H
         quxi(mm,i,2)=quxi(mm,i,2)+quxi(mm,i,3)
         quxi(mm,i,3)=0.0
         if ncp eq 5 then quxi(mm,i,3)=quxi(mm,i,4)
        end
        


goto,ohne_ausgabe
;BBBBBBBBBBBBBBBBB
  ;===start output profile===        
  if i eq jflu-1 and mm eq   0 then begin
    zz=0
    ;---sp3=sp3+sp3H
    if ncp ge 4 and mitsp eq 1 then zz=1
    ff='(a23,'+string(1+ncp-1-zz)+'(3H & ,a10),3H \\)'
    print,'center[A]  density[a/A^3]  atomic fraction [-]'
    print,'   xxx     &    dns     &',text1(1:ncp-zz)      ,format=ff
  end
  
  if i eq jflu-1 and mm lt 600 then begin
     if kk eq 0 then begin
      ak=a
      bk=b
      hk=quxi(mm,i,*)
      kk=1
      ff='(f10.5,'+string(1+ncp-zz)+'(3H & ,f10.5),3H \\)'
      ;---all values
      print,ak,bk,hk(0:ncp-1-zz),format=ff
      kk=0
     end else begin
      ak=a+ak
      bk=b+bk
      hk=quxi(mm,i,*)+hk
      kk=kk+1
      ;---dx=5  2..dx=900    20...dx=100
      if kk eq 10 then begin
        ff='(f10.5,'+string(1+ncp-zz)+'(3H & ,f10.5),3H \\)'
        print,ak/kk,bk/kk,hk(0:ncp-1-zz)/kk,format=ff
       kk=0
      end
    end  
  end
  ;===end output profile===           
ohne_ausgabe:       
        
        
        
       if mm eq      0 then comp_surf(*,i)=help(*) ;1.layer
       ;if mm eq      1 then comp_surf(*,i)=help(*) ;2.layer
       if i  eq jflu-1 then comp_dep(*,mm)=help(*)

       if ncp ge 4 and mitsp eq 1 then begin
        ;---sp3=sp3+sp3H
        comp_dep(2,mm)=comp_dep(2,mm)+  comp_dep(3,mm) 
        comp_dep(3,mm)=0.0
        if ncp eq 5 then comp_dep (3,mm)=comp_dep (4,mm)
       end
     
      end ;mm
 
      if ncp ge 4 and mitsp eq 1 then begin
       ;---sp3=sp3+sp3H
       comp_surf(2,i)= comp_surf(2,i)+comp_surf(3,i) 
       comp_surf(3,i)=0.0    
       if ncp eq 5 then comp_surf(3,i) =comp_surf(4,i)
      end
    
    end ;---iii
    end ;---i
  close,31
 
  
  
  
  
  if ncp ge 4 and mitsp eq 1 then begin
   ;---sp3=sp3(2)+sp3H(3)
   ncp=ncp-1
  end
  



 xytext=['fluence [atoms/A**2]','surface [A]']

  ;---prepare 6 picture
  druck1=druck
  if druck eq -1 then druck1=0
  if kkk eq 2 then _fenster,druck1,drname,form,nummer=9  
   spa=2 & zei=3
   !P.Multi=[0,spa,zei] 
   if form eq 0 then pos=[0.00, 0.05 , .99, .96]
   if form eq 1 then pos=[0.01, 0.1, .98, .97]
   ric=0
   dspa=25 ;% Zwischenraum
   dzei=25 ;% Zwischenraum
   dx=(pos(2)-pos(0))/spa
   dy=(pos(3)-pos(1))/zei
   isp=0
   jze=0
   if ric eq 0 then jze=1 else isp=1
   druckfak=1
   if druck eq 1 then druckfak=.8
   dd=1.5*druckfak
   gr=2.3*druckfak
   gd=1.5*druckfak
  
  ;---surface(idrel=0) or depth (idrel=1) picture 1
  _posit,ric,isp,jze,spa,zei,dspa,dzei,dx,dy,pos,pos1
  
  if idrel eq 0 then begin
    ;---surface
    if iyr1 eq 0 then begin
      yr1=[min(surf),max(surf)]
      if abs(yr1(1)-yr1(0)) lt 5 then begin 
        if yr1(0) lt 0 then  yr1(0)=min([yr1(0),-3])
        if yr1(1) gt 0 then  yr1(1)=max([yr1(1), 3])
      end
    end
    
    xx=[min(flu) ,max(flu)]
    yy=yr1
    ;xx=[0 ,10]
    ;yy=[-80,40]
    if abs(yy(0)-yy(1)) lt 1.0e-20 then yy=[-.1, .1]
    
   
   if ixxaa eq 1 then begin
      lmin=3 & lmax=4
      _achslevels,xx(0),xx(1),lmin,lmax,ndx,idx,strdx ;,forma=format_y
      plot,xx,yy,xtitle=xytext(0),ytitle=xytext(1),title='surface' $
       ,Xticks=ndx, Xtickv=idx ,Xtickname=strdx $
       ,yrange=[max(yy),min(yy)] $
       ,thick=dd,charsize=gr,charthick=gd,position=pos1,/nodata
    end else begin
        plot,xx,yy,xtitle=xytext(0),ytitle=xytext(1),title='surface' $
        ,yrange=[max(yy),min(yy)] $
       ,thick=dd,charsize=gr,charthick=gd,position=pos1,/nodata
    end
    oplot,flu,surf
    oplot,flu,surf2,linestyle=2
 ;oplot,[0,50],[0,-600],color=250   
    ;oplot,flu,surf-surf2,linestyle=3
  
   print,'nh   ',nh  
   print,'idout',idout   
   print,'jflu ',jflu 
   print,'dsurf ',surf(jflu-1)-surf2(jflu-1)  
  ;if  jflu eq 10 then read,aaa  
  ;if  jflu eq 20 then read,aaa  

  end else begin
    ;---depth
    maxidep=0.0d0
    maxdep=0.0d0
    nnn=1
    nprojb=1.d0
    idepb=1
    maxwert=0.0d0
  
    print,' -------'
    print,' Picture 1: stop position:' 
    jjjm=2
    if idrel lt 0 then jjjm=1
    for jjj=1,jjjm do begin
      if jjj eq 1 then openr,1,'depth_proj.dat'
      if jjj eq 2 then openr,1,'depth_recoil.dat'
      if jjj eq 1 then print,'  INPUT: read depth_proj.dat'
      if jjj eq 2 then print,'  INPUT: read depth_recoil.dat
      ;---header
      readf,1,text ;& print,text
      readf,1,text ;& print,text
      readf,1,nnn  ;& print,nnn
      for anz=1,nnn do begin
       readf,1,text   & print,anz,text
       readf,1,idepb,nprojb & print,anz,idepb,nprojb
       text2b=strarr(8)
       text3b=strarr(8)
       readf,1,text2b,format='(20a12)' & text2b(*)=strtrim(text2b(*),1)
       readf,1,text3b,format='(20a12)' & text3b(*)=strtrim(text3b(*),1)
       text3b(0)='depth [A]'
       text3b(1)='stop position'
       text2b(1)='particels (normalized to max)' ; to: nproj)
       wert=dblarr(7,idepb)
       hilf=dblarr(7)
       for i=0,idepb-1 do begin
        readf,1,hilf
        wert(*,i)=hilf(*)
       end   
       readf,1,text  & print,'suma',text
       
       ihilf=idepb-1
       for i=idepb-1,0,-1 do begin
         if  wert(1,i) eq 0.0 and wert(3,i) eq 0. and wert(4,i) eq 0. $
         and wert(5,i) eq 0.0 and wert(5,i) eq 0. then ihilf=i
       end
       if ihilf gt maxidep then begin
          maxidep=ihilf
          maxdep=wert(0,maxidep-1)
       end
       
       k=1 ;STOP-POSITION
       if nprojb > 0 then begin
         ihilf=where( wert(k,*) eq max(wert(k,*)) )
         if max(wert(k,*)) gt 0.0 then begin
          print,'       max stop (normalized to nproj) ',text1(anz),' ',max(wert(k,*))/nprojb ,' maxdepth :',wert(0,ihilf)
         end else begin
          print,'       max stop (normalized to nproj) ',text1(anz),' ',max(wert(k,*))/nprojb
         end
       end
       if jjj eq 2 then begin        
        readf,1,text   & print,anz,text
        readf,1,text   & print,anz,text
       end
       readf,1,text   ;& print,'leer',text
     end ;anz 
      
      close,1 
    end;jjj 
    
    k=1 ;STOP-POSITION
    xx=[0.0,1.0]
    if iyr1 eq 0 then yr1=[maxdep,0.0]

    lmin=3 & lmax=4
    _achslevels,xx(0),xx(1),lmin,lmax,ndx,idx,strdx ;,forma=format_y
    _achslevels,yr1(0),yr1(1),lmin,lmax,ndy,idy,strdy ;,forma=format_y
    
    plot,xx,yr1,xtitle=text2b(k),ytitle=text3b(0),title=text3b(k),/nodata $
       ,Xticks=ndx, Xtickv=idx ,Xtickname=strdx $
       ,Yticks=ndy, Ytickv=idy ,Ytickname=strdy $
       ,xrange=xx ,yrange=yr1 $
      ,thick=dd,charsize=gr,charthick=gd,position=pos1
    for jjj=1,jjjm do begin
    lll=0
      if jjj eq 1 then openr,1,'depth_proj.dat'
      if jjj eq 2 then openr,1,'depth_recoil.dat'
      ;---header
      readf,1,text
      readf,1,text
      readf,1,nnn
      for anz=1,nnn do begin
       readf,1,text
       readf,1,idepb,nprojb
       text2b=strarr(8)
       text3b=strarr(8)
       readf,1,text2b,format='(20a12)' & text2b(*)=strtrim(text2b(*),1)
       readf,1,text3b,format='(20a12)' & text3b(*)=strtrim(text3b(*),1)
       wert=dblarr(7,idepb)
       hilf=dblarr(7)
       for i=0,idepb-1 do begin
        readf,1,hilf
        wert(*,i)=hilf(*)
       end       
       readf,1,text  & print,'sumb',text
       ihilf=idepb-1
       if ihilf eq 0 then ihilf=1
       lsty=0
       if jjj eq 1 then lsty=2
        k=1 ;STOP-POSITION  (ihilf only to last stop level)
        oplot,wert(k,0:ihilf-1)/max(wert(k,0:ihilf-1)),wert(0,0:ihilf-1),color=farbe(lll),linestyle=lsty;,psym=-1
        ;---ohne redisplaced  E < Ed 
        ;if jjj eq 2 then oplot,(wert(k,0:ihilf-1)-wert(9,0:ihilf-1))/max(wert(k,0:ihilf-1)-wert(9,0:ihilf-1)*0),wert(0,0:ihilf-1),color=farbe(lll),linestyle=3;,psym=-1
       
        
        xyouts,.001,pos1(3)-(pos1(3)-pos1(1))/8*(lll+1) $
        ,text1(lll+1),charsize=gr/2,charthick=1.5,/normal,color=farbe(lll)  
        lll=lll+1  
        if jjj eq 2 then begin        
         readf,1,text   & print,anz,text
         readf,1,text   & print,anz,text
        end
        readf,1,text   ;& print,'leer',text
      end ;anz 
     close,1
    
    end ;jjj
    print,'   NR*NH or nproj:',nprojb,'   (values of normalized)'
  
  end ;stop ;pos.  idrel=1
 

  ;---particle reflection coefficient   picture 2
  print,' -------'
  print,' Picture 2: reflection :' 
  backscatt(*,0)=0
  backscatt2(*,0)=0
  for i=1,jflu-1 do begin
   for j=0,ncp-1 do begin  
    if (nproj(j,i)-nproj(j,i-1)) gt 0 then begin
     backscatt (j,i)=(iback_p (j,i)-iback_p (j,i-1))/(nproj (j,i)-nproj (j,i-1))
     backscatt2(j,i)=(iback_p2(j,i)-iback_p2(j,i-1))/(nproj2(j,i)-nproj2(j,i-1))
   end else begin
     backscatt(j,i)=0
     backscatt2(j,i)=0
    end
   end
  end ;i
  if jflu ge 5 then begin
   print,'  Scatt ---------------'
   print,'  Scatt.(',string(flu(jflu-5),format='(f6.2)'),'):',backscatt(0:ncp-1,jflu-5)
   print,'  Scatt.(',string(flu(jflu-4),format='(f6.2)'),'):',backscatt(0:ncp-1,jflu-4)
   print,'  Scatt.(',string(flu(jflu-3),format='(f6.2)'),'):',backscatt(0:ncp-1,jflu-3)
   print,'  Scatt.(',string(flu(jflu-2),format='(f6.2)'),'):',backscatt(0:ncp-1,jflu-2)
   print,'  Scatt.(',string(flu(jflu-1),format='(f6.2)'),'):',backscatt(0:ncp-1,jflu-1)
   hilf0=dblarr(ncp)
   hilf0(0:ncp-1)=  backscatt(0:ncp-1,jflu-5) $
                   +backscatt(0:ncp-1,jflu-4) $
                   +backscatt(0:ncp-1,jflu-3) $
                   +backscatt(0:ncp-1,jflu-2) $
                   +backscatt(0:ncp-1,jflu-1) 
    print,'  SctSumm 5:     ',hilf0(0:ncp-1)/5.0
   end
  
  if jflu ge 10 then begin
   hilf0=dblarr(ncp)
   hilf0(0:ncp-1)=  backscatt(0:ncp-1,jflu-10) $
                   +backscatt(0:ncp-1,jflu-9) $
                   +backscatt(0:ncp-1,jflu-8) $
                   +backscatt(0:ncp-1,jflu-7) $
                   +backscatt(0:ncp-1,jflu-6) $
                   +backscatt(0:ncp-1,jflu-5) $
                   +backscatt(0:ncp-1,jflu-4) $
                   +backscatt(0:ncp-1,jflu-3) $
                   +backscatt(0:ncp-1,jflu-2) $
                   +backscatt(0:ncp-1,jflu-1) 
    print,'  SctSumm 10:    ',hilf0(0:ncp-1)/10.0
   end
   ;---smooth

 for i=1,jflu-2 do begin
;  backscatt(*,i)=1.0/3.0*(backscatt(*,i-1)+backscatt(*,i)+backscatt(*,i+1))
;   iback_r(*,i)=1.0/3.0*(iback_r(*,i-1)+iback_r(*,i)+iback_r(*,i+1))
;   ichem  (*,i)=1.0/3.0*(ichem  (*,i-1)+ichem  (*,i)+ichem  (*,i+1))
;   reem   (*,i)=1.0/3.0*(reem   (*,i-1)+reem   (*,i)+ichem  (*,i+1))
 end 
 i=jflu-1
 ;if i gt 1 then backscatt(*,i)=1.0/3.0*(backscatt(*,i-1)+backscatt(*,i)+backscatt(*,i-2))

 for i=2,jflu-3 do begin
;   backscatt(*,i)=1.0/5.0*(backscatt(*,i-2)+backscatt(*,i-1)+backscatt(*,i)+backscatt(*,i+1)+backscatt(*,i+2))
;   iback_r(*,i)=1.0/5.0*(iback_r(*,i-2)+iback_r(*,i-1)+iback_r(*,i)+iback_r(*,i+1)+iback_r(*,i+2))
;   ichem  (*,i)=1.0/5.0*(ichem  (*,i-2)+ichem  (*,i-1)+ichem  (*,i)+ichem  (*,i+1)+ichem  (*,i+2))
;   reem   (*,i)=1.0/5.0*(reem   (*,i-2)+reem   (*,i-1)+reem   (*,i)+ichem  (*,i+1)+ichem  (*,i+2))
 end

 
  _posit,ric,isp,jze,spa,zei,dspa,dzei,dx,dy,pos,pos1    
  xx=[0.0 ,max(flu)]
  hilf=backscatt(0:npro-1,*)
  ;if min(hilf) eq 0 then hilf(where(hilf eq 0 ))=+10
  yy=[min(backscatt(0:npro-1,*)) ,max(backscatt(0:npro-1,*))]
  yy=[min(hilf                 ) ,max(backscatt(0:npro-1,*))]
  yy(1)=yy(1)+0.005
  ;yy(1)=0.013
  
  lmin=3 & lmax=4
  
  if iyr2 eq 0 then yr2=yy
  
  fff='(f6.3)'
  if yr2(1) lt 0.001 then fff='(f6.4)'
  if yr2(1) lt 0.0011 then fff='(f6.4)'
  if yr2(1) lt 0.00011 then fff='(f7.5)'
  if yr2(1) lt 0.000011 then fff='(f7.6)'
  _achslevels,yr2(0),yr2(1),lmin,lmax,ndy,idy,strdy,forma=fff

  if ixxaa eq 1 then begin
      lmin=3 & lmax=4
      _achslevels,xx(0),xx(1),lmin,lmax,ndx,idx,strdx ;,forma=format_y
     plot,xx,yr2,xtitle=xytext(0),ytitle=' ',title='part. refl. coeff.',/nodata $
     ,Xticks=ndx, Xtickv=idx ,Xtickname=strdx $
     ,Yticks=ndy, Ytickv=idy ,Ytickname=strdy $
     ,xrange=xx ,yrange=yr2 $
     ,thick=dd,charsize=gr,charthick=gd,position=pos1
  
  end else begin
    plot,xx,yr2,xtitle=xytext(0),ytitle=' ',title='part. refl. coeff.',/nodata $
     ;,Xticks=ndx, Xtickv=idx ,Xtickname=strdx $
     ,Yticks=ndy, Ytickv=idy ,Ytickname=strdy $
     ,xrange=xx ,yrange=yr2 $
     ,thick=dd,charsize=gr,charthick=gd,position=pos1
   end
     
  j=1
  for i=0,npro-1 do begin
    if idrel eq 0 then oplot,flu(1:jflu-1),backscatt(i,1:jflu-1),color=farbe(i)
    if idrel eq 0 and nn2 ne nn then oplot,flu(1:jflu-1),backscatt2(i,1:jflu-1),color=farbe(i),linestyle=2

    if idrel eq 1 then begin
      bmittel=dblarr(jflu)
      bmittel(0)=0
      for jj=1,jflu-1 do begin
        bmittel(jj)=(bmittel(jj-1)*(jj-1)+backscatt(i,jj))/jj
      end
      oplot,flu(1:jflu-1),bmittel(1:jflu-1),color=farbe(i)
      ;print,bmittel(1:jflu-1)
    end
    xyouts,.501,pos1(3)-(pos1(3)-pos1(1))/8*(j) $
    ,text1(i+1),charsize=gr/2,charthick=gd,/normal,color=farbe(i)  
    j=j+1
  end
  xyouts,.501,pos1(3)-(pos1(3)-pos1(1))/8*(j) $~/TRIDYN/SDTrimSP/IAEA_H/H_CW_2000
  ,text2(1),charsize=gr/2,charthick=gd,/normal  
 
  ;---backsputter picture 3
  print,' -------'
  print,' Picture 3: backsputter :' 

  backsputt(*,0)=0
  bchem=backsputt
  bbrem=backsputt     
  backsputt2(*,0)=0
  bchem2=backsputt2
  bbrem2=backsputt2     
  tt1='  ?--------------'
  tt2='  ?--------------'
  tt3='  ?---------------'

  for i=1,jflu-1 do begin
   for j=0,ncp-1 do begin  
    if ( sumnproj(i)-sumnproj(i-1) ) gt 0 then begin
     
     s=0 ; 0...backsputtering calculation with first projectiles of species
     ;s=5
     if ncp eq 6 then begin
       ;if j eq 0 then s=0        ; first
       ;if j eq ncp-1 then s=ncp-1 ; last  on last
     end
     if nproj(s,i)-nproj(s,i-1) gt 0 then begin
     tt1='  Sputt --------------'
     tt2='  Chem  --------------'
     tt3='  Reem ---------------'
     backsputt(j,i)=(iback_r (j,i) -iback_r (j,i-1)  )/(nproj(s,i)-nproj(s,i-1))
         bchem(j,i)=(ichem   (j,i) -ichem   (j,i-1)  )/(nproj(s,i)-nproj(s,i-1))
         bbrem(j,i)=(reem    (j,i) -reem    (j,i-1)  )/(nproj(s,i)-nproj(s,i-1))
    backsputt2(j,i)=(iback_r2(j,i) -iback_r2(j,i-1)  )/(nproj2(s,i)-nproj2(s,i-1))
        bchem2(j,i)=(ichem2  (j,i) -ichem2  (j,i-1)  )/(nproj2(s,i)-nproj2(s,i-1))
        bbrem2(j,i)=(reem2   (j,i) -reem2   (j,i-1)  )/(nproj2(s,i)-nproj2(s,i-1))
    
     backsputt(j,i)=(iback_r (j,i) -iback_r (j,i-1)  )/(sumnproj(i)-sumnproj(i-1))
         bchem(j,i)=(ichem   (j,i) -ichem   (j,i-1)  )/(sumnproj(i)-sumnproj(i-1))
         bbrem(j,i)=(reem    (j,i) -reem    (j,i-1)  )/(sumnproj(i)-sumnproj(i-1))
    backsputt2(j,i)=(iback_r2(j,i) -iback_r2(j,i-1)  )/(sumnproj2(i)-sumnproj2(i-1))
        bchem2(j,i)=(ichem2  (j,i) -ichem2  (j,i-1)  )/(sumnproj2(i)-sumnproj2(i-1))
        bbrem2(j,i)=(reem2   (j,i) -reem2   (j,i-1)  )/(sumnproj2(i)-sumnproj2(i-1))
    
     end
    end else begin
     backsputt(j,i)=0.0
     bchem(j,i)=0.0
     bbrem(j,i)=0.0
     backsputt2(j,i)=0.0
     bchem2(j,i)=0.0
     bbrem2(j,i)=0.0
    end
   end
  
  
  ;;---ausgabe yield als Tabelle
  ;if i mod 10 eq 0 then print,i/10,flu(i),backsputt(0:ncp-1,i)
  ;if i mod 10 eq 0 then print,i   ,flu(i),backsputt(0:ncp-1,i)
  
  
  end

  
  



if jflu ge 5 then begin
 print,tt1
 print,'  Sputt.(',string(flu(jflu-5),format='(f6.2)'),'):',backsputt(0:ncp-1,jflu-5)
 print,'  Sputt.(',string(flu(jflu-4),format='(f6.2)'),'):',backsputt(0:ncp-1,jflu-4)
 print,'  Sputt.(',string(flu(jflu-3),format='(f6.2)'),'):',backsputt(0:ncp-1,jflu-3)
 print,'  Sputt.(',string(flu(jflu-2),format='(f6.2)'),'):',backsputt(0:ncp-1,jflu-2)
 print,'  Sputt.(',string(flu(jflu-1),format='(f6.2)'),'):',backsputt(0:ncp-1,jflu-1)
 hilf=dblarr(ncp)
 hilf(0:ncp-1)=  backsputt(0:ncp-1,jflu-5) $
                +backsputt(0:ncp-1,jflu-4) $
                +backsputt(0:ncp-1,jflu-3) $
                +backsputt(0:ncp-1,jflu-2) $
                +backsputt(0:ncp-1,jflu-1) 
 print,'  SptSumm 5:     ',hilf(0:ncp-1)/5.0
 print,tt2
 print,'  Chem. (',string(flu(jflu-5),format='(f6.2)'),'):',bchem(0:ncp-1,jflu-5)
 print,'  Chem..(',string(flu(jflu-4),format='(f6.2)'),'):',bchem(0:ncp-1,jflu-4)
 print,'  Chem..(',string(flu(jflu-3),format='(f6.2)'),'):',bchem(0:ncp-1,jflu-3)
 print,'  Chem..(',string(flu(jflu-2),format='(f6.2)'),'):',bchem(0:ncp-1,jflu-2)
 print,'  Chem..(',string(flu(jflu-1),format='(f6.2)'),'):',bchem(0:ncp-1,jflu-1)
 hilf1=dblarr(ncp)
 hilf1(0:ncp-1)= bchem(0:ncp-1,jflu-5) $
                +bchem(0:ncp-1,jflu-4) $
                +bchem(0:ncp-1,jflu-3) $
                +bchem(0:ncp-1,jflu-2) $
                +bchem(0:ncp-1,jflu-1) 
 ;if ncp eq 4 then print,'  SptSumm:       ',hilf(0:ncp-1)/5.0,hilf(1)/5.0+hilf(2)/5.0+hilf(3)/5.0
 ;if ncp eq 5 then print,'  SptSumm:       ',hilf(0:ncp-1)/5.0,hilf(2)/5.0+hilf(3)/5.0+hilf(4)/5.0
 print,'  Ch-Summ 5:     ',hilf1(0:ncp-1)/5.0
 print,tt3
 print,'  Reem. (',string(flu(jflu-5),format='(f6.2)'),'):',bbrem(0:ncp-1,jflu-5)
 print,'  Reem. (',string(flu(jflu-4),format='(f6.2)'),'):',bbrem(0:ncp-1,jflu-4)
 print,'  Reem. (',string(flu(jflu-3),format='(f6.2)'),'):',bbrem(0:ncp-1,jflu-3)
 print,'  Reem. (',string(flu(jflu-2),format='(f6.2)'),'):',bbrem(0:ncp-1,jflu-2)
 print,'  Reem. (',string(flu(jflu-1),format='(f6.2)'),'):',bbrem(0:ncp-1,jflu-1)
 hilf2=dblarr(ncp)
 hilf2(0:ncp-1)= bbrem(0:ncp-1,jflu-5) $
                +bbrem(0:ncp-1,jflu-4) $
                +bbrem(0:ncp-1,jflu-3) $
                +bbrem(0:ncp-1,jflu-2) $
                +bbrem(0:ncp-1,jflu-1) 

 print,'  Re-Summ 5:     ',hilf2(0:ncp-1)/5.0
 ;print,'  '
 ;print,bez 
 ;
 ;ff='(a15,a10,'+string(ncp-1)+'(3H & ,a10),3H \\)'
 ;print,'              &',text1(1:ncp)      ,format=ff
 ;ff='(a15,f10.5,'+string(ncp-1)+'(3H & ,f10.5),3H \\)'
 ;print,'  scatter R   &',hilf0(0:ncp-1)/5.0,format=ff
 ;print,'  sputter Yp  &',hilf (0:ncp-1)/5.0,format=ff
 ;print,'  chem    Yc  &',hilf1(0:ncp-1)/5.0,format=ff
 ;print,'  outgas  Yd  &',hilf2(0:ncp-1)/5.0,format=ff
 ;print,'  summ        &',(hilf0(0:ncp-1)+hilf(0:ncp-1)+hilf1(0:ncp-1)+hilf2(0:ncp-1))/5.0,format=ff
 ;print,'  '
 
 ;;print,' areal density [10**16 atoms/cm**2]'
 ;print,'  ard.(',string(flu(jflu-3),format='(f5.2)'),'):',ard(0:ncp-1,jflu-3)
 ;print,'  ard.(',string(flu(jflu-2),format='(f5.2)'),'):',ard(0:ncp-1,jflu-2)
 ;print,'  ard.(',string(flu(jflu-1),format='(f5.2)'),'):',ard(0:ncp-1,jflu-1)
end

if jflu ge 10 then begin
 hilf=dblarr(ncp)
 hilf(0:ncp-1)=  backsputt(0:ncp-1,jflu-10) $
                +backsputt(0:ncp-1,jflu-9) $
                +backsputt(0:ncp-1,jflu-2) $
                +backsputt(0:ncp-1,jflu-3) $
                +backsputt(0:ncp-1,jflu-4) $
                +backsputt(0:ncp-1,jflu-5) $
                +backsputt(0:ncp-1,jflu-4) $
                +backsputt(0:ncp-1,jflu-3) $
                +backsputt(0:ncp-1,jflu-2) $
                +backsputt(0:ncp-1,jflu-1) 
 hilf1=dblarr(ncp)
 hilf1(0:ncp-1)= bchem(0:ncp-1,jflu-10) $
                +bchem(0:ncp-1,jflu-9) $
                +bchem(0:ncp-1,jflu-8) $
                +bchem(0:ncp-1,jflu-7) $
                +bchem(0:ncp-1,jflu-6) $
                +bchem(0:ncp-1,jflu-5) $
                +bchem(0:ncp-1,jflu-4) $
                +bchem(0:ncp-1,jflu-3) $
                +bchem(0:ncp-1,jflu-2) $
                +bchem(0:ncp-1,jflu-1) 
 hilf2=dblarr(ncp)
 hilf2(0:ncp-1)= bbrem(0:ncp-1,jflu-10) $
                +bbrem(0:ncp-1,jflu-9) $
                +bbrem(0:ncp-1,jflu-8) $
                +bbrem(0:ncp-1,jflu-7) $
                +bbrem(0:ncp-1,jflu-6) $
                +bbrem(0:ncp-1,jflu-5) $
                +bbrem(0:ncp-1,jflu-4) $
                +bbrem(0:ncp-1,jflu-3) $
                +bbrem(0:ncp-1,jflu-2) $
                +bbrem(0:ncp-1,jflu-1) 
 print,'  SptSumm 10:    ',hilf2(0:ncp-1)/10.0
 print,'  '
 
 print,bez 

 ff='(a15,a10,'+string(ncp-1)+'(3H & ,a10),3H \\)'
 print,'              &',text1(1:ncp)      ,format=ff
 ff='(a15,f10.5,'+string(ncp-1)+'(3H & ,f10.5),3H \\)'
 print,'  scatter R   &',hilf0(0:ncp-1)/10.0,format=ff
 print,'  sputter Yp  &',hilf (0:ncp-1)/10.0,format=ff
 print,'  chem    Yc  &',hilf1(0:ncp-1)/10.0,format=ff
 print,'  outgas  Yd  &',hilf2(0:ncp-1)/10.0,format=ff
 print,'  sum         &',(hilf0(0:ncp-1)+hilf(0:ncp-1)+hilf1(0:ncp-1)+hilf2(0:ncp-1))/10.0,format=ff
 
 if ncp gt 2 then print,'  sum 1+2 1/2 ',(hilf(1)+hilf(2))/10,hilf(1)/hilf(2),2.0/5.0,format=ff
 
 ;print,'  sum         &',(hilf0(0:ncp-1)+hilf(0:ncp-1)+hilf1(0:ncp-1)+hilf2(0:ncp-1))/10.0
 ;if ncp gt 2 then print,'  sum 1+2 1/2 ',(hilf(1)+hilf(2))/10,hilf(1)/hilf(2),3.0/2.0

; if ncp ge 4 and mitsp eq 1 then begin
;   hhh=(hilf0(3)+hilf(3)+hilf1(3)+hilf2(3)) 
;   if hhh ne 0 then print,'   ',(hilf0(1)+hilf(1)+hilf1(1)+hilf2(1)+hilf0(2)+hilf(2)+hilf1(2)+hilf2(2) )/ hhh          
;   if hhh eq 0 then print,'     '      
; end else begin
;  if ncp ge 3 then begin
;   hhh=(hilf0(2)+hilf(2)+hilf1(2)+hilf2(2)) 
;   if hhh ne 0 then print,'     ',(hilf0(1)+hilf(1)+hilf1(1)+hilf2(1))/ hhh          
;   if hhh eq 0 then print,'     '      
;  end 
; end

end

print,'min(backsputt),max(backsputt)',min(backsputt),max(backsputt) 
;print,min(iback_r),max(iback_r) 

   _posit,ric,isp,jze,spa,zei,dspa,dzei,dx,dy,pos,pos1
  ; if iyr3 eq 0 then yr3=[0,  max([max(backsputt(0:ncp-1,*)),max(bbrem(0:ncp-1,*))])]
  if iyr3 eq 0 then yr3=[0,  max([max(backsputt(0:ncp-1,*)),1*max(bbrem(0:ncp-1,*))])]
  ;yr3=yr3*2
    if ixxaa eq 1 then begin
      lmin=3 & lmax=4
      _achslevels,xx(0),xx(1),lmin,lmax,ndx,idx,strdx ;,forma=format_y
     plot,[0,max(flu)],[0,max(backsputt(0:ncp-1,*))],xtitle=xytext(0) $
       ,Xticks=ndx, Xtickv=idx ,Xtickname=strdx $
      ,yrange=yr3,title='part. sputt. coeff.' $
      ,thick=dd,charsize=gr,charthick=gd,position=pos1,/nodata
    end else begin
      plot,[0,max(flu)],[0,max(backsputt(0:ncp-1,*))],xtitle=xytext(0) $
      ,yrange=yr3,title='part. sputt. coeff.' $
      ,thick=dd,charsize=gr,charthick=gd,position=pos1,/nodata
   end
   j=1
    bmittel2=dblarr(jflu)
    bmittel2(0)=0

 
   for i=0,ncp-1 do begin
     if i eq 0 then fff=1.0 else fff=1.0
     
     if idrel eq 0 then begin
                        oplot,flu(1:jflu-1),backsputt (i,1:jflu-1)*fff,color=farbe(i)              ;sput
      if nn2 ne nn then oplot,flu(1:jflu-1),backsputt2(i,1:jflu-1)*fff,color=farbe(i),linestyle=2  ;sput
      
                oplot,flu(1:jflu-1),bchem     (i,1:jflu-1)*fff,color=farbe(i),linestyle=3  
 if i eq 0 then oplot,flu(1:jflu-1),bbrem     (i,1:jflu-1)*fff,color=farbe(i),linestyle=3  ;sput+reem i=0 (gas)
 ;              oplot,flu(1:jflu-1),(backsputt(i,1:jflu-1)+bbrem(i,1:jflu-1))*fff,color=farbe(i),linestyle=2  ;sput+rem
     end
     if idrel eq 1 then begin
       bmittel=dblarr(jflu)
       bmittel(0)=0
       for jj=1,jflu-1 do begin
         if jj eq 1 then begin
           bmittel(jj)=backsputt(i,jj)
           ;print,jj,bmittel(jj),backsputt(i,jj)
         end else begin
           ;print,jj,bmittel(jj-1),backsputt(i,jj)
           
           bmittel(jj)=(bmittel(jj-1)*(jj-1)+backsputt(i,jj))/jj
           ;bmittel2(jj)=bmittel2(jj)+backsputt(i,jj)
           ;print,jj,bmittel(jj)
         end
       end
       oplot,flu(1:jflu-1),bmittel(1:jflu-1)*fff,color=farbe(i)
       ;if i eq 1 then oplot,flu,bmittel2(1:jflu-1),color=farbe(i+1)
     end
     xyouts,.001,pos1(3)-(pos1(3)-pos1(1))/8*j $
     ,text1(i+1),charsize=gr/2,charthick=gd,/normal,color=farbe(i)  
      j=j+1
   end
   
;---Ta2O5 Y(Ta)+2*Y(O)  or Y(Si)+2*Y(O) 
if ncp eq 3 then oplot,flu(1:jflu-1),backsputt(1,1:jflu-1)+backsputt(2,1:jflu-1),color=farbe(5)   ;sput
if ncp eq 3 then oplot,flu(1:jflu-1),backsputt(1,1:jflu-1)/backsputt(2,1:jflu-1)/10.,color=farbe(5),linestyle=2   ;sput
   
   ;---unit
   xyouts,.001,pos1(3)-(pos1(3)-pos1(1))/8*(j) $
   ,text2(1),charsize=gr/2,charthick=gd,/normal  

  comp =comp_surf
  comp2=comp_surf2
  ihelp=where(comp gt 1)
  if ihelp(0) ge 0 then comp(where(comp gt 1))=1.0
  if ihelp(0) ge 0 then print,'atomic fraction > 1 --> =1
  ihelp=where(comp lt 0)
  if ihelp(0) ge 0 then comp(where(comp lt 0))=0.0
  if ihelp(0) ge 0 then print,'atomic fraction < 0 --> =0

   
   ;picture 4
   ;---atomic fraction at surface or 
   ;   max. penetration depth of backscatt.part. 
   _posit,ric,isp,jze,spa,zei,dspa,dzei,dx,dy,pos,pos1
  if idrel eq 0 then begin
      
      
    if ixxaa eq 1 then begin
      lmin=3 & lmax=4
      _achslevels,xx(0),xx(1),lmin,lmax,ndx,idx,strdx ;,forma=format_y
      ; ,Xticks=ndx, Xtickv=idx ,Xtickname=strdx $
    end

 
   if iyr4 lt 2 then begin
     if iyr4 eq 0 then yr4=[min(comp)-.001,max(comp)+.001]
     if ixxaa eq 1 then begin
      plot,flu,comp(0,*),xtitle=xytext(0) $
       ,Xticks=ndx, Xtickv=idx ,Xtickname=strdx $
       ,yrange=yr4,title='atomic fraction at surface' $
       ,thick=dd,charsize=gr,charthick=gd,position=pos1
     end else begin 
      plot,flu,comp(0,*),xtitle=xytext(0) $
       ,yrange=yr4,title='atomic fraction at surface' $
       ,thick=dd,charsize=gr,charthick=gd,position=pos1
     end
   end else begin
    if ixxaa eq 1 then begin
     plot,flu,comp(0,*),xtitle=xytext(0) $
     ,yrange=[-0.05,1.05],title='atomic fraction at surface' $
     ,Xticks=ndx, Xtickv=idx ,Xtickname=strdx $
     ,thick=dd,charsize=gr,charthick=gd,position=pos1 $
     ,Yticks=7 $
     ,Ytickv=[-0.05, 0, .2 ,.4   ,.6  , .8, 1.0, 1.05   ] $
     ,Ytickname=[' ','0.0','0.2','0.4','0.6','0.8','1.0',' '] 
    end else begin 
     plot,flu,comp(0,*),xtitle=xytext(0) $
     ,yrange=[-0.05,1.05],title='atomic fraction at surface' $
     ,thick=dd,charsize=gr,charthick=gd,position=pos1 $
     ,Yticks=7 $
     ,Ytickv=[-0.05, 0, .2 ,.4   ,.6  , .8, 1.0, 1.05   ] $
     ,Ytickname=[' ','0.0','0.2','0.4','0.6','0.8','1.0',' '] 
    end 
   end
   j=1   
   
    
   for i=0,ncp-1 do begin
     th=0
     if strmid(text2(i+1),0,4) eq 'Sp3H' then th=2
     oplot,flu,comp(i,*),color=farbe(i),linestyle=th
     oplot,flu,comp2(i,*),color=farbe(i),linestyle=2

;andock
; if i eq 5 then oplot,flu,comp(i,*)+comp(i+1,*),color=farbe(i),linestyle=th

     xyouts,.501,pos1(3)-(pos1(3)-pos1(1))/8*(j) $
     ,text1(i+1),charsize=gr/2,charthick=gd,/normal,color=farbe(i)  
     j=j+1
   end
   xyouts,.501,pos1(3)-(pos1(3)-pos1(1))/8*(j),text2(1),charsize=gr/2,charthick=gd,/normal  
  end else begin
     openr,1,'mpe_ex_p.dat'
      ;---header
      readf,1,text
      readf,1,text
      readf,1,nnn,nh
      nproj=1.0d0
      idepb=1
      maxtief=0.0d0
      maxwert=0.0d0
      for anz=1,nnn do begin
        readf,1,nproj
        readf,1,text
        readf,1,text
        nenerg=1
        readf,1,nenerg,idepb
        readf,1,text
        text2b=strarr(8)
        text3b=strarr(8)
         text3b(0)='depth [A]'
         text3b(1)='penetration depth of refl. part.'
         text2b(1)='particles (normalized to: nproj)'
        ;---values
        a=1.0d0
        wert=dblarr(idepb)
        tief=dblarr(idepb)
        hilf=dblarr(idepb)
        readf,1,a,tief
        for i=0,nenerg-1 do begin
          readf,1,a,hilf
          wert(*)=wert(*)+hilf(*)
       end   
        readf,1,a,wert ;sum
      end
      maxtief=max([maxtief,max(tief)])
      if nproj > 0 then maxwert=max([maxwert,max(wert/nproj)])
     close,1
     
     print,' -------'
     print,' Picture 3: penetration depth of refl. part.:' 
     print,'  max depth     :',maxtief
     print,'  max particles :',maxwert
     print,'  NR*NH or nproj:',nprojb,'   (values of normalized)'
   
     if maxwert eq 0 then maxwert=1
     xx=[0.0,maxwert]
     yy=[maxtief,0.0]
     lmin=3 & lmax=4
     _achslevels,xx(0),xx(1),lmin,lmax,ndx,idx,strdx ;,forma=format_y
     _achslevels,yy(0),yy(1),lmin,lmax,ndy,idy,strdy ;,forma=format_y
     plot,xx,yy,xtitle=text2b(k),ytitle=text3b(0),title=text3b(k),/nodata $
      ,Xticks=ndx, Xtickv=idx ,Xtickname=strdx $
      ,Yticks=ndy, Ytickv=idy ,Ytickname=strdy $
      ,xrange=xx ,yrange=yy $
      ,thick=dd,charsize=gr,charthick=gd,position=pos1
    openr,1,'mpe_ex_p.dat'
     ;---header
     readf,1,text
     readf,1,text
     readf,1,nnn,nh
     nproj=1.0d0
     maxtief=0.0d0
     maxwert=0.0d0
     lll=0
     for anz=1,nnn do begin
       readf,1,nproj
       text5=strarr(2)
       readf,1,text5,format='(a64,a5)' & text5(*)=strtrim(text5(*),1)
       ;print,text5(0)
       ;print,text5(1)
       readf,1,text
       nenerg=1
       readf,1,nenerg,idepb
       readf,1,text
       ;---values
       wert=dblarr(idepb)
       tief=dblarr(idepb)
       hilf=dblarr(idepb)
       readf,1,a,tief
       for i=0,nenerg-1 do begin
         readf,1,a,hilf
         wert(*)=wert(*)+hilf(*)
       end   
       readf,1,a,wert ;sum
       if nproj > 0 then begin
         oplot,wert/nproj,tief,color=farbe(lll)
         xyouts,.501,pos1(3)-(pos1(3)-pos1(1))/8*(lll+1) $
         ,text5(1),charsize=gr/2,charthick=1.5,/normal,color=farbe(lll)  
         lll=lll+1
       end     
     end       
    close,1       
  end
  
  ;--------------Picture 5: density / (number recoils / NH)
   _posit,ric,isp,jze,spa,zei,dspa,dzei,dx,dy,pos,pos1
   densconst=dens(jflu-1,idep-1)
   print,'-------'
   print,' Picture 5: density:' 
   print,'  density:',densconst,' number layer: 0 -',idep-1 ;,' fluence:',flu(jflu-1) 
   ii=0
   iiq=0
   for j=jflu-1,jflu-2,-1 do begin ;j=jflu-1,0,-1
     iiqh=0
     for i=idep-1,1,-1 do begin
      if dens(j,i) ne densconst and ii lt i then begin
        ii=i
      end
      if    comp_dep(0,i) ne comp_dep(0,i-1) and iiqh eq 0 then iiqh=i

      if ncp gt 1 then begin
         if comp_dep(1,i) ne comp_dep(1,i-1) and iiqh eq 0 then iiqh=i
      end
     end
     iiq=max([iiq,iiqh])
   end
   ii=max([ii,iiq])
   ;---ii...index from this layer density is constant
   if ii ne idep-1 then ii=ii+1
   if ii ne idep-1 then print,'  density until layer(center)',ii,' =',dep(ii),' A constant'
  
  if idrel eq 1 then begin
   close,1
   neng=1 & ndep=1 & ilog=1
   ncp=1 & nh=long(1) & nr=long(1)
   openr,1,'morigin_ex_bs.dat'
    readf,1,text                   ; SDTrimSP: VERSION 4.16    23.07.2009
    readf,1,text                   ; 40 keV Xe -> Si (KrC, dx=10)                                                   
    readf,1,ncp,nh,nr              ;           2        4000           5    number species of particle , NH, NR
    for n=0,ncp-1 do begin
     readf,1,text                  ;   20000.0000000000        sum(nproj) == nh*nr
     readf,1,text                  ;  BACKWARD SPUTTERED ATOMS, COMPONENT(1): Xe   
     readf,1,text                  ;  DIMENSION:  lin.energy       depth   log_energ
     readf,1,neng,ndep,ilog        ;                     100          10           0
     readf,1,text                  ;ENERGY [eV]  DEPTH [A]
     depth=dblarr(ndep)
     enger=dblarr(neng)
     mat =dblarr(ndep,neng)
     hilf=dblarr(ndep)
     readf,1,a,depth  ;& print,depth
     for j=0,neng-1 do begin
       readf,1,a,hilf
       enger(j)=a
       mat(*,j)=hilf(*)
     end
     readf,1,a,hilf
     a=total(hilf)
     ;print,'read morigin_ex_bs.dat number of values',a
     
     for j=0,ndep-1 do begin
       hilf(j)=total(mat(j,*) )
     end
     if n eq 0 then maxwert=max(hilf)  else maxwert=max([maxwert,max(hilf)])
     if n eq 0 then maxtief=max(depth) else maxtief=max([maxtief,max(depth)])
     if maxwert eq 0 then maxwert=1
     ;print,'max:',maxwert,maxtief
    end 
   close,1
   
   openr,1,'morigin_ex_bs.dat'     ;matrix origin energy x backsputtering
    readf,1,text                   ; SDTrimSP: VERSION 4.16    23.07.2009
    readf,1,text                   ; 40 keV Xe -> Si (KrC, dx=10)                                                   
    readf,1,ncp,nh,nr              ;           2        4000           5    number species of particle , NH, NR
    for n=0,ncp-1 do begin
      readf,1,text                  ;   20000.0000000000        sum(nproj) == nh*nr
      readf,1,text                  ;  BACKWARD SPUTTERED ATOMS, COMPONENT(1): Xe   
      readf,1,text                  ;  DIMENSION:  lin.energy       depth   log_energ
      readf,1,neng,ndep,ilog        ;                     100          10           0
      readf,1,text                  ;ENERGY [eV]  DEPTH [A]
      depth=dblarr(ndep)
      enger=dblarr(neng)
      mat =dblarr(ndep,neng)
      hilf=dblarr(ndep)
      readf,1,a,depth  ;& print,depth
      for j=0,neng-1 do begin
       readf,1,a,hilf
       enger(j)=a
       mat(*,j)=hilf(*)
      end
      readf,1,a,hilf
      a=total(hilf)
      ;print,'read morigin_ex_bs.dat number of values',a
     
      for jd=0,ndep-1 do begin
       hilf(jd)=total(mat(jd,*) )
       ;print,jd,depth(jd),hilf(jd)
      end
      if n eq 0 then begin
        xx=[0.0,maxwert]/nr/nh

        yy=[maxtief,0.0]
        if iyr5 eq 0 then yr5=yy
        lmin=4 & lmax=5
        _achslevels,xx(0),xx(1),lmin,lmax,ndx,idx,strdx ;,forma=format_y
        _achslevels,yr5(0),yr5(1),lmin,lmax,ndy,idy,strdy ;,forma=format_y
        
        text3b(0)='depth [A]'
        text3b(1)='depth of origin'
        text2b(1)='particles (normalized to: number part.)'

        plot,xx,yr5,xtitle=text2b(1),ytitle=text3b(0),title=text3b(1),/nodata $
         ,Xticks=ndx, Xtickv=idx ,Xtickname=strdx $
         ,Yticks=ndy, Ytickv=idy ,Ytickname=strdy $
         ,xrange=xx ,yrange=yr5 $
         ,thick=dd,charsize=gr,charthick=gd,position=pos1
      end ;n==0
     
      oplot,hilf/nr/nh,depth,color=farbe(n)
      ;if n eq 0 then oplot,hilf/nr/nh*10,depth,color=100; farbe(n)
     
      for je=0,neng-1 do begin
        for jd=0,ndep-1 do begin
          hilf(jd)=mat(jd,je)
          ;print,j,depth(j),hilf(j)
        end
        ;oplot,hilf/max(hilf),depth ,color=250-je*10 
        ;print,je,enger(je)
        ;aaa=''
        ;read,aaa
      end
    end ;n=0,ncp-1 
    close,1
     ;xyouts,.2,.2,'Xe*10',charsize=gr,charthick=gd,/normal  
  
  end else begin
     
    ;---density picture 5
     if fix(min(dens)*10000) eq fix(max(dens)*10000) then begin
       lev=[dens(0)*.9,dens(0),dens(0)*1.1] 
       texta='density constant'
       _real2string,dens(0),dens(0),s
       textb=s+' [atoms/A**2]'
     end else begin
       texta=''
       textb=''
     _achsenwerte1,[min(dens)*100,max(dens)*100],lev,j,8,15
     end
 
;;BBBBBBBBBBBBBBBBB 
; print,'lev',lev
; lev=[12.15000,      12.2000,      12.2500,      12.3000 ,      12.3500]   
 ;lev=[6.25000, 6.30000, 6.35]   
;lev=[6.0, 6.5, 7.00000d0      ,      8.00000,    9.00000   ,    10.0000,     11.0000, 11.4000, 11.70]

;lev=[6.0, 6.5,7.0, 7.5, 8.0,8.5,9.0,9.5, 10.0000,     11.0000, 11.5000, 11.70]
; lev=[6.0,6.25,6.5,6.75,7.0,7.25, 7.5,7.75, 8.0,8.5,9.0,9.5, 10.0000,     11.0000, 11.5000, 11.70]

;lev=[ 12.1000,12.15,   12.2000 ,12.25,  12.3000, 12.35,     12.4]

;lev=[5.6,5.7,5.8,5.9,6.0,6.1,6.2,6.3,6.4, 6.5]
;lev=[6.0, 7.00000d0      ,      8.00000,    9.00000   ,    10.0000,     11.0000, 12]
;lev=[6.0, 7.00000d0 ,      10.00000 ,11.1 ,11.5   ]

     lab=lev
     lab(*)=0
     ;---each second script
     hilf=size(lab)
     for jd=0,hilf(1)-1,2 do begin 
       lab(jd)=1
     end
     
     if iyr5 eq 0 then yr5=[dep(ii),0]
     if ixxaa eq 1 then begin
      lmin=3 & lmax=4
      _achslevels,xx(0),xx(1),lmin,lmax,ndx,idx,strdx ;,forma=format_y
     contour,dens*100,flu,dep $
     ,xtitle='fluence [atoms/A**2]',ytitle='depth [A]' $
      ,Xticks=ndx, Xtickv=idx ,Xtickname=strdx $
      ,xrange=[0,max(flu)] $
     ,yrange=yr5 $
     ,title='density [100*atoms/A**3]',levels=lev,c_labels=lab $
     ,thick=dd,charsize=gr,charthick=gd,position=pos1
    
    end else begin

     contour,dens*100,flu,dep $
     ,xtitle='fluence [atoms/A**2]',ytitle='depth [A]' $
     ,xrange=[0,max(flu)] $
     ,yrange=yr5 $
     ,title='density [100*atoms/A**3]',levels=lev,c_labels=lab $
     ,thick=dd,charsize=gr,charthick=gd,position=pos1
    end
    xyouts,pos1(0)+(pos1(2)-pos1(0))/10.,pos1(1)+(pos1(3)-pos1(1))*2./3.,texta $
      ,charsize=gr/2,charthick=gd,/normal  
    xyouts,pos1(0)+(pos1(2)-pos1(0))/10.,pos1(1)+(pos1(3)-pos1(1))/3. ,textb $
      ,charsize=gr/2,charthick=gd,/normal   
  end
  comp=comp_dep 
  comp2=comp_dep2 
  ihelp=where(comp gt 1)
  if ihelp(0) ge 0 then comp(where(comp gt 1))=1.0d0
  ;if ihelp(0) ge 0 then print,'atomic fraction > 1 --> =1'
  ihelp=where(comp lt 0)
  if ihelp(0) ge 0 then comp(where(comp lt 0))=0.0d0
  ;if ihelp(0) ge 0 then print,'atomic fraction < 0 --> =0'

  ;---atomic fraction picture 6
  _posit,ric,isp,jze,spa,zei,dspa,dzei,dx,dy,pos,pos1

   ii=max([ii,iiq])
   
   if iyr6 eq 0 then yr6=[dep(ii),0]
   plot,comp(0,*),dep,ytitle='depth [A]' $
   ;,xrange=[min(comp)-.001,max(comp)+.001]   $ 
    ,xrange=[0,1]   $ 
   ,yrange=yr6 $
   ,title='atomic fraction (end)' $
   ,thick=dd,charsize=gr,charthick=gd,position=pos1,/nodata
   j=1

  
   
   for i=0,ncp-1 do begin 
     c=dblarr(idep)
     d=dblarr(idep)
    d2=dblarr(idep)
    c2=dblarr(idep)
    for j2=0,idep-1 do begin
       c (idep-1-j2)=comp (i,j2)
       c2(idep-1-j2)=comp2(i,j2)
;andock
;if i eq 6-1 then c(idep-1-j2)=comp(i,j2)+comp(i+1,j2)
 
       d (idep-1-j2)=dep (j2)
       d2(idep-1-j2)=dep2(j2)
     end
     th=0
     if strmid(text2(i+1),0,4) eq 'Sp3H' then th=2
     oplot,c ,d ,color=farbe(i),linestyle=th
     oplot,c2,d2,color=farbe(i),linestyle=2
     xyouts,pos1(0)-.022*ncp+(pos1(2)-pos1(0))/5.9*(j) ,pos1(1)*.68 $
     ,text1(i+1),charsize=gr/2,charthick=gd,/normal,color=farbe(i)  
     j=j+1
     print,'  qmax(end):',text1(i+1),max(c)
   end

   xyouts,pos1(0)+(pos1(2)-pos1(0))/8*(j),pos1(1)*.68 $
   ,text2(1),charsize=gr/2,charthick=gd,/normal  
   
;   xyouts,.10,.05,'!6'+unt,charsize=gr*.7,charthick=gd,/normal
   if idrel eq 0 then xyouts,.10,.05,'!6ANALYSIS OF DYNAMICS   '+bez,charsize=gr*.7,charthick=gd,/normal  
   if idrel eq 1 then xyouts,.10,.05,'!6ANALYSIS OF STATC   '+bez,charsize=gr*.7,charthick=gd,/normal  
  ;xyouts,.10,.02,'!6'+strmid(titlecase(0),0,k),charsize=gr*.6,charthick=gd,/normal
   xyouts,.65,.00,'!6'+textdatum,charsize=gr*.4,charthick=gd,/normal  
   if nn ne nn2 then begin
    plots,[.10,.14],[.03,.03],/normal
    plots,[.40,.44],[.03,.03],/normal,linestyle=2
    xyouts,.15,.025,'!3'+ttnn,charsize=gr*.6,charthick=gd,/normal  
    xyouts,.45,.025,'!3'+ttn2,charsize=gr*.6,charthick=gd,/normal
   end
 _druckende,druck,drname,form
   ende: 
   aaa=' '
  ;if eee gt 2 then read,aaa 
 
 end ;kkk
end
