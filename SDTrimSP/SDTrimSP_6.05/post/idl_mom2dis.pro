@/afs/ipp/home/a/aam/public/archiv/TRIDYN/SDTrimSP/post/window.inc 
@/afs/ipp/home/a/aam/public/archiv/TRIDYN/SDTrimSP/post/level.inc 
@/afs/ipp/home/a/aam/public/archiv/TRIDYN/SDTrimSP/post/achsen.inc 
pro run
;---plots of distribution, calculated from moments(mom2dis.F90)

;---input : E0_34_distribut.dat 
;---output: distribution****.ps
 
  _start
  loadct,39
  druck=0
  print,'druck:0/1    0...screen 1...postscript'
  read,druck
  drname='????.ps'
  form=0

  close,31
  print,'----READ: E0_34_distribut.dat'
  openr,31,'E0_34_distribut.dat'
  ianz1=1 & ianz2=1
  a=1.0d0
  b=1.0d0
  c=1.0d0
  d=1.0d0
  text=' '
  textb=' '
  version=' '
  title=' '
  
  ;---read given distribution
  readf,31,version
  readf,31,title
  readf,31,text
  readf,31,text
  print,'  ',version
  print,'  ',title

  nh=long(1)& nh_nr=long(1) & ncp=1 & idout=long(1) & ncp_proj=1 & idrel=1 & debugmom=0
  readf,31,nh,nh_nr,ncp,idout,ncp_proj,idrel,debugmom
  print,'nh,ncp,idout,ncp_proj,idrel:',nh,ncp,idout,ncp_proj,idrel,debugmom
  symbol=strarr(ncp)
  readf,31,symbol,format='(20a10)' & symbol(*)=strtrim(symbol(*),1)
  
  readf,31,text
  e0=dblarr(ncp)
  readf,31,e0
  
  nr_flu=nh/idout
  for iflu=1,nr_flu do begin 
    
    im=1 & np=1
    ;---from mom2dis.F90
    ;                                            calculate  given
    ;1 Energy refl. coeff.     (lin) i_back_p     ncp_proj  ncp_proj
    ;2 Energy refl. coeff.     (log) i_back_plog  ncp_proj  
    ;3 Energy transm. coeff.   (lin) i_tran_p     ncp_proj  ncp_proj
    ;4 Energy transm. coeff.   (log) i_tran_plog  ncp_proj  
    ;5 Energy sputt. yield     (log) i_back_rlog  ncp       ncp    
    ;6 Energy transm.sp. yield (log) i_tran_rlog  ncp       ncp    
    ;7 Depth  impl. proj.      (lin) i_stop_p     ncp_proj  ncp_proj
    ;8 Radius impl. proj.      (lin) i_stop_p     ncp_proj  ncp_proj
    
    ;distrib. 2        + 2  +  2 = 6*(ncp/ncp_proj) given distribution 
    anz_given=2*ncp_proj+2*ncp+2*ncp_proj
    ;moments  4        + 2  +  2 = 8*(ncp/ncp_proj) distribution from 8 momente
    anz_calcu=4*ncp_proj+2*ncp+2*ncp_proj
    ;if ncp_proj=1 ncp=3 ==> 10 picture
    
    ;---DEBUGMOM == 1---------------------
    if debugmom eq 1 then begin
     ;print,'given'
     for nn=1,anz_given do begin
      ;---im...number moments
      readf,31,text
      readf,31,text
      readf,31,im,np,ianz1
      print,'im,np,ianz1:',im,np,ianz1
      if( nn eq 1 ) then begin
        xxrec=dblarr(8,ncp,ianz1)
        yyrec=dblarr(8,ncp,ianz1)
      end
      readf,31,text
      readf,31,text
      for i=0,ianz1-1 do begin
        readf,31,a,b
        xxrec(im-1,np-1,i)=a
        yyrec(im-1,np-1,i)=b
        ;print,a,b
      end
    
      ;---im...number moments
     end ;nn
    end ;debug
    
    
    unter=[ $
      'energy refl. coeff.'     $  
     ,'energy refl. coeff.'     $
     ,'energy transm. coeff'    $
     ,'energy transm. coeff'    $
     ,'energy sputt. yield'     $
     ,'energy transm.sp. yield' $
     ,'depth impl. proj.'       $
     ,'radius impl. proj.'      $
     ]
    xt=[ $
      'energy' $  
     ,'energy' $
     ,'energy' $
     ,'energy' $
     ,'energy' $
     ,'energy' $
     ,'x'      $
     ,'r'      $
     ]
    yt=[ $
      'counts/de/NH' $  
     ,'counts/de/NH' $
     ,'counts/de/NH' $
     ,'counts/de/NH' $
     ,'counts/de/NH' $
     ,'counts/de/NH' $
     ,'counts/dx/NH' $
     ,'counts/dr/NH' $
     ]
    col=[ $
      250 $  ;(lin)
     , 60 $  ;(log)
     ,250 $  ;(lin)
     , 60 $  ;(log)
     , 60 $  ;(log)
     , 60 $  ;(log)
     ,250 $  ;(lin)
     ,250 $  ;(lin)
     ]

    flue=1.0d0
    bez=strarr(8)
    ianzp=intarr(8,ncp)
    ianzp(*,*)=0
    for nn=1,anz_calcu do begin
      readf,31,text
      readf,31,textb 
      readf,31,text;symbol
      readf,31,flue
      readf,31,im,np,ianz2
      print,textb,im,np,ianz2 
      readf,31,text
      bez(im-1)=textb
      if total(ianzp) eq 0 then begin
        xxcal=dblarr(8,ncp,ianz2)
        yycal=dblarr(8,ncp,ianz2)
      end
      ianzp(im-1,np-1)=ianz2
      for i=0,ianz2-1 do begin
        readf,31,a,b,c 
        xxcal(im-1,np-1,i)=a
        yycal(im-1,np-1,i)=c
      end
    end
    ;picture projectiles im=1,2,3,4,6,8
    ;picture recoils     im=5,6
    
    anzpic=0
    for im=1,8 do begin
     if im eq 2 then goto,weiter
     if im eq 4 then goto,weiter
     for np=0,ncp-1 do begin
      if ianzp(im-1,np) gt 0 then anzpic=anzpic+1 
     end 
     weiter:
    end
  
   
    druckfak=1
    if druck eq 1 then druckfak=.8
    dd=1.5*druckfak
    gr=2.3*druckfak
    gd=1.5*druckfak
    
    ;---picture for projctles
    ipage=0 ;page
    pic=1 
    for im=1,8 do begin
      if idrel eq 1 and im eq 7 then begin
        print,'----READ: depth_proj.dat'    
        print,'    + from  depth_proj.dat'    
        close,12
        openr,12,'depth_proj.dat'
        ;---header
        readf,12,text
        readf,12,text
        readf,12,nnn
        ;for anz=1,nnn do begin -> np
      end
      for np=0,ncp-1 do begin
        if ianzp(im-1,np) gt 0 then begin
          if pic eq 7 then pic = 1
          if pic eq 1 then begin
           ipage=ipage+1
           drname='distribution'+string(ipage,format='(i1)')+string(iflu,format='(i3.3)')+'.ps'
           if ipage gt 1 then begin
             xyouts,.10,.05,'!6Distributions, calc. from moments(lin...red/log...blue) (projectiles)',charsize=gr*.7,charthick=gd,/normal  
             xyouts,.10,.02,'!6'+' fluence='+title+string(flue,format='(f5.3)'),charsize=gr*.7,charthick=gd,/normal  
             _druckende,druck,drname,form
           end
           _fenster,druck,drname,form,nummer=ipage  
           spa=2 & zei=3
           !P.Multi=[0,spa,zei] 
           if form eq 0 then pos=[0.00, 0.05 , .99, .96]
           if form eq 1 then pos=[0.01, 0.1, .98, .97]
           ric=0   ; row or column wise
           dspa=25 ;% Zwischenraum
           dzei=25 ;% Zwischenraum
           dx=(pos(2)-pos(0))/spa
           dy=(pos(3)-pos(1))/zei
           isp=0
           jze=0
           if ric eq 0 then jze=1 else isp=1
          end ; pic eq 1
        
          if im ne 2 and im ne 4 and im ne 5 and im ne 6 then begin
           pic=pic+1
           _posit,ric,isp,jze,spa,zei,dspa,dzei,dx,dy,pos,pos1
           if debugmom eq 1 then begin
             yymi=min([min(yyrec(im-1,np,*)),min(yycal(im-1,np,*))])
             yyma=max([max(yyrec(im-1,np,*)),max(yycal(im-1,np,*))])
           end else begin
             yymi=min(yycal(im-1,np,*))
             yyma=max(yycal(im-1,np,*))
           end
           
           ;plot,[xxcal(im-1,np,0),xxcal(im-1,np,ianzp(im-1,np)-1)],[yymi,yyma] $ 
           plot,[.1,xxcal(im-1,np,ianzp(im-1,np)-1)],[yymi,yyma] $ 
            ;,title=bez(im-1)+symbol(np) $ 
            ,title=unter(im-1)+'  '+symbol(np) $ 
            ,xtitle=xt(im-1) $
            ;,/xlog $
            ,ytitle=yt(im-1) $
            ,thick=dd,charsize=gr,charthick=gd,position=pos1,/nodata

           baldiag,xxcal(im-1,np,*),yycal(im-1,np,*),ianzp(im-1,np),col(im-1)
           if im eq 1 or im eq 3 then baldiag,xxcal(im,np,*),yycal(im,np,*),ianzp(im,np),col(im)
           if debugmom eq 1 then oplot,xxrec(im-1,np,*),yyrec(im-1,np,*),psym=2
           
          end
          if idrel eq 1 and im eq 7 then begin
            readf,12,text
            readf,12,idepb,nprojb
            text2b=strarr(8)
            text3b=strarr(8)
            readf,12,text2b,format='(20a12)' & text2b(*)=strtrim(text2b(*),1)
            readf,12,text3b,format='(20a12)' & text3b(*)=strtrim(text3b(*),1)
            text3b(0)='depth [A]'
            text3b(1)='stop position'
            text2b(1)='particels (normalized to: nproj)'
            ;---values
            xwert=dblarr(idepb)
            vwert=dblarr(idepb)
        
            hilf=dblarr(8)
            for i=0,idepb-1 do begin
              readf,12,hilf
              xwert(i)=hilf(0)
              vwert(i)=hilf(1)
            end   
            readf,12,hilf ;sum
            ;end ;anz ->np 
            ;print,(xwert(1)-xwert(0)),nh_nr
            ;---values from 'depth_proj.dat' 
            oplot,xwert(*),vwert(*)/nh_nr/(xwert(1)-xwert(0)),psym=1 ;,color=250
          end
        end ;ianzp(im-1,np) gt 0
      end ;np
      if idrel eq 1 and im eq 7 then close,12
    end ;im
    if anzpic gt 6 then begin
     xyouts,.10,.05,'!6Distributions, calc. from moments(lin...red/log...blue) (projectiles)',charsize=gr*.7,charthick=gd,/normal  
     xyouts,.10,.02,'!6'+title+' fluence='+string(flue,format='(f5.3)'),charsize=gr*.7,charthick=gd,/normal  
     _druckende,druck,drname,form
    endif
    
    ;---picture for recoils
    ipage=10 ;page
    if anzpic gt 6 then pic=1 
    for im=5,6 do begin
      for np=0,ncp-1 do begin
        if ianzp(im-1,np) gt 0 then begin
          if pic eq 7 then pic = 1
          if pic eq 1 then begin
           ipage=ipage+1
           drname='distribution'+string(ipage,format='(i1)')+string(iflu,format='(i3.3)')+'.ps'
           if ipage gt 11 then begin
             xyouts,.10,.05,'!6Distributions, calc. from moments(lin...red/log...blue) (recoils)',charsize=gr*.7,charthick=gd,/normal  
             xyouts,.10,.02,'!6'+title+' fluence='+string(flue,format='(f5.3)'),charsize=gr*.7,charthick=gd,/normal  
            _druckende,druck,drname,form
           end
           _fenster,druck,drname,form,nummer=ipage  
           spa=2 & zei=3
           !P.Multi=[0,spa,zei] 
           if form eq 0 then pos=[0.00, 0.05 , .99, .96]
           if form eq 1 then pos=[0.01, 0.1, .98, .97]
           ric=0   ; row or column wise
           dspa=25 ;% Zwischenraum
           dzei=25 ;% Zwischenraum
           dx=(pos(2)-pos(0))/spa
           dy=(pos(3)-pos(1))/zei
           isp=0
           jze=0
           if ric eq 0 then jze=1 else isp=1
          end ; pic eq 1
        
           pic=pic+1
           _posit,ric,isp,jze,spa,zei,dspa,dzei,dx,dy,pos,pos1
           plot,xxcal(im-1,np,*),yycal(im-1,np,*) $ 
            ;,title=bez(im-1)+symbol(np) $ 
            ,title=unter(im-1)+'  '+symbol(np) $ 
            ,xtitle=xt(im-1) $
            ;,/xlog $
            ,ytitle=yt(im-1) $
            ,thick=dd,charsize=gr,charthick=gd,position=pos1,/nodata
           baldiag,xxcal(im-1,np,*),yycal(im-1,np,*),ianzp(im-1,np),col(im-1)
           if debugmom eq 1 then oplot,xxrec(im-1,np,*),yyrec(im-1,np,*),psym=2
        end ;ianzp(im-1,np) gt 0
      end ;np
    end ;im
    if anzpic gt 6 then begin
     xyouts,.10,.05,'!6Distributions, calc. from moments(lin...red/log...blue) (recoils)',charsize=gr*.7,charthick=gd,/normal  
    end else begin
     xyouts,.10,.05,'!6Distributions, calculated from moments (lin...red/log...blue)',charsize=gr*.7,charthick=gd,/normal  
    end
    xyouts,.10,.02,'!6'+title+' fluence='+string(flue,format='(f6.3)'),charsize=gr*.7,charthick=gd,/normal  
    _druckende,druck,drname,form
    
    aaa=''
    if nr_flu ne 1 and druck eq 0 then read,aaa
  end ;flu
  close,31
end           
pro baldiag,xx,yy,ianzp,col
  i=0
  dxx=0.5*(xx(i+1)-xx(i))
  oplot,[xx(i)-dxx,xx(i)+dxx],[yy(i),yy(i)],thick=gd,color=col
  i=ianzp-1
  dxx=0.5*(xx(i)-xx(i-1))
  oplot,[xx(i)-dxx,xx(i)+dxx],[yy(i),yy(i)],thick=gd,color=col
  for i=1,ianzp-2 do begin
    dxxl=0.5*(xx(i  )-xx(i-1))
    dxxr=0.5*(xx(i+1)-xx(i  ))
    oplot,[xx(i)-dxxl,xx(i)-dxxl,xx(i)+dxxr,xx(i)+dxxr] $
    ,[yy(i-1),yy(i),yy(i),yy(i+1)],thick=gd,color=col 
  end
end 
