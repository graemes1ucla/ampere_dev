;/afs/ipp-garching.mpg.de/home/a/aam/TRIDYN/SDTrimSP/post/idl_matrix.pro
;@/afs/ipp/home/a/aam/public/archiv/TRIDYN/SDTrimSP/post/window.inc 
;@/afs/ipp/home/a/aam/public/archiv/TRIDYN/SDTrimSP/post/level.inc 
;@/afs/ipp/home/a/aam/public/archiv/TRIDYN/SDTrimSP/post/achsen.inc 
@/afs/ipp/home/a/aam/IDL/UP/window.inc 
@/afs/ipp/home/a/aam/IDL/UP/level.inc 
@/afs/ipp/home/a/aam/IDL/UP/achsen.inc 
pro run
;input :meagb.dat
;       meagbt.dat
;       meags.dat
;       meagst.dat
;output:meagb.dat.ps
;       meagbt.dat.ps
;       meags.dat.ps
;       meagst.dat.ps
;       meagb.dat2.ps (color contour more as 1 component)
;       meagbt.dat2.ps
;       meags.dat2.ps
;       meagst.dat2.ps

;  goto,Worter
;  Bild1
;  Bild2
;  Bild3

  _start
  druck=0
  print,'druck:0/1    0...screen 1...postscript'
  ;read,druck

  form=0
  text='' & text1='' & bez='' & text2=''& textdatum=''
  ke=1 & na=1 & np=1  & nr=long(1) & nh=long(1)
  lmatout_cos_angle=0 
  idep=1
  ncp=1
  a=1.0d0 & b=1.0d0
  loadct,39
  if druck eq 0 then farbe=[255,250,150,80,100,200,40,215,180,255,250,150] else $
                     farbe=[0  ,250,150,80,100,200,40,215,180,0  ,250,150]

   druckfak=1
   if druck eq 1 then druckfak=.8
   dd=1.5*druckfak
   gr=2.5*druckfak
   gd=1.5*druckfak

  for anz=1,2 do begin  ;4
   if(anz eq 1) then name='meagb_p.dat' 
   if(anz eq 2) then name='meagb_s.dat' 
   if(anz eq 3) then name='meagt_p.dat' 
   if(anz eq 4) then name='meagt_s.dat' 

   close,1
   print,'  '
   print,'-----------INPUT:',name
   openr,1,name
   readf,1,textdatum
   readf,1,bez  & bez=strtrim(bez,1)
   print,bez
   readf,1,text
   readf,1,ncp,nh,nr,lmatout_cos_angle
   print,ncp,nh,nr,lmatout_cos_angle   

   if lmatout_cos_angle eq 1 then begin
     print,'polar angle -->cosinus'
     print,'---graphic only for polar angle in degree---'
     ;stop
   end
   
   titlecase=strarr(ncp) 
   nnnsum=0
   
   for logg=0,0 do begin
     ma=dblarr(ncp)
     for nnn=0,ncp-1 do begin
       readf,1,text
       titlecase(nnn)=text
       readf,1,text
       readf,1,ke,na,np 
       print,'number energy values :',ke
       print,'number azimuth values:',na
       print,'number polar values  :',np
       if nnn eq 0 then matrix=dblarr(ncp,ke,na,np)
       if nnn eq 0 then ener  =dblarr(ke)
       if nnn eq 0 then azim  =dblarr(na)
       if nnn eq 0 then polar =dblarr(np)
       if nnn eq 0 then mat_energ=dblarr(ncp,na,np)
       texte='' & texta='' & textp='' &
       readf,1,texte
       readf,1,ener
       readf,1,texta
       readf,1,azim
       readf,1,textp
       readf,1,polar
       hilf =dblarr(np)
       hilfr=dblarr(np)
       test1=0
       for k=0,ke-1 do begin   ; energy
         readf,1,text          ; title angle
         for  i=0,na-1 do begin
           readf,1,hilf
           matrix(nnn,k,i,*)=hilf(*)
           ma(nnn)=ma(nnn)+total(hilf)
         end
       end
       readf,1,text
       for  i=0,na-1 do begin
         readf,1,hilfr
         mat_energ(nnn,i,*)=hilfr(*)
       end
       nnnsum=nnnsum+ma(nnn)
       print,titlecase(nnn)
      ;print,' sum:',ma(nnn),'sum of matrix /nr:',fix(.5+ma(nnn)/nr) ???
       print,' sum:',ma(nnn),'sum of matrix /(nr*nh)=coeff:',ma(nnn)/nr/nh
     end ;nnn
   end ;logg
   close,1

   name=name+'_yield'

   summa=ma
   if nnnsum eq 0 then print,'--------no draw------------'
   if nnnsum eq 0 then goto,next_bitte
   
   ;-----------------neues Blatt-------------------------
   
   drname=name+string(0,format='(i2.2)')+'.ps'
   _fenster,druck,drname,form,nummer=anz   
   spa=1 & zei=3
   !P.Multi=[0,spa,zei] 
   if form eq 0 then pos=[0.02, 0.05 , .97, .96]
   if form eq 1 then pos=[0.01, 0.1, .98, .97]
   ric=0
   dspa=25 ;% Zwischenraum
   dzei=25 ;% Zwischenraum
   dx=(pos(2)-pos(0))/spa
   dy=(pos(3)-pos(1))/zei
   isp=0
   jze=0
   if ric eq 0 then jze=1 else isp=1

   ;===Raumwinkel
   ;---Omega=dphi * dtheta * sin(theta)
   ;---Omega=dphi * d(cos(theta)) 
      
   ;-------------------BILD1---azimuthal energy-----------------------------
   print,'---azimuthal yield distribution ------------'

   ;winkel,energy  nur zum zeichnen: 2*na
   ma=dblarr(ncp,na*2) ;number of Particle
   wi=dblarr(2*na)
   en=dblarr(ncp,2*na)
   en2=dblarr(ncp,2*na)

   for nnn=0,ncp-1 do begin  
     ii=0
     wi(ii)=0.0
     for i=0,na-1 do begin
                        wi(ii+1)=azim(i)
      if i ne na-1 then wi(ii+2)=azim(i)
      ;---sum anzahl ueber energy,polarwinkel 
      for k=0,ke-1 do begin     ;energy
         for j=0,np-1 do begin  ;polarwinkel 
           ;---Omega=dphi * dtheta * sin(theta)
           ;---Omega=dphi * d(cos(theta)) 
           fla=azim(0)*!dpi/180d0*polar(0)*!dpi/180d0*sin((polar(j)-polar(0)/2)*!dpi/180d0)
           if lmatout_cos_angle eq 1 then fla=azim(0)*!dpi/180d0*polar(0)
            ma(nnn,ii)  =ma(nnn,ii)  +matrix(nnn,k,i,j)/(nh*nr)/fla/np ;average /np 
            ma(nnn,ii+1)=ma(nnn,ii+1)+matrix(nnn,k,i,j)/(nh*nr)/fla/np ;average /np 
          end
      end
      
      ;---matrix na=1...180      Zeichnen Halbkreis  Counts in SDTrimSP aber alle (Vollkreis) 
      ;   matrix=1...360=2*(1...180)  
      ; sum of all_pol is double:  na=0...3 degree -> sum na=0...3 + na=0...-3
      ma(nnn,ii  )=ma(nnn,ii  )/2.0
      ma(nnn,ii+1)=ma(nnn,ii+1)/2.0
      
      ii=ii+2
     end 
   end
  
  ;yieldmax=max([max(ma),.001])
  yieldmax=max(ma)
  
  for nnn=0,ncp-1 do begin      
     k=strpos(titlecase(nnn),':')+2
     l=max([strlen(titlecase(nnn)),4])
     text=strmid(titlecase(nnn),k,l)
     print,'1.max(Yield) ',text,':',max(ma(nnn,*))
  end
  xa=-yieldmax & xe=yieldmax & ya=0 & ye=yieldmax
  lmin=3 & lmax=5
  if anz eq 1 then ueber='AZIMUTHAL AVERAGE SCATTERING PER SOLID ANGLE'
  if anz eq 2 then ueber='AZIMUTHAL AVERAGE YIELD PER SOLID ANGLE'
  if anz eq 1 then xtt='Scattering per solid angle' 
  if anz eq 2 then xtt='Yield per per solid angle' 
  ytt=' '
  unter=' '
  xrichtung=0 & yrichtung=0 & sca=1 
   
  _posit,ric,isp,jze,spa,zei,dspa,dzei,dx,dy,pos,pos1
  _bild,xa,xe,ya,ye,lmin,lmax,pos1,xtt,ytt,ueber,unter,gr,di,form $
         ,xrichtung,yrichtung,sca $
        ; ,neg_achs_pos=1 $ 
         ;,xachsread=xachsread,yachsread=yachsread $
         ,format_x='(e8.1)' $
         ,format_y='(f0.0)'
 
   for nnn=0,ncp-1 do begin
       oplot,ma(nnn,*),wi*!dpi/180.d0,/polar,color=farbe(nnn)
       ;---the same
       ;oplot,en2(nnn,*),wi*!dpi/180.d0,/polar,color=250
   end   
   
   oplot,[0,2*yieldmax],[0, 30*!dpi/180.d0],/polar,linestyle=1
   oplot,[0,2*yieldmax],[0, 60*!dpi/180.d0],/polar,linestyle=1
   oplot,[0,2*yieldmax],[0, 90*!dpi/180.d0],/polar,linestyle=1
   oplot,[0,2*yieldmax],[0,120*!dpi/180.d0],/polar,linestyle=1
   oplot,[0,2*yieldmax],[0,150*!dpi/180.d0],/polar,linestyle=1
 
   hr=fltarr(181)
   hw=fltarr(181)   

   _achslevels,xa,xe,lmin,lmax,ndx,xdx,strdx ;same as bild
   i=where(xdx  gt 0)
   for k=i(0),ndx-1 do begin
    for j=0,180 do begin
     hr(j)=xdx(k-1)+0.5*(xdx(k)-xdx(k-1))
     hw(j)=j*!dpi/180.
    end
    oplot,hr,hw,/polar,linestyle=1
    for j=0,180 do begin
     hr(j)=xdx(k)
     hw(j)=j*!dpi/180.
    end
    oplot,hr,hw,/polar,linestyle=1
    
   end
   
   for j=30,150,30 do begin
     _int2string,j,j,s
     xyouts,hr(j)*1.05*cos(hw(j)),hr(j)*1.05*sin(hw(j)),s+'!9%!6',/data
   end
   j=1
   for i=0,ncp-1 do begin
     k=strpos(titlecase(i),':')+2
     l=max([strlen(titlecase(i)),4])
     text=strmid(titlecase(i),k,l)
     if summa(i) gt 0 then begin
       xyouts,pos1(0)-.1,pos1(3)-(pos1(3)-pos1(1))/6*(j) $
        ,text,charsize=gr/2,charthick=gd,/normal,color=farbe(i)  
       j=j+1
     end
   end

;-------------------BILD2--polar energy------------------------------
   print,'---polar yield distribution ------------'

   ma=dblarr(ncp,2*np)
   en=dblarr(ncp,2*np)
   wi=dblarr(2*np)
   for nnn=0,ncp-1 do begin  
     jj=0
     wi(jj)=0.0
     if lmatout_cos_angle eq 1 then wi(jj)=90.0
     for  j=0,np-1 do begin                        
       if lmatout_cos_angle eq 0 then begin
                           wi(jj+1)=polar(j)
         if j ne np-1 then wi(jj+2)=polar(j);
       end else begin
                           wi(jj+1)=acos(polar(j))*180./!dpi ;+( wi(jj)-acos(polar(j))*180./!dpi)*.5
         if j ne np-1 then wi(jj+2)=acos(polar(j))*180./!dpi ;+( wi(jj)-acos(polar(j))*180./!dpi)*.5
       end
       ;---sum anzahl ueber energy, azimutalwinkel 
       for k=0,ke-1 do begin
         for  i=0,na-1 do begin
           fla=azim(0)*!dpi/180d0*polar(0)*!dpi/180d0*sin((polar(j)-polar(0)/2)*!dpi/180d0)
           if lmatout_cos_angle eq 1 then fla=azim(0)*!dpi/180d0*polar(0) 
           ;if k eq 0 and i eq 0 then print,j,polar(j),fla, azim(0)     
           ma(nnn,jj)  =ma(nnn,jj)  +matrix(nnn,k,i,j)/(nh*nr)/fla/na ;average /na
           ma(nnn,jj+1)=ma(nnn,jj+1)+matrix(nnn,k,i,j)/(nh*nr)/fla/na ;average /na
         end
       end
      ;---matrix na=1...180 matrix=1...360=2*(1...180)
      ; sum of all_azi(1...360) is not double:  na=0...3 degree -> sum na=0...3 + na=0...-3
      ;ma(nnn,j   )=ma(nnn,jj  )/2.0
      ;ma(nnn,jj+1)=ma(nnn,jj+1)/2.0

       jj=jj+2
     end 
   end
   ;---Flaeche einer Kugelkappe mit 3 Grad r=1 
   ;h=1d0-cos(3.0*!dpi/180)
   ;print,'h',h
   ;print,'A/120',2d0*!dpi*h/120  ;*3/360
   ;fla=azim(0)*!dpi/180d0*polar(0)*!dpi/180d0*sin((polar(0)-polar(0)/2)*!dpi/180d0)
   ;print,'fla  ',fla
   

  ;yieldmax=max([max(ma),.000001])
  yieldmax=max(ma)
  for nnn=0,ncp-1 do begin  
     k=strpos(titlecase(nnn),':')+2
     l=max([strlen(titlecase(nnn)),4])
     text=strmid(titlecase(nnn),k,l)
     print,'2.max(reflction) ',text,':',max(ma(nnn,*))
  end
  xa=0.0 & xe=yieldmax & ya=0 & ye=yieldmax
  lmin=3 & lmax=5
  if anz eq 1 then ueber='POLAR AVERAGE SCATTERING PER SOLID ANGLE'
  if anz eq 2 then ueber='POLAR AVERAGE YIELD PER SOLID ANGLE'
  if lmatout_cos_angle eq 1 then ueber=ueber+' (COS)'
  if anz eq 1 then ytt='Scattering per solid angle'
  if anz eq 2 then ytt='Yield per solid angle'
  xtt=' '
  unter=' '
  xrichtung=0 & yrichtung=0 & sca=1 
  _posit,ric,isp,jze,spa,zei,dspa,dzei,dx,dy,pos,pos1
   pos1(2)=pos1(2)-.2
  _bild,xa,xe,ya,ye,lmin,lmax,pos1,xtt,ytt,ueber,unter,gr,di,form $
         ,xrichtung,yrichtung,sca $ 
         ;,xachsread=xachsread,yachsread=yachsread $
         ,format_x='(f0.0)'$
         ,format_y='(e8.1)'

   for nnn=0,ncp-1 do begin
     if lmatout_cos_angle eq 0 then $
       oplot,ma(nnn,*),(90-wi)*!dpi/180.d0,/polar,color=farbe(nnn) $
     else $  
       oplot,ma(nnn,*),(90-wi)*!dpi/180.d0,/polar,color=farbe(nnn)
   end
   oplot,[0,2*yieldmax],[0, 30*!dpi/180.d0],/polar,linestyle=1
   oplot,[0,2*yieldmax],[0, 60*!dpi/180.d0],/polar,linestyle=1
   hr=fltarr(91)
   hw=fltarr(91)   
   _achslevels,xa,xe,lmin,lmax,ndx,xdx,strdx ;same as bild
   i=where(xdx  gt 0)
   for k=i(0),ndx-1 do begin
    for j=0,90 do begin
     hr(j)=xdx(k)
     hw(j)=j*!dpi/180.
    end
    oplot,hr,hw,/polar,linestyle=1
   end
   rhilf=yieldmax/2.
   if ndx-1-i(0) gt 1 then rhilf=xdx(i(0)+(ndx-1-i(0))/2)
   for j=0,90 do begin
     hr(j)=rhilf
     hw(j)=j*!dpi/180.
   end
   oplot,hr,hw,/polar,linestyle=1
   for j=30,60,30 do begin
     _int2string,90-j,90-j,s
     xyouts,hr(j)*1.05*cos(hw(j)),hr(j)*1.05*sin(hw(j)),s+'!9%!6',/data
   end
   j=1
   for i=0,ncp-1 do begin
     k=strpos(titlecase(i),':')+2
     l=max([strlen(titlecase(i)),4])
     text=strmid(titlecase(i),k,l)
     if summa(i) gt 0 then begin
       xyouts,pos1(2)+.1,pos1(3)-(pos1(3)-pos1(1))/6*(j) $
       ,text,charsize=gr/2,charthick=gd,/normal,color=farbe(i)  
       j=j+1
     end
   end
   k=strpos(titlecase(0),',')
   xyouts,.10,.05,'!6ANALYSIS   '+bez,charsize=gr*.7,charthick=gd,/normal  
   xyouts,.10,.02,'!6'+strmid(titlecase(0),0,k),charsize=gr*.6,charthick=gd,/normal  
   xyouts,.65,.00,'!6'+textdatum,charsize=gr*.4,charthick=gd,/normal  
   _druckende,druck,drname,form  
   if druck eq 1 then print,'ps2pdf '+drname+' ->pdf'   
   if druck eq 1 then spawn,'ps2pdf '+drname     



;-----------------neues Blatt-------------------------contur
   for nnn=0,ncp-1 do begin  

  ;-------------------BILDER polar azimut-------------------------------
   print,'---polar-azimut yild distribution per solid angle------------ncp:',nnn
   
   ma =dblarr(na,np)
   ma2=dblarr(na,np)
   wi =polar 
   wi2=polar 
   for i=0,na-1 do begin ;azi
       for j=0,np-1 do begin ;polar
         ;sum anzahl uber energy  
         for k=0,ke-1 do begin ;energy
           ;Omega=dphi * dtheta * sin(theta)
           ;Omega=dphi * d(cos(theta)) 
           fla=azim(0)*!dpi/180d0*polar(0)*!dpi/180d0*sin((polar(j)-polar(0)/2)*!dpi/180d0)
           wi(j)=polar(j)-polar(0)*0.5
           if lmatout_cos_angle eq 1 then begin
             if j eq 0 then wi(j)=acos(polar(j))*180./!dpi+( 90.0   -acos(polar(j))*180./!dpi)*.5 $
                       else wi(j)=acos(polar(j))*180./!dpi+( wi(j-1)-acos(polar(j))*180./!dpi)*.5
             wi2(j)=acos(polar(j))*180./!dpi
             fla=azim(0)*!dpi/180d0*polar(0) 
           end
           ma (i,j)=ma (i,j)+matrix(nnn,k,i,j)/(nh*nr)/fla
           ma2(i,j)=ma2(i,j)+matrix(nnn,k,i,j)
           ; ma is double:  na=0...3 degree -> sum na=0...3 + na=0...-3

         end
         ; ma is double:  na=0...3 degree -> sum na=0...3 + na=0...-3
         ma (i,j)=ma (i,j)/2.0
         ma2(i,j)=ma2(i,j)
       end
   end     
  
  if  max(ma2) lt 0.5 then goto,nextnnn
   
   drname=name+string(nnn+1,format='(i2.2)')+'.ps'
   _fenster,druck,drname,form,nummer=anz*5+nnn  
   spa=1 & zei=3
   !P.Multi=[0,spa,zei] 
   if form eq 0 then pos=[0.02, 0.05 , .97, .96]
   if form eq 1 then pos=[0.01, 0.1, .98, .97]
   ric=0
   dspa=25 ;% Zwischenraum
   dzei=25 ;% Zwischenraum
   dx=(pos(2)-pos(0))/spa
   dy=(pos(3)-pos(1))/zei
   isp=0
   jze=0
   if ric eq 0 then jze=1 else isp=1

  ;print,'polar'
  ;print,polar
  
  ;print,'wi'
  ;print,wi
   
   z=[0,max(ma)]
   ;z=[0,0.45]
   lmin2=7 & lmax2=11
   lmin2=11 & lmax2=21
   _achsenwerte1,z,zhilf,levanz,lmin2,lmax2
   levanz=levanz+1
   lev=fltarr(levanz)
   lev(0:levanz-2)=zhilf(0:levanz-2)
   if zhilf(0) gt 0 then begin 
       levanz=levanz+1
       lev=fltarr(levanz)
       lev(1:levanz-2)=zhilf(0:levanz-3)
       lev(0)=0.0
   end
   lev(levanz-1)=lev(levanz-2)+(lev(levanz-2)-lev(levanz-3))
   print,levanz,' levels(all):',lev(0),lev(1),lev(2),'...',lev(levanz-2),lev(levanz-1)
   lab=lev
   lab(*)=1
  
   k=strpos(titlecase(nnn),':')+2
   l=max([strlen(titlecase(nnn)),4])
   text=strmid(titlecase(nnn),k,l)
   text=strtrim(text)

  xa=0.0 & xe=180 & ya=0 & ye=90
  lmin=3 & lmax=5
  if anz eq 1 then  ueber=text+' DISTRIBUTION SCATTERING PER SOLID ANGLE'
  if anz eq 2 then  ueber=text+' DISTRIBUTION YIELD PER SOLID ANGLE'
  if anz eq 3 then  ueber=text+' DISTRIBUTION TRANSMISSION PER SOLID ANGLE'
  if anz eq 4 then  ueber=text+' DISTRIBUTION TRANSMISSION SPUTTERING PER SOLID ANGLE'
  xtt='azimuthal angle [ degree ]'
  ytt='polar angle [ degree ]'
  unter=' '
  xrichtung=0 & yrichtung=0 & sca=0 

;===Bild1 

  _posit,ric,isp,jze,spa,zei,dspa,dzei,dx,dy,pos,pos1
  pos1(2)=pos1(2)-.10
  
  _bild,xa,xe,ya,ye,lmin,lmax,pos1,xtt,ytt,ueber,unter,gr,di,form $
         ,xrichtung,yrichtung,sca  
         ;,xachsread=xachsread,yachsread=yachsread $
         ;,format_x=format_x
         ;,format_y='(f0.0)'
     col=lev
     for jj=0,levanz-1 do begin
       col(jj)=250*lev(jj)/max(lev)
     end     
     col2=lev
     col2(levanz-1)=250
     for jj=0,levanz-2 do begin
       col2(jj)=col(jj+1)
     end

     contour,ma(*,*) $
      ,azim,wi $
      ,/overplot $
      ,c_color=col2,levels=lev,c_labels=lab,/cell_fill;,min_value=.01
     contour,ma(*,*),azim,wi $
      ,/overplot,levels=lev,c_labels=lab; ;,min_value=.01
  
     
     balkq=0
     xab=pos1(2)+.01
     yab=pos1(1)
     dxb=.01
     dyb=(pos1(3)-pos1(1))/levanz
     balkint=2
     grb=gr/2
     
     legend_balk=0        ;each level == level legende
     anz_leg_balk=levanz  ;no influence if legend_balk=0 
     leg_balk    =lev     ;no influence if legend_balk=0 
     levanf=lev(0)        ;no influence if legend_balk=0 
     levend=lev(levanz-1) ;no influence if legend_balk=0 

     _balken2,lev,balkq,xab,dxb,yab,dyb,col,balkint,grb,di $
     ;,skalfak=skalfak
      ,forma='(e8.1)' $
      ,legend_balk,anz_leg_balk,leg_balk,levanf,levend

;===Bild2 
     _posit,ric,isp,jze,spa,zei,dspa,dzei,dx,dy,pos,pos1
     pos1(2)=pos1(2)-.10
 
     _bild,xa,xe,ya,ye,lmin,lmax,pos1,xtt,ytt,ueber,unter,gr,di,form $
         ,xrichtung,yrichtung,sca  
         ;,xachsread=xachsread,yachsread=yachsread $
         ;,format_x=format_x
         ;,format_y='(f0.0)'
     
     xa=pos1(2)+.01
     ya=pos1(1)
     dxb=.01
     dyb=(pos1(3)-pos1(1))/levanz
     _balken2,lev,balkq,xa,dxb,ya,dyb,col,balkint,grb,di $
     ;,skalfak=skalfak
      ,forma='(e8.1)' $
      ,legend_balk,anz_leg_balk,leg_balk,levanf,levend
   
   for i=0,na-1 do begin ;azi
       for j=0,np-1 do begin ;polar
         fa=0
         for k=0,levanz-1 do begin
           ;print,k,lev(k),col(k),ma(i,j)
           if ma(i,j) ge lev(k)  then fa=col2(k)
         end
         
         if j eq 0 and i eq 0 then polyfill,[0        ,azim(i),azim(i),0        ,0        ],[wi2(j),wi2(j),90       ,90     ,wi2(j)],color=fa
         if j ne 0 and i eq 0 then polyfill,[0        ,azim(i),azim(i),0        ,0        ],[wi2(j),wi2(j),wi2(j-1),wi2(j-1),wi2(j)],color=fa
         if j eq 0 and i ne 0 then polyfill,[azim(i-1),azim(i),azim(i),azim(i-1),azim(i-1)],[wi2(j),wi2(j),90      ,90      ,wi2(j)],color=fa
         if j ne 0 and i ne 0 then polyfill,[azim(i-1),azim(i),azim(i),azim(i-1),azim(i-1)],[wi2(j),wi2(j),wi2(j-1),wi2(j-1),wi2(j)],color=fa
       end
   end  

   ;--Nochmal  
    _bild,xa,xe,ya,ye,lmin,lmax,pos1,xtt,ytt,ueber,unter,gr,di,form $
         ,xrichtung,yrichtung,sca  

     ;contour,ma(*,*),azim,wi $
     ; ,/overplot,levels=lev,c_labels=lab; ;,min_value=.01

;===Bild3  

;print,'alpha:'
;alpha=1.0d0
;read,alpha
   _posit,ric,isp,jze,spa,zei,dspa,dzei,dx,dy,pos,pos1
   pos1(2)=pos1(2)-.10

    sca=1
    xtt=ytt
    Ytt=' '
    _bild,-ye,ye,ya,ye,lmin,lmax,pos1,xtt,ytt,ueber,unter,gr,di,form $
         ,xrichtung,yrichtung,sca $ 
         ;,xachsread=xachsread,yachsread=yachsread $
         ;,format_x=format_x
         ,format_y='(f0.0)'

     xab=pos1(2)+.01
     yab=pos1(1)
     dxb=.01
     dyb=(pos1(3)-pos1(1))/levanz
     _balken2,lev,balkq,xab,dxb,yab,dyb,col,balkint,grb,di $
     ;,skalfak=skalfak
      ,forma='(e8.1)' $
      ,legend_balk,anz_leg_balk,leg_balk,levanf,levend
 

   sca=1 
     ;---contour with fill
     ;col=lev
     ;for jj=0,levanz-1 do begin
     ;  col(jj)=250*lev(jj)/max(lev)
     ;end
   
     ;contour,zz,x2,y2 $
     ; ,c_color=col,levels=lev,c_labels=lab,/cell_fill,/irregular,/overplot;,min_value=.01

     ;contour,zz,x2,y2 $
     ; ,levels=lev,c_labels=lab,/irregular,/overplot ;,min_value=.01
 
     sfla=0.0d0
 
     for i=0,na-1 do begin ;azi
       for j=0,np-1 do begin ;polar
         zzz=0.0
         for k=0,ke-1 do begin ;energy           
           fla=azim(0)*!dpi/180d0*polar(0)*!dpi/180d0*sin((polar(j)-polar(0)/2)*!dpi/180d0)
           if lmatout_cos_angle eq 1 then fla=azim(0)*!dpi/180d0*polar(0)
            zzz=zzz+matrix(nnn,k,i,j)/(nh*nr)/fla
         end
         zzz=zzz/2 ;zaehlung doppelt sie ma(
         if(i eq 0 and j eq 0)then print,'TTT',total(matrix(nnn,*,i,j)),(nh*nr),fla
         sfla=sfla+fla
       
         fa=0
         for k=0,levanz-1 do begin
           ;print,k,lev(k),col(k),ma(i,j)
           if zzz ge lev(k)  then fa=col2(k)
         end        
         wi0=0
         if lmatout_cos_angle eq 1 then wi0=90
         
                                   xx =wi2(j  )*cos(azim(i  )*!dpi/180d0)
         if j ne 0            then xx2=wi2(j-1)*cos(azim(i  )*!dpi/180d0) else xx2=wi0     *cos(azim(i  )*!dpi/180d0)
         if j ne 0 and i ne 0 then xx3=wi2(j-1)*cos(azim(i-1)*!dpi/180d0) else xx3=wi0     *cos(0        *!dpi/180d0)
         if j eq 0 and i ne 0 then                                             xx3=wi0     *cos(azim(i-1)*!dpi/180d0)
         if j ne 0 and i eq 0 then                                             xx3=wi2(j-1)*cos(0        *!dpi/180d0)
         if            i ne 0 then xx4=wi2(j  )*cos(azim(i-1)*!dpi/180d0) else xx4=wi2(j  )*cos(0        *!dpi/180d0)
        
                                   yy =wi2(j  )*sin(azim(i  )*!dpi/180d0) 
         if j ne 0            then yy2=wi2(j-1)*sin(azim(i  )*!dpi/180d0) else yy2=wi0     *sin(azim(i  )*!dpi/180d0) 
         if j ne 0 and i ne 0 then yy3=wi2(j-1)*sin(azim(i-1)*!dpi/180d0) else yy3=wi0     *sin(0        *!dpi/180d0)
         if j eq 0 and i ne 0 then                                             yy3=wi0     *sin(azim(i-1)*!dpi/180d0)
         if j ne 0 and i eq 0 then                                             yy3=wi2(j-1)*sin(0        *!dpi/180d0)
         if            i ne 0 then yy4=wi2(j  )*sin(azim(i-1)*!dpi/180d0) else yy4=wi2(j  )*sin(0        *!dpi/180d0)
         
         polyfill,[xx    ,xx2    ,xx3      ,xx4    ,xx    ],[yy        ,yy2     ,yy3     ,yy4      ,yy      ],color=fa
       
       end
     end     
     print,'SUM area (1/4 surface of sphere):',sfla
     

   ;---nochmal
   _bild,-ye,ye,ya,ye,lmin,lmax,pos1,xtt,ytt,ueber,unter,gr,di,form $
         ,xrichtung,yrichtung,sca $ 
         ;,xachsread=xachsread,yachsread=yachsread $
         ;,format_x=format_x
         ,format_y='(f0.0)'
   oplot,[0,100],[0, 30*!dpi/180.d0],/polar,linestyle=1
   oplot,[0,100],[0, 60*!dpi/180.d0],/polar,linestyle=1
   oplot,[0,100],[0, 90*!dpi/180.d0],/polar,linestyle=1
   oplot,[0,100],[0,120*!dpi/180.d0],/polar,linestyle=1
   oplot,[0,100],[0,150*!dpi/180.d0],/polar,linestyle=1
   
   hr=fltarr(181)
   hw=fltarr(181)   
   for k=1,3 do begin
    for j=0,180 do begin
     if k eq 1 then hr(j)=30
     if k eq 2 then hr(j)=60
     if k eq 3 then hr(j)=90
     if k eq 4 then hr(j)=120
     if k eq 5 then hr(j)=150
     hw(j)=j*!dpi/180.
    end
    oplot,hr,hw,/polar,linestyle=1
   end

   for j=30,150,30 do begin
     _int2string,90-j,90-j,s
     _int2string,j,j,s
     if j lt 90 then xyouts,hr(j)*1.05*cos(hw(j)),hr(j)*1.05*sin(hw(j)),'!3'+s+'!9%!3',/data
     if j gt 90 then xyouts,hr(j)*1.10*cos(hw(j)),hr(j)*1.05*sin(hw(j)),'!3'+s+'!9%!3',/data
   end

   
   
   k=strpos(titlecase(0),',')
   xyouts,.10,.05,'!6ANALYSIS   '+bez,charsize=gr*.7,charthick=gd,/normal 
   if lmatout_cos_angle eq 0 then begin 
     xyouts,.10,.02,'!6'+strmid(titlecase(0),0,k)+' (linear polar-distr.)',charsize=gr*.6,charthick=gd,/normal  
   end else begin
     xyouts,.10,.02,'!6'+strmid(titlecase(0),0,k)+' (cosine polar-distr.)',charsize=gr*.6,charthick=gd,/normal     
   end
   xyouts,.65,.00,'!6'+textdatum,charsize=gr*.4,charthick=gd,/normal  
   _druckende,druck,drname,form  
    if druck eq 1 then print,'ps2pdf '+drname+' ->pdf'   
    if druck eq 1 then spawn,'ps2pdf '+drname   


   open_window=0
;   if itest gt 1  then begin
;    ;---contour with fill ,more as one, less as 3, new window/page
;    j=1
;    open_window=0
;    for inn=0,ncp-1 do begin
;     if summa(inn) gt 0 then begin
;      if j eq 1 or j eq 4 or j eq 7 then begin
;        if j eq 1 then drname=name+'2.ps'
;        if j eq 4 then drname=name+'3.ps'
;        if j eq 7 then drname=name+'4.ps'
;        _fenster,druck,drname,form,nummer=anz*10+j
;         open_window=1   
;        isp=0
;        jze=0
;        if ric eq 0 then jze=1 else isp=1
;      end
;      _posit,ric,isp,jze,spa,zei,dspa,dzei,dx,dy,pos,pos1
;      pos1(2)=pos1(2)-.1
;      _bild,xa,xe,ya,ye,lmin,lmax,pos1,xtt,ytt,ueber,unter,gr,di,form $
;          ,xrichtung,yrichtung,sca  
;      z=[0,max(en)]
;      lmin=7 & lmax=11
;      _achsenwerte1,z,zhilf,levanz,lmin,lmax
;      levanz=levanz+1
;      lev=fltarr(levanz)
;      lev(0:levanz-2)=zhilf(0:levanz-2)
;      lev(levanz-1)=lev(levanz-2)+(lev(levanz-2)-lev(levanz-3))
;      s=string(inn+1,format='(i3)')
;      print,levanz,' levels(',s,'):',lev(0),lev(1),lev(2),'...',lev(levanz-2),lev(levanz-1)
;      lab=lev
;      lab(*)=1
;      ;---contour with fill
;      col=lev
;      for jj=0,levanz-1 do begin
;        col(jj)=250*lev(jj)/max(lev)
;      end
;      contour,en(inn,*,*),azim,hilf $
;       ,c_color=col,/overplot,levels=lev,c_labels=lab,/cell_fill;,min_value=.01
;      contour,en(inn,*,*),azim,hilf $
;       ,/overplot,levels=lev,c_labels=lab;,min_value=.01
;      balkq=0
;      xab=pos1(2)+.01
;      yab=pos1(1)
;      dxb=.01
;      dyb=(pos1(3)-pos1(1))/levanz
;      balkint=1
;      grb=gr/2
;      
;      legend_balk=0        ;each level == level legende
;      anz_leg_balk=levanz  ;no influence if legend_balk=0 
;      leg_balk    =lev     ;no influence if legend_balk=0 
;      levanf=lev(0)        ;no influence if legend_balk=0 
;      levend=lev(levanz-1) ;no influence if legend_balk=0 
;      
;      _balken2,lev,balkq,xab,dxb,yab,dyb,col,balkint,grb,di $ 
;              ,legend_balk,anz_leg_balk,leg_balk,levanf,levend
;
;      k=strpos(titlecase(inn),':')+2
;      l=max([strlen(titlecase(inn)),4])
;      text=strmid(titlecase(inn),k,l)
;      xyouts,pos1(0)-.15,pos1(3)-(pos1(3)-pos1(1))/6*(j) $
;      ,text,charsize=gr/2,charthick=gd,/normal,color=farbe(inn)
;      
;      if j eq 3 or j eq 6 or j eq 9 then begin
;        k=strpos(titlecase(0),',')
;        xyouts,.10,.05,'!6ANALYSIS   '+bez,charsize=gr*.7,charthick=gd,/normal  
;        xyouts,.10,.02,'!6'+strmid(titlecase(0),0,k),charsize=gr*.6,charthick=gd,/normal  
;        xyouts,.65,.00,'!6'+textdatum,charsize=gr*.4,charthick=gd,/normal  
;        _druckende,druck,drname,form         
;        if druck eq 1 then print,'ps2pdf '+drname+' ->pdf'   
;        if druck eq 1 then spawn,'ps2pdf '+drname   
;        open_window=0   
;       end
;      j=j+1  
;     end
;    end
;   end ;itest gt 1   

;  if open_window eq 1 then begin
;    k=strpos(titlecase(0),',')
;    xyouts,.10,.05,'!6ANALYSIS   '+bez,charsize=gr*.7,charthick=gd,/normal  
;    xyouts,.10,.02,'!6'+strmid(titlecase(0),0,k),charsize=gr*.6,charthick=gd,/normal  
;    xyouts,.65,.00,'!6'+textdatum,charsize=gr*.4,charthick=gd,/normal  
;    _druckende,druck,drname,form  
;    if druck eq 1 then print,'ps2pdf '+drname+' ->pdf'   
;    if druck eq 1 then spawn,'ps2pdf '+drname   
;   end
  nextnnn:
  end ;---nnn  

goto,next_bitte
;=========================================================   
   ;winkel,energy  wegen zeichnung 2*na
   ma_spek=dblarr(ncp,ke) ;number of Particle f(ener)
   ;---ener(ke)
   for nnn=0,ncp-1 do begin  
     for k=0,ke-1 do begin     ;energy
       ;sum anzahl ueber azimut and polar-winkel 
       for i=0,na-1 do begin
         for j=0,np-1 do begin  ;polarwinkel 
          if k eq 0 then hilf=0.0 else hilf=ener(k-1)
           ma_spek(nnn,k) =ma_spek(nnn,k)  +matrix(nnn,k,i,j)/(nr*nh)/(ener(k)-hilf)
          end
      end
     end 
     ;if nnn eq 0 then plot,ener(*),ma(nnn,*) else  oplot,ener(*),ma(nnn,*)
   end
   drname=name+'5.ps'
   form=2
   _fenster,druck,drname,form,nummer=4+anz,xxsize=20,yysize=20 ;5,6,7,8,xxsize=20,yysize=15
   xa=0
   xe=max(ener)
   ya=0
   ye=max(ma_spek(*,0:ke-2))
   print,xa,xe,ya,ye
   pose=[.15,.15,.99,.95]
   xtt='energy [eV]'
   ytt='d(N/N!D0!N)/dE  [-]'
   ueber='relative frequency of energy per incident particle'
   sca=0
   _bild,xa,xe,ya,ye,lmin,lmax,pose,xtt,ytt,ueber,unter,gr,di,form $
        ,xrichtung,yrichtung,sca  

    for nnn=0,ncp-1 do begin
     ;oplot,ener(*),ma_spek(nnn,*),color=farbe(nnn)  
     k=0
     oplot,[        0,ener(k)],[ma_spek(nnn,k),ma_spek(nnn,k  )],color=farbe(nnn)
     oplot,[ener(k  ),ener(k)],[ma_spek(nnn,k),ma_spek(nnn,k+1)],color=farbe(nnn)
     for k=1,ke-3 do begin     ;energy
       oplot,[ener(k-1),ener(k)],[ma_spek(nnn,k),ma_spek(nnn,k  )],color=farbe(nnn)
       oplot,[ener(k  ),ener(k)],[ma_spek(nnn,k),ma_spek(nnn,k+1)],color=farbe(nnn)
     end
     k=ke-2
     oplot,[ener(k-1),ener(k)],[ma_spek(nnn,k),ma_spek(nnn,k  )],color=farbe(nnn)
     ;oplot,[ener(k  ),ener(k)],[ma_spek(nnn,k),ma_spek(nnn,k+1)],color=farbe(nnn)
     ;k=ke-1
     ;oplot,[ener(k-1),ener(k)],[ma_spek(nnn,k),ma_spek(nnn,k  )],color=farbe(nnn)
     
     inn=nnn
     k=strpos(titlecase(inn),':')+2
     l=max([strlen(titlecase(inn)),4])
     text=strmid(titlecase(inn),k,l)
     xyouts,pose(0)-.13,pose(3)-(pose(3)-pose(1))/6*(inn+1),text,charsize=gr/2,charthick=gd,/normal,color=farbe(inn)


   end
     
    k=strpos(titlecase(0),',')
    xyouts,.10,.05,'!6ANALYSIS   '+bez,charsize=gr*.7,charthick=gd,/normal  
    xyouts,.10,.02,'!6'+strmid(titlecase(0),0,k),charsize=gr*.6,charthick=gd,/normal  
    xyouts,.65,.00,'!6'+textdatum,charsize=gr*.4,charthick=gd,/normal  
    _druckende,druck,drname,form  
    if druck eq 1 then print,'pstopdf '+drname+' ->pdf'   
    if druck eq 1 then spawn,'pstopdf '+drname   

   form=0
   
   
   next_bitte:
;aaa=''
;read,aaa
  end;anz
end  
