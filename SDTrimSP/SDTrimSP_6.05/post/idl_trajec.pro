@/afs/ipp/home/a/aam/public/archiv/TRIDYN/SDTrimSP/post/window.inc 
@/afs/ipp/home/a/aam/public/archiv/TRIDYN/SDTrimSP/post/level.inc 
@/afs/ipp/home/a/aam/public/archiv/TRIDYN/SDTrimSP/post/achsen.inc 
pro run
;input : trajec_stop_p.dat
;        trajec_back_p.dat
;        trajec_tran_p.dat
;        trajec_stop_r.dat
;        trajec_back_r.dat
;        trajec_tran_r.dat
;        trajec_all.dat
;output:trajec.ps
  
  _start
  druck=0
  print,'druck 0...sreen  1...postscript:'
  read,druck
  form=0
  text='' & text1='' & bez=''& textdatum=''
  jflu=1
  idep=1
  ncp=1
  a=1.0d0 & b=1.0d0 & c=1.0d0
  loadct,39
  close,1
  if druck eq 0 then farbe=[255,250,150,80,100,200] else $
                     farbe=[0,250,150,80,100,200]

 ;---fe...1 STOPP         BACKSCATTER    TRANSMITT'
 ;---fe...2 STOPP.SPUTTER BACKS.SPUTTER TRANS.SPUTTER'
 for fe=1,2 do begin  
   drname='trajec'+string(fe,format='(I1)')+'.ps'
   
   if fe eq 1 then namefiles=[ 'trajec_stop_p.dat' $  
                              ,'trajec_back_p.dat' $ 
                              ,'trajec_tran_p.dat' ]
   if fe eq 1 then        ub=[ 'STOPP' $  
                              ,'BACKSCATTER' $ 
                              ,'TRANSMITT' ]
   
   if fe eq 2 then namefiles= ['trajec_stop_r.dat' $
                              ,'trajec_back_r.dat' $ 
                              ,'trajec_tran_r.dat']   
   if fe eq 2 then        ub= ['STOPP.SPUTTER' $
                              ,'BACKW.SPUTTER' $ 
                              ,'TRANSM.SPUTTER']   
   
   ;---test trajectories exist --> ibild... 0,1,2,3 rows of Picture
   itramax=lonarr(3)
   i=long(1)
   ibild=0
   for ll=0,2 do begin
     name=namefiles(ll)
     openr,1,name
      ;---header
      readf,1,textdatum
      readf,1,bez  & bez=strtrim(bez,1)
      readf,1,i
      itramax(ll)=i
      if i ne 0 then ibild=ibild+1
     close,1
   end
   
   if max(itramax) ne 0 then begin 
    _fenster,druck,drname,form,nummer=fe   
    spa=1 & zei=ibild
    !P.Multi=[0,spa,zei] 
    if form eq 0 then pos=[0.02, 0.05 , .97, .96]
    if form eq 1 then pos=[0.01, 0.1, .98, .97]
    ric=0
    dspa=25 ;% Zwischenraum
    dzei=25 ;% Zwischenraum
    if zei eq 2 then dzei=20
    if spa eq 1 then dspa=10
    dx=(pos(2)-pos(0))/spa
    dy=(pos(3)-pos(1))/zei
    isp=0
    jze=0
    if ric eq 0 then jze=1 else isp=1
    druckfak=1
    if druck eq 1 then druckfak=.8
    dd=1.5*druckfak
    gr2=2.5*druckfak 
    if ibild ge 3 then gr=2.5*druckfak else gr=1.25*druckfak
   end
   
   for ll=0,2 do begin
     name=namefiles(ll)
     if itramax(ll) ne 0 then begin
       ;---only itra,xmax,ymax  read   xmax,ymax from all --> 2
       _max_tra,name,xmax,ymax,itra,2
       xa=xmax(0) & xe=xmax(1) & ya=ymax(0) & ye=ymax(1) 
       if xe-xa lt .5*(ye-ya) then begin
          dxx=xe-xa
          xa=xa-dxx*.5
          xe=xe+dxx*.5
       end
       if ye-ya lt .5*(xe-xa) then begin
          dxx=ye-ya
          ya=ya-dxx*.5
          ye=ye+dxx*.5
       end
       lmin=3 & lmax=5
       ueber=ub(ll)
       xtt='y [A] '
       ytt='x [A] '
       unter=' '
       xrichtung=0 & yrichtung=1 & sca=1
 
       _posit,ric,isp,jze,spa,zei,dspa,dzei,dx,dy,pos,pos1
       _bild,xa,xe,ya,ye,lmin,lmax,pos1,xtt,ytt,ueber,unter,gr,di,form $
              ,xrichtung,yrichtung,sca
           ;,xachsread=xachsread,yachsread=yachsread $
           ;,format_x=format_x
           ;,format_y='(f0.0)'
 
 
       oplot,[-1000,1000],[0,0],linestyle=1
       ;---read trajectories and plot     all--> 0
       _plot_tra,name,itra,0
     end
   end
   
   if max(itramax) ne 0 then begin
    xyouts,.10,.05,'!6PROJECTION OF TRAJECTORIES   '+bez,charsize=gr2*.7,charthick=gd,/normal  
    if fe eq 1 then xyouts,.10,.02,'!6PROJECTILES',charsize=gr2*.6,charthick=gd,/normal  
    if fe eq 2 then xyouts,.10,.02,'!6RECOILS',charsize=gr2*.6,charthick=gd,/normal  
    xyouts,.65,.00,'!6'+textdatum,charsize=gr2*.4,charthick=gd,/normal  
    _druckende,druck,drname,form
   end   
 end
 
 ;------------------trajec_all.dat-------------------- 

 ;---full trajectory
   drname='trajec3.ps'
   namefiles='trajec_all.dat'
   ibild=1
   itramax=long(0)
   irecoil=0
   openr,1,namefiles
      ;---header
      readf,1,textdatum
      readf,1,bez  & bez=strtrim(bez,1)
      readf,1,itramax,irecoil
   close,1
   print,' read:',namefiles
   if max(itramax) ne 0 then _fenster,druck,drname,form,nummer=3   
   spa=1 & zei=2
   !P.Multi=[0,spa,zei] 
   if form eq 0 then pos=[0.02, 0.05 , .97, .96]
   if form eq 1 then pos=[0.01, 0.1, .98, .97]
   ric=0
   dspa=25 ;% Zwischenraum
   dzei=25 ;% Zwischenraum
   if zei eq 2 then dzei=20
   if spa eq 1 then dspa=10
   dx=(pos(2)-pos(0))/spa
   dy=(pos(3)-pos(1))/zei
   isp=0
   jze=0
   if ric eq 0 then jze=1 else isp=1
   druckfak=1
   if druck eq 1 then druckfak=.8
   dd=1.5*druckfak
   gr2=2.5*druckfak 
   if ibild ge 3 then gr=2.5*druckfak else gr=1.25*druckfak
   name=namefiles
   if itramax ne 0 then begin
       _max_tra,name,xmax,ymax,itra,3 ;also recoils for dimension
       xa=xmax(0) & xe=xmax(1) & ya=ymax(0) & ye=ymax(1)
       lmin=3 & lmax=5
       ueber='PROJECTILES'
       xtt='y [A] '
       ytt='x [A] '
       unter=' '
       xrichtung=0 & yrichtung=1 & sca=1
 
       _posit,ric,isp,jze,spa,zei,dspa,dzei,dx,dy,pos,pos1
       _bild,xa,xe,ya,ye,lmin,lmax,pos1,xtt,ytt,ueber,unter,gr,di,form $
              ,xrichtung,yrichtung,sca
           ;,xachsread=xachsread,yachsread=yachsread $
           ;,format_x=format_x
           ;,format_y='(f0.0)'
       oplot,[-1000,1000],[0,0],linestyle=1
       ;---all values parent==0 read -->1
       _max_tra,name,xmax,ymax,itrap,1 ; read itrap only for projectiles
       _plot_tra,name,itra,1,itrap=itrap
   end
   
   if irecoil ne 0 then begin
     name=namefiles
     if itra ne 0 then begin
       _max_tra,name,xmax,ymax,itra,3
       ;print,'xmax',xmax
       ;print,'ymax',ymax
       xa=xmax(0) & xe=xmax(1) & ya=ymax(0) & ye=ymax(1)
       lmin=3 & lmax=5
       ueber='PROJECTILES with RECOILS'
       xtt='y [A] '
       ytt='x [A] '
       unter=' '
       xrichtung=0 & yrichtung=1 & sca=1
 
       _posit,ric,isp,jze,spa,zei,dspa,dzei,dx,dy,pos,pos1
       _bild,xa,xe,ya,ye,lmin,lmax,pos1,xtt,ytt,ueber,unter,gr,di,form $
              ,xrichtung,yrichtung,sca
           ;,xachsread=xachsread,yachsread=yachsread $
           ;,format_x=format_x
           ;,format_y='(f0.0)'
       oplot,[-1000,1000],[0,0],linestyle=1
       ;---all values  read --> 2
       _plot_tra,name,itra,2
     end
   end
   if max(itramax) ne 0 then begin
    xyouts,.10,.05,'!6PROJECTION OF TRAJECTORIES   '+bez,charsize=gr2*.7,charthick=gd,/normal  
    if irecoil eq 0 then xyouts,.10,.02,'!6ONLY PROJECTILES'       ,charsize=gr2*.6,charthick=gd,/normal  
    if irecoil ne 0 then xyouts,.10,.02,'!6PROJECTILES and RECOILS',charsize=gr2*.6,charthick=gd,/normal  
    xyouts,.65,.00,'!6'+textdatum,charsize=gr2*.4,charthick=gd,/normal  
    _druckende,druck,drname,form  
  end
end
pro _plot_tra,name,itra,itest,itrap=itrap   
   ;---itest..0   all values plot                 color
   ;---itest..1   all values parent==0 plot       color
   ;---itest..2   all values plot          only 2 color
   farbe=250
   itramax=long(1) ;number trajectories
   i=1
   text=''
   bez=''    
   openr,1,name
    ;---header   
    readf,1,text
    print,text
    readf,1,bez  
    print,bez
    readf,1,itramax
    print,'----------'
    print,'filename:',name
    print,'   max number of trajectories:',itra
    ih      =long(1)
    iproj   =long(1)    
    parent  =1    
    ncoll   =long(1)   
    species =1       
    ibn     =1  
    termflag=1
    ifarb=0
    
    for it=long(0),itra-1 do begin
      readf,1,text
      readf,1, i,ih,iproj,parent,ncoll,species,ibn,termflag
      ;print,'   ',it,ibn,' points of trajectorie'
      readf,1,text
      x=dblarr(ibn) & a=1.d0
      y=dblarr(ibn) & b=1.d0
      z=dblarr(ibn) & c=1.d0
      t=dblarr(ibn) & d=1.d0
      for k=0,ibn-1 do begin
        readf,1,i,a,b,c,d
        x(k)=a
        y(k)=b
        z(k)=c
        t(k)=d
        r=sqrt(y*y+z*z)
        r=y
      end ;k

      if itest eq 0  then begin
        ;---all  
        oplot,r,x,color=50+200/itra*ifarb,psym=-2,symsize=.4
        ifarb=ifarb+1
      end
      if itest eq 1 and parent eq 0 then begin
        ;---only projectiles 
        oplot,r,x,color=50+200/itrap*ifarb,psym=-2,symsize=.4
        ifarb=ifarb+1
      end
      if itest eq 2  then begin
        ;---projectiles+recoils
        if parent eq 0 then  oplot,r,x,psym=-2,symsize=.4 $
                       else  oplot,r,x,color=250,psym=3;,psym=-2,symsize=.4
      end
    end  ;it 

  close,1 
end
pro _max_tra,name,xmax,ymax,itra,itest   
   ;---itest..1   all values parent==0 read 
   ;---itest..2   all values read 
   ;---itest..3   all values read and trajec-all.dat
   maxitra=long(1) ;number trajectories
   i=1    
   text=''
   openr,1,name
    xmax=[100000.,-100000.]
    ymax=[100000.,-100000.]
    ;---header   
    readf,1,text
    readf,1,text 
    readf,1,maxitra
    ih      =long(1)
    iproj   =long(1)    
    parent  =1    
    ncoll   =long(1)   
    species =1       
    ibn     =1  
    termflag=1
    if itest eq 3 then maxitra=500000
    
    if itest ge 1 then begin
     for it=long(0),maxitra-1 do begin
      readf,1,text
      if strtrim(text,2) eq 'ende' then goto,ende
      itra=it+1
      readf,1, i,ih,iproj,parent,ncoll,species,ibn,termflag
      readf,1,text
      x=dblarr(ibn) & a=1.d0
      y=dblarr(ibn) & b=1.d0
      z=dblarr(ibn) & c=1.d0
      t=dblarr(ibn) & d=1.d0
      for k=0,ibn-1 do begin
        readf,1,i,a,b,c,d
        x(k)=a
        y(k)=b
        z(k)=c
        t(k)=d
        r=sqrt(y*y+z*z)
        r=y
      end ;k
      if itest eq 1 and parent eq 0 then begin
        if min(r) lt xmax(0) then xmax(0)=min(r)
        if min(x) lt ymax(0) then ymax(0)=min(x)
        if max(r) gt xmax(1) then xmax(1)=max(r)
        if max(x) gt ymax(1) then ymax(1)=max(x)
      end
      if itest ge 2 then begin
        if min(r) lt xmax(0) then xmax(0)=min(r)
        if min(x) lt ymax(0) then ymax(0)=min(x)
        if max(r) gt xmax(1) then xmax(1)=max(r)
        if max(x) gt ymax(1) then ymax(1)=max(x)
      end
     end  ;it
    
    end
   ende:
   close,1 
end
