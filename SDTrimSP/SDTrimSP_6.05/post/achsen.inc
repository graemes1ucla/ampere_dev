pro begin_achsen_inc__________
end
;;pro _dim2,dx,dy,pos,xt,yt,ueber,unter,gr,di,form,k,nxr=nxr,nyr=nyr
;;pro _dim3,dx,dy,pos,xt,yt,ueber,unter,gr,di,form,nxr=nxr,nyr=nyr,farbe=farbe
;;pro _achsenwerte1,z,zhilf,j,lmin,lmax
;;pro _achsenwerte2,z,zhilf,j,lmin,lmax
;;pro _dim,dx,dy,idx,idy,strdx,strdy,pos,tx,ty,ueber,unter,gr,di,form $
;;        ,nxr=nxr,nyr=nyr,xminor=xminor,yminor=yminor
;;--- _bild...vollst. Dimensionierung (_achslevels(x/y)+_dim)
;;pro _bild,xa,xe,ya,ye,lmin,lmax,pos,xtt,ytt,ueber,unter,gr,di,form $
;;         ,xrichtung,yrichtung,sca,xachsread=xachsread,yachsread=yachsread $
;;         ,format_x=format_x,format_y=format_y
;;pro _achslevels,xa,xe,lmin,lmax,ndx,xdx,strdx,forma=forma


;;================================================================
;pro _dim2,dx,dy,pos,xt,yt,ueber,unter,gr,di,form,k,nxr=nxr,nyr=nyr
;if n_elements(nxr) eq 0 then nxr=0
;if n_elements(nyr) eq 0 then nyr=0
;;---alte Version mit k und ohne farbe ---> _dim3
;
;;dx,dy ...   realwerte x,y
;;pos   ...   pos[0.,0.,1.,1.]
;;titel ...   xt,yt,ueber,unter
;;gr    ...
;;di    ...
;;form  ...   0...hoch  1...quer
;;nxr  ... 1 x Achse umdrehen
;;nyr  ... 1 y Achse umdrehen
;;k    ... sollte 0 sein
;  hilf=size(dx)
;  imax=hilf(1)
;  hilf=size(dy)
;  jmax=hilf(1)
;   a=fltarr(2)
;   b=fltarr(2)
;   a(0)=dx(0)
;   b(0)=dy(0)
;   a(1)=dx(imax-1)
;   b(1)=dy(jmax-1)
;   xmin=dx(0)
;   xmax=dx(imax-1)
;   ymin=dy(0)
;   ymax=dy(jmax-1)
;   yl=(ymax-ymin)
;   xmin=xmin+xmin/10000.+1.0e-5
;   ymin=ymin+ymin/10000.+1.0e-5
;   xmax=xmax-xmax/10000.-1.0e-5
;   ymax=ymax-ymax/10000.-1.0e-5
;   if nxr eq 0 then xr=[xmin,xmax] else xr=[xmax,xmin]
;   if nyr eq 0 then yr=[ymin,ymax] else yr=[ymax,ymin]
;;hoch
;  if(form eq 0 ) then begin
;   neu=pos(2)-(xmax-xmin)/(yl)*(pos(3)-pos(1))*27./18.
;   if neu lt pos(0) then $
;   pos(1)=pos(3)-(pos(2)-pos(0))*(yl)/(xmax-xmin)*18./27. $
;   else pos(0)=neu
;  end
;;quer
;  if(form eq 1 ) then begin
;   neu=pos(2)-(xmax-xmin)/(yl)*(pos(3)-pos(1))*18./27.
;   if neu lt pos(0) then $
;   pos(1)=pos(3)-(pos(2)-pos(0))*(yl)/(xmax-xmin)*27./18. $
;   else pos(0)=neu
;  end
; ;---> Position + Skalierung
; if k eq 0 then $
;                     plot,a,b,$
;                     charsize=gr,$
;                     charthick=di,$
;                     thick=di,$
;                     subtitle=unter,$
;                     title=ueber ,$
;                     xtitle=xt,$
;                     ytitle=yt,$
;                     xrange=xr,$
;                     yrange=yr,$
;                     position=pos,$
;                     /nodata,/noerase $
;                     ,background=0 $
;            else $
;                     plot,a,b,$
;                     charsize=gr,$
;                     charthick=di,$
;                     thick=di,$
;                     subtitle=unter,$
;                     title=ueber ,$
;                     xtitle=xt,$
;                     ytitle=yt,$
;                     xrange=[xmin,xmax],$
;                     yrange=[ymin,ymax],$
;                     position=pos,$
;                     /nodata,/noerase
;end
;;===============================================================
;pro _dim3,dx,dy,pos,xt,yt,ueber,unter,gr,di,form,nxr=nxr,nyr=nyr,farbe=farbe
;if n_elements(nxr) eq 0 then nxr=0
;if n_elements(nyr) eq 0 then nyr=0
;if n_elements(farbe) eq 0 then farbe=0
;
;;Version 3    30.08.99 mit farbe
;;dx,dy ...   realwerte x,y
;;pos   ...   pos[0.,0.,1.,1.]
;;titel ...   xt,yt,ueber,unter
;;gr    ...
;;di    ...
;;form  ...   0...hoch  1...quer
;;nxr  ... 1 x Achse umdrehen
;;nyr  ... 1 y Achse umdrehen
;  hilf=size(dx)
;  imax=hilf(1)
;  hilf=size(dy)
;  jmax=hilf(1)
;   a=fltarr(2)
;   b=fltarr(2)
;   a(0)=dx(0)
;   b(0)=dy(0)
;   a(1)=dx(imax-1)
;   b(1)=dy(jmax-1)
;   xmin=dx(0)
;   xmax=dx(imax-1)
;   ymin=dy(0)
;   ymax=dy(jmax-1)
;   yl=(ymax-ymin)
;   xmin=xmin+xmin/10000.+1.0e-5
;   ymin=ymin+ymin/10000.+1.0e-5
;   xmax=xmax-xmax/10000.-1.0e-5
;   ymax=ymax-ymax/10000.-1.0e-5
;   if nxr eq 0 then xr=[xmin,xmax] else xr=[xmax,xmin]
;   if nyr eq 0 then yr=[ymin,ymax] else yr=[ymax,ymin]
;;hoch
;  if(form eq 0 ) then begin
;   neu=pos(2)-(xmax-xmin)/(yl)*(pos(3)-pos(1))*27./18.
;   if neu lt pos(0) then $
;   pos(1)=pos(3)-(pos(2)-pos(0))*(yl)/(xmax-xmin)*18./27. $
;   else pos(0)=neu
;  end
;;quer
;  if(form eq 1 ) then begin
;   neu=pos(2)-(xmax-xmin)/(yl)*(pos(3)-pos(1))*18./27.
;   if neu lt pos(0) then $
;   pos(1)=pos(3)-(pos(2)-pos(0))*(yl)/(xmax-xmin)*27./18. $
;   else pos(0)=neu
;  end
; ;---> Position + Skalierung
;                     plot,a,b,$
;                     charsize=gr,$
;                     charthick=di,$
;                     thick=di,$
;                     color=farbe,$
;                     subtitle=unter,$
;                     title=ueber ,$
;                     xtitle=xt,$
;                     ytitle=yt,$
;                     xrange=xr,$
;                     yrange=yr,$
;                     position=pos,$
;                     /nodata,/noerase $
;                     ,background=0
;end
;;----------------------------------------------------------------------
pro _achsenwerte1,z,zhilf,j,lmin,lmax
  ;--- Berechnung der Levels fur beliebige Achsen, innerhalb des Bereiches
   ;---max.Anzahl der Werte =100
    k=100
    lmin=max([lmin,3])
    lmax=min([lmax,k])
    lmax=max([lmax,lmin])

    ;print,'Min./Max. number of scale-values:',lmin,lmax
    zhilf=fltarr(k)

   ;---Einfabewerte
    z=z(sort(z))
    ;print,'Z-werte:'
    ;print,z
     anz=1
b:  maxhilf=0
    minhilf=0
   ;---Dimension (dd) und Skalierungsanfang (da)
    dd=exp(fix(alog10(abs(max(z)-min(z))))*alog(10))
    ;print,'10-Dimension of scal: ',dd

a:  da=fix(min(z)/dd)*dd-dd
    ;print,'scal begin: ',da

     ;---Werte setzen
     j=0
     jhilf=0
     for i=0,k do begin
       ;---Werte setzen innerhalb (beginnnend mit 1)
       if( (da gt min(z)) and (da lt max(z)) ) then begin
          zhilf(j)=da
          if abs(zhilf(j)) lt abs(dd/100.0) then zhilf(j)=0.0
          j=j+1
       endif

       da=da+dd
     endfor

      dd=exp(fix(alog10(abs(max(z)-min(z))))*alog(10))
      
     ;---Anzahl der Skalierungen zu klein
       if (j+1 le lmin) and (maxhilf eq 0) then begin
           ;print,'to small',dd
           if minhilf eq 0 then dd=dd/2
           if minhilf eq 1 then dd=dd/4
           if minhilf eq 2 then dd=dd/5
           if minhilf eq 3 then dd=dd/10
           if minhilf eq 4 then dd=dd/20
           if minhilf eq 5 then dd=dd/40
           if minhilf eq 6 then dd=dd/50
           minhilf=minhilf+1
           if minhilf eq 7 then begin
             if lmin gt 3 then lmin=lmin-1 else goto,ende
             goto,b
           end
           goto,a
      end
     ;---Anzahl der berechneten Skalierungen zu gross
     ;          und noch nicht auf 'to smal' verandert
      if (j+1 gt lmax) and (minhilf eq 0) then begin
           ;print,'to great',dd
           if maxhilf eq 0 then dd=dd*2
           if maxhilf eq 1 then dd=dd*2.5
           if maxhilf eq 2 then dd=dd*5
           if maxhilf eq 3 then dd=dd*10
           if maxhilf eq 4 then dd=dd*20
           if maxhilf eq 5 then dd=dd*25
           if maxhilf eq 6 then dd=dd*100
           maxhilf=maxhilf+1
           if maxhilf eq 7 then begin
              lmax=lmax+1
             goto,b
           end
           goto,a
     end
     ende: zhilf=reform(zhilf(0:j-1))
     
     ;---'nochmaliger Test auf lmax (lmax=3/4 )
      if (lmax eq 4) and (j eq 7) then begin
        j=4
        hilf=zhilf
        zhilf=fltarr(j)
        zhilf(0)=hilf(0)
        zhilf(1)=hilf(2)
        zhilf(2)=hilf(4)
        zhilf(3)=hilf(6)
      end
      if (lmax eq 4) and (j eq 6) then begin
        j=3
        hilf=zhilf
        zhilf=fltarr(j)
        zhilf(0)=hilf(0)
        zhilf(1)=hilf(2)
        zhilf(2)=hilf(4)
      end
      if (lmax eq 3) and (j eq 6) then begin
        j=3
        hilf=zhilf
        zhilf=fltarr(j)
        zhilf(0)=hilf(1)
        zhilf(1)=hilf(2)
        zhilf(2)=hilf(4)
      end

      if ((lmax eq 3) or (lmax eq 4) )and (j eq 5) then begin
        j=3
        hilf=zhilf
        zhilf=fltarr(j)
        zhilf(0)=hilf(0)
        zhilf(1)=hilf(2)
        zhilf(2)=hilf(4)
      end
      if (lmax eq 3) and (j eq 4) then begin
        j=3
        hilf=zhilf
        zhilf=fltarr(j)
        zhilf(0)=hilf(0)
        zhilf(1)=hilf(1)
        zhilf(2)=hilf(2)
      end
end
;---------------------------------------------------
;pro _achsenwerte2,z,zhilf,j,lmin,lmax
;  ;--- Berechnung der Levels fur beliebige Achsen,ausserhalb des Bereiches
;
;   ;---max.Anzahl der Werte =100
;    k=100
;    lmin=max([lmin,3])
;    lmax=min([lmax,k])
;    lmax=max([lmax,lmin])
;
;    ;print,'Min./Max. Anzahl der Skalierungwerte:',lmin,lmax
;    zhilf=fltarr(k)
;
;   ;---Einfabewerte
;    z=z(sort(z))
;    ;print,'Z-werte:'
;    ;print,z
;     anz=1
;b:  maxhilf=0
;    minhilf=0
;   ;---Dimension (dd) und Skalierungsanfang (da)
;    dd=exp(fix(alog10(abs(max(z)-min(z))))*alog(10))
;    ;print,'10-Dimension der Skalierung: ',dd
;
;a:  da=fix(min(z)/dd)*dd-dd
;    ;print,'Skalierungsanfang: ',da
;
;     ;---Werte setzen
;     j=1
;;;     j=0
;     jhilf=0
;     for i=0,k do begin
;       ;---Werte setzen innerhalb (beginnnend mit 1)
;       if( (da gt min(z)) and (da lt max(z)) ) then begin
;          zhilf(j)=da
;          if abs(zhilf(j)) lt abs(dd/100.0) then zhilf(j)=0.0
;          j=j+1
;       endif
;
;       ;---AnfangsWerte setzen ausserhalb
;       if (j eq 2) and (jhilf eq 0) then begin
;          zhilf(0)=da-dd
;          if abs(zhilf(0)) lt abs(dd/100.0) then zhilf(0)=0.0
;          jhilf=1
;       end
;
;       da=da+dd
;     endfor
;
;     ;---Endwerte setzen ausserhalb
;      zhilf(j)=zhilf(j-1)+dd
;      j=j+1
;
;      dd=exp(fix(alog10(abs(max(z)-min(z))))*alog(10))
;     ;---Anzahl der Skalierungen zu klein
;      if j+1 le lmin then begin
;           if minhilf eq 0 then dd=dd/2
;           if minhilf eq 1 then dd=dd/4
;           if minhilf eq 2 then dd=dd/5
;           if minhilf eq 3 then dd=dd/10
;           if minhilf eq 4 then dd=dd/20
;           if minhilf eq 5 then dd=dd/40
;           if minhilf eq 6 then dd=dd/50
;           minhilf=minhilf+1
;           if minhilf eq 7 then begin
;             if lmin gt 3 then lmin=lmin-1 else goto,ende
;             goto,b
;           end
;           goto,a
;      end
;     ;---Anzahl der berechneten Skalierungen zu gross
;     ;          und noch nicht auf 'zu klein' verandert
;      if (j+1 gt lmax) and (minhilf eq 0) then begin
;           if maxhilf eq 0 then dd=dd*2
;           if maxhilf eq 1 then dd=dd*2.5
;           if maxhilf eq 2 then dd=dd*5
;           if maxhilf eq 3 then dd=dd*10
;           if maxhilf eq 4 then dd=dd*20
;           if maxhilf eq 5 then dd=dd*25
;           if maxhilf eq 6 then dd=dd*100
;           maxhilf=maxhilf+1
;           if maxhilf eq 7 then begin
;              lmax=lmax+1
;             goto,b
;           end
;           goto,a
;     end
;     ende: zhilf=reform(zhilf(0:j-1))
;end
;
;-----------------------------------------------------
pro _dim,dx,dy,idx,idy,strdx,strdy,pos,tx,ty,ueber,unter,gr,di,form $
        ,nxr=nxr,nyr=nyr,xminor=xminor,yminor=yminor,scal=scal
if n_elements(nxr) eq 0 then nxr=0
if n_elements(nyr) eq 0 then nyr=0
if n_elements(xminor) eq 0 then xminor=0
if n_elements(yminor) eq 0 then yminor=0
if n_elements(scal) eq 0 then scal=1

;---Variante  von dim2(alt) und dim3(neu), aber mit xminor/yminor
;---                 und idx,idy,strdx,strdy 
;  dx,dy             ... realwerte x/y
;  idx,idy           ... index der Beschriftung bei zeichnen ueber Index
;  strdx,strdy       ... Beschriftung
;  pos               ... Position[0.,0.,1.,1.]
;  tx,ty,ueber,unter ... Beschriftung
;  gr,di             ... Groesse,Dicke
;  form              ... 0...Hoch-/1...Querformat
;  nxr               ... 1 x Achse umdrehen
;  nyr               ... 1 y Achse umdrehen
;  xminor            ... Anzahl der Zwischenraume kl. Striche X-Achse
;  yminor            ... Anzahl der Zwischenraume kl. Striche Y-Achse
;  scal              ... 1...Gleiche Skalierung der Achsen / 0...
  hilf =size(idx)
  ndx=hilf(1)-1
  hilf=size(idy)
  ndy =hilf(1)-1
  hilf=size(dx)
  imax=hilf(1)
  hilf=size(dy)
  jmax=hilf(1)
   a=fltarr(2)
   b=fltarr(2)
   a(0)=dx(0)
   b(0)=dy(0)
   a(1)=dx(imax-1)
   b(1)=dy(jmax-1)
  xmin=dx(0)
  xmax=dx(imax-1)
  ymin=dy(0)
  ymax=dy(jmax-1)
  yl=(ymax-ymin)

  if scal eq 1 then begin
    if(form eq 0 ) then begin
      ;---hoch
      neu=pos(2)-(xmax-xmin)/(yl)*(pos(3)-pos(1))*27./18.
      if neu lt pos(0) then $
      pos(1)=pos(3)-(pos(2)-pos(0))*(yl)/(xmax-xmin)*18./27. $
      else pos(0)=neu
    end
    if(form eq 1 ) then begin
      ;---quer
      neu=pos(2)-(xmax-xmin)/(yl)*(pos(3)-pos(1))*18./27.
      if neu lt pos(0) then $
      pos(1)=pos(3)-(pos(2)-pos(0))*(yl)/(xmax-xmin)*27./18. $
      else pos(0)=neu
    end
  endif
  if nxr eq 0 then xr=[xmin,xmax] else xr=[xmax,xmin]
  if nyr eq 0 then yr=[ymin,ymax] else yr=[ymax,ymin]

 ;---> Position + Skalierung
       plot,a,b,$
       charsize=gr,$
       charthick=di,$
       thick=di,$
       xthick=di,$
       ythick=di,$
       xticklen=.02*gr, $
       yticklen=.02*gr, $
       subtitle=unter,$
       title=ueber,$
       xtitle=tx,$
       ytitle=ty,$
       xrange=xr,$
       yrange=yr,$
       position=pos,$
       Xticks=ndx, Yticks=ndy   ,$
       Xtickv=idx, Ytickv=idy,$
       Xtickname=strdx ,$
       Ytickname=strdy ,$
       xminor=xminor ,$
       yminor=yminor ,$
       /nodata,$
       /noerase,$
       background=0
end
;-----------------------------------------------------
pro _bild,xa,xe,ya,ye,lmin,lmax,pos,xtt,ytt,ueber,unter,gr,di,form $
         ,xrichtung,yrichtung,sca,xachsread=xachsread,yachsread=yachsread $
         ,format_x=format_x,format_y=format_y
   ;---sca         ... 1 -->alle Bilder in gleichem Mass-Stab (1/0)
   ;   lmin        ... min Anzahl der Beschriftung
   ;   lmax        ... max Anzahl der Beschriftung z.B.: lmin=3, lmax=15
   ;   xrichtung   ... 1--> X-Achse gedreht (1/0)
   ;   yrichtung   ... 1--> Y-Achse gedreht (1/0)
   ;   pos         ... Bildrahmen z.B. [0,0,1,1]
   ;   unter/ueber ... Unter-/ Ueberschrift
   ;   xtt,ytt     ... Achsbezeichnungen
   ;   gr/di       ... Groesse/Dicke der Strings
   ;   form        ... 0...Hoch- / 1...Querformat
   ;   format      ... '(f0.0)'keine Beschriftung mit Zahlen sonst(z.B.'(f10.2)')
   ;   format      ... nicht vorhanden -->standard

   if n_elements(xachsread) eq 0 then xachsread=0
   if n_elements(yachsread) eq 0 then yachsread=0
   xpos=[xa,xe]
   ypos=[ya,ye]

   ;---alle Bilder in gleichem Mass-Stab... scal=1
   ;   z.B.: lmin=3
   ;         lmax=15
   ;---_achsenwerte1, innerhalb des Bereiches
   ;_achsenwerte1,xpos,xachs,xanz,lmin,lmax --> in _achslevels aufgerufen
   _achslevels,xa,xe,lmin,lmax,ndx,xdx,strdx,forma=format_x
   ;_achsenwerte1,ypos,yachs,yanz,lmin,lmax --> in _achslevels aufgerufen
   _achslevels,ya,ye,lmin,lmax,ndy,ydy,strdy,forma=format_y

   if n_elements(format_y) ne 0 then if format_y eq '(f0.0)' then strdy(*)=' '
   if n_elements(format_x) ne 0 then if format_x eq '(f0.0)' then strdx(*)=' '

   ;---Achsenbeschriftung einlesen (xachsread=1,yachsread=1)
     if xachsread eq 1 then begin
       fname='xachs.dat'
       find=FINDFILE(fname,COUNT=exist)
       if(exist eq 0) then begin
         close,1
         print,'file "xachs.dat" no exist'
         print,'write file "xachs.dat"'
         openw,1,fname
          printf,1,ndx,'  number of ticks (x-values)'
          printf,1,'    nr.(integer)         x-tick(real)     tickname(string)'
          for i=0,ndx-1 do begin
            printf,1,i,xdx(i),' ',strdx(i)
          end
         close,1
       end
       openr,1,fname
        readf,1,ndx
        xdx=fltarr(ndx)
        strdx=strarr(ndx)
        hilf=1.0 & shilf='' & ihilf=1
        readf,1,shilf
        for i=0,ndx-1 do begin
          readf,1,ihilf,hilf,shilf
          xdx(i)=hilf
          strdx(i)=shilf
        end
       close,1
     endif
     if yachsread eq 1 then begin
       ;---Achsenbeschriftung einlesen
       fname='yachs.dat'
       find=FINDFILE(fname,COUNT=exist)
       if(exist eq 0) then begin
         close,1
         print,'file "yachs.dat" no exist'
         print,'write file "yachs.dat"'
         openw,1,fname
          printf,1,ndy,'  number of ticks (y-values)'
          printf,1,'    nr.(integer)         y-tick(real)     tickname(string)'
          for i=0,ndy-1 do begin
            printf,1,i,ydy(i),' ',strdy(i)
          end
         close,1
       end
       openr,1,fname
        readf,1,ndy
        ydy=fltarr(ndy)
        strdy=strarr(ndy)
        hilf=1.0 & shilf='' & ihilf=1
        readf,1,shilf
        for i=0,ndy-1 do begin
          readf,1,ihilf,hilf,shilf
          ydy(i)=hilf
          strdy(i)=shilf
        end
       close,1
     endif
   _dim,xdx,ydy,xdx,ydy,strdx,strdy,pos $
        ,xtt,ytt,ueber,unter,gr,di,form $
        ,nxr=xrichtung, nyr=yrichtung,scal=sca

end
;==========================================================
pro _achslevels,xa,xe,lmin,lmax,ndx,xdx,strdx,forma=forma
;input:  xa,xe     ... Anfangs- und Endpunkt
;        lmin      ... min Anzahl der Beschriftung
;        lmax      ... max Anzahl der Beschriftung z.B.: lmin=3, lmax=15
;                      z.B.: lmin=3,lmax=15
;        xa,xe     ... Anfangs- und Endpunkt
;output: ndx       ... (int) Anzahl der X-Werte
;        xdx       ... (fltarr) X-Werte in Real
;        strdx     ... (strarr) X-Werte in String
;        forma     ... Formatangabe des Achsenwerte
;_achsenwerte1, innerhalb des Bereiches xa,xe
; Anwendung z.B. bei plot:
;                         ,Xticks=ndx      $
;                         ,Xtickv=xdx      $
;                         ,Xtickname=strdx $
;           oder
;   pro _dim,xdx,ydy,ndx,ndy,strdx,strdy,pos,tx,ty,ueber,unter,gr,di,form $
;           ,nxr=nxr,nyr=nyr,xminor=xminor,yminor=yminor
   if xa eq xe then xe=2*xa
   if (xa eq 0) and (xe eq 0) then xe=1
   
   if lmin lt 3    then print,'replace lmin=3'
   if lmin lt 3    then lmin=3
   if lmax lt lmin then print,'replace lmax=',lmin+1
   if lmax lt lmin then lmax=lmin+1
   xpos=[xa,xe]
   _achsenwerte1,xpos,xachs,xanz,lmin,lmax
   ;--- anz-Werte werden beschriftet
   ;--- range-Werte werden beachtet
   if xa lt xachs(0) then il=1 else il=0
   if xe gt xachs(xanz-1) then ir=1 else ir=0
   xdx=fltarr  (xanz+il+ir)
   strdx=strarr(xanz+il+ir)
   xdx(0)=xa
   strdx(0)=' '
   min_achs=min(xachs)
   max_achs=max(xachs)
   if max_achs lt -10.0*min_achs then max_achs=min_achs
  ;if max_achs lt abs(min_achs) then max_achs=min_achs
   for ii=0,xanz-1 do begin
     xdx(ii+il)=xachs(ii)
     _real2string,max_achs,xachs(ii),s,forma=forma
     strdx(ii+il)=s
   end
   if ir eq 1 then xdx(xanz-1+il+ir)=xe
   if ir eq 1 then strdx(xanz-1+il+ir)=' '
   hilf =size(xdx)
   ndx=hilf(1)-1
end
;=======================================================================
pro end_achsen_inc__________
end
