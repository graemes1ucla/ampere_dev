module work_moment
  implicit none
  integer  , parameter :: iwp      = 8  !---double precision    
  integer  , parameter :: io_out   = 6  !---standard aoutpou   
  
  !---constant real
  real(iwp), parameter  :: pi    = 3.141592653589793238d0
  real(iwp), parameter  :: pi2   = 3.141592653589793238d0*2.0d0
  
  integer   :: ndeg
  real(iwp) :: yscale,xscale,xl(10)

end module work_moment

program plmaxent
  use work_moment
  implicit none
  integer, parameter  :: vvi=25
  real(iwp) ::vvv(vvi),vvx(vvi),vvr(vvi+1)
  
  real(iwp) :: random,ra1,ra2,xanf,xend,xdif
  integer   :: seed1,seed2
  real(iwp) :: xmina,xmaxa
  
  real(iwp) ::x1s,x2s,x3s,x4s,x5s,x6s,x1sl,x2sl,x3sl,x4sl,x5sl,x6sl  
  real(iwp) ::udt ,fctexpl,fctexp3 !,fctexp2   ,fctexp,fctexp1,
  integer   ::i,j,ianz,k,nstutz,itest
  real(iwp) ::eng,es1l,es2l,es3l,x
  real(iwp) ::scalhelp,scalhelp2,fff


   !--random
   seed1=1  
   seed2=1 

   !---startvalue sum of moments
   !--log
   x1sl=0.0d0
   x2sl=0.0d0
   x3sl=0.0d0
   x4sl=0.0d0
   x5sl=0.0d0
   x6sl=0.0d0
   
   !--lin
   x1s=0.0d0
   x2s=0.0d0
   x3s=0.0d0
   x4s=0.0d0 
   x5s=0.0d0
   x6s=0.0d0
   
   !---distribution
   vvx(:)=0.0d0
   vvv(:)=0.0d0
   
   !---mumber of value
   ianz=0
   
   !100 stellen zwischen 0-10
   nstutz=100
   xanf=5.0d0
   xend=25.d0
   xdif=xend-xanf

   print*,'----------calculation distribution'
   print*,'X-anf:',xanf
   print*,'X-end:',xend
   print*,'number intervalls:',vvi
   
   do k=1,vvi+1 
     vvr(k)=xanf+(k-1)*xdif/vvi 
   enddo
   write(*,*)'distribution X-value'
   write(*,11)'vvr(k)','vvx(k)','vvr(k+1)'
   do k=1,vvi 
     vvx(k)=0.5d0*(vvr(k)+vvr(k+1))
   write(*,10)vvr(k),vvx(k),vvr(k+1)
   enddo
   
   do i=1,100000 
      x=xanf+(modulo(i-1,nstutz)+0.5d0)*xdif/nstutz
      !print*,modulo(i-1,nstutz),(modulo(i-1,nstutz)+0.5d0),x
      ra2=random(seed1,seed2) 
      
      itest=4
      !---constant
      if(itest ==1) fff=0.8
      !---sin-distribution
      if(itest ==2) fff=sin( (x-xanf)/xdif*pi)
      !---sin/2-distribution
      if(itest ==3) fff=sin( (x-xanf)/xdif*pi/2.d0)
      if(itest ==4) fff=(x-xanf)/xdif
      if ( ra2 <= fff )then
        ianz=ianz+1
        do k=1,vvi 
          if( vvr(k)<= x .and. x< vvr(k+1) )vvv(k)=vvv(k)+1
        enddo

        !---calculate moments log
        x1sl=x1sl + log10(x)
        x2sl=x2sl + log10(x)**2
        x3sl=x3sl + log10(x)**3
        x4sl=x4sl + log10(x)**4 
        x5sl=x5sl + log10(x)**5
        x6sl=x6sl + log10(x)**6
    
        !---calculate moments lin
        x1s=x1s + x
        x2s=x2s + x**2
        x3s=x3s + x**3
        x4s=x4s + x**4  
        x5s=x5s + x**5
        x6s=x6s + x**6

      endif
   enddo  
   
   !---output example of distribution
   write(*,*)'---output calculated distribution:'
   !write(*,11)'vvx','vvv','vvv/anz','vvv*vvi/anz'
   write(*,11)'X','counts' ,'P','P*dx/(Xe-Xa)'
   do k=1,vvi 
     write(*,10)vvx(k),vvv(k),vvv(k)/ianz,vvv(k)*vvi/ianz
   enddo
   write(*,11)'---','sum counts' ,'sum P','sum P*dx/(Xe-Xa)'
   write(*,10)0.0  ,sum(vvv(:)) ,sum(vvv(:)/ianz),sum(vvv(:)*vvi/ianz)
   write(*,10)


  !--input:
  !  moments      : x1s...x6s or x1sl...x6sl
  !  range        : xanf,xend
  !  number counts: ianz
   
   xdif=xend-xanf

   x1sl=x1sl/ianz
   x2sl=x2sl/ianz
   x3sl=x3sl/ianz
   x4sl=x4sl/ianz
   x5sl=x5sl/ianz
   x6sl=x6sl/ianz
   
   x1s=x1s/ianz
   x2s=x2s/ianz
   x3s=x3s/ianz
   x4s=x4s/ianz  
   x5s=x5s/ianz
   x6s=x6s/ianz
   
   !---output   
   write(*,*)'---output moments and range :'
   print*,'X-anf:',xanf
   print*,'X-end:',xend
   print*,'number/ianz:',ianz
   

   scalhelp =  SQRT(x2sl)
   scalhelp2 = SQRT(x2s)
   print*,'Eingabe:'
   write(*,11)'log. moment','lin. moment','norm moment(lg)','norm moment(lin)'
   write(*,11)'log(x)/anz','(x)/anz','log(x)/anz/Fak','(x)/anz/Fak'
   write(*,10)x1sl,x1s,x1sl/scalhelp    ,x1s/scalhelp2    
   write(*,10)x2sl,x2s,x2sl/scalhelp**2 ,x2s/scalhelp2**2 
   write(*,10)x3sl,x3s,x3sl/scalhelp**3 ,x3s/scalhelp2**3 
   write(*,10)x4sl,x4s,x4sl/scalhelp**4 ,x4s/scalhelp2**4 
   write(*,10)x5sl,x5s,x5sl/scalhelp**5 ,x5s/scalhelp2**5 
   write(*,10)x6sl,x6s,x6sl/scalhelp**6 ,x6s/scalhelp2**6 
   print*,'---------------'


   !---distribution with help of linear moments
   xmina=xanf
   xmaxa=xend
   call moments(xdif,x1s,x2s,x3s,x4s,x5s,x6s,xmina,xmaxa)

   !---vvi  ... number of depth-intervall between Xa and Xe (-->dx)
   !---ianz ... number of counts from calculation of moments
   !---udt/vvi*ianz ... recalculated counts
   write(*,11) 'x','distr(lin)','counts_old','counts_neu'
   write(20,*) vvi
   write(20,11) 'x','distr(lin)','counts_old','counts_neu'
   do k=1,vvi 
      udt=fctexp3(vvx(k))
      write(*,10) vvx(k), udt,vvv(k),udt/vvi*ianz
      write(20,10) vvx(k), udt,vvv(k),udt/vvi*ianz
   enddo

   !---distribution with help of logarithm moments
   xmina=log10(xanf)  !X-min
   xmaxa=log10(xend)  !X-max
   call moments(xdif,x1sl,x2sl,x3sl,x4sl,x5sl,x6sl,xmina,xmaxa)
   
   !---vvi  ... number of depth-intervall between Xa and Xe (-->dx)
   !---ianz ... number of counts from calculation of moments
   !---udt/vvi*ianz ... recalculated counts
   write(*,11) 'x','distr(log)','counts_old','counts_neu'
   write(20,11) 'x','distr(log)','counts_old','counts_neu'
   do k=1,vvi 
      udt=fctexpl(vvx(k))
      write(*,10) vvx(k), udt,vvv(k),udt/vvi*ianz
      write(20,10) vvx(k), udt,vvv(k),udt/vvi*ianz
   enddo

10    format(50f20.6)
11    format(50a20)
END

!--------------------------------------------------------------
subroutine moments(x0s,x1s,x2s,x3s,x4s,x5s,x6s,x0h,x1h)
  use work_moment
  implicit none

  integer, parameter :: ndst = 1000
  integer, parameter :: nd   = 10
  real(iwp)  :: x0s,x1s,x2s,x3s,x4s,x5s,x6s,x0h,x1h
  real(iwp)  :: ww(ndst),xx(ndst),r(2*nd)
  real(iwp)  :: eta(2*nd)
  real(iwp)  :: a(nd,nd),b(nd,nd),v(nd),epscon,epsmax,diffold,diff
  integer    :: indx(nd),nst0,nloopmax,ndeg0,neq,j,isw,i
  character*70 filename
  real(iwp)  :: x0,x1
  
  ndeg = 6
  ndeg = ndeg + 1

  !--skalierung
  !xscale    =  x1s
  xscale    =  sqrt(x2s)
  yscale    =  x0s
  
  x0=x0h/xscale
  x1=x1h/xscale

  eta(1)   =  1.d0
  eta(2)   =  x1s/xscale
  eta(3)   =  x2s/xscale**2
  eta(4)   =  x3s/xscale**3
  eta(5)   =  x4s/xscale**4
  eta(6)   =  x5s/xscale**5
  eta(7)   =  x6s/xscale**6
  !... # of integration points in simpson
  !         (the actual number is 2*nst0+1)
  nst0    = 100

  !... max. # of iterations
  nloopmax  =1000

  !... initial values
  !... mode = 0: degree gradually increase
  !... else xl(i) read and max. moment used

  !write(10,'(1x,"ndeg  :",i5)')ndeg
  ! write(6,'(1x,"eta(i) :")')
  ! do j = 1, ndeg
  !   write(6,'(1x,i5,f16.8)')j,eta(j)
  ! enddo

   epscon    = 1.0d-8
   epsmax    = 1.0d+20

   ndeg0 = ndeg
   neq   = 1
   do j = 1, ndeg0
     xl(j) = 0.0d0
   enddo

   diffold = 1.0d30
   isw  = 1

!111  write(6,'(1x,"maximum moment ",i5)')ndeg

      do j = 1, ndeg0
        b(j,1) = 0.0d0
      enddo
      do j = ndeg+1, ndeg0
        xl(j) = 0.0d0
      enddo

      do i = 1, nloopmax
        write(10,'(1x,"xl :")')
        do j = 1, ndeg
          xl(j)  = xl(j) + .1*b(j,1) !---faktor 0.1*b()
          write(10,'(1x,i5,g16.8)')j,xl(j)
        enddo
        call defmat(a,ww,xx,ndst,nst0,x0,x1,isw,r,xl,ndeg,nd)
        isw = 0
        do j = 1, ndeg
          b(j,1) = (r(j)-eta(j))
        enddo
        diff = 0.0d0
        do j = 1, ndeg
          diff = diff + abs(b(j,1))
        enddo
        write(10,'(1x,"diff = ",f20.10)')diff
        
        if (diff < epscon) goto 999
        if (diff > epsmax) goto 888
        diffold = diff
        
        !---output in file
        write(10,'(1x,"phi :")')
        do j = 1, ndeg
          write(10,'(1x,i5,3f18.10)')j,b(j,1),r(j),eta(j)
        enddo
        
        call gauss(a,v,indx,b,nd,ndeg,neq)
      enddo

!     if (ndeg == ndeg0) goto 999
!     ndeg = ndeg+1
!     goto 111

888   print*,'not converged'
      pause
999   continue

      !write(6,'(1x,"xl(i) :")')
      !do j = 1, ndeg
      !  write(6,'(1x,i5,f16.8)')j,xl(j)
      !enddo
end
!*=====================================================================
subroutine defmat(a,ww,xx,ndst,nst0,x0,x1,isw,r,xl,ndeg,nd)
!
!     simpson integration
!     the actual number of integration points is   2*nst0 + 1
!
  use work_moment, only: iwp !,xscale,yscale,ndeg,xl
  implicit none
  real(iwp)  :: xx(*),factor,xl(10),r(2*nd),a(nd,nd),ww(*),x0,x1
  integer :: i,nst,nst0,isw,ndst,ndeg,nd,is,j,j1,j2    
  real(iwp)  :: dx,x,ee,fctexp2

      nst = 2*nst0+1

      if (isw == 1) then
        if (nst > ndst) then
           print*,'ndst should be .ge.',nst
           stop 1111
        endif
        factor = (x1-x0)/(6*nst0)
        is = 1
        do i = 2, nst-1
          ww(i) = (3 + is)*factor
          is    = -is
        enddo
        ww(1)   = factor
        ww(nst) = factor
        dx     = (x1-x0)/(nst-1)
        do i = 1, nst
          xx(i) = x0 + dx*(i-1)
        enddo
      endif

      do j = 1, 2*ndeg
        r(j) = 0.0d0
      enddo
      do i = 1, nst
      
      x   = xx(i)
      !---ee .. wahrscheinlichkeit von x
      ee  = fctexp2(x) !log

      !---bildung der momente r(1..12) r=f(x)
      do j = 1, 2*ndeg
        r(j) = r(j)  + ww(i)*ee
        ee   = ee*x
      enddo
      
      enddo
      do j1 = 1, ndeg
        do j2 = j1, ndeg
          a(j1,j2) = r(j1+j2-1)
          !... original indices are j0 +1 = j
          !... hence: (j01+j02) + 1-> j1+j2-2 + 1 = j1+j2-1
          a(j2,j1) = a(j1,j2)
        enddo
      enddo

end

!============================
function fctexp2(x)
  !---only call from defmat
  use work_moment, only: iwp,ndeg,xl
  implicit none
  real(iwp)  :: arg,factor,fctexp2,e0,x
  integer :: i    
  arg    = 0.0d0
  factor = 1.0d0
  do i = 1, ndeg
    arg    = arg + xl(i) * factor
    factor = factor*x
  enddo
  arg = -arg
  if (abs(arg) < 82.0) then
      e0 = exp(arg)
  else if (arg < 0.0) then
      e0 = 1.d0-20
  else if (arg > 0.0) then
      e0 = 1.d0+20
  endif
  fctexp2 = e0
end

!============================
function fctexpl(xx)
  use work_moment, only: iwp,xscale,yscale,ndeg,xl
  implicit none
  real(iwp)  :: fctexpl,xx,arg,factor,e0,x
  integer :: i    

  arg    = 0.0d0
  factor = 1.0d0
  x      = log10(xx)/xscale

  do i = 1, ndeg
    arg    = arg + xl(i) * factor
    factor = factor*x
  enddo

  arg = -arg
  if (abs(arg) < 82.0) then
      e0 = exp(arg)
  else if (arg < 0.0) then
      e0 = 1.d0-20
  else if (arg > 0.0) then
      e0 = 1.d0+20
  endif
  fctexpl = yscale*e0/xscale/xx/alog(10.)
  fctexpl = max(fctexpl,1.d-8)
end
!============================
function fctexp3(xx)
  use work_moment, only: iwp,xscale,yscale,ndeg,xl
  implicit none
  real(iwp)  :: fctexp,xx,arg,factor,e0,fctexp3,x
  integer :: i    
  arg    = 0.0d0
  factor = 1.0d0
  x      = xx/xscale
  do i = 1, ndeg
    arg    = arg + xl(i) * factor
    factor = factor*x
  enddo
  arg = -arg
  if (abs(arg) < 82.0) then
    e0 = exp(arg)
  else if (arg < 0.0) then
      e0 = 1.0d-20
  else if (arg > 0.0) then
      e0 = 1.d0+20
  endif
  fctexp3 = yscale*e0/xscale
end

!=====================================
subroutine gauss(a,v,indx,b,nd,n,neq)
use work_moment ,only: iwp
  implicit none
  ! ---compute invers of a in b 
  ! ---destroying a
  real(iwp)  :: a(nd,nd),b(nd,nd),v(nd),disi0
  integer:: indx(nd),n,neq,nd,j

  call dludcp(a,v,n,nd,indx,disi0)
  do j = 1, neq
    call dluksb(a,n,nd,indx,b(1,j))
  enddo
end subroutine gauss

!*======================================
subroutine dludcp(a,vv,n,np,indx,d)
use work_moment ,only: iwp
  implicit none
  real(iwp),parameter  :: tiny=1.0d-20
  real(iwp)  :: a(np,np),vv(np),d
  integer    :: indx(n),n,np
  real(iwp)  :: summe,aamax,dum
  integer    :: i,j,k,imax
  d=1.d0
  do i=1,n
    aamax=0.0d0
    do j=1,n
      if (abs(a(i,j)).gt.aamax) aamax=abs(a(i,j))
    enddo
    if (aamax == 0.0d0) then
      write(6,*)'singular matrix.'
      stop
    endif
    vv(i)=1./aamax
  enddo
  do j=1,n
    if (j > 1) then
      do i=1,j-1
        summe=a(i,j)
        if (i > 1)then
          do k=1,i-1
            summe=summe-a(i,k)*a(k,j)
          enddo
          a(i,j)=summe
        endif
      enddo
    endif
    aamax=0.0d0
    do i=j,n
      summe=a(i,j)
      if (j > 1)then
        do k=1,j-1
          summe=summe-a(i,k)*a(k,j)
        enddo
        a(i,j)=summe
      endif
      dum=vv(i)*abs(summe)
      if (dum.ge.aamax) then
        imax=i
        aamax=dum
      endif
    enddo
    if (j.ne.imax) then
      do k=1,n
        dum=a(imax,k)
        a(imax,k)=a(j,k)
        a(j,k)=dum
      enddo
      d=-d
      vv(imax)=vv(j)
    endif
    indx(j)=imax
    if (j.ne.n)then
      if(a(j,j)==0.0d0)a(j,j)=tiny
      dum=1./a(j,j)
      do i=j+1,n
        a(i,j)=a(i,j)*dum
      enddo
    endif
  enddo
  if(a(n,n) == 0.0d0)a(n,n)=tiny
end subroutine dludcp

!*======================================
subroutine dluksb(a,n,np,indx,b)
use work_moment ,only: iwp
  implicit none
  real(iwp)  :: a(np,np),b(n)
  integer    :: indx(n)
  integer    :: ii,ll,i,j,n,np
  real(iwp)  :: summe
  ii=0
  do i=1,n
    ll=indx(i)
    summe=b(ll)
    b(ll)=b(i)
    if (ii .ne. 0) then
      do j=ii,i-1
        summe=summe-a(i,j)*b(j)
      enddo
    else if (summe .ne. 0.0d0) then
      ii=i
    endif
    b(i)=summe
  enddo
  do i=n,1,-1
     summe=b(i)
     if (i < n)then
       do j=i+1,n
         summe=summe-a(i,j)*b(j)
       enddo
     endif
     b(i)=summe/a(i,i)
   enddo
end subroutine dluksb

!------------------------------------------------------------------------
function random(seed_g,iseed2) result (r)
    !---kombinierte multiplikativ linear kongruenter  generator
    !   independence of maschine
    !   sufficient 32 bit integer arithmetic 
    !---period   : (m1-1)*(m2-1)/2 = 2.3e18
    !---interval : (m1-1)          = 2.147.483.562
    !---exact 0 not possibility ,be-cause  c=0
    !   based on : iseed(j+1)=(a*iseed(j)+c) mod m   
    !              iq=m/ia
    !              ir=mod(m,ia)
    !              r=iseed/(m-1)
    implicit none
    integer  , parameter :: iwp      = 8  !---double precision    
    integer, intent (inout) :: seed_g,iseed2  
    real(iwp)         :: r
    integer           :: k,iz
    integer,parameter :: m1=2147483563,m1min1=2147483562,ia1=40014,iq1=53668,ir1=12211
    integer,parameter :: m2=2147483399,                  ia2=40692,iq2=52774,ir2= 2791
    real   ,parameter :: one_d_m1min1=4.6566130595601735E-10
    real   ,parameter :: one_d_iq1=1.8633077439069838E-05
    real   ,parameter :: one_d_iq2=1.8948724750824271E-05
    
    !---produce integer random number x1 from first mlcg
    !k=seed_g/iq1
    k=seed_g*one_d_iq1
    seed_g=ia1*(seed_g-k*iq1)-k*ir1
    if(seed_g < 0) seed_g=seed_g+m1
  
    !---produce integer random number x2 from second mlcg
    !k=iseed2/iq2
    k=iseed2*one_d_iq2
    iseed2=ia2*(iseed2-k*iq2)-k*ir2
    if(iseed2 < 0) iseed2=iseed2+m2
  
    !---combine seed_g and iseed2
    iz=seed_g-iseed2
    if(iz < 1) iz=iz+m1min1
    
    !---normalize and transform to floating point
    !r=real(iz) / m1min1
    r=iz * one_d_m1min1
  
end function random
