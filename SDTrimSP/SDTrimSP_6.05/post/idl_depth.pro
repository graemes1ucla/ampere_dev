@/afs/ipp/home/a/aam/public/archiv/TRIDYN/SDTrimSP/post/window.inc 
@/afs/ipp/home/a/aam/public/archiv/TRIDYN/SDTrimSP/post/level.inc 
@/afs/ipp/home/a/aam/public/archiv/TRIDYN/SDTrimSP/post/achsen.inc 
pro run
;input :depth_proj.dat
;       depth_recoil.dat
;output:depth_proj_*.ps    (*...ncp)
;       depth_recoil_*.ps  (*...ncp)
  _start
  
  druck=0
  print,'druck:0/1    0...screen 1...postscript'
  read,druck
  form=0
  titlecase='' & text1='' & bez='' & text2=''& textdatum=''
  idep=1
  ncp=1 & nh=1
  a=1.0d0 & b=1.0d0
  loadct,39
  close,1
  if druck eq 0 then farbe=[255,250,150,80,100,200] else $
                     farbe=[0,250,150,80,100,200]
  ;---prepare 6 picture

  for anzahl=1,2 do begin
   if anzahl eq 1 then name='depth_proj'  
   if anzahl eq 2 then name='depth_recoil'  
   print,'read:',name+'.dat'
   maxidep=0.0
   maxstop=0.0
   openr,1,name+'.dat'
   ;---header
   readf,1,textdatum &         textdatum=strtrim(textdatum,1)
   readf,1,bez,format='(a50)' & bez=strtrim(bez,1)
   readf,1,ncp ;only number projectiles
   nproj=1.d0
   etot=1.0d0
   for anz=1,ncp do begin
     drname=name+'_'+string(anz,format='(I1)')+'.ps'
     print,'printname:',drname
     _fenster,druck,drname,form,nummer=anzahl*10+anz   
     spa=2 & zei=3
     !P.Multi=[0,spa,zei] 
     if form eq 0 then pos=[0.02, 0.05 , .97, .96]
     if form eq 1 then pos=[0.01, 0.1, .98, .97]
     ric=0
     dspa=25 ;% Zwischenraum
     dzei=25 ;% Zwischenraum
     dx=(pos(2)-pos(0))/spa
     dy=(pos(3)-pos(1))/zei
     isp=0
     jze=0
     if ric eq 0 then jze=1 else isp=1
     druckfak=1
     if druck eq 1 then druckfak=.8
     dd=1.5*druckfak
     gr=2.5*druckfak
     gd=1.5*druckfak
   
     readf,1,titlecase
     readf,1,idep,nproj,etot
     print,'Number layer:',idep
     print,'Number projectils:',nproj,'  Total energy:',etot,' [MeV]'
     text2=strarr(8)
     text3=strarr(8)
     readf,1,text2,format='(20a12)' & text2(*)=strtrim(text2(*),1)
     readf,1,text3,format='(20a12)' & text3(*)=strtrim(text3(*),1)
     text3(0)='DEPTH '
     xytext=text3+' '+text2 
     ;---values
     wert=dblarr(8,idep)
     hilf=dblarr(8)
     for i=0,idep-1 do begin
      readf,1,hilf
      wert(*,i)=hilf(*)
     end
     readf,1,hilf ;sum
     lmin=3 & lmax=5  
     ihilf=idep-1
     print,'xe::::::::::::::::',ihilf-1
     for i=idep-1,0,-1 do begin
       if wert(1,i) eq 0.0 and wert(3,i) eq 0. and wert(4,i) eq 0. $
       and wert(5,i) eq 0.and wert(5,i) eq 0. then ihilf=i
     end
     if ihilf-1 le 0 then ihilf=1 
     xe=max(wert(0,0:ihilf-1))
     if xe eq 0 then xe=1
     _achslevels,0.0,xe,lmin,lmax,ndy,idy,strdy ;,forma=format_y
     if wert(0,ihilf-1) gt maxidep   then maxidep=wert(0,ihilf-1)
     if max(wert(1,*))/nh gt maxstop then maxstop=max(wert(1,*))/nh
     k=strpos(titlecase,':')+2
     l=max([strlen(titlecase),4])
     text=' '+strmid(titlecase,k,l)
  
     ;---normalized to partial doses: nproj
     if nproj > 0 then wert(1,*)=wert(1,*)/nproj
     text2(1)='projectils (normalized to: nproj)'
     ;---normalized to partial doses: etot
     if etot  > 0 then wert(3:7,*)=wert(3:7,*)/etot
     text2(3:7)='energy / total energy'

     k=1 ;STOP-POSITION
     xe=max(wert(k,*))
     if xe eq 0 then xe=1
     _achslevels,0.0,xe,lmin,lmax,ndx,idx,strdx ;,forma=format_y
     _posit,ric,isp,jze,spa,zei,dspa,dzei,dx,dy,pos,pos1    
      
      plot,wert(k,0:ihilf-1)/1,wert(0,0:ihilf-1),xtitle=text2(k),ytitle=xytext(0),title=text3(k)+text $
       ,yrange=[wert(0,ihilf-1),wert(0,0)] $
       ,Xticks=ndx, Xtickv=idx ,Xtickname=strdx $
       ,Yticks=ndy, Ytickv=idy ,Ytickname=strdy $
       ,thick=dd,charsize=gr,charthick=gd,position=pos1
  
     for kk=2,5 do begin
       if kk eq 2 then k=4 ;ELECT.LOSS
       if kk eq 3 then k=3 ;NUCL.LOSS
       if kk eq 4 then k=5 ;NUCL.LOSS<ED
       if kk eq 5 then k=6 ;NUCL.LOSS>ED
       xe=max(wert(k,0:ihilf-1))
       if xe eq 0 then xe=1
       _achslevels,0.0,xe,lmin,lmax,ndx,idx,strdx ;,forma=format_y
       _posit,ric,isp,jze,spa,zei,dspa,dzei,dx,dy,pos,pos1
       plot,wert(k,0:ihilf-1) ,wert(0,0:ihilf-1),xtitle=text2(k),ytitle=xytext(0),title=text3(k) $
        ,yrange=[wert(0,ihilf-1),wert(0,0)] $
        ,Xticks=ndx, Xtickv=idx ,Xtickname=strdx $
        ,Yticks=ndy, Ytickv=idy ,Ytickname=strdy $
        ,thick=dd,charsize=gr,charthick=gd,position=pos1
       oplot,wert(k,0:ihilf-1) ,wert(0,0:ihilf-1),color=farbe(kk-2)
     end
     k=3  ;3+5+6
     xe=max(wert(k,0:ihilf-1))
     if xe eq 0 then xe=1
     _achslevels,0.0,xe,lmin,lmax,ndx,idx,strdx ;,forma=format_y
     _posit,ric,isp,jze,spa,zei,dspa,dzei,dx,dy,pos,pos1
     plot,wert(k,0:ihilf-1) ,wert(0,0:ihilf-1),xtitle=text2(k),ytitle=xytext(0),title=text3(k) $
      ,yrange=[wert(0,ihilf-1),wert(0,0)] $
      ,Xticks=ndx, Xtickv=idx  ,Xtickname=strdx $
      ,Yticks=ndy, Ytickv=idy ,Ytickname=strdy $
      ,thick=dd,charsize=gr,charthick=gd,position=pos1
     oplot,wert(k,0:ihilf-1) ,wert(0,0:ihilf-1),color=farbe(1)
     k=5
     oplot,wert(k,0:ihilf-1) ,wert(0,0:ihilf-1),color=farbe(2)
     k=6
     oplot,wert(k,0:ihilf-1) ,wert(0,0:ihilf-1),color=farbe(3)
   
     xyouts,.10,.05,'!6'+bez,charsize=gr*.8,charthick=gd,/normal  
     xyouts,.10,.02,'!6'+titlecase,charsize=gr*.6,charthick=gd,/normal  
     xyouts,.55,.00,'!6idl_depth.pro:'+textdatum,charsize=gr*.4,charthick=gd,/normal  
    _druckende,druck,drname,form  
   end ;anz 
   close,1
  end
  stop  
end
