module trajectory_description

   this module stores the information about particle trajectories, 
      projectiles and recoils

   implicit none

   used by histories.F90, work.F90

   include mpif.h


   integer :: ipp, i ,j ,k,errcode

 
   definitions
      intl        integer length
      MPI_TRAJECTORY   length of "trajectory description" in words
      nbn         maximum length of single trajectory (1000)
      parent      species of parent, which has created a recoil
      reall       real length
      sequence    needed to ensure contiguity in memory
      
   1 ih           calculation step (ihist)
   2 iproj        number of projectile
   3 parent       species of parent, which has created a recoil
   4 ncoll        generation
   5 species      species of particle
   6 termflag     index if a particle is stopped, scattered or transmitted
   7  ibn         index of the number of trajectories
   8  ihilf       aps%iproj   Warum braucht man das?
   9  x           x position of particle
   0  y           y position of particle
  11  z           z position of particle
  12  e           energy of particle
  13  ptime       time of the actual position of a particle
  14  eel0        elastic energy loss in the central cylinder (e_transfer)
  15  eels        total elastic energy loss in all cylinders  (e_transfer_sum)
  16  iel         inelastic energy loss of a particle         (e_electr)
      
   

varibale in trajectory-outputfiles
    maxnumber-history/numb_hist ...max possible number of all complete trajectories
    ioutput_hist                ...max possible number of all (stopped/scattered/sputtered) trajectories
    i_traj   ... number of the trajectory  
    ihist    ... calculation step    
    iproj    ... number from which projectile 
    parent   ... species of parent, which has created a recoil  
    ncoll    ... generation (actual number of collisions of a particle)
    species  ... species of particle    
    ibn      ... index of the number of trajectories
    termflag ... index if a particle is stopped, scattered or transmitted
    x,y,z    ... position
    ptime    ... time     
    energy   ... energy of particle

