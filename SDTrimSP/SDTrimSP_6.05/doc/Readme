         PROGRAM SDTrimSP VERSION 4.10
         Static and Dynamic Trim  Seqential and Parallel 
         BASED ON TRIM.SP AND TRIDYN

         R.Dohmen, W.Eckstein, A.Mutzke    June 2004

Table of contens
 
   1. The code SDTrimSP:
   2. Structure:
   3. Contents of the main directories:
   4. How to copy all file:
   5. How to compile:
   6. How to run a case:
   7. Inputfiles:
   8. Outputfiles
   9. Examples
  10. Post-programs:


1. The code SDTrimSP:
=====================

The code SDTrimSP serves to describe impact effects of energetic ions
in amorphous solids. It treats the movement of both incident ions and
recoil atoms as a series of subsequent collisions. There are two ver-
sions of the code: 

static version (former TRIM.SP): the target composition is fixed during
                                 the whole simulation
dynamic version (former TRIDYN): modifications of the target caused by the
                                 ion bombardment are taken into account

In the dynamic version the target is updated at regular intervals, i.e. 
after a certain number nr_pproj of projectiles and corresponding showers;
nr_pproj has to be specified as a parameter in the Namelist tri.inp


The code is drawn up to work in different modes and on different 
architectures:

1. MODE=SEQ for execution on any sequential architecture with a F90
   and a C compiler


2. MODE=PPROJ for parallel execution on any architecture with a F90 and
   a C compiler and the MPI communication library. In this version, the
   nr_pproj particle showers are distributed over the processors.

2. Structure:
=============

The code is organized according to the following directory structure:

                             |---  *.F90
            |----  src   ----|---  *.inc
            |                |---  Makefile
            |
            |
            |                |---  <arch_1> 
            |                |        .
            |                |        .
            |----  bin   ----|        .
            |                |---  <arch_n>  
            |                |---  <new_arch> ---  mk
            |                |---  run
            | 
SDTrimSP ---|
            | 
            |                |---  <case_1>
            |                |        .
            |                |        .
            |----  cases ----|        .
            |                |---  <case_m>
            |                |
            |                |                    |--- tri.inp
            |                |---  <new_case> --- |
            |                |                    |--- run 
            |
            |---  <tables>
            |
            |
            |----  doc
            |
            |----  post


Contents of the main directories:
--------------------------------

src:   - source code and one Makefile for all supported architectures 
         and modes. The architecture is determined by the environment 
         variable $OSTYPE. The mode has to be specified with the make 
         macro MODE (default: MODE=PPROJ)

bin:   - one directory per architecture to hold the .o files, the 
         executable and the compile script "mk",
       - a template directory "new_arch" containing a sample compile 
         script "mk",
       - one run script for all supported parallel architectures.
     
cases: - one directory per case containing input and output files,
       - a template directory "new_case" containing a sample input and 
         the "run" script which is a link to the "bin" directory,
       - the directory "tables" containing input tables.

doc:   - documentation containig explanatory remarks for most of the
         source files

post:  - preliminary material for the postprocessor
   

Note, that except one all scripts work with relative paths so that 
the complete directory SDTrimSP as well as sub-directories can be 
duplicated without difficulties. The only exception is the script 
SDTrimSP/bin/run in which the variable ROOT has to be set to the
path where the root directory SDTrimSP resides.

3. How to copy all file:
========================
The administrator of all program is:
  Dr. Andreas Mutzke
  Max-Planck-Institut fuer Plasmaphysik
  Wendelssteinstrasse 1
  Greifswald
  Tel.:  3834 882435
  email: aam@ipp.mpg.de 

  changes,tips,notices and errors or mistakes send to A. Mutzke 
  
  UNIX/LINUX: 
  1. go in your work directory
  2. cp -r /afs/ipp-garching.mpg.de/home/a/aam/TRIDYN/SDTrimSP .
   
  you find the program now under the subdirectory "SDTrimSP"
  
  For parallel calculation change the root directory in SDTrimSP/case/<cases>/run
  ROOT=~/TRIDYN/SDTrimSP --> ROOT=~/<workdirectory>/SDTrimSP


4. How to compile:
==================
1. Choose an architecture and check if the correspondig directory
   SDTrimSP/bin/<arch> already exists. If not, copy the template 
   SDTrimSP/bin/<new_arch> to SDTrimSP/bin/<arch>.

2. Adapt the file SDTrimSP/src/Makefile according to your needs. 
   Choose the mode by setting the macro MODE (SEQ | PPROJ).
   Choose the method of random number generation by setting the macro
   RAND (ART_RAND | CRAY ). In the case ART_RAND reproducible random
   numbers are used and the result will be the same regardless of how
   many processors are used. For a new architecture add a section for
   the compilers and compiler options to the Makefile.
 
3. When executing the dynamic case, determine the number of showers 
   to be calculated between target updates: Choose the parameter
   nr_pproj in the Namelist tri.inp  (default: nr_pproj = 10)

4. Change to the directory SDTrimSP/bin/<arch> and use the script "mk" 
   to compile the code. 
   


5. How to run a case:
=====================
1. Choose a case and check if the corresponding directory 
   SDTrimSP/cases/<case> already exists. If not copy the template
   SDTrimSP/cases/<new_case> to SDTrimSP/cases/<case>.

2. Change to the directory SDTrimSP/cases/<case>.

3. Adapt the input file tri.inp according to your needs.

4. To run a parallel job on nprocs processors, use the script "run", 
   which is a link to ../../bin/run: run <nprocs>
   ./run <nprocs>
   
   To run a sequential job call the executable 
   ../../bin/SDTrimSP/SDTrimSP.exe

7. Inputfiles:
==============
All inputfile have the ending on ".inp". in /SDTrimSP/cases/<case>
1. tri.inp     ... main inputfile
2. angle.inp   ... distripution of angle       
3. energy.inp  ... distripution of energy        
4. layer.inp   ... layer thick and composition of target) 
5. all tables in /SDTrimSP/table/* (physical values)
   table1       
   table2           
   table3
   table4           
   table5                    
   table.compound  

8. Outputfiles:
===============
All outputfile have the ending ".dat". in /SDTrimSP/cases/<case.
At the begin of each outputfile exists a header. 
output.dat          ... main sttistic output
time.dat            ... calculation time
energy_analyse.dat  ... energyanalyse
E0_31_target.dat    ... target
E0_33.dat           ... sputtered yield by generation (idrel=0)
E0_34_moments.dat   ... moments 
depth_*.dat         ... depthdistribution of stops,pathlength,nucl.loss... 
m*.dat              ... matrices(distribution of energy(logarithmis/linear)
                                                azimuthal-angle(degree/cos)
                                                polar-angle    (degree/cos)
partic*.dat         ... output information about particles: 
  lparticle_r = .true.   ...only projectile
  lparticle_p = .true.   ...only recoils
  ioutput_part = 10000, 10000, 0, 10000, 10000, 0,
                 ... number particle 
                     of stopped,backscatered and transmitted of projectiles
                     of stopped,backsputtered and transsputtered of recoils

trajec*.dat         ...trajecectories
  output trajectories:
    ltraj_p = .true.   ...only projectile
    ltraj_r = .true.   ...only recoils
    numb_hist = 10 ,   ...number histories of trajectories
    ioutput_hist = 10, 10, 0, 10, 10, 0,
                   ... number histories of trajectories
                       of stopped,backscatered and transmitted of projectiles
                       of stopped,backsputtered and transsputtered of recoils

serie.dat          ... energy,alpha,refl.coeff,sputt.coeff (number_calc>1)

time_run.dat       ... timeoutput during the run  

9. Examples:
============
The survey of all examples is given in /SDTrimSP/case/example.txt
The directory "cases" contains several examples with output to check the
correctness of the program.  The output has 
been obtained with linux-cluster or aix ), MODE=PPROJ (specified in "mk") 
Each output directory contains furthermore a copy of the respective input file 
"tri.inp" used to obtain the results.


10. Post-programs:
==================  
  FORTRAN: (all FORTRAN programs work with dynamic variables)
    Makefilepost*       Makefile
    mkpost*             script of compile         (like mk)
     readtridyn31.F90   input: E0_31_target.dat  output: composion,depth ...
    readmatrix4.F90     input: meag*.dat         output: matrices 2-dim, ...
    readparticle6.F90   input: partic_*.dat      output: partic_stop.dat ...
    mom2dis.F90         input: E0_34_moments.dat output: E0_34_distribut.dat
  
  IDL:
  idl_31.pro      input : E0_31.dat, depth_recoil.dat, depth_proj.dat
  idl_33.pro      input : E0_33_sputt.dat
  idl_matrix.pro  input : meagb.dat, meagbt.dat, meags.dat, meagst.dat
  idl_depth.pro   input : depth_proj.dat, depth_recoil.dat
  idl_trajec.pro  input : trajec_stop_p.dat, trajec_back_p.dat, trajec_tran_p.dat,
                          trajec_stop_r.dat, trajec_back_r.dat, trajec_tran_r.dat,
                          trajec_all.dat
  includes: achsen.inc,level.inc,window.inc


-----------------------------------------------------------------------
In case of problems or questions please contact:

      Dr. Andreas Mutzke
      Max-Planck-Institut fuer Plasmaphysik
      Wendelssteinstrasse 1
      Greifswald
      Tel.:  3834 882435
      email: Andreas.Mutzke@ipp.mpg.de
