last change 24.10.2013
%==============================================================================%
Global parameters 
-----------------------
in param.F90: 
             ncpm = 8                     maximum number of elements       
             nqxm = 2000                  maximum number of depth intervals

in dbl.F90:  
              pemax = 32                   maximum number of PEs

in task_descr.F90 (only MODE: PAR) 
             ntpmax=16384                  size of global task queue
             ntqmax=131072                 size of local task queue 

%==============================================================================%
Input file 'tri.inp' for a SDTrimSP calculation

Form of input file (header + namelist): 
---------------------------------------

Headline (Text), format A80
&TRI_INP
<varible and values>
/

The sequence of the input values in the input file is arbitrary (namelist)

Necessary input variables (no default values):
-----------------------------------------------
 alpha0(1:ncp) angle of incidence (degree) of ncp species in case_alpha==0,5
 
 e0(1:ncp)     energies (eV) of projectiles (qubeam > 0.) for case_e0=0,5 
               e0=ttemp * boltzm     (e0 < 0) of projectiles for case_e0=2,3 
               temperature (eV) (kT) (e0 > 0) of projectiles for case_e0=2,3 

 flc           incident fluence (10^{16} atoms/cm^2 or atoms/A^2)) in case 
               of idrel==0

 ipot          interaction potential
               = 1 : KrC
               = 2 : Moliere
               = 3 : ZBL
               = 4 : Nakagawa-Yamamura 
               = 5 : Si-Si
               = 6 : power
 
 isbv          surface binding model, determines the composition dependent surface
               binding energy sbv(1:ncp,1:ncp) from the elemental
               surface binding energies e_surfb(1:ncp) taken from table1
               1 : sbv(ip,jp)=e_surfb(jp) for ip=jp
               2 : sbv(ip,jp)=e_surfb(jp) for all ip, jp
               3 : sbv(ip,jp)=0., if e_surfb(ip)=0 or e_surfb(jp)=0
                   sbv(ip,jp)=0.5*(e_surfb(ip)+e_surfb(jp)) 
               4 : sbv(ip,jp)=f(e_surfb, qu, deltahf) for solid/solid compound 
               5 : sbv(ip,jp)=f(e_surfb, qu, deltahf, deltahd) for solid/gas 
                   compound
               6 : input of given matrix of the surface-bindig-energy: 'mat_surfb.inp' 
               7 : calculate face-bindig-energy according to 'Kudriavtsev' 
 
 ncp           number of species (projectiles + target species)
               more than one projectile species is allowed
               
 nh            number of histories (projectiles)

 nqx           number of depth intervals of the target (discretization)

 qubeam(1:ncp) projectile atomic fractions (in incident beam) of ncp species,
               qubeam > 0. , Note: sum(qubeam(1:ncp))=1
               <= 1. .for projectiles, 
               =  0. for target atoms,

 qu(1:ncp)     initial target atomic fractions of ncp species in case of 
               homogenous initial composition (iq0 = 0)

 symbol(1:ncp) ncp chemical symbols of elements according to table1
               (special symbol: H2, D, T, He3, He, C_a, C_g, C_d, C_f, Sp2, Sp3, Sp3H, Xe_g). 
 
 two_comp      symbol of two-component target according to table.compound 
               (e.g. two_comp ="Ta2O5"). 
               Note: only selected compounds in table.compound

Optional variables: 
-------------------

These values have default values (see default_init.txt).
If values different from the default values are needed, then these values 
have to be given explicitly in the input file.
If a value is specified in the first column, it is the default value.
Other choices are provided in the second column.

 angleinp='./' directory of inputfile 'angle.inp' 
               (see also: layerinp, tableinp, energyinp) 

 a_mass(1:ncp)      mass (in amu) of ncp elements; default from table1
 
 a_num_z(1:ncp)     atomic number of ncp elements; default from table1
 
 case_alpha=0  flag for the choice of the angle of incidence 
               = 0 : angle of incidence (degree) counted from the surface normal
                     alpha0 = 0... 90 (starting above surface)
                     alpha0 = 90...180 (starting in solid)
                     (azimuthal angle phi = 0)
               = 1 : random distribution of angles of incidence (only from above
                     surface) (alpha and phi random)
               = 2 : cosine distribution of angles of incidence (only from above
                     surface)
               = 3 : cosine distribution of angles of incidence 
                     alpha=0...Pi/2,max: by 0     phi=0...2Pi
               = 4 : input of a given incident angular distribution from file 
                     angle.inp 
               = 5 : series of calculations with different angles of incidence
                     ( alpha = (i-1) * alpha0; i = 1, number_calc )
                     output      : output.*dat            
                     default set :lmatrices   = .false.
                                  ltraj_p     = .false.    
                                  ltraj_r     = .false.
                                  lparticle_r = .false. 
                                  lparticle_p = .false. 
                                  case_e0     = 0
                     (note: all *.dat output files from last calculation)
               = 6 : distribution of alpha and energy,input-file: 'ene_ang.inp'

 case_e0=0     flag for the choice of the incident energy
               = 0 : fixed incident energies (eV) of projectiles (qubeam>0.) 
               = 1 : input of a given energy distribution from file energy.inp       
               = 2 : temperature (eV) of a Maxwellian velocity distribution of 
                     projectiles
               = 3 : temperature (eV) of a Maxwellian energy distribution of 
                     projectiles
               = 5 : series of calculations with different projectile energies
                     ( e0(1)>0: linear
                          energy = i * e0          ; i = 1, number_calc )  
                     ( e0(1)<0: logarithmic
                          energy = 10**( (i-1) * e0; i = 1, number_calc )  
                     output: output.??dat            
                     default set: lmatrices   = .false.
                                  ltraj_p     = .false.    
                                  ltraj_r     = .false.
                                  lparticle_r = .false. 
                                  lparticle_p = .false. 
                                  case_alpha  = 0
                     (note: all *.dat files from last calculation)
                = 6 : distribution of alpha and energy.input-file: 'ene_ang.inp'

 case_layer_thick=0      mixing chema of target
                         0 mixing lauer wit neighbour lauer, if thick: 150% or 50% 
                         1 mixing to constant layer thick 
 
 ca_scre(1:ncp,1:ncp)=1. correction factor for the screening length
                         in the interaction potential (not applicable for
                         KrC and ZBL potentials)

 charge(1:ncp)=0.   charge of species if case_e0=2,3 and sheath>0 (plasma)
                    >=1. for qubeam>0 (projectiles) 
                    = 0. for qubeam=0 (target atoms)

 ck_elec(1:ncp,1:ncp)=1. correction factor for the inelastic energy loss;
                         correction factors for hydrogen (below 25 keV)
                         are given in table3

 deltahd(1:ncp)     heat of dissociation (eV) of a molecular target
                    default from table1
 
 deltahf            heat of formation (eV) of a molecular target
                    default from table.compound

 diff_koeff1(1:ncp)=0.  diffusionskoeffcient  if loutgas=.true. [A**4/ion]
 
 diff_koeff2(1:ncp)=0.  transportcoeffiecient if loutgas=.true. [A**3/ion]

 dist_nx=60         x-size of the matrix of energy distribution in target 
 
 dist_ny=60         y-size of the matrix of energy distribution in target
 
 dist_nz=60         z-size of the matrix of energy distribution in target
 
 dist_delta=2.0     distance between the matrix points of energy distribution in target  
 
 
 dns0(1:ncp)        atomic density (atoms/A^3) of ncp elements; 
                    default from table1

 dsf=5.             average depth (A) for surface composition

 e_bulkb(1:ncp)=0.  bulk binding energy; if e_bulkb>0., e_bulkb has to be
                    subtracted from the surface binding energy e_surfb

 e_cutoff(1:ncp)    cutoff energy (eV) of ncp species; defaults from table1
                    (0.05 eV for noble gases; 1 eV for H, D, T; 
                    e_surfb-0.05 eV for selfbombardment)
 
 e_displ(1:ncp)     displacement energy (eV); default from table1
                    (if in table1 e_displ==0 then e_displ=15) 

 e_surfb(1:ncp)     surface binding energy (eV) (heat of sublimation); 
                    default from table1

 energyinp='./'     directory of inputfile 'energy.inp'
                    (see also: layerinp, tableinp, angleinp) 
 
 flux=1.0           flux of incident atoms ($atoms/A^2/s = 10^{20} atoms/m^2/s $) \\


 idrel=1            mode of simulation
                    = 0 : full dynamic calculation (TRIDYN)
                    > 0 : suppression of dynamic relaxation (TRIM), 
                         full static calculation
                    < 0 : suppression of dynamic relaxation and cascades,
                          static calculation (TRIM), 
                          only projectiles (no recoils) are followed


 idout=-1      control output, determines the outputfiles: E0_31_target.dat,
               E0_34_moments.dat, partic*.dat, trajec*.dat and restart_file
               = -1 : output after each fluence step of nh/100,
                      100 fluence steps
               =  0 : output only after the last fluence step
               >  0 : output after each idout'th fluence step and last step

 iintegral=2   integration method 
               = 0 : MAGIC, only valid for KrC, ZBL, Moliere
               = 1 : Gauss-Mehler quadrature, ipivot >= 8 recommended
               = 2 : Gauss-Legendre quadrature, ipivot <= 16

 imcp=0        flag indicating whether (flib)-moments of distributions are calculated 
               = 0 : no moment calculation 
               = 1 : moments of depth distributions for all projectiles
                     (qubeam>0.)

 inel0(1:ncp)=3     inelastic loss model 
                    = 1 : Lindhard-Scharff; nessary condition:  E < 25 * Z**(4/3) * M (in keV),
                          where E, Z, M  are the energy, the atomic number and the atomic mass of 
                          the moving particle, respectively   
                    = 2 : Oen-Robinson; nessary condition:  E < 25 * Z**(4/3) * M (in keV)
                    = 3 : equipartition of 1 and 2
                    = 4 : high energy hydrogen (H,D,T) (E > 25 keV), values from table3
                    = 5 : high energy helium (He3,He)  (E > 100 keV), values from table4
                    = 6 : Ziegler-Biersack;  values from table6a and table6b


 ioutput_hist(1:6)=10    number of traced trajectories for:   
                         stopped, backscattered and transmitted projectiles,
                         stopped, backsputtered, transmission sputtered recoils 
                         (see also: ltraj_p, ltraj_r
 
 ioutput_part(1:6)=10    number of traced particles for: 
                         stopped, backscattered and transmitted projectiles,
                         stopped, backsputtered, transmission sputtered recoils
                    (see also: lparticle_p, lparticle_r)

 ipivot=8      number of pivots in the Gauss-Mehler and Gauss-Legendre 
               integration, the minimum number is 4 (larger numbers increase 
               the computing time)

 iq0=0         initial composition flag
               < 0 : initial depth dependent composition taken from file 
                     layer.inp  
               = 0 : initial composition homogeneous, one layer with 
                     constant depth intervals 

 irand=1       random seed

 irc0=-1       flag for subthreshold recoil atoms
               < 0 : subthreshold recoil atoms free
               >=0 : subthreshold atoms bound 

 isot(1:ncp)=0 flag for isotope mass
               = 0 : natural isotope mixture (mass from table1)
               = 1 : isotope masses and natural abundances from table2
                     (valid for projectiles as well as for target species)
 i_diff_algo=2 flag for method of diffusions
               =0 : simple diffusion
               =1 : explicit (lambda=0)
               =2 : implicit (lambda=1)
               =3 : Crank-Nicolson (lambda=0.5)
               =4 : Crank-Nicolson with lambda=lambda_cn=[0,1]

 i_two_comp=1  method to determine the densities dns0(:) from the compound 
               density in a two-component target (table.compound)
               =1: dns0 for the first target species is set equal to the elemental density;  
                   nessary if the second element is a gas (e.g. Ta2O5)
               =2: dns0 for the second target species is set equal to the elemental density  
               =3: iterative determination of both dns0(:); recommended if the elemental 
                   densities are different   

 iwc=2         number of ring cylinders for weak simultaneous collisions 
               for projectiles; for high energies (MeV H or He) iwc can be 
               reduced to 1 or 0 to reduce computing time

 iwcr=2        number of ring cylinders for weak simultaneous collisions 
               for recoils
 
 k_start=0     start counter intern \\

 layerinp='./'       directory of inputfile 'layer.inp'
                     (see also: tableinp, angleinp, energyinp)

 lchem_ch=false     calculation with chemical erosion H on C(SP2,SP3,SP3H), D on C(SP2,SP3,SP3H) \\
                    mix of methods of Hopf, Roth and Mech    \\

 lenergy_distr=.false.  output of energy distribution in target
                        (energy of stop ,electric loss and elastic nuclear loss)
                        E_distr_all.dat E_distr_stop.dat E_distr_inel.dat E_distr_nucl.dat
 
 lmatrices=false          .true.  : output of matrices, if idrel /= 0
                          .false. : no matrix output

 lmatout_log_energ =false.   energy in matrix      
                            .false. : linear energy intervals 
                            .true. : logarithmic energy intervals

 lmatout_cos_angle=.false.  angle in matrix      
                         .false. : angle in degree intervals 
                         .true. : cosine intervals
 
 lmatrices=.false.  .true.  ; output of matrices, if idrel /= 0
                    .false. : no matrix output

 lmoments=.false.    output of moments for energy distributions (linear and 
                    logarithmic) of projectiles and recoils and for range 
                    distributions (linear) of projectiles
                    .true.  : moments are written in to a output file 
                    .false. : moments are not written

 loutgas=.false.     calculation with outgasing transport and diffusion
                     (see also: diff_koeff1, diff_koeff2)  

 lparticle_p=.false.     .true.  : output of projectile information 
                         .false. : no output of projectile information
                          (see also: ioutput_part)
 
 lparticle_r=.false.     .true.  : output of recoil information 
                         .false. : no output of recoil information
                         (see also: ioutput_part)

 lpart_r_ed=.true.     .true.  : output of stop recoil information only grater e_displ 
                       .false. : output of all stop recoil information
 
 l_pot_thick=.true.   .true. : thickness of surface potential is a funktion of N 
                      .false.: thickness of surface potential is a funktion of N,iwc,iwcr 

 ltableread=.true. .true.  : read from table1, table2, table3, table4 
                              or table.compound
                    .false. : no table read, a_num_z, a_mass, dns0, e_surfb, 
                              e_displ have to be given 
                    tables:
                     table 1 : chemical symbol (symbol), nuclear charge (a_num_z),
                               atomic mass (a_mass),  
                               mass density, atomic density (dns0)
                               surface binding energy (e_surfb), displacement
                               energy (e_displ), cutoff energy (e_cutoff)
                     table 2 : chemical symbol, nuclear charge, isotope mass, 
                               atomic weight (in amu), natural abundance
                     table 3 : inelastic stopping coefficients for hydrogen: 
                               symbol, nuclear charge, inelastic stopping
                               coefficients a1 to a12 (ch_h), ck
                     table 4 : inelastic stopping coefficients for helium: 
                               symbol, nuclear charge, inelastic stopping
                               coefficients a1 to a9 (ch_he)
                     table.compound : symbol of two-component target and 
                      physical values 

l term_dif=.false.  .false. : nothing calculate 
                    .true.  : calculation of thermal diffusion
                    input of given matrix a_0 and e_act
                    parameter: i_diff_algorithm=0...4
                    input-files: a0_tdiff.inp, eact_tdiff.inp

 ltraj_p=.false.    .true.  : output of projectile trajectories
                    .false. : no output of projectile trajectories
                    (see also: numb_hist, ioutput_hist)

 ltraj_r=.false.    .true.  : output of recoil trajectories
                    .false. : no output of recoil trajectories
                    (see also: numb_hist, ioutput_hist)

 lrestart=.false.   .true.  : output of restartfiles after each idout
                    .false. : no restart-files
 
 matrix_e_min=0       minimum of lin. energy distribution in matrices 
 
 matrix_e_max=max(e0) maximum of lin. energy distribution in matrices 
 

 nm=-1         = -1 : not a molecular target
               >  1 : number of atoms in a two-component molecule

 nm1=-1        number of first atoms in a two-component molecule
 nm2=-1        number of second atoms  in a two-component molecule

 nr_pproj=10   number of projectiles between two target updates (idrel = 0)
 
 numb_hist=20  number of traced trajectories of projectiles and recoils      
 
 number_calc=1 number of calculations if a series of calculations is 
               carried out (case_e0 == 5 or case_alpha == 5)

 nx\_mat=1000  intervall of depth origin
               nx_mat= 100  intervall depth origin: input target '
               nx_mat=1000  intervall depth origin: 1 A '
               nx_mat=1001  intervall depth origin: 0.5 A '

 qu\_int      &.false. &  linear interpolation of atomic fractions between 
               the depth intervals  \\

 qumax(1:ncp)  maximum atomic fractions in the target for ncp species, if 
               idrel==0

 rhom          atomic density of a two-component target; 
               default from table.compound [g/cm**3)

 sfin=0        = 0 : no inelastic energy loss outside the target surface (x=0.)
               = 1 : inelastic energy loss outside the target surface (-su>x>0.)

 shth=0.       = 0 : no sheath potential 
               > 0 : sheath potential (eV), usually  = 3*|e0|, 
                     only if case_e0=2,3 (Maxwellian distribution, plasma)

 tableinp='../../tables'    directory of inputfile for tables
                         (see also: layerinp, angleinp, energyinp) 

 text         & & comment in NAMELIST \\

 ttarget       total target thickness in Angstrom (A)

 ttemp=300.    target temperature, only of interest at high
               temperatures, it reduces the surface binding energy
               according to a Maxwellian energy distribution

 x0(1:ncp)=0.  starting position of projectile
               <= 0. : outside the surface at x=xc=-su
               >  0  : inside the solid


%==============================================================================%
short list of variable

parameters in param.F90: 
                       default      error  alternate
 ncpm                        8        -       -        max. num. of elements  
 nqxm                     1000        -       -        max. num. depth interv.
 time_between_r_files    18000        -       -        time between restart files

parameters in work.F90:  
                       default      error  alternate
 pemax                      64        -       -        max. number of PEs
 ntpmax                  16384        -       -        recoil buffer per PE

variable in tri.inp:
             default      error  alternate
 alpha0(:)        0.        0.       -          angle of incidence 
 a_mass(:)        -         -      table1       mass (in amu) of ncp elements
 a_num_z(:)       -         -      table1       atomic number of ncp elements
 angleinp       './'        -        -          directory of input 'angle.inp' 
 case_alpha       0         -        -          flag of the angle of incidence 
 case_e0          0         -        -          flag of the incident energy
 case_layer_thick 0         -        -          mixing chema of target
 ca(:,:)          1.        -        -          correction screening length
 charge(:)        0         -        -          charge of species 
 ck(:,:)          1.        -        -          correction inel. energy loss;
 deltahd(:)       -        -1.     table1       heat of dissociation 
 deltahf          -        -1      table1       heat of formation 
 diff_koeff1      0.        -        -          damage-diffusion-coefficient
 diff_koeff2      0.        -        -          pressure-transport-coefficient
 dist_nx         60         -        -          x-size of matrix e distrib. 
 dist_ny         60         -        -          y-size of matrix e distrib.
 dist_nz         60         -        -          z-size of matrix e distrib.
 dist_delta     2.0         -        -          distance of the matrix points   
 dns0(:)          -        -1      table1       atomic density  
 dsf              5.        -        -          average depth surface comp.
 e0(:)            -         0.       -          energies / temperature proj.
 e_bulkb(:)       0.        0.       -          bulk binding energy
 e_cutoff(:)      -        -1      table1       cutoff energy 
 e_displ(:)       -        -1      table1       displacement energy 
 e_surfb(:)       -        -1      table1       surface binding energy  
 energyinp=     './'        -        -          directory of input 'energy.inp'
 flc              -        -1        -          incident fluence  
 flux           1.0         -        -          flux of incident atoms 
 idout            -        -1        -          determines the E0_31.dat output
 idrel            1         -        -          mode of simulation
 iintegral        2         -        -          integration method 
 imcp             1         -        -          flag moments are calculated 
 inel0(:)         3         -        -          inelastic loss model 
 ioutput_hist(:) 10         -        -          number of traced trajectories  
 ioutput_part(:) 10         -        -          number of traced particles 
 ipivot          16         -        -          number of pivots e 
 ipot             -        -1        -          interaction potential
 iq0              0         -        -          initial composition flag
 irand            1         -        -          random seed
 irc0            -1         -        -          flag for subthreshold recoil 
 isbv             -        -1        -          surface binding model
 isot(:)          0         -        -          flag for isotope mass
 iwc              2         -        -          number of ring cylinders proj. 
 iwcr             2         -        -          number of ring cylinders rec. 
 i_two_comp       1         -        -          method compute dns0 in two-comp
 k_start          0         -        -          start counter intern
 layerinp       './'        -        -          directory of input 'layer.inp'
 lchem_ch      .false.      -        -           calculation with chemical erosion H,D on C
 lenergy_distr .false.      -        -          output energy distrib. in target
 lmatrices     .false.      -        -          output of matrices
 lmatout_log_energ. false.  -        -          energy spacing: linear or logarithmic energy intervals 
 lmatout_cos_angle .false.  -        -          angular spacing: angle in degree or cosine intervals   
 lmoments      .true.       -        -          output of moments  
 loutgas       .false.      -        -          calculation with outgasing diffusion(DDF) and transport (PDF)
 lparticle_p   .false.      -        -          output of projectile information
 lparticle_r   .false.      -        -          output of recoil information 
 lpart_r_ed    .true.       -        -          output of stop recoil information only grater e_displ
 l_pot_thick   .true.       -        -          method of calculated thickness of surface potential 
 lrestart      .false.      -        -          output of restartfile 
 ltableread    .true.       -        -          read from table1 
 lterm_dif     .false.      -        -          calculation of thermal diffusion
 ltraj_p       .false.      -        -          output of proj. trajectories
 ltraj_r       .false.      -        -          output of recoil trajectories
 matrix_e_min   0           -        -          minimum of lin. energy distribution in matrices 
 matrix_e_max  max(e0)      -        -          maximum of lin. energy distribution in matrices 
 ncp              -        -1        -          number of species  
 nm               -        -1    table.compound number of atoms molecular target
 nm1              -        -1    table.compound number of first atoms in a two-component molecule
 nm2              -        -1    table.compound number of second atoms  in a two-component molecule
 nh               -        -1        -          number of histories 
 nqx              -        -1        -          number of depth intervals
 nr_pproj        10         -        -          numb. proj. (target updates)
 numb_hist       20         -        -          number of traced trajectories
 number_calc      1         -        -          number of series of calculations
 nx_mat        1000         -        -          intervall of depth origin
 qubeam(:)        -        -1.       -          projectile atomic fractions 
 qumax(:)         1.        -        -          maximum atomic fractions  
 qu(:)            -        -1        -          initial target atomic fractions 
 rhom             -        -1        -          atomic density (two-component) 
 symbol(:)        -         -      table1       ncp elemental symbols 
 sfin             0         -        -          inel. energy loss outside target
 shth             0.        -        -          sheath potential 
 tableinp  '../../tables'   -        -          directory of input for tables
 text             -         -        -          comment in NAMELIST 
 ttarget          -        -1.   calculate      total target thickness
 ttemp          300.        -        -          target temperature
 two_comp         -      '???'  table.compound  symbol of two-component target
 x0(:)            0.        -        -          starting position of projectile

example:
500 eV  He -> Ta2O5 
&TRI_INP
     case_e0=0
     e0  =  500, 0.00, 0.00 ,
     nm  =  7,
     ncp =  3, 
     symbol = "He", "Ta", "O",
     two_comp ="Ta2O5" 

     nh    = 500000,
     idrel = 0, 
     flc   = 25.000, 
     sfin  = 0,
     ipot  = 1, 
     isbv  = 5, 

     tt      =  5000E+0, 
     ttdyn   =  5000E+0, 
     nqx     =  500, 

     e_cutoff= 0.05,  6.5,      6.0 ,
     qu      = 0.0 ,  0.285714, 0.714286 ,

     case_alpha=0
     alpha0 = 0.000, 0.000, 0.000 ,
     qubeam = 1.000, 0.000, 0.000,
     qumax  = 0.000, 1.000, 0.714286 ,
     
     lparticle_r = .true., 
     lparticle_p = .true., 
     ioutput_part = 100, 100, 0, 100, 100, 0,
/ 
