---more information about values: see tri.inp.txt   
   
subroutine default_init : default input values
use work
use parameters, only:ncpm

this default values can overwrite with inputfiles
-------------------------------------------------
 angleinp ='./'          directory of inputfile 'energy.inp'
 alpha0(1:ncpm) = 0.0    angle of incidence (degree), may be default
 case_alpha    = 0       alpha_case...0  angle=constant
 case_e0       = 0       case_e0...0     energy=constant
 ca_scre(1:ncpm,1:ncpm) = 1.  correction factor for the screening length 
                              in the interaction potential (not applicable for 
                              KrC and ZBL potentials)
 charge(1:ncpm)=0             charge of projectiles (not 0 if case_e0>=2)
 ck_elec(1:ncpm,1:ncpm) = 1.  correction factor for the inelastic energy loss
                              correction factors for hydrogen (below 25 keV) 
                              are given in table3
 deltahd(1:ncp)=-1.      heat of dissociation (eV) of a molecular target
                         no default, control of Inputs
 deltahf       =-1       heat of formation (eV) of a molecular target
                         no default, control of Inputs
 dns0(1:ncp)   =-1       atomic density (atoms/A^3) of ncp elements; 
                         no default, control of Inputs
 dsf  = 5.               averaging depth (in A) for surface composition
 e0(1:ncpm)      = 0.    energies (eV) of projectiles
 e_bulkb(1:ncpm) = 0.    bulk binding energy, if be>0. it has to be 
                         subtracted from the surface binding energy sbej
 e_cutoff(:)= -1         cutoff energy (eV) of ncp species; defaults from table1
                         (0.05 eV for noble gases; 1 eV for H, D, T; 
                         e_surf - 0.05 eV for selfbombardment)
                         no default, control of Inputs
 e_displ(:) = -1.        displacement energy (eV); default from table1
                         (if in table1 e_displ==0 then e_displ=15) 
                         no default, control of Inputs
 e_surfb(:) = -1.        surface binding energy (eV) (heat of sublimation); 
                         default from table1
                         ! no default, control of Inputs
 energyinp='./'          directory of inputfile 'layer.inp' 
 idrel=1                 mode of simulation (1...static mode)
 idout = -1              control output, determines the outputfiles:
                         E0_31_target.dat, E0_34_moments.dat, partic*.dat, 
                         trajec*.dat and restartfile 
                         = -1 : idout = nh/100, 100 fluence steps      
                         =  0 : output suppressed
                         >  0 : data output after each idout'th history
 iintegral = 0           integration by MAGIC (1: Gauss-Mehler)
 imcp = 1                1...moments calculated 
 inel0(1:ncpm) = 3       inelastic loss model
                         1 : Lindhard-Scharff
                         2 : Oen-Robinson
                         3 : equipartition of 1 and 2
                         4 : high energy hydrogen (energy > 25 keV), table3
                         5 : high energy helium (energy > 100 keV), table4
 ioutput_energ = 0       output in matrix 0...linear 1..log
 ioutput_hist(:) = 10    number of output of trajectories of 6 species
 ioutput_part(:) = 10    number of output 6 kind particles
 ioutput_polar = 0       output in matrix 0...degree 1..cos
 ipivot = 16             number of pivots in the Gauss_Mehler (no limitation)
                         and Gauss_Legendre quadrature (<= 16)
                         (larger numbers increase the computing time)
 iq0 = 0                 initial target composition
                         < 0 : given target composition <-layer.inp
                         = 0 : homogeneous target composition 
 irand=1                 start seed for random
 iseed2=1                start seed for random
 seed_g =1                start seed for random
 irc0 = -1               flag for subthreshold 
 isbv = -1               surface binding model, determines sbv(1:ncp,1:ncp) 
                         from the surface binding energy e_surfb(1:ncp) 
                         taken from table1
                         no default, control of Inputs
 isot=0                  flag for isotope mass, no consider isotop      
 iwc = 2                 Number of ring cylinders for weak simultaneous 
                         collisions (projectiles), for high energies 
                         (MeV H or He) iwc can be reduced to 1 or 0 to 
                         reduce computing time
 iwcr = 2                Number of ring cylinders for weak simultaneous 
                         collisions (recoils)
 layerinp ='./'          directory of inputfile all tables  
 lmatrices = .false.     output of matrices
 lmoments=.true.         output of moments for energy distributions  
 lparticle_p = .false.   output of projectile information
 lparticle_r = .false.   output of recoil information
 ltableread = .true.     reads tables for input (projectiles and recoils)
 ltraj_p = .false.       output of projectile trajectories
 ltraj_r = .false.       output of recoil trajectories
 lrestart = .false.      output of restartfile after each idout-step
 matrix_e_min=-1         minimum of lin. energy distribution in matrices 
                         if no input program set matrix_e_min=0
 matrix_e_max=-1         minimum of lin. energy distribution in matrices
                         if no input program set matrix_e_max=max(e0) 
 nm  = -1                - 1 : not a molecular target,
                         > 1 : number of atoms in a molecule
 nr_pproj   = 10         number pseudo particle (necessary for parallel calculation)
                         idrel == 0 ... number projectiles before target update (statistic)
                         idrel /= 0 ... number projectiles =nr_pproj*nh
 numb_hist = 20          number of output of all trajectories
 number_calc = 1         number of series
 qu(1:ncpm)     = -1.0   no default, control of Inputs 
 qubeam(1:ncpm) = -1.0   projectile atomic fractions 
                         no default, control of Inputs
 qumax(1:ncpm) = 1.0     maximum atomic fractions in the target
 rhom       = -1.        density of 2-compound target   
 sfin = 0                = 0 : no inelastic energy loss outside the target 
                               surface (x=0.)
                         = 1 : inelastic energy loss outside the target 
                               surface (-su>x>0.)
                         no default, control of Inputs 
 shth = 0.               sheath potential (in eV); only of interest in
                         connection with a Maxwellian velocity distribution 
 tableinp ='../tables'   directory of inputfile 'angle.inp' 
 ttarget    = -1.        total target thickness 
                         no default, control of Inputs
 ttemp  = 0.             target temperature, only of interest at high 
                         temperatures, it reduces the surface binding energy 
                         according to a Maxwellian energy distribution
 two_comp='???'          formel of 2-compound target example: Ta2O5 
 x0(1:ncpm) = 0.         starting position of projectile
                           <= 0. : starting outside the surface at x=xc=-su
                           > 0   : starting inside the solid
   
   
this default values only set
----------------------------
   
 maxpart =500            only startvalue (dynamic allocation)
 dmaxpart=500            only startvalue (dynamic allocation)
 maxt    =500            only startvalue (dynamic allocation)
 dmaxt   =500            only startvalue (dynamic allocation) 
                          
 
 io   = 6  !6  Standard output
 io7  = 7  !6   time output
 io29 = 29 !--- general output
 io30 = 30 !--- tri.inp
 io31 = 31 !            output
 io33 = 33 !--- idrel=0 output
 io34 = 34 !--- idrel=0 output
 io100=100 !--- time_run.dat
 
 ncp   = -1       ! no default, control of Inputs
 flc   = -1.0     ! no default, control of Inputs
 nh    = -1       ! no default, control of Inputs
 ipot  = -1       ! no default, control of Inputs
 nqx   = -1       ! no default, control of Inputs
 
 icase_e0    = 0 use for series
 icase_alpha = 0 use for series


