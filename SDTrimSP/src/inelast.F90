!subroutine inelst (e_loc_elec_sum,rk23,x,ix,dlang,energie,j,e_electr)
!subroutine inelh  (e_electr, x, e, asigt, hlm, hlmt, squx, ncp, m1, ch)
!subroutine inelhe (e_electr, x, e, asigt, hlm, hlmt, squx, ncp, m1, ch)

subroutine inelst(e_loc_elec_sum,rk23,x,ix,dlang,energie,j,e_electr)
  !---call from projectile.F90 and recoil.F90
  use work
  implicit none
  real   ,intent(in)  :: e_loc_elec_sum !---sum local electronic loss
  real   ,intent(in)  :: dlang !--d_pathlength,distance between collisions
  real   ,intent(in)  :: rk23  !--sum((ck*K)*%Anteil) Eckstein,91 S.67 after gl(5.2.1)
  real   ,intent(in)  :: x     !--depth
  integer,intent(in)  :: ix    !--number layer
  integer,intent(in)  :: j     !--projectile:jin / recoil:aps%label
  real   ,intent(in)  :: energie  !---energy:e5 / aps%er
  real   ,intent(out) :: e_electr !---continuous electronic loss 
                                  !   / local electronic loss 
  real :: help,asigt
  integer::i
  !---inelastic energy loss dE=s * n *S_in  !Tridyn,88 S.15 gl(28) 
  !   s=lm-tau+tau_theta=dlang   n=dns(layer)
  asigt = dlang*dns(ix)
  
  select  case (inel)

    case (1) 
      !---dE ~v  ~sqrt(v^2)  ~sqrt(E) (Lindhard,Scharf) (continuous)
      if (x < hlm .or. x > hlmt) then
       !---outside target+hlm
       e_electr = 0.
      else
       !---kl Eckstein.91 S.67 gl(5.2.1) (Lindhard/Scharf) -->SDTrimSP.F90
       !---ck Eckstein,91 S.67 after gl(5.2.1)
       !   rk23 --> projectile.F90/recoil.F90
       ! dE=      s            * n * S_in
       !   = (lm-tau+tau_theta)*dns*(ck*K)*%Anteil *sqrt(E)
       !   = asigt                 *(kiel)*qux     *sqrt(E)
       !   = asigt                 *    rk23       *sqrt(E)
       e_electr = asigt*rk23*sqrt(energie)
      endif
      help = max (energie - e_electr, 0.)

    case (2)
      !---dE ~ depend on Stossparameter (Oen-Robenson) (local)
      e_electr = e_loc_elec_sum
      help = max (energie, 0.) 
    
    case (3)
      !---dE ~ 50%(1) + 50%(2)
      if (x < hlm .or. x > hlmt) then
        e_electr = e_loc_elec_sum
      else
        e_electr = 0.5 * (asigt*rk23*sqrt(energie) + e_loc_elec_sum)
      endif
      help = max (energie - e_electr, 0.)
  
    case (4)
      !---dE ~ high energy only for H over 20KeV / 25KeV 
      !---Variable ch_h(1:12, 1:ncp) is read from Table in tridyn.F90
      call inelh(e_electr,x,energie,asigt,hlm,hlmt,qux(ix,1:ncp),ncp,a_mass(j),ch_h)
      help = max (energie - e_electr, 0.)
      
    case (5)
      !---dE ~ only for He over 50 Kev / 100 KeV 
      !---Variable ch_he(1:9, 1:ncp) is read from Table in tridyn.F90
      call inelhe(e_electr,x,energie,asigt,hlm,hlmt,qux(ix,1:ncp),ncp,a_mass(j),ch_he)
      help = max (energie - e_electr, 0.)
    end select
    
    !--- t=s/v  E=m/2*v**2 v=sqrt(E/m*2) --> t=  sqrt(m/2) * s/sqrt(E)
    !    pots.F90 --> tmpr(i) = sqrt(4.*eec_const*m(i)) 
    if (dlang > 0.) ptime = ptime + tmpr(j) * dlang/(sqrt(energie) + sqrt(help))
  return  
end 
!----------------------------------------------------------------------------------
!----  INELASTIC STOPPING FOR HYDROGEN case 4  ------------------------------------------
!----------------------------------------------------------------------------------
  !The next two routine calculates the inelastic (electronic) energy 
  !loss for high energy (> 20 keV) hydrogen and helium (>100 keV). 
  !Formulae in units of keV
subroutine inelh(e_electr, x, e, asigt, hlm, hlmt, squx, ncp, m1, ch)
  !---for H, D and T table1
  implicit none
  real                      :: e_electr  !inelastic (electronic) energy
  real                      :: x         !depth
  real                      :: e         !energy 
  real                      :: asigt     !effective distance between collisions
  real                      :: hlm, hlmt !distance above front
  real                      :: m1        !mass of hydrogen isotope
  integer                   :: ncp       !number of species
  real, dimension (ncp)     :: squx      !atomic fraction of species 
  real, dimension (12, ncp) :: ch        !constants from table 3    
  real, dimension (ncp)     :: sh
  real                      :: ek, ekm, vel2
  real                      :: s_high ,s_low,eklog,eklog2   !optimierung
  integer                   :: j          
  real, parameter           :: prm = 9.3149432e+5 ! proton rest mass in keV
  
  if (x < hlm .or. x > hlmt) then
    e_electr = 0.
    return
  endif  
  
  !---Eckstein,91 S.70/71
  ek  = e * 0.001     !energy  in keV 
  ekm = ek / m1       !energy-coeff. : energy/m1  in keV 

  if (ek < 1000.) then
    if (ek < 10.) then
      !---Eckstein,91 S.70 after gl(5.2.5)    (1eV < E < 10keV)
      !print*,'inel:ek < 10',e 
      do j = 1, ncp
        sh(j) = ch(1,j)*sqrt(ekm)
      enddo
    else
      !---Eckstein,91 S.70 after gl(5.2.6)    (10keV < E < 1MeV)
      !print*,'inel:10keV < E < 1MeV',e 
      do j = 1, ncp
        s_low  =ch(2,j)*ekm**0.45
        s_high =ch(3,j)/ekm*log(1.+ ch(4,j)/ekm + ch(5,j)*ekm)
        !---Eckstein,91 S.70 after gl(5.2.5)
        !   1/sh=1/S_low+1/S_high sh=S_low*S_high/(S_low+S_high)  
        sh(j) =  s_low * s_high / (s_low + s_high )
      enddo
    endif
  else
    !---Eckstein,91 S.70 after gl(5.2.7)      (1MeV < E < 100MeV)
    !print*,'inel:1MeV < E < 100MeV',e 
    eklog  = log(ekm) 
    eklog2 = eklog*eklog 
    do j = 1, ncp
       !---E=m/2*v**2 -> v**2=2*E/m
       vel2 = 2. * ekm / prm
       sh(j)= ch(6,j)/vel2 * (log(ch(7,j)*vel2/(1.-vel2))   &
              - vel2                   &
              - ch( 8,j)               &
              - ch( 9,j)*eklog         &
              - ch(10,j)*eklog2        &
              - ch(11,j)*eklog*eklog2  &
              - ch(12,j)*eklog2*eklog2 )
    enddo
  endif
  e_electr = 10. * asigt * sum(sh(1:ncp) * squx(1:ncp)   )
  return
end
!----------------------------------------------------------------------------------
!----  INELASTIC STOPPING FOR HELIUM  case 5   ------------------------------------------
!----------------------------------------------------------------------------------
subroutine inelhe(e_electr, x, e, asigt, hlm, hlmt, squx, ncp, m1, ch)
  !---for He3 and He table1
  implicit none
  real                     :: e_electr  !inelastic (electronic) energy
  real                     :: x         !depth
  real                     :: e         !energy
  real                     :: asigt     !effective distance between collisions
  real                     :: hlm, hlmt !distance above front
  real                     :: m1        !mass
  integer                  :: ncp       !number of species
  real, dimension (ncp)    :: squx !qux(ix,1:ncp)
  real, dimension (9, ncp) :: ch
  real, dimension (ncp)    :: sh
  real     :: em, ek, fak
  integer  :: j
  real                     :: eklog,eklog2,s_high,s_low   !optimierung

  if (x < hlm .or. x > hlmt) then
    e_electr = 0.
    return
  endif 

  if (m1 < 4) then
    ek=e*0.0013333 !energy-coeff in keV + fak=1.333 (correction for He3: m(He)/m(He3))
  else
    ek=e*0.001     !energy-coeff in keV             (for He=He4)
  endif
  em=ek*0.001      !energy-coeff in MeV
  
  if (ek < 10000) then
    do j = 1, ncp
      !---Eckstein,91 S.70 after gl(5.2.8)          (1keV < E < 10MeV) 
      s_low =ch(1,j)*ek**ch(2,j)
      s_high=ch(3,j)/em * log( 1.+ ch(4,j)/em + ch(5,j)*em )
      !---Eckstein,91 S.70 after gl(5.2.5)
      !   1/sh=1/S_low+1/S_high sh=S_low*S_high/(S_low+S_high)  
      sh(j) =  s_low * s_high / (s_low + s_high )
    enddo
    !print*,'(1keV < E < 10MeV'
  else
    !eklog = log(1.0/em)
    eklog = -log(em) !---log(1.0/em) = -log(em)
    eklog2 = eklog*eklog
    do j = 1, ncp
     !---Eckstein,91 S.70 after gl(5.2.9)              (E > 10MeV )
     sh(j) = exp(  ch(6,j)             &
                 + ch(7,j)*eklog       &
                 + ch(8,j)*eklog2      &
                 + ch(9,j)*eklog2*eklog )
    !print*,ch(6,j),ch(7,j),ch(8,j),ch(9,j),em
    enddo
    !print*,'10MeV < E'
  endif
  e_electr = 10. * asigt * sum( sh(1:ncp) * squx(1:ncp) )
  
  return
end
