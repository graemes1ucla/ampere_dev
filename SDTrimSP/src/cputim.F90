subroutine cputim(dsec)
  implicit none
  !---scalar arguments
  real   dsec
  !---locals
  integer icount,irate
  
  call system_clock (icount,irate)
  dsec=float(icount)/float(irate)
  return
end subroutine cputim

subroutine cputim2(dsec)
  implicit none
  !---scalar arguments
  real   dsec
  !---locals
  integer icount,irate,countmax
  
  call system_clock (icount,irate,countmax)
  dsec=float(countmax)/float(irate)
  return
end subroutine cputim2
