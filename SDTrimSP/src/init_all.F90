!subroutine sbv_init
!subroutine angle_init
!subroutine energy_init
!subroutine target_init
!
!---unit: l  [1A]=[10^10 m] =[10^8 cm]    A ...Angstr�m
!---unit: E  [eV]                         eV...Elektronen-Volt

!-----------------------------------------------------
subroutine sbv_init
  !---calculate surface binding energies   isbv <-tri.inp ; e_surfb <-table1
  use work
  use MPP_functions
  !   isbv... (1-5)cases of surface binding energies 
  !   sbv... berechnete    surface binding energies sbv(ncpm,ncpm)
  !   e_surfb...tabellarische surface binding energies sbj(ncpm)
  !---Faelle 1. sbv=e_surfb 
  !---Faelle 2. sbv=e_surfb 
  !---Faelle 3. sbv=0.5*(e_surfb(ip)+e_surfb(jp)) or  sbv=0,if e_surfb=0
  !---Faelle 4. sbv=0.5*(e_surfb(ip)+e_surfb(jp))+0.5....solid/solid compound
  !---Faelle 5. sbv=0.5*e_surfb(ip)+....                 solid/gas
  sbv(:,:)=0.
  select case (isbv)
  case (1)
    !---Tridyn,01 S.8 gl(2) sbv in text SBV_ij=SBE_i  simple case
    do jp = 1, ncp 
      sbv(jp,jp) = e_surfb(jp)
    enddo
  case (2)
    do jp = 1, ncp 
      do ip = 1, ncp
       sbv(ip,jp) = e_surfb(jp)
      enddo
    enddo
  case (3)
    do jp = 1, ncp 
      do ip = 1, ncp
        if (e_surfb(jp) < 0.0000001 .or. e_surfb(ip) <0.0000001) then 
          sbv(ip,jp) = 0.0
        else
          sbv(ip,jp) = (e_surfb(ip)+e_surfb(jp))*0.5
        endif
      enddo
    enddo
  case (4)
    !---solid/solid compound (two components!):
    !---Tridyn,01 S.11 gl(14)
    !   SBV_aa=SBE_a SBV_bb=SBE_b  SBV_ab=(SBE_a+SBE_b)/2 +  
    do jp = 1, ncp-2 
      sbv(jp,jp) = e_surfb(jp)
    enddo
    if (count(qubeam(1:ncp) < 0.0000001) == 2) then 
      if (nm <= 0) then
         write(io,*) 'nm = ', nm, ' is not allowed; nm has to be chosen' & 
                    ,'according to the target composition'
         goto 1000  !---stop
      endif
      if (pid == 0) then
        if(deltahf == 0.)write(io,*)'WARNING solid/gas compound: deltahf = 0.'
      endif
      sbv(ncp-1,ncp-1)=e_surfb(ncp-1)
      sbv(ncp  ,ncp  )=e_surfb(ncp  )
      !---sbv=f(e_surfb,deltahf,qux)
      sbv(ncp-1,ncp  )=0.5*(e_surfb(ncp-1)+e_surfb(ncp))+0.5/(nm*qux(1,ncp-1)*qux(1,ncp))*deltahf
      sbv(ncp  ,ncp-1)=sbv(ncp-1,ncp)
    endif
  case (5)
    !---solid/gas compound (two components!):
    do jp = 1, ncp-2 
      sbv(jp,jp) = e_surfb(jp)
    enddo
    if (count(qubeam(1:ncp) < 0.0000001) == 2) then   
     if (nm <= 0) then
       write(io,*) 'nm = ', nm, ' is not allowed; nm has to be chosen' &
                  ,'according to the target composition'
       goto 1000  !---stop
     endif
     if (pid == 0) then
      if(deltahd(ncp)==0.)write(io,*)'WARNING solid/gas compound: deltahd = 0'
      if(deltahf     ==0.)write(io,*)'WARNING solid/gas compound: deltahf = 0'
     endif
     sbv(ncp-1,ncp-1)= e_surfb(ncp-1)
     sbv(ncp  ,ncp  )= e_surfb(ncp  )
     !---new
     !---sbv=f(e_surfb,deltahf,deltahd,qux)
     sbv(ncp-1,ncp)= 0.5 * e_surfb(ncp-1)  &
              +0.5/(nm*qux(1,ncp-1)*qux(1,ncp))*deltahf+0.25/qux(1,ncp-1)*deltahd(ncp)
     sbv(ncp,ncp-1) = sbv(ncp-1,ncp)
    endif
  end select
  return

1000 continue
  write(io,*)'stop from subroutine sbv_init (init_all.F90)'
  call allstop

end subroutine sbv_init
!-----------------------------------------------------
subroutine angle_init
  !---INPUT OF ANGULAR DISTRIBUTION FROM FILE (ONLY FOR 1st COMPONENT)
  use work
  use random_numbers
  use MPP_functions
  implicit none
  character              :: filename*1024,dummy_c*1
  integer                :: i, ii, k, input_ok 
  real                   :: dummy,alpha
  real, dimension(nalpha):: fai !--- only for output  of angles distribution
  integer(iwpi)   :: seed_help

  input_ok=1
  if (pid == 0) then
     angle_a(:)  =0.0
     distr_a(:)=0.0
     filename=angleinp(1:len_trim(angleinp)) // 'angle.inp'
     open(21,file=filename)
     nia = 0
     read (21,*) dummy_c
     do i = 1, nalpha
        read(21,*, END=66) angle_a(i), distr_a(i)
        if ( 90 < angle_a(i) ) then
         write(io,*) 'Error: alpha input > 90, change angle.inp '
         input_ok=0
         goto 100  !---stop
        endif
        nia = nia + 1
     enddo
66   continue
     close(21)
     if (nia > nalpha) then
        write(io,*) 'alpha too small: ', nalpha, ', should be: ', nia
        input_ok=0
     endif
  endif !pid==0
  100 continue
  call barrier
  call broadcast(input_ok)
  if (input_ok == 0) then
     goto 1000  !---stop
  endif
  call barrier
  call broadcast(nia)
  call broadcast(angle_a(1:nia))
  call broadcast(distr_a(1:nia))
  m_distr_a=maxval(distr_a)
   
  if (pid == 0) then
    !---test distribution
     seed_help=1
     fai(:)=0.0  !---only for output
     do i=1,10000
       do 
         alpha = random(seed_help)*90.
         dummy = random(seed_help)*m_distr_a
         do ii=nia,1,-1
           if(alpha < angle_a(ii)) k=ii 
         enddo
         if(dummy <= distr_a(k)) exit 
       enddo
       !write(*,*)'use 'alpha
       fai(k)=fai(k)+1  !---only for output
     enddo
     write(*,*) ' i    angle      given distribution    distribution(10000)  '
     do i=1,nia
       write(*,'(i3,3f15.10)')i,angle_a(i),distr_a(i)/sum(distr_a(:)),fai(i)/sum(fai(:))
     enddo
  endif
  return

1000 continue
  write(io,*)'stop from subroutine angle_init (init_all.F90)'
  call allstop
end subroutine angle_init
!------------------------------------------------------------------
subroutine energy_init
  !---INPUT OF ENERGY DISTRIBUTION FROM FILE (ONLY FOR 1st COMPONENT) 
  !---e0... energy of projectiles   0.1 < e0 < 10**9 eV
  use work
  use random_numbers
  use MPP_functions
  implicit none
  character              :: filename*1024,dummy_c*1
  integer                :: i, ii, k, input_ok 
  real                   :: dummy ,qubeam_sum,e_part
  real, dimension(nemax ):: fei  !--- only for output  of energies distribution
  real, dimension(ncpm)  :: velo !--- only forMaxwellian distribution
  integer(iwpi)          :: seed_help

  input_ok=1
  select case (case_e0)
   case (0)
    !---energy constant
    do jp = 1,ncp
     if (qubeam(jp) > 0) then
       if (e0(jp) <=0.0 .and. icase_e0 == 0) then
         write(*,*)' case_e0=',case_e0,icase_e0
         write(*,*)' e0 <=0 not allowed; e0(',jp,')=',e0(jp)
         goto 1000  !---stop
       endif
     endif
    enddo
   case (1)
    !---input of energy distribution <--read energy.inp
    if (pid == 0) then
      write(io,*)'--- read: energy.inp'
      werte_e(:)=0
      distr_e(:) =0
      filename=energyinp(1:len_trim(energyinp)) // 'energy.inp'
      open(21,file=filename)
      read (21,*) dummy_c
      nie = 0
      do i = 1, nemax
        !---          energy, distribution
        read(21,*,END=77) werte_e(i),distr_e(i)
        nie = nie + 1
      enddo
77    continue
      close(21)
      if (nie > nemax) then
        write(io,*) 'nemax too small: ', nemax, ', should be: ', nie
        input_ok=0
      endif
    endif
    call barrier
    call broadcast(input_ok)
    if (input_ok == 0) then
      goto 1000  !---stop
    endif
    call barrier
    call broadcast(nie)
    call broadcast(werte_e(1:nie))
    call broadcast(distr_e(1:nie))
    m_distr_e =maxval(distr_e)   !---max. distribution
    m_werte_e =maxval(werte_e)  !---max. energy
    do jp = 1,ncp
     if (qubeam(jp) > 0) then
       e0(jp) = m_werte_e !---for scaling of output
     endif
    enddo
     
    if (pid == 0) then
     !---test distribution
      seed_help=1
      fei(:)=0.0  !---only for output
      do i=1,10000
        do 
          e_part = random(seed_help)*m_werte_e
          dummy  = random(seed_help)*m_distr_e
          do ii=nie,1,-1
            if(e_part < werte_e(ii)) k=ii 
          enddo
          if(dummy <= distr_e(k)) exit 
        enddo
        !write(*,*)'use ',e_part,dummy,distr_e(k)
        fei(k)=fei(k)+1  !---only for output
      enddo  
      write(*,*) ' i    werte_e  given distribution    distribution(10000)  '
      do i=1,nie
        write(*,'(i3,3f15.10)')i,werte_e(i),distr_e(i)/sum(distr_e(:)),fei(i)/sum(fei(:))
      enddo
    endif
   case (2,3)      
    !---calculation of energy distribution e=f(charge(jin)*shth, kti)
    !---
    do jp = 1,ncp
     if (qubeam(jp) > 0) then
       if (e0(jp) <=0.0) then
         write(*,*)' e0... temperature of plasma/target'
         write(*,*)' e0 <=0 not allowed; e0(',jp,')=',e0(jp)
         goto 1000  !---stop
       endif
     endif
    enddo
    if(shth < 0) write(*,*)'WARNING:sheat potential negativ: not standard'
  end select  
  
  !---calculation for projectiles
  if (case_e0 >= 2) then
    !--use a Maxwellian distribution
    if (shth < dble_prec) then
      if(pid == 0)write(io,*)'Error: shth eq 0'
      goto 1000  !---stop
    endif 
    if (charge(1) > 1.+dble_prec .or. charge(1) < 1.-dble_prec) then
      if(pid == 0)write(io,*)'Error: charge(1) ne 1, charge(1)=',charge(1)
      goto 1000  !---stop
    endif 
    !---charge for qubeam(1) every 1  --> charge=1  (H)
    !---qubeam... more projectiles because larger velocity
    velo(:)=0.0
    do jp = 1, ncp_proj
      velo(jp) = sqrt(2.0 * (e0(jp)+charge(jp)*shth )/a_mass(jp))
    enddo
      qubeam_sum=sum( qubeam(1:ncp_proj) * velo(1:ncp_proj) )
    do jp = 1, ncp_proj  
      qubeam(jp)= qubeam(jp)*velo(jp)/qubeam_sum
    enddo
  else
    qubeam(1)  = 1. - sum(  qubeam(2:ncp)) 
  endif
  return

1000 continue
  write(io,*)'stop from subroutine energy_init (init_all.F90)'
  call allstop
end subroutine energy_init
!-----------------------------------------------
subroutine target_init
  !---INPUT OF ANGULAR DISTRIBUTION FROM FILE (ONLY FOR 1st COMPONENT)  
  use work
  use random_numbers
  use MPP_functions
  implicit none
  integer    :: i, j, kk 
  real       :: range, rangemax, dnsm, qubeam_sum
  character  :: filename*1024, dummy_c*1

  if(pid == 0)write(io,*)'--target-------------------------'
  
  !=====SET INITIAL ATOMIC FRACTIONS AND DENSITIES 
  !   (iq0 < 0)  -->layer.inp
  !   (iq0==0)   -->constant depth interval   default iq0=0 
  !   dqx     ... thickness of depth interval, constant depth interval (iq0==0)
  !   ttarget ... thickness of target [A]      <-tri.inp
  !   nqx     ... number of depth interval     <-tri.inp

  !---check
  if (nqx <=0) then
    if(pid==0)write(io,*)'  ERROR : nqx <=0 is not allowed, nqx must be > 0, set it in tri.inp'
    if(pid==0)write(io,*)'  stop from target_init (init_all.F90)'
    call allstop
  endif 
  
  dqx=0.0
  if (iq0 < 0) then
    !---initial composition according to vector input 
    if (pid == 0) then
      !---read input data
      filename=layerinp(1:len_trim(layerinp)) // 'layer.inp'
      !open (55, FILE="layer.inp", ACTION="read")
      open (55, FILE=filename, ACTION="read")
       read (55,*) dummy_c
       read (55,*) dummy_c
       write(io,*)'--- read layer.inp ---'
       !---xxx...thick of depth interval 
       !---xno...startpoint of depth interval (xno(1)=0, xno(nqx+1)=ttarget) 
       xno(1)=0.0 
       mm=1
       do 
         read (55,*) j, xxx(mm),qux(mm,2:ncp)      

         if (j /=0 ) then
           if ( xxx(mm) <0.1 )then
            write(io,*)'   mm:',mm,' xxx(mm):',xxx(mm),' thickness of depth interval < 0.1)'
            goto 1000  !---stop
           endif
           
           if(mm > nqx )write(io,*)'   WARNING: nqx less then depth intervals from layer.inp'
           if(mm > nqx ) exit
           
           !---calculate end-depth interval --> xno(mm+1)
           xno(mm+1)=xno(mm)+xxx(mm)
           mm=mm+1
           !---repeat one depth interval if j>1 
           do i=2,j 
             if(mm > nqx )write(io,*)'   WARNING: nqx less then depth intervals from layer.inp'
             if(mm > nqx ) exit
             xxx(mm)  =xxx(mm-1)
             xno(mm+1)=xno(mm)+xxx(mm) 
             qux(mm,2:ncp)=qux(mm-1,2:ncp)
             mm=mm+1
           enddo
         else
           if(mm > nqx ) exit
           write(io,*)'   WARNING: repeat all depth intervals until nqx ' 
           i=1
           kk=mm
           do 
             xxx(mm)  =xxx(i)
             xno(mm+1)=xno(mm)+xxx(mm) 
             qux(mm,2:ncp)=qux(i,2:ncp)
             mm=mm+1
             i=i+1
             if(i==kk)i=1
             if(mm > nqx ) exit
           enddo             
           exit
         endif  
       enddo
       xno(nqx+1)=xno(nqx)+xxx(nqx)
       ttarget=xno(nqx+1)      
       if (pid == 0) write(io,*)'   ttarget(calculate with layer.inp):',ttarget
      close(55)
      
      !---output (all qux)
      open (55, FILE="layer.dat", ACTION="write")
       write(55,*)'  mm      xno(mm)          xxc(mm)     qux(mm,1:ncp)'  
       do mm=1,nqx
         qux(mm,1) = 1. - sum(qux(mm,2:ncp))
         dns(mm)   = 1./( sum(qux(mm,1:ncp)/dns0(1:ncp))  )
         !---xxc...center of depth interval 
         xxc(mm)=xno(mm)+0.5*(xno(mm+1)-xno(mm))
         write(55,'(i5,2e15.5,50f10.5)')mm,xno(mm),xxc(mm),qux(mm,1:ncp)
       enddo      
       write(55,'(i5,2e15.5)')nqx,xno(nqx+1)
      close(55)
      write(io,*)'--- end read layer.inp ---'
      !---only for output:
      quinp(:) = 0.         

    endif !PID=0    
    call barrier
    call broadcast(xxx)
    call broadcast(xxc)
    call broadcast(xno)
    call broadcast(qux)
    call broadcast(dns)    
    call broadcast(ttarget)    
  endif 
  
  !---calculate ttdyn
  rangemax = 0.
  dnsm = 0.
  do j = 1, ncp
    if (qubeam(j) < 0.0000001) dnsm = dnsm + dns0(j)*qu(j)  
  enddo
  do i = 1,ncp
    if (qubeam(i) > 0.0000001) then
     range = 0.
     do j = 1, ncp
      if (qubeam(j) < 0.0000001) then  
       !---two possibilities to determine range
       !range=(((e0(i)*f(i,j))**0.3 + 0.1) **3)/(dns0(j)*a_screen(i,j)**2)
       range=(((e0(i)*eps_d_e(i,j))**0.3+0.1)**3)/(dnsm*a_screen(i,j)**2)
       rangemax = max(rangemax,range)
      endif
     enddo
    endif
  enddo
  ttdyn = ceiling(rangemax/nqx) * nqx
  
  if (pid == 0) then
    write(io,*)'  ttdyn(estimate):',ttdyn,' target=',ttarget
    if(ttarget < ttdyn ) write(io,*) '  WARNING: ttdyn > ttarget'
    if(ttarget < -0.5  ) write(io,*) '  INFO: because of ttarget<0, was set: ttarget=ttdyn'
  endif 
  if(ttarget < -0.5 )ttarget=ttdyn
  
  if (iq0 == 0) then
    !---initial composition homogeneous  qu <- tri.inp
    dqx = ttarget/nqx
    if ( dqx <0.1 )then
      if( pid == 0 )write(io,*)'  dqx:',dqx,' thickness of depth interval < 0.1)'
      goto 1000  !---stop
    endif
    do mm = 1, nqxm !nqx
      qux(mm,1:ncp) = qu(1:ncp)
    enddo
    !---xxc...center of depth interval 
    !---xxx...dx 
    !---xno...startpoint of depth interval (xno(1)=0, xno(nqx+1)=ttarget) 
    do mm = 1, nqxm !nqx
      xxc(mm) = (mm -.5)*dqx
      xxx(mm) = dqx
      xno(mm) = (mm - 1)*dqx
      qux(mm,1) = 1. - sum(qux(mm,2:ncp))
      dns(mm)   = 1. /( sum(qux(mm,1:ncp)/dns0(1:ncp))  )
      if ( qux(mm,1) < 0.0 )then
        if( pid == 0 )write(io,*)' q(1)=',qux(mm,1) 
        if( pid == 0 )write(io,*)' sum of composition not 1 )'
        goto 1000  !---stop
      endif
    enddo       
    !---ttarget...max depth of calulation
    xno(nqx+1) = ttarget
    
    !---quinp: only for output
    quinp(1:ncp) = qu(1:ncp)
  endif
  
  quxb(1:ncp)       = qux(nqx,1:ncp)   !---quxb... bottom
  dnsb              = dns(nqx)         !---dnsb... bottom

  !---hlm...distance above the surface (x=0),where an inelastc energy loss 
  !---sfin... default=0 or 1   with or without hlmt
  if (sfin ==0) then
    !---default
    hlm = 0.
    hlmt = ttarget
  else
    !hlm = -0.5 * dns0m**(-1./3.)
    !---rho=1.0 / sum( qu(1:ncp)/dns0(1:ncp) )
    !---lm=rho**(-1./3.)
    hlm = -0.5 * ( 1.0/sum(qu(1:ncp)/dns0(1:ncp))  )**(-1./3.)
    hlmt = ttarget - hlm  
  endif 

  !---set surface of target
  srrc = 0.
  if(pid == 0)write(io,*)'---------------------------------'

  return

1000 continue
  write(io,*)'stop from subroutine target_init (init_all.F90)'
  call allstop
end subroutine target_init
