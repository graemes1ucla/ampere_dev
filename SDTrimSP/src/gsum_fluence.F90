subroutine gsum_fluence()
  use work
  use MPP_functions
  implicit none
  integer :: nrp,j
  if (idrel == 0) then
    nrp = nr_pproj
  else
    nrp = 1
  endif
  
  j=ncp_proj
  
  !---all
  e_sbe_p(1:j  ) = e_sbe_p(1:j  ) + global_sum(e_sbe_p_par(1:j  ),j  )/nrp
  e_sbe_r(1:ncp) = e_sbe_r(1:ncp) + global_sum(e_sbe_r_par(1:ncp),ncp)/nrp
  e_tot_r(1:ncp) = e_tot_r(1:ncp) + global_sum(e_tot_r_par(1:ncp),ncp)/nrp
  e_rec  (1:ncp) = e_rec  (1:ncp) + global_sum(e_rec_par  (1:ncp),ncp)/nrp
  e_sbe_p_par(1:j  ) = 0.
  e_sbe_r_par(1:ncp) = 0.
  e_tot_r_par(1:ncp) = 0.
  e_rec_par  (1:ncp) = 0.
  
  !---sum und distribution of all pid
  del(1:nqx,1:ncp) = global_sum(del_par(1:nqx,1:ncp),nqx*ncp)/nrp
  del_par(1:nqx,1:ncp) = 0.

  iback_rp(1:ncp,1:ncp) = iback_rp(1:ncp,1:ncp) + global_sum(iback_rp_par(1:ncp,1:ncp),ncp*ncp)/nrp
  iback_rp_par(1:ncp,1:ncp) = 0.
  
  call gsum_fluence2() 
end subroutine

!---------------------------------------------------------------------
subroutine gsum_fluence2()
  use work
  use MPP_functions
  implicit none
  integer :: nrp,j,i
  if (idrel == 0) then
    nrp = nr_pproj
  else
    nrp = 1
  endif
  
  !---projectil
  j=ncp_proj
  nproj  (1:j) = nproj  (1:j) + global_sum(nproj_par  (1:j),j)/nrp
  e_tot_p(1:j) = e_tot_p(1:j) + global_sum(e_tot_p_par(1:j),j)/nrp
  nproj_par  (1:j) = 0.
  e_tot_p_par(1:j) = 0.


  !---2 backscaterred
  j=ncp_proj 
  ipart_help(1:j)  = global_sum(iback_p_par(1:j),j)

  iback_p(1:j)     = iback_p(1:j) + ipart_help(1:j)               /nrp
  eback_p(1:j)     = eback_p(1:j) + global_sum(eback_p_par(1:j),j)/nrp
  iback_p_par(1:j) = 0.
  eback_p_par(1:j) = 0.
       
  i_rec  (1:ncp) = i_rec  (1:ncp) + global_sum(i_rec_par  (1:ncp),ncp)/nrp
  i_rec_par  (1:ncp) = 0.
  
  if (lmoments) then
  
#ifdef DEBUGMOM
    vvv(:,:,:) = vvv(:,:,:) + global_sum(vvv_par(:,:,:))/nrp
    vvv_par(:,:,:)=0.0
#endif
    
    do j=1,ncp_proj 
      if (ipart_help(j) > 0) then
       ebp_mom(1:6,j) = ebp_mom (1:6,j) + global_sum(ebp_mom_par(1:6,j),6)/nrp
       ebp_mom_par(1:6,j) =  0.
       ebp_mom(7,j)=global_min(ebp_mom(7,j))
       ebp_mom(8,j)=global_max(ebp_mom(8,j))
        
       !---back_p_log
       iback_p_log(   j) = iback_p_log(j) + global_sum(ib_pl_par(j))/nrp
       ib_pl_par  (   j) = 0.

       ebp_mol    (1:6,j) = ebp_mol(1:6,j) + global_sum(ebp_mol_par(1:6,j),6)/nrp
       ebp_mol_par(1:6,j) = 0.
       ebp_mol(7,j)=global_min(ebp_mol(7,j))
       ebp_mol(8,j)=global_max(ebp_mol(8,j))
      endif
    enddo
  endif

  !---3 transmitted tran_p
  j=ncp_proj 
  ipart_help(1:j)  = global_sum(itran_p_par(1:j),j)
  do j=1,ncp_proj 
    if (ipart_help(j) > 0) then
      itran_p    (1:j) = itran_p(1:j) + ipart_help(1:j)               /nrp 
      etran_p    (1:j) = etran_p(1:j) + global_sum(etran_p_par(1:j),j)/nrp
      itran_p_par(1:j) = 0.
      etran_p_par(1:j) = 0.
    endif
  enddo
      
  if (lmoments) then
    do j=1,ncp_proj 
      if (ipart_help(j) > 0) then
       etp_mom    (1:6,j) = etp_mom (1:6,j) + global_sum(etp_mom_par(1:6,j),6)/nrp
       etp_mom_par(1:6,j) = 0.
       etp_mom(7,j)=global_min(etp_mom(7,j))
       etp_mom(8,j)=global_max(etp_mom(8,j)) 
       
       !---tran_p_log
       itran_p_log(   j) = itran_p_log(j) + global_sum(it_pl_par(j))/nrp 
       it_pl_par    (   j) = 0.
       etp_mol    (1:6,j) = etp_mol(1:6,j) + global_sum(etp_mol_par(1:6,j),6)/nrp
       etp_mol_par(1:6,j) = 0.
       etp_mol(7,j)=global_min(etp_mol(7,j))
       etp_mol(8,j)=global_max(etp_mol(8,j)) 
      endif
    enddo
  endif
  
  
  !---1 stopped  istop_p
  j=ncp_proj
  istop_p(1:j) = istop_p(1:j) + global_sum(istop_p_par(1:j),j)/nrp 
  estop_p(1:j) = estop_p(1:j) + global_sum(estop_p_par(1:j),j)/nrp
  istop_p_par(1:j) = 0.
  estop_p_par(1:j) = 0.
  
  !---stop_p x
  !   moments are used if lmoments==T and also for statstic in outp(output.F90)
  xx_mom    (1:6,1:j) = xx_mom(1:6,1:j) + global_sum(xx_mom_par(1:6,1:j),6*j)/nrp
  xx_mom_par(1:6,1:j) = 0.0
  
  if (lmoments) then
   xx_mom(7,j)=global_min(xx_mom(7,j))
   xx_mom(8,j)=global_max(xx_mom(8,j)) 

   !---stop_p y/z=r
   yz_mom    (1:6,1:j) = yz_mom(1:6,1:j) + global_sum(yz_mom_par(1:6,1:j),6*j)/nrp
   yz_mom_par(1:6,1:j) = 0.0
   yz_mom(7,j)=global_min(yz_mom(7,j))
   yz_mom(8,j)=global_max(yz_mom(8,j)) 
  endif
  
  !---5 back_sputtered  iback_r
  ipart_help(1:ncp)  = global_sum (iback_r_par(1:ncp),ncp)
  j=ncp 
  iback_r    (1:j) = iback_r(1:j) + ipart_help(1:j)               /nrp 
  eback_r    (1:j) = eback_r(1:j) + global_sum(eback_r_par(1:j),j)/nrp
  iback_r_par(1:j) = 0.
  eback_r_par(1:j) = 0.
  !---back_sputtered from first layer 
  iback_r1    (1:j) = iback_r1(1:j) + global_sum (iback_r1_par(1:ncp),ncp)/nrp 
  iback_r1_par(1:j) = 0.
      
  if (lmoments) then
    do j=1,ncp 
      if (ipart_help(j) > 0) then
       !---back_r_log
       iback_r_log(j) = iback_r_log(j) + global_sum (ib_rl_par(j))/nrp
       ib_rl_par  (j) = 0. 
       ebr_mol    (1:6,j) = ebr_mol(1:6,j) + global_sum (ebr_mol_par(1:6,j),6)/nrp
       ebr_mol_par(1:6,j) = 0.
       ebr_mol(7,j)=global_min(ebr_mol(7,j))
       ebr_mol(8,j)=global_max(ebr_mol(8,j)) 
      endif
    enddo
  endif
  
  !---6 transm, sputtered itran_r
  ipart_help(1:ncp)  = global_sum (itran_r_par(1:ncp),ncp)
  do j=1,ncp 
    if (ipart_help(j) > 0) then
      itran_r    (1:j) = itran_r(1:j) + ipart_help(1:j)               /nrp 
      etran_r    (1:j) = etran_r(1:j) + global_sum(etran_r_par(1:j),j)/nrp
      itran_r_par(1:j) = 0.
      etran_r_par(1:j) = 0.
    endif
  enddo  

  if (lmoments) then
    do j=1,ncp 
      if (ipart_help(j) > 0) then
       !---tran_r_log
       itran_r_log(j) = itran_r_log(j) + global_sum(it_rl_par(j))/nrp
       it_rl_par  (j) = 0.
       etr_mol    (1:6,j) = etr_mol(1:6,j) + global_sum (etr_mol_par(1:6,j),6)/nrp
       etr_mol_par(1:6,j) = 0.
       etr_mol(7,j)=global_min(etr_mol(7,j))
       etr_mol(8,j)=global_max(etr_mol(8,j)) 
      endif
    enddo  
  endif

  !---1 stopped istop_r
  istop_r(1:ncp)     = istop_r(1:ncp) + global_sum(istop_r_par(1:ncp),ncp)/nrp 
  estop_r(1:ncp)     = estop_r(1:ncp) + global_sum(estop_r_par(1:ncp),ncp)/nrp
  istop_r_par(1:ncp) = 0.
  estop_r_par(1:ncp) = 0.
  
end subroutine

