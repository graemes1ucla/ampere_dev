From abs@pc.rzg.mpg.de Mon Apr 10 12:54 MET 2000
Date: Mon, 10 Apr 2000 10:54:18 GMT
From: abs@pc.rzg.mpg.de (Ari Seitsonen)
Content-Type: text
Content-Length: 326

/* see also  man getlim */
#if defined(unicos)
#include <errno.h>
#include <sys/category.h>
#include <sys/resource.h>
void TREMAIN_MPP(long *used, long *ierr)
{
struct resclim rptr;
rptr.resc_resource = L_MPPT;
rptr.resc_category = C_JOB;
rptr.resc_type = L_T_ABSOLUTE; 
*ierr=getlim(0,&rptr);
*used=rptr.resc_used;
}
#endif


