#if defined(unicos)
   /* This works on T3E in Grenoble. */
   /* It could not work for other CRAY computers (T.D.) */
   /* Give the job time limit */
   #include <sys/category.h>
   #include <sys/time.h>
   #include <sys/resource.h>
   long GETJOBLIMIT ()
   {
     long time;
     time = limit(C_PROC, 0, L_MPPT, -1);
     return(time);
   }
#else
   #include <sys/time.h>
   #include <sys/resource.h>
   #include <unistd.h>

  #if defined(aix)
    long getjoblimit ()
  #else
    long getjoblimit_ () 
  #endif
   {
     struct rlimit rlim;
     long time;
     int rv;
     rv = getrlimit (RLIMIT_CPU, &rlim);
     time = rlim.rlim_max;
     printf("time = %d\n", time);
     return(time);
   }
#endif
