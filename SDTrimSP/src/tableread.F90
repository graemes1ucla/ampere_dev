!subroutine read_all_tables(table_ok)
!subroutine table1read(tableinp,symbol, z, m, rho, arho, sbe, ed, delta_hd, io)
!subroutine table2read(tableinp,symbol, m, qu, nisot,io,nummer)
!subroutine table3read(tableinp,symbol, ch, io)
!subroutine table4read(tableinp,symbol, ch, io)
!subroutine table_compound(tableinp,two_comp, m, rho, arho, delta_hf, io)


subroutine read_all_tables(table_ok)
  use work
  use MPP_functions, only:npe
  implicit none        
  !---unit: l  [1A]=[10^10 m] =[10^8 cm]    A ...Angstr�m
  !---unit: E  [eV]                         eV...Elektronen-Volt
  !                 av = 6.0221367e23       ! Avogadro number (mol-1)
  real,parameter :: an = 0.60221367         ! [mol/cm**3] * an => [atoms/A**3]
  integer   :: i,j,ii,ncp_old,nisot_tot,ik,k,kk,izz,ilen,table_ok
  real      :: dns0_tbl, sbej_tbl, ed_tbl, ef_tbl, dhd_tbl, m_tabl, a_rho_tbl, dhf_tbl 
  real      :: rhom_tbl,dns_molec_tbl,ck_tbl,dns1,dns2
  character :: newsymbol*100
  integer, dimension(ncpm)      :: nisot
  real   , dimension(ncpm)      :: velo
  real   , dimension(12,ncpm)   :: mhelp, quhelp
  integer, dimension(12,ncpm)   :: nummer
  
  table_ok=1
  
  !---check
  if (ncp <= 0) then
   write(io,*)'ERROR: ncp:',ncp,', number of species must be > 0'
   goto 1000  !---stop
  endif
  do i=1,ncp
   if (qubeam(i) < 0) then
    write(io,*)'ERROR: qubeam(',i,') < 0 not allowed, qubeam must be > 0, set it in tri.inp'
    goto 1000
   endif
  enddo
  
  if (ltableread) then
   
   do i=1, ncp
     call table1read(tableinp,symbol(i),a_num_z(i),a_mass(i),a_rho_tbl,dns0_tbl,sbej_tbl,ed_tbl,ef_tbl,dhd_tbl,io)
     if (dns0(i)     < -0.5)  a_rho (i)  = a_rho_tbl 
     if (dns0(i)     < -0.5)  dns0  (i)  = dns0_tbl 
     if (e_surfb (i) < -0.5) e_surfb(i) = sbej_tbl 
     if (e_displ (i) < -0.5) e_displ(i) = ed_tbl  
     if (e_cutoff(i) < -0.5) e_cutoff(i)= ef_tbl  
     if (deltahd (i) < -0.5) deltahd(i) = dhd_tbl ! heat of dissociation
     dns0e(i)=dns0(i) !---only for output
     
     !---4...only for H D T energy over 1KeV 
     if (count(inel0(1:ncp) == 4) > 0) then
       if (inel0(i)==4 .and. symbol(i)/="H" .and. symbol(i)/="D" .and. symbol(i)/="T" ) then 
         write(io,*)'ERROR: inelastic (electronic) energy  model inel0(',i,')=4, only for H,D,T'
         goto 1000  !---stop
       endif
       !---ck_elec(1:ncpm,1:ncpm) = 1.  !correction factor for inelastic energy loss
       call table3read(tableinp,symbol(i),ch_h(:,i),ck_tbl,io)
       do j=1, ncp
         if(symbol(j)=="H" .or. symbol(j)=="D" .or. symbol(j)=="T" ) ck_elec(j,i)=ck_tbl
       enddo
     endif
     
     !---5...only for He energy over 1 KeV  
      if (count(inel0(1:ncp) == 5) > 0) then
       if (inel0(i)==5 .and. symbol(i)/="He3" .and. symbol(i)/="He" ) then
         write(io,*)'ERROR: inelastic (electronic) energy  model inel0(',i,'))=5, only for He3, He'
         goto 1000  !---stop
       endif
       call table4read(tableinp,symbol(i),ch_he(:,i),io)
     endif
   enddo
   
   ncp_old = ncp
   nisot_tot = 0
   do i=1,ncp
     if (isot(i) == 1) then
       call table2read(tableinp,symbol(i),mhelp(:,i),quhelp(:,i),nisot(i),io,nummer(:,i))
       nisot_tot = nisot_tot + nisot(i)-1
     else
       nisot(i) = 1
     endif
   enddo
   
   !---target with isotops (table 2), sort according to elements 
   if (nisot_tot > 0) then
     ncp = ncp + nisot_tot
     ca_help(1:ncp_old,1:ncp_old) = ca_scre(1:ncp_old,1:ncp_old)
     ck_help(1:ncp_old,1:ncp_old) = ck_elec(1:ncp_old,1:ncp_old)
     j = ncp
     do i=ncp,1,-1
       do ii=nisot(i),1,-1
         if (nisot(i) > 1) then
           newsymbol=symbol(i)
           ilen=len_trim(newsymbol)
           izz=nummer(ii,i)
           if(izz > 99)          write(newsymbol(ilen+1:ilen+3),'(i3)')izz
           if(9<izz .and. izz<99)write(newsymbol(ilen+1:ilen+2),'(i2)')izz
           if(izz <   9)         write(newsymbol(ilen+1:ilen+1),'(i1)')izz
           symbol  (j) = newsymbol
           a_num_z (j) = a_num_z (i)
           a_mass  (j) = mhelp   (ii,i)
           dns0    (j) = dns0    (i)
           dns0e   (i) = dns0    (i) !---only for output
           e_surfb (j) = e_surfb (i)
           e_displ (j) = e_displ (i)
           e0      (j) = e0      (i)
           e_cutoff(j) = e_cutoff(i)
           alpha0  (j) = alpha0  (i)
           qumax   (j) = qumax   (i)
           inel0   (j) = inel0   (i)
           x0      (j) = x0      (i)
           e_bulkb (j) = e_bulkb (i)
           charge  (j) = charge  (i)
           if (qu(i)>0) then
              qu(j) = quhelp(ii,i)
           else
              qu(j) = 0.0
           endif
           if (qubeam(i)>0) then
              qubeam(j) = qubeam(i)*quhelp(ii,i)
           else
              qubeam(j) = 0.0
           endif
         endif
         ik = 1
         do k=1,ncp_old
           do kk=1,nisot(k)
             ca_scre(ik,j) = ca_help(k,i)
             ck_elec(ik,j) = ck_help(k,i)
             ik = ik+1
           enddo
         enddo
         j = j-1     
       enddo
     enddo
   endif    ! nisot_tot > 0
  endif !---(ltableread)
  
  if (npe > pemax) then
    write(io,*)'ERROR: npe > pemax; increase pemax in work.F90'
    goto 1000  !---stop 
  endif

  if (nqx > nqxm) then
    write(io,*)'ERROR:' 
    write(io,*)'nqxm to small, nqxm>=nqx nqx:',nqx,' nqxm:',nqxm
    write(io,*)'nqxm to small, nqxm>=nqx nqx:',nqx,' nqxm:',nqxm
    write(io,*)'set in  param.F90 or tri.inp' 
    goto 1000  !---stop
  endif  
    
  if (count(qumax(1:ncp) < 0.9999999) == ncp) then
    !---(0 < qumax(jp) < 1)   more as ncp
    write(io,*)'qumax:',qumax(1:ncp)
    write(io,*)'ERROR: qumax, to many limit for qumax'
    goto 1000  !---stop
  endif
  
  if (sum(qubeam(1:ncp)) > 1.000001 ) then
    !---(0 < qumax(jp) < 1)   more as ncp
    write(io,*)'ERROR: sum(qubeam(1:ncp)) greater then 1 '
    goto 1000  !---stop
  endif
  
  !---data output after each idout'th history (default:-1)
  if (idout < 0)write(io,*)'INFO: idout not exist, set: idout=',nh / 100 
  if (idout < 0 )idout = max(1, nh/100) !nh,idout <- namelist TRI_INP
  if (idout > nh)write(io,*)'INFO: idout > nh, set: idout=',nh 
  if (idout > nh )idout = nh            !nh,idout <- namelist TRI_INP
  
 
  if (ncp > ncpm) then 
    write(io,*)'ERROR:'
    write(io,*)'ncpm to small, ncpm>=ncp ncp:',ncp,' ncpm:',ncpm
    write(io,*)'ncpm to small, ncpm>=ncp ncp:',ncp,' ncpm:',ncpm
    write(io,*)'set in  param.F90 or tri.inp' 
    goto 1000  !---stop
  endif
  
  do i=1,ncp 
    if (e_displ(i)  <= 0.0000001) then
      e_displ(i) = 15
      write(io,*)'WARNING: e_displ(',i,') <= 0 , was set to 15 eV'
    endif
    if ( e_cutoff(i) <= 0.0000001) then
      write(io,*)'ERROR:: e_cutoff(',i,') == 0 '
      goto 1000  !---stop
    endif
  enddo
  
  !-----TWO-ELEMENT TARGET
  !---dns0 <- tri.inp
  !   dns0 <- f(rhom,m,qu,nm...) if(iq0 >= 0)
  !        rhom <- tri.inp
  !        rhom <- table_compound
  !        dns_molec(rhom) <- table_compound
  !        dns_molec(rhom) <- calculate
  if (count(qubeam(1:ncp) < 0.0000001) == 2 .or. nm > 1) then  
    !write(io,*)'---------------------------------'
    write(io,*)' '
    write(io,*)'--two-element target-- chem. name:',two_comp
    if (nm < 0)then
      write(io,*)'nm  no  set'
      goto 1000
    endif
    write(io,*)
    write(io,'(3x,3a10,a14)')     'symbol','mol.mass','rho','dns0(element)'
    !write(io,'(3x,a10,5f10.5)')symbol(ncp-1),a_mass(ncp-1),a_rho(ncp-1),dns0(ncp-1)
    !write(io,'(3x,a10,5f10.5)')symbol(ncp  ),a_mass(ncp  ),a_rho(ncp  ),dns0(ncp  )
    do j=1,ncp
      write(io,'(3x,a10,5f10.5)')symbol(j),a_mass(j),a_rho(j),dns0e(j)
    enddo
    dns_molec=-1.
    if ( (iq0 >= 0)) then  
     !-number atoms per volume: N/V=rho*av/M = Dichte*Avogadro/Molare-Masse
     !-Tantal:
     !-N/V=16.6(g/cm**3)*6.0221367e23(mol-1)/180.9479(g/mol)=0.0552e30(m**3)
     !-N/V=16.6(g/cm**3)* an                /180.9479(g/mol)=0.0552   (A**3)
     mass_two=0.0

     !---compute N/V=dns_molec for two-element target

     if (rhom > 0.0 ) then
       !---M = ( n*M1 + m*M2)/(m+n) =q1*M1+q2*M2  ...mass_two
       !---dns_molec=rho*an / (q1*m1+q2*m2)   ,Tridyn,01 S.14 gl(20)
       mass_two=(nm * (qu(ncp-1)*a_mass(ncp-1) + qu(ncp)*a_mass(ncp)) )
       dns_molec=nm * rhom * an / mass_two 
     endif
     
     write(io,*)
     write(io,'(3x,a12,50a10)')two_comp,'mol.mass','rhom  ','atoms/A**3', 'deltahf'
     write(io,'(3x,a12,5f10.5)')'calculate:',mass_two,rhom,dns_molec,deltahf
 
     call table_compound(tableinp,two_comp, m_tabl, rhom_tbl,dns_molec_tbl, dhf_tbl,io )
     write(io,'(3x,a12,5f10.5)')'tab.comp :',m_tabl,rhom_tbl,dns_molec_tbl,dhf_tbl

     if(deltahf   < -.5) deltahf  = dhf_tbl  ! heat of formation
     if(rhom      < -.5) mass_two = m_tabl   ! Mol.Mass of 2-compound target
     if(rhom      < -.5) rhom     = rhom_tbl ! density of 2-compound target
     if(dns_molec < -.5) dns_molec= dns_molec_tbl !N/V of 2-compound target

     if (rhom > 0.0 .and. dns_molec < -0.5) then
       if (mass_two >0.0) then
          dns_molec=nm * rhom * an / mass_two
       else
         dns_molec=      rhom * an / (qu(ncp-1)*a_mass(ncp-1) + qu(ncp)*a_mass(ncp)) 
       endif
     endif

     if (deltahf   < -0.5) deltahf  = 0.0
     if (dns_molec <= 0.0 ) then
       write(io,*)'WARNING: molecular density rhom has to be given in the input file'
       goto 1000  !---stop
     else
       write(io,*) 
       write(io,'(3x,a12,50a10)')two_comp,('dns0',j=1,ncp)
       write(io,'(3x,a12,5f10.5)')'dns0_element :',dns0e(1:ncp)

       if (dns_molec_tbl > 0.0 ) then
        dns2=1./(1./(   qu(ncp)*dns_molec_tbl)-qu(ncp-1)/(qu(ncp)*dns0(ncp-1)))
        write(io,'(3x,a12,5f10.5)')'dns0_tbl   :',dns0(1:ncp-1),dns2
       endif 
       
        if (qu(ncp) < 0.000001 .or. qu(ncp-1) < 0.000001 ) then
          write(io,*)'ERROR:' 
          write(io,*)'tableread: qu(ncp-1) or qu(ncp) not 0'
          goto 1000  !---stop
        endif
       
       select case (i_two_comp)
       case (1)
        !---first  element of target: density (dns0) from element
        !---second element of target: density (dns0) compute
        !      1./N= q1/N1 + q2/N2   ,Tridyn,01  S.14 gl(21)
        dns2       =1./(1./(   qu(ncp  )*dns_molec)-qu(ncp-1)/(qu(ncp  )*dns0(ncp-1)))      
        dns0(ncp)=dns2
        write(io,'(3x,a12,5f10.5)')'dns0(first):',dns0(1:ncp)!, 1./(qu(ncp-1)/dns0(ncp-1)+qu(ncp)/dns2)
       case (2)
        !---second  element of target: density (dns0) from element
        !---first element of target: density (dns0) compute
        !      1./N= q1/N1 + q2/N2   ,Tridyn,01  S.14 gl(21)     
        dns1       =1./(1./(   qu(ncp-1)*dns_molec)-qu(ncp  )/(qu(ncp-1)*dns0(ncp  )))
        dns0(ncp-1)=dns1
        write(io,'(3x,a12,5f10.5)')'dns0(sec.) :',dns0(1:ncp)!, 1./(qu(ncp-1)/dns1+qu(ncp)/dns0(ncp))
       case (3)
        !---iterative 1. step
        !write(io,'(3x,a12,5f10.5)')'start:',dns0(ncp-1),dns0(ncp)
        dns1=1./(1./(   qu(ncp-1)*dns_molec)-qu(ncp  )/(qu(ncp-1)*dns0(ncp  )))
        dns2=1./(1./(   qu(ncp  )*dns_molec)-qu(ncp-1)/(qu(ncp  )*dns0(ncp-1)))      
        if(dns1>0)dns0(ncp-1)=0.5*(dns0(ncp-1)+dns1)
        if(dns2>0)dns0(ncp  )=0.5*(dns0(ncp  )+dns2)
        !write(io,'(3x,a12,5f10.5)')'mit1:',dns0(ncp-1),dns0(ncp)
        
        !---iterative 2. step
        dns1=1./(1./(   qu(ncp-1)*dns_molec)-qu(ncp  )/(qu(ncp-1)*dns0(ncp  )))
        dns2=1./(1./(   qu(ncp  )*dns_molec)-qu(ncp-1)/(qu(ncp  )*dns0(ncp-1)))      
        if(dns1>0)dns0(ncp-1)=0.5*(dns0(ncp-1)+dns1)
        if(dns2>0)dns0(ncp  )=0.5*(dns0(ncp  )+dns2)
        !write(io,'(3x,a12,5f10.5)')'mit2:',dns0(ncp-1),dns0(ncp)
       
        !---iterative 3. step
        dns1=1./(1./(   qu(ncp-1)*dns_molec)-qu(ncp  )/(qu(ncp-1)*dns0(ncp  )))
        dns2=1./(1./(   qu(ncp  )*dns_molec)-qu(ncp-1)/(qu(ncp  )*dns0(ncp-1)))      
        if(dns1>0)dns0(ncp-1)=0.5*(dns0(ncp-1)+dns1)
        if(dns2>0)dns0(ncp  )=0.5*(dns0(ncp  )+dns2)
        !write(io,'(3x,a12,5f10.5)')'mit3:',dns0(ncp-1),dns0(ncp)
        
        !---iterative last step
        dns0(ncp)=1./(1./(   qu(ncp  )*dns_molec)-qu(ncp-1)/(qu(ncp  )*dns0(ncp-1)))
        write(io,'(3x,a12,5f10.5)')'dns0(iter.):',dns0(1:ncp)!,1./(qu(ncp-1)/dns0(ncp-1)+qu(ncp)/dns0(ncp))
       end select  
        
       if ( dns0(ncp-1)<=0 .or. dns0(ncp)<=0) then
         write(io,*)'ERROR:  dns0 <= 0' 
         write(io,*)'  method: i_two_comp=', i_two_comp,' not works'
         goto 1000  !---stop
       endif

     endif
     write(io,'(3x,a12,5f10.5)')'dns0 used  :',dns0(1:ncp)
    else
     write(io,'(3x,a20)')'dns0 not calculated:'
     write(io,'(3x,a12,5f10.5)')'dns0 used:',dns0(1:ncp)
    endif  
    write(io,*)'---------------------------------'

    select case (int(a_num_z(ncp-1)))
    case (1,2,7,8,9,10,17,18,36,53,54,86) 
         j=1
    case default 
         j=0
    end select  
   
    select case (int(a_num_z(ncp)))
    case (1,2,7,8,9,10,17,18,36,53,54,86) 
         i=1
    case default 
         i=0
    end select  

    if (isbv == 5 .and. j==1) then
     write(io,*)'ERROR:' 
     write(io,*)'target:two-elements, surface binding model isbv=5 (solid-gas)' 
     write(io,*)'first component not solid'
     goto 1000  !---stop
    endif
    if (isbv == 5 .and. i==0) then
     write(io,*)'ERROR:' 
     write(io,*)'target:two-elements, surface binding model isbv=5 (solid-gas)'
     write(io,*)'second component not gaseous'
     goto 1000  !---stop
    endif
    if (isbv == 4 .and. j==1) then
     write(io,*)'ERROR:' 
     write(io,*)'target:two-elements, surface binding model isbv=4 (solid-solid)' 
     write(io,*)'first component not solid'
     goto 1000  !---stop
    endif
    if (isbv == 4 .and. i==1) then
     write(io,*)'ERROR:' 
     write(io,*)'target:two-elements, surface binding model isbv=4 (solid-solid)' 
     write(io,*)'second component not solid'
     goto 1000  !---stop
    endif
    if (isbv /= 4 .and. isbv /= 5) then
     write(io,*)'WARNING: target:two-elements, surface binding model not isbv= 4 or 5'
    endif
   else
    if (isbv == 4 .or. isbv == 5) then
     write(io,*)'ERROR:' 
     write(io,*)'target:not two elements, surface binding model is isbv= 4 or 5'
     goto 1000  !---stop
    endif
   endif !two components
  
   if (count(dns0(1:ncp) < 0.0) > 0) then 
     write(io,*)'ERROR:' 
     write(io,*)'target: not dns0',dns0(1:ncp)
     goto 1000  !---stop
   endif
  
  return

1000 continue
  write(io,*)'stop from subroutine read_all_tables (tableread.F90) '
  table_ok=0
end subroutine read_all_tables

!-----------------------------------------------------------------------------
subroutine table1read(tableinp,symbol, z, m, rho, arho, sbe, ed, ef, delta_hd, io) 
       implicit none
       real, parameter :: an = 0.60221367             ! [mol/cm**3] * an => [atoms/A**3]
       real, parameter :: av = 6.0221367e23           ! Avogadro number (mol-1)
       real, parameter :: kj_ev = 6.2415060e+18*1000  !  k[J] * kj_ev => [eV]
       character(len = 5),intent(in) :: symbol
       integer           ,intent(in) :: io
       real              ,intent(out):: z, m, rho, arho, sbe, ed, ef,delta_hd 
       character :: element*5,filename*1024,tableinp*1024
       character(len=72) :: title1
       integer :: j, i
!       real    :: h_f,   h_v,  h_at,  h_dis ,sbe_neu

       filename=tableinp(1:len_trim(tableinp)) // '/table1'
       !open(22,file='../tables/table1'
       open(22,file=filename)
        do i = 1, 11
          read(22,*) title1
        enddo
        !write(io,*) 'element, z, rho,  rho/rho_alt,  hilf, hilf/arho.  h_at*kj_ev/av , h_at*kj_ev/av/sbe,  h_dis'
        !do i=1,109
        !  read(22,*,END=333) element, z, m, rho, arho, sbe, ed, ef,delta_hd    !,h_f,   h_v,  h_at
        !  write(6,'(a5,f4.0,10f15.5)') element,z,m,rho,arho,sbe,ed,ef,delta_hd   !,h_at*kj_ev/av,sbe-ef
        !enddo
        !333  stop
        j = 0
        do while (j < 103)
          j=j+1
          read(22,*) element, z, m, rho, arho, sbe, ed, ef, delta_hd
!         write(io,*) element, symbol
          if(element.eq.symbol) j = 200
        enddo      
       close(22)
       if (j /= 200) then
          write(io,*) 'ERROR: ',symbol, ',symbol is missing or is not an element of table1'
          write(io,*)'stop from table1read (tableread.F90)'
          stop
       end if
       !write(io,*) ' z=',z,' m=',m,' rho=',rho,' sbe=',sbe,' ed=',ed
       if(rho.eq.0.)write(io,*)'WARNING: table1read: rho(density)=zero for symbol ',   &
                                symbol, '. Make your own choice'
       if(sbe.eq.0.)write(io,*)'WARNING: table1read: sbe=zero for symbol ',   &
                                symbol, '. Make your own choice'
       if(ed .eq.0.)write(io,*)'WARNING: table1read: ed=zero for symbol ',    &
                               symbol, '. Make your own choice'
       return
end 
!-------------------------------------------------------
subroutine table2read(tableinp,symbol, m, qu, nisot,io,nummer) 
       implicit none
       character(len = 5),intent(in) :: symbol
       integer           ,intent(in) :: io
       real   , dimension(12),intent(out) :: m, qu
       integer, dimension(12),intent(out) :: nummer
       integer               ,intent(out) :: nisot
       character :: element*5,filename*1024,tableinp*1024
       character(len=72) :: title1
       integer :: j, i, dummy1
       
       filename=tableinp(1:len_trim(tableinp)) // '/table2'
       open(22,file='../tables/table2')
       open(22,file=filename)
        do i = 1, 4
          read(22,*) title1
        enddo
        j = 1
        do 
          read(22,*,END=334) element, dummy1, nummer(j), m(j), qu(j)
          if ((element.eq.symbol) .and. (qu(j)>1e-5)) j = j+1
        enddo      
 334   close(22)
       nisot = j-1
       write(io,*)'symbol mass:', symbol, m( 1:nisot)
       write(io,*)'symbol qu  :', symbol, qu(1:nisot)
       return
end 
!-------------------------------------------------------
subroutine table3read(tableinp,symbol, ch, ck, io)
       implicit none
       character(len = 4),intent(in) :: symbol
       integer           ,intent(in) :: io
       real              ,intent(out):: ch(12)
       real              ,intent(out):: ck
       character         :: element*4,filename*1024,tableinp*1024
       character(len=72) :: title1
       integer :: i, j
       real    :: z
       ck=1
       filename=tableinp(1:len_trim(tableinp)) // '/table3'
       !---open(23,file='../tables/table3') 
       open(23,file=filename)
        do i = 1, 9
          read(23,*)  title1
          !write(io,*) title1
        enddo
        j = 0
        do while (j == 0)
          read(23,*) element, z, ch(1:12),ck
          !write(io,*) 'element, symbol:',element,z, symbol,ck
          if(element == symbol) j = 1
        enddo
       close(23)
       !write(io,*)'element, symbol:',element,symbol
       !write(io,*)'ch:',ch
       !write(io,*)'ck:',ck
       return
      end
!-------------------------------------------------------
subroutine table4read(tableinp,symbol, ch, io)
       implicit none
       character(len = 4),intent(in) :: symbol
       integer           ,intent(in) :: io
       real              ,intent(out):: ch(9)
       character         :: element*4,filename*1024,tableinp*1024
       character(len=72) :: title1
       integer :: i, j
       real    :: z

       filename=tableinp(1:len_trim(tableinp)) // '/table4'
       !---open(23,file='../tables/table4')
       open(23,file=filename)
        do i = 1, 9
          read(23,*) title1
        enddo
        j = 0
        do while (j == 0)
          read(23,*) element, z, ch(1:9)
          !write(io,*) 'element, symbol:',element,z, symbol
          if(element.eq.symbol) j = 1
        enddo
       close(23)
       !write(io,*)'element, symbol:',element,symbol
       !write(io,*)'ch:',ch
       return
end 
!-------------------------------------------------------
subroutine table_compound(tableinp,two_comp, m, rho, arho, delta_hf, io) 
  implicit none
  real, parameter :: an = 0.60221367            ! [mol/cm**3] * an => [atoms/A**3]
  real, parameter :: av = 6.0221367e23          ! Avogadro number (mol-1)
  real, parameter :: kj_ev = 6.2415060e+18*1000 ! [kJ] * kJ_ev => [eV]
  character,intent(in) :: two_comp*10
  integer  ,intent(in) :: io
  real     ,intent(out):: m        !---Molare Masse: M [g/mol]
  real     ,intent(out):: rho      !---Dichte(density): rho [g/cm^3]
  real     ,intent(out):: arho     !---density: rho [atoms/A^3]
  real     ,intent(out):: delta_hf !---heat of formation: delta_H_f [eV]
  character            :: formel*10,title1*72,filename*1024,tableinp*1024
  integer              :: j, i
  real                 :: dh_f     !---molare Bildungsenthalpie: delta_H_f [kJ/mol]

      filename=tableinp(1:len_trim(tableinp)) // '/table.compound'
!      open(22,file='../tables/table.compound')
      open(22,file=filename)
      do i = 1, 8
        read(22,*) title1
      enddo
      !do i=1,19
      !  read(22,*) formel, m, rho, dh_f, arho, delta_hf , j
      !  arho=j * rho * an / m
      !  delta_hf=dh_f*kj_ev/av 
      !  write(6,'(3x,a5,5f12.4,i5)')formel,m,rho,dh_f,arho,delta_hf,j 
      !enddo

      j = 0
      do while (j == 0)
        read(22,*,END=333) formel, m, rho, dh_f, arho, delta_hf 
        !write(io,*) formel,two_comp
        !write(6,'(a5,10f15.5)') element,m,rho,dh_f,arho,delta_hf
        !delta_hd = dh_f * kj_ev/av   !---  [mol/cm**3] => [atoms/A**3]
        !arho     = rho  * an / m     !---  [kJ]        => [eV]
        if(trim(formel) == trim(two_comp)) j = 1
      enddo      
333   close(22)
      if (j == 0) then
       write(io,*)'WARNING:', two_comp, ' is not an component of table.compound'
       !write(io,*)'table_compound: set delta_H_f=0.0'
       m=0
       arho=-1.0
       rho =-1.0
       delta_hf=0.0
      endif
  return
end
