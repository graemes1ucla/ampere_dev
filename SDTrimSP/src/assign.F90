function assign_address_2d(a,n1,n2) result (p)
   implicit none

   integer, intent(in)      :: n1, n2
   real, target, intent(in) :: a(n1,n2)
   real, pointer :: p(:,:)

   p => a
end function

function assign_address_1d(a,n1) result (p)
   implicit none

   integer, intent(in)      :: n1
   integer, target, intent(in) :: a(n1)
   integer, pointer :: p(:)

   p => a
end function
