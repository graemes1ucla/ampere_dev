! declarations included from file:  parallel_part.inc                       
   type (task_descriptor) :: aps
   real :: asigtr
   real :: br
   real :: c2r
   real :: ctr
   real :: cur
   real :: cxr
   real :: cyr
   real :: czr
   real :: deer
   real :: deeror
   real :: deers
   real :: desr
   real :: densrt
   real :: denrt
   real :: deert
   real :: dedrt
   real :: e0rc
   real :: enor
   real :: epsr
   real :: esp
   real :: esp3l
   real :: esp4l
   real :: espql
   real :: ex1r
   real :: ex2r
   real :: ex3r
   real :: ex4r
   character(len=16) :: flag
   integer :: ii
   integer :: ixrc
   integer :: ixrv
   integer :: ixir
   integer :: iprr
   integer :: kl2
   integer :: koi
   integer :: l
   integer :: label
   integer :: nrec2
   real :: phirr
   real :: pmaxv
   real :: pr
   real :: ral
   integer :: rc
   real :: rocinv
   real :: rrv
   real :: s2r
   real, dimension(1:ncpm) :: squxv
   real :: str
   real :: sxr
   real :: t
   real :: taupsir
   real :: taur
   real :: ts
   real :: v1r
   real :: vr
   real :: x2
   real :: xrmax, yrmax, zrmax
   real :: plr

   real :: sbex, sbem
   
   real :: doo
   integer :: ncollr
