====== pysrim run =======
        SRIM-2013.00
==================================================
      ION and final RECOIL ATOM Distributions     
 See  SRIM Outputs\TDATA.txt for calculation details
==================================================
====== TRIM Calc.= Xe(100 eV ) ==> None(  25 A) ==============================
------------------------------------------------------------------------------
See file :  SRIM Outputs\TDATA.txt   for details of calculation
 Ion    = Xe   Energy = 0.1  keV
============= TARGET MATERIAL ======================================
Layer  1 : None
Layer Width =     3.E+01 A   Layer # 1- Density = 6.030E22 atoms/cm3 = 2.702 g/cm3
  Layer # 1- Al = 100  Atomic Percent = 100  Mass Percent
====================================================================
 Total Ions calculated =29999.99
 Ion Average Range =   377.7E-02 A   Straggling =   241.8E-02 A
 Ion Lateral Range =   204.4E-01 A   Straggling =   205.7E-01 A
 Ion Radial  Range =   207.9E-01 A   Straggling =   233.6E-02 A
====================================================================
 Transmitted Ions =;  Backscattered Ions =18567
 (These are not included in Skewne- and Kurtosis below.)

 Range Skewne- = 000.7248  &= [-(X-Rp)^3]/[N*Straggle^3]
 Range Kurtosis = 003.2781  &= [-(X-Rp)^4]/[N*Straggle^4]
 Statistical definitions above are those used in VLSI implant modelling.
====================================================================
=================================================================
  Table Distribution Units are >>>  (Atoms/cm3) / (Atoms/cm2)  <<<  
=================================================================
   DEPTH         Xe       Recoil     
   (Ang.)       Ions     Distribution
-----------  ----------  ------------
260000.E-06  3.5733E+06  1.1008E+08
510000.E-06  4.0133E+06  2.4107E+07
760000.E-06  4.8267E+06  2.4573E+07
101000.E-05  5.2267E+06  2.3507E+07
126000.E-05  5.9867E+06  2.2973E+07
151000.E-05  5.8800E+06  2.0760E+07
176000.E-05  5.6267E+06  1.8467E+07
201000.E-05  5.9200E+06  1.7787E+07
226000.E-05  5.7333E+06  1.7733E+07
251000.E-05  6.3067E+06  2.0240E+07
276000.E-05  6.0667E+06  1.5947E+07
301000.E-05  6.2267E+06  1.4787E+07
326000.E-05  6.1067E+06  1.4107E+07
351000.E-05  6.0133E+06  1.3840E+07
376000.E-05  6.1867E+06  1.3040E+07
401000.E-05  5.7867E+06  1.2093E+07
426000.E-05  5.4400E+06  1.0520E+07
451000.E-05  5.0667E+06  1.0120E+07
476000.E-05  4.6667E+06  9.8533E+06
501000.E-05  4.5200E+06  9.7200E+06
526000.E-05  4.5600E+06  5.8533E+06
551000.E-05  3.5733E+06  4.0133E+06
576000.E-05  3.8400E+06  3.7867E+06
601000.E-05  3.4133E+06  3.5333E+06
626000.E-05  3.1067E+06  3.2533E+06
651000.E-05  2.9467E+06  3.2267E+06
676000.E-05  2.7067E+06  2.8400E+06
701000.E-05  2.6800E+06  2.9200E+06
726000.E-05  2.3733E+06  2.9467E+06
751000.E-05  1.9733E+06  2.7600E+06
776000.E-05  1.7600E+06  2.9200E+06
801000.E-05  1.6133E+06  3.0400E+06
826000.E-05  1.0933E+06  3.0667E+06
851000.E-05  1.4133E+06  2.8533E+06
876000.E-05  9.2000E+05  2.5733E+06
901000.E-05  7.7333E+05  2.3600E+06
926000.E-05  7.8667E+05  2.2800E+06
951000.E-05  5.7333E+05  2.7067E+06
976000.E-05  6.4000E+05  2.5600E+06
100100.E-04  3.6000E+05  2.4800E+06
102600.E-04  3.7333E+05  2.2267E+06
105100.E-04  2.4000E+05  2.2800E+06
107600.E-04  2.5333E+05  1.8267E+06
110100.E-04  2.8000E+05  1.7467E+06
112600.E-04  2.1333E+05  1.5733E+06
115100.E-04  1.2000E+05  1.6267E+06
117600.E-04  1.6000E+05  1.7600E+06
120100.E-04  9.3333E+04  1.5733E+06
122600.E-04  1.4667E+05  1.4400E+06
125100.E-04  2.6667E+04  1.2533E+06
127600.E-04  5.3333E+04  1.4000E+06
130100.E-04  2.6667E+04  1.1333E+06
132600.E-04  4.0000E+04  9.2000E+05
135100.E-04  2.6667E+04  1.0400E+06
137600.E-04  0.0000E+00  8.9333E+05
140100.E-04  4.0000E+04  7.6000E+05
142600.E-04  1.3333E+04  6.4000E+05
145100.E-04  1.3333E+04  5.4667E+05
147600.E-04  0.0000E+00  4.9333E+05
150100.E-04  0.0000E+00  3.3333E+05
152600.E-04  2.6667E+04  4.1333E+05
155100.E-04  0.0000E+00  3.6000E+05
157600.E-04  0.0000E+00  3.2000E+05
160100.E-04  0.0000E+00  1.7333E+05
162600.E-04  0.0000E+00  1.6000E+05
165100.E-04  0.0000E+00  1.6000E+05
167600.E-04  0.0000E+00  1.2000E+05
170100.E-04  0.0000E+00  9.3333E+04
172600.E-04  0.0000E+00  4.0000E+04
175100.E-04  0.0000E+00  6.6667E+04
177600.E-04  0.0000E+00  4.0000E+04
180100.E-04  0.0000E+00  9.3333E+04
182600.E-04  0.0000E+00  2.6667E+04
185100.E-04  0.0000E+00  4.0000E+04
187600.E-04  0.0000E+00  8.0000E+04
190100.E-04  0.0000E+00  1.3333E+04
192600.E-04  0.0000E+00  0.0000E+00
195100.E-04  0.0000E+00  0.0000E+00
197600.E-04  0.0000E+00  1.3333E+04
200100.E-04  0.0000E+00  1.3333E+04
202600.E-04  0.0000E+00  4.0000E+04
205100.E-04  0.0000E+00  0.0000E+00
207600.E-04  0.0000E+00  0.0000E+00
210100.E-04  0.0000E+00  0.0000E+00
212600.E-04  0.0000E+00  0.0000E+00
215100.E-04  0.0000E+00  0.0000E+00
217600.E-04  0.0000E+00  0.0000E+00
220100.E-04  0.0000E+00  0.0000E+00
222600.E-04  0.0000E+00  0.0000E+00
225100.E-04  0.0000E+00  0.0000E+00
227600.E-04  0.0000E+00  0.0000E+00
230100.E-04  0.0000E+00  0.0000E+00
232600.E-04  0.0000E+00  0.0000E+00
235100.E-04  0.0000E+00  0.0000E+00
237600.E-04  0.0000E+00  0.0000E+00
240100.E-04  0.0000E+00  0.0000E+00
242600.E-04  0.0000E+00  0.0000E+00
245100.E-04  0.0000E+00  0.0000E+00
247600.E-04  0.0000E+00  0.0000E+00
250100.E-04  0.0000E+00  0.0000E+00
