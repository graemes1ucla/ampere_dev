====== pysrim run =======
        SRIM-2013.00
=======================================
      Data  of  TRIM  Calculation      
=======================================
====== TRIM Calc.= Xe(150 eV ) ==> None(  25 A) ==============================
------------------------------------------------------------------------------
Ion = Xe (54)   Ion Mass= 131.2930
Energy  = 1500000.E-07 keV
Ion Angle to Surface = 89 degrees
============= TARGET MATERIAL ======================================
  Layer # 1 - None
  Layer # 1 - Bottom Depth=     3.E+01 A
  Layer # 1- Density = 6.030E22 atoms/cm3 = 2.702 g/cm3
  Layer # 1- Al = 100  Atomic Percent = 100  Mass Percent
====================================================================
Target energies for target atom = Al
    Displacement = 25 eV, Binding = 3 eV, Surface = 3.36 eV
====================================================================
Depth Range of Tabulated Data=  000000.E+00  -  250000.E-04  Angstroms
====================================================================
Total Ions calculated =  029999
Average Range         =   45229.E-04 Angstroms
Average Straggling    =   29732.E-04 Angstroms
Average Vacancy/Ion   =   21469.E-04
====================================================================
Total Backscattered Ions=  19068 
Total Transmitted Ions  =  0 
====================================================================
See files SRIM Outputs\*.txt for tables :
   TDATA.txt    -- Details of the calculation.
   RANGE.txt    -- Final distribution of Ions/Recoils
   VACANCY.txt  -- Distribution of Vacancies
   IONIZ.txt    -- Distribution of Ionization
   PHONON.txt   -- Distribution of Phonons
   E2RECOIL.txt -- Energy transferred to recoils
   NOVAC.txt    -- Replacement collisions
   LATERAL.txt  -- Lateral Spread of Ions
