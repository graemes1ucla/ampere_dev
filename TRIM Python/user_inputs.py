from srim import Target, Layer
import numpy as np

NUM_IONS = 30000

class UserInputs:
    def __init__(self):
        # Define calculations in code
        self.ions = [
                    {'identifier': 'Xe', 'energy': 1}, # eV
                ]

        self.layer = Layer({
            'Al': {
                'stoich': 1,
                'E_d': 25.0, # Displacement Energy
                'lattice': 3.0,
                'surface': 3.36
            },
        }, density=2.702, width=25.0)

        self.target = Target([self.layer])

        # number of ions to simulate for each ion
        self.num_ions = NUM_IONS

        # See TRIMSettings and TRIM for full options
        # you can do any setting that you could do in TRIM
        self.trim_settings = {
            'calculation': 3, # sputter
            'ranges': True,
            'sputtered': True,
            'collisions': False,
            'angle_ions': 30,
        }

        # directory to srim executable (must be SRIM 2013)
        self.srim_exec_dir = r'H:\My Drive\AMPERE\SRIM'
        self.output_dir = r'H:\My Drive\AMPERE\TRIM Python\Outputs'
        self.ion_friendly_name = str(self.ions[0]["identifier"])
        self.layer_friendly_name = str(self.layer.chemical_formula.split()[0])
        self.combined_trim_file = "_".join([self.layer_friendly_name, self.ion_friendly_name, str(NUM_IONS) + ".txt"])

        inc = 50 # energy increment between energy runs (in eV)
        Emin = 100 # minimum ion energy (eV) for all runs in this simulation
        Emax = 150 # maximum ion energy (eV) for all runs in this simulation
        #list of energies to run (keV). I keep them in reverse order, as TRIM wants max energy first.
        Elist = np.arange(Emin,Emax+inc,inc) # includes Emax
        self.energy_range = Elist
        self.angle_range = [0,89]
