import os
import shutil
from collections import Counter
from random import randint
from pathlib import Path
import numpy as np
import re
import time

from helper_functions.main_helper_functions.helper_functions import run, parse_directory

from srim import Ion, Element
from srim.output import Results
from user_inputs import UserInputs 

user_inputs = UserInputs()
DATA_DIRECTORY = user_inputs.output_dir 
try:
    shutil.rmtree(DATA_DIRECTORY)
except OSError as e:
    print ("Error: %s - %s. Continuing..." % (e.filename, e.strerror))
    pass

trim_tic = time.perf_counter()

# Run SRIM calculations for each ion
for ion in user_inputs.ions:
    for energy in user_inputs.energy_range:
        for angle in user_inputs.angle_range:
            ion['energy'] = energy
            print(ion['energy'])
            user_inputs.trim_settings['angle_ions'] = angle
            output_path = os.path.join(DATA_DIRECTORY, ion['identifier'])
            run(Ion(**ion), user_inputs.target, user_inputs.num_ions, output_path, user_inputs.srim_exec_dir, user_inputs.trim_settings, energy)

trim_toc = time.perf_counter()
trim_time = time.strftime('%H:%M:%S', time.gmtime(trim_toc-trim_tic))
print("Total TRIM time: ", trim_time)
pp_tic = time.perf_counter()

src_fpath = os.path.join(Path(user_inputs.output_dir), "output_file.txt")

with open(src_fpath,'wb') as wfd:
    for dir in os.listdir(DATA_DIRECTORY):
        abs_dir_path = os.path.join(DATA_DIRECTORY, dir, 'SPUTTER.txt')
        if os.path.exists(abs_dir_path):
            with open(abs_dir_path,'rb') as fd:
                shutil.copyfileobj(fd, wfd)
                wfd.write(b"\n")
                print('parsed', os.path.join(abs_dir_path))


## Clean up SPUTTER.txt file. Here are some TRIM bugs.
# 1. If particle count exceeds 9999, TRIM will not put enough spaces in for it in the output file,
#    so the 10,000th collision entry will be "S10000" instead of "S 10000".
#    If particle count exceeds 99999, TRIM will begin truncating the most significant digit.
#    For example, the 100000 particle will be recorded as "S00000" instead of "S 100000"...
# 2. Let's say you want to simulate Xe ions hitting a surface at 25, 50, and 75 eV. TRIM will
#    simulate these energies correctly. BUT, it rounds the numbers in the output text file to
#    30, 50, and 80 eV. THIS CODE DOES NOT HANDLE THAT PROBLEM. You're welcome to add that in,
#    but I'm currently just working around it (either running "round" energies or changing it manually later).
# 3. SRIM does not save the initial ion angle in "SPUTTER.txt". This code adds it back in at
#    the line starting with "Ion Data:".
# 4. SRIM does not save information on the surface binding energies of the target to SPUTTER.txt. This
#    code does not fix that.

dest_fpath = os.path.join(DATA_DIRECTORY, user_inputs.combined_trim_file) # name of output file desired

os.makedirs(os.path.dirname(DATA_DIRECTORY), exist_ok=True) #make sure destination directory exists, else create
fout = open(dest_fpath, "w+") # open the output file stream
thetaIndex = 0
thetaList = user_inputs.angle_range

# add back the missing TRIM digits
with open(src_fpath) as fp:
    lastIonCount = 0

    for line in fp: #gets next line, so be careful
        lineArr = line.split()

        # if line is empty, skip 2 lines, in accordance with SPUTTER.txt file format
        if not lineArr:
            for _ in range(2):
                try:
                    next(fp)
                except:
                    print("End of file concatenation...")
                    break

        # add back the initial ion angle to the output file
        elif lineArr[0] == "Ion" and lineArr[1] == "Data:":
            lineArr.extend([",", "Angle", "(deg)", "\n"])
            line = " ".join(lineArr)
            fout.write(line)

            line = next(fp)
            lineArr = line.split()
            if thetaIndex == len(thetaList):
                thetaIndex = 0 #if we've moved onto a new energy, restart count of which theta we're on
            lineArr.extend( [ str(thetaList[thetaIndex]), "\n" ] )
            thetaIndex = thetaIndex + 1
            line = " ".join(lineArr)

        # if currently on a line with sputterant data
        elif lineArr[0][0] == "S" and lineArr[0] != "SPUTTERING":
            # if the ion number got connected to the "S", split it up again (>9999)
            if lineArr[0] != "S":
                tempstrArr = [lineArr[0][0], lineArr[0][1:]] +   lineArr[1:] + ["\n"]
                tempIon = int(lineArr[0][1:])
                # if the most significant digit got cut off (>99999)
                if tempIon < lastIonCount:
                    while tempIon < lastIonCount:
                        tempIon += int(1e5)
                    tempstrArr[1] = str(tempIon)
                lineArr = tempstrArr
                line = " ".join(lineArr)
            #update last ion number
            lastIonCount = int(lineArr[1])
        fout.write(line)
    fout.close()
    
pp_toc = time.perf_counter()
pp_time = time.strftime('%H:%M:%S', time.gmtime(pp_toc-pp_tic))
print("Total post-processing time: ", pp_time)
tot_time = time.strftime('%H:%M:%S', time.gmtime(pp_toc-trim_tic))
print("Total simulation time: ", tot_time)
