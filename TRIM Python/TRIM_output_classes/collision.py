# class Collision: For every target simulation from TRIM, there could be hundreds or
#   thousands of collisions. Each collision contains the ion identifier
#   (1st, 10th, or 3000th ion of the simulation), the atomic number of the ion,
#   the energy of the ion, and the number of atoms that ion sputtered. The number
#   of atoms sputtered will determine the length of Zatom,Eatom, and alpha/beta/gamma-Atom,
#   which are arrays of the atomic numbers, atomic energies, and emission angles
#   of all sputtered atoms in a single collision. Each index corresponds to a single sputterant.
#   Directions of final trajectory of sputtered atom:
#   1. alpha: angle of atom with x-axis, which is the target depth axis. This is the
#             angle the atom's trajectory makes with the surface normal. All angles in degrees
#   2. beta: angle of atom with y-axis, transverse axis for target.
#   3. gamma: angle of atom with z-axis, transverse axis for target.
class Collision:
    def __init__(self, id = -1, Zion = 0, Eion = -1, ThetaIon = 0, Natom = 0, Zatom = None, \
                       Eatom = None, alphaAtom = None, betaAtom = None, gammaAtom = None ):
        self._ID = id # ion identifier
        self._Zion = Zion # atomic number of ion
        self._Eion = Eion # energy of ion (eV)
        self._ThetaIon = ThetaIon # incidence angle of the ion (degrees)
        self._Natom = Natom # quantity of atoms sputtered from the target
        self._Zatom = [] if Zatom==None else Zatom # list of atomic numbers of each sputtered atom
        self._Eatom = [] if Eatom==None else Eatom # list of energies (eV) of each sputtered atom
        self._alphaAtom = [] if alphaAtom==None else alphaAtom # list alpha angles(deg) for each sputtered atom
        self._betaAtom = [] if betaAtom==None else betaAtom # list beta angles(deg) for each sputtered atom
        self._gammaAtom = [] if gammaAtom==None else gammaAtom # list gamma angles(deg) for each sputtered atom

    # Collision does not have a toFile() function like other classes because, there are so many
    #    collision objects, you'd spend a lot of clock cycles just opening and closing the file.
    #    This is faster.
    def toString(self):
        tostr = ""
        for i in range(self._Natom):
            # spacing sseems arbitrary, cuz it is. It's to just get the columns more or less aligned with the header.
            tostr += str(self._Eion) + " ,         "+ str(self._ThetaIon) + " ,             " + \
                     str(self._ID) + " ,    " + str(self._Zion) + " ,     "+ str(self._Zatom[i]) + " ,   " + \
                     str(self._Eatom[i]) + " ,     " + str(self._alphaAtom[i]) + \
                     " ,     " + str(self._betaAtom[i]) + " ,     " + str(self._gammaAtom[i]) + "\n"
        return tostr