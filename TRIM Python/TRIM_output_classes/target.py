from TRIM_output_classes.collision import Collision
from helper_functions.main_helper_functions.helper_functions import getZ
from collections import defaultdict


# class Target: A target object contains the filepath to the relevant file, the actual
#    filename, the name of the target (as the user desires it to appear), the list
#    of elements composing the target surface, and a dictionary of Collision() objects.
class Target:

    def __init__(self,filepath = "", filename = "", targName = "", elementList = None, collideDict = None ):
        self._filepath = filepath # filepath to the TRIM output file necessary to analyze this target
        self._filename = filename # filename of the TRIM output file
        self._targName = targName # name of the target ("SS316L, Cu, Al2O3, etc")
        self._elementList = [] if elementList==None else elementList # list of element name strings, eg ["Mg","O","Al"]...
        self._dictCollisions = {} if collideDict==None else collideDict # dictionary of lists of Collision() objects

    # initCollisionDict() initializes the Target() object's collision dictionary.
    #    By knowing the Ion() for the simulation, the dictionary can be preallocated
    #    with the number of ions run for each simulation, where the keys of the dict are
    #    tuples of ion energy and angle, and the values are Collision objects.
    def initCollisionDict(self,ionObject):
        self._dictCollisions = defaultdict(list)

        for energy in ionObject._energyList:
            for theta in ionObject._thetaList:
                for i in range(ionObject._ionsPerSim):
                    collision = Collision(id = i+1, Zion = getZ(ionObject._ionName), Eion = energy, ThetaIon = theta )
                    self._dictCollisions[(energy, theta)].append(collision)
    def toFile(self, filepath):
        with open(filepath, "a+") as fout: #open in append mode
            fout.write("\n/*** Target Data ***/\n")
            fout.write("Target_Name , " + self._targName + "\n")
            fout.write("Composition , " + str(self._elementList) + "\n")
            fout.write("Filepath , " + self._filepath + "\n")
            fout.write("Filename , " + self._filename + "\n")
            fout.write("E_ion(eV) , Theta_ion(deg) , Ion_ID , Z_ion ,  Z_atom , E_atom(eV) , Alpha_atom(deg),        Beta_atom(deg),          Gamma_atom(deg)" + "\n")

            for (e,theta), collisionList in reversed( sorted( self._dictCollisions.items() ) ):
                for collision in collisionList:
                    fout.write(collision.toString())