    
# class Ion: contains the name of the ion (Abbreviated), the number of ions emitted
#    per TRIM simulation (must be constant), the list of energies at which the ion
#    sputters the target (in eV), and the list of angles at which it impacts (in degrees).
class Ion:
    def __init__( self, ionName = "", ionsPerSim = 0, energyList = None, thetaList = None ):
        self._ionName = ionName # ion abbreviation, as it appears in getZ() function!
        self._ionsPerSim = ionsPerSim # how many ions were run in each TRIM simulation. Must be constant
        self._energyList = [] if energyList==None else energyList # list of ion energies simulated, eg [100,200,500,1000] eV
        self._thetaList = [] if thetaList==None else thetaList # list of ion angles simulated, eg [0, 10, 50, 85] deg

    def toFile(self, filepath):
        with open(filepath, "a+") as fout: #open in append mode
            fout.write("/*** Ion Data ***/\n")
            fout.write("Ion_Name , " + self._ionName   + "\n")
            fout.write("Ions_per_Simulation , "  + str(self._ionsPerSim) + "\n")
            fout.write("Energy_List(eV) , "  + str( list( reversed(self._energyList) ) ) + "\n")
            fout.write("Angle_List(deg) , "  + str( list( reversed(self._thetaList) ) ) + "\n")

