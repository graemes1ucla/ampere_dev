Within SRIM SETUP in the SRIM package, right click and run "_SRIM-Setup (Right-Click)" as admin. Follow the steps. 

I've attached a zip of everything you should need to run Packages 1 and 2. There is already simulation data saved in the Outputs folder, so you should just be able to run the post_process.py file after making the directory changes below. 
Move the srim folder contained in the zip to the following file path (enter your specific data in the bolded sections): "C:\Users\Pablo?\AppData\Local\Programs\Python\Python39?\Lib\site-packages"
This contains the source code for pysrim that I modified to work with our code. 
Set the SRIM_EXECUTABLE_DIRECTORY and DATA_DIRECTORY in the user_inputs file to their correct locations. 
Workflow:
Alter the parameters in the user_inputs file and then run py main.py.
If that runs and saves the data successfully, then run post_process.py.