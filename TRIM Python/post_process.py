# -*- coding: utf-8 -*-
from matplotlib.colors import Normalize
import numpy as np
from numpy.core.fromnumeric import size
from user_inputs import UserInputs 
from helper_functions.post_helper_functions.sputter_study import SputterStudy 

from TRIM_output_classes.target import Target
from TRIM_output_classes.ion import Ion

################################################################################
### Main. MONOLAYER COLLISION RUNS
################################################################################
TargObjList = []

user_inputs = UserInputs()
ionName = user_inputs.ion_friendly_name # abbreviation of your ion, must match what's in getZ()
ionsPerSim = user_inputs.num_ions # how many ions were run at each simulation. must be a constant for all sims
energyList = np.array(user_inputs.energy_range).tolist()
#list of initial ion angles in your sim. Put in increasing order
thetaList = np.array(user_inputs.angle_range).tolist()

####################################################
#specify the output folder to read files from
trimOutputPath = user_inputs.output_dir + "\\"
#specify file name
trimFileName = user_inputs.combined_trim_file #input filename (modified SPUTTER.txt TRIM file)
targName = user_inputs.layer_friendly_name #target name as you want it to appear in files/plots
targElements = [user_inputs.layer_friendly_name] #elements within your target
TargObjList.append( Target(trimOutputPath,trimFileName,targName,targElements) )
#####################################################

# No need to touch this unless you want to set isDisplayed to True for the plots
ionObject = Ion(ionName,ionsPerSim,energyList,thetaList)
sputterStudyObj = SputterStudy(ionObject,TargObjList, user_inputs)
sputterStudyObj.readSputterFiles()

# Plot Total Sputtering Yield as a function of Energy for each Surface at each ion angle
# sputterStudyObj.pltYvsE(True)
# sputterStudyObj.pltYvsTheta(True)
# sputterStudyObj.pltYcompVsE(True, 0)
# sputterStudyObj.pltPdfEnergy(True)
# sputterStudyObj.pltPdfSingleAngle(True)
# sputterStudyObj.pltPdfAngle3D(True)
sputterStudyObj.pltPdfAngle2D(True)
# sputterStudyObj.exportSputter("exportFile.txt")
