from srim import TRIM, Target, Layer, Ion, Element
from itertools import count
from pathlib import Path
import os
import numpy as np
import shutil
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d

def run(ion, target, num_ions, output_path, srim_exec_dir, trim_settings, energy):
        # Initialize a TRIM calculation with given target and ion for 25 ions, quick calculation
    trim = TRIM(target, ion, number_ions=num_ions, **trim_settings)
    trim.run(srim_exec_dir)
    save_directory = os.path.join(output_path + '_' + str(energy) + 'eV_' + str(trim_settings['angle_ions']).zfill(3) + 'deg')
    print('calculation saved to:', save_directory)
    if os.path.exists(save_directory) and os.path.isdir(save_directory):
        shutil.rmtree(save_directory)
    os.makedirs(save_directory, exist_ok=False)
    TRIM.copy_output_files(srim_exec_dir, save_directory)

 # Helper functions
def parse_directory(dir):
    """ Parse directory with expected structure <dir>/<ion_symbol>/<int>/COLLISON.txt
    
    This functions will collect the position <x, y, z> of every ion and recoil collision.
    
    It will write to a numpy array the parsing. This file will be about 10% of the original
    """
    if not os.path.exists(os.path.join(dir, 'collision.dat.npy')):
        positions = []
        with open(os.path.join(dir, 'COLLISON.txt'), 'rb') as f:
            for line in f.readlines():
                line = line.decode('latin-1')
                if line.endswith('Start of New Cascade  ³\r\n'):
                    tokens = line.split(chr(179))[1:-1]
                    positions.append([float(tokens[2]), float(tokens[3]), float(tokens[4])])
                elif line.startswith('Û 0'):
                    tokens = line.split()[1:-1]
                    positions.append([float(tokens[3]), float(tokens[4]), float(tokens[5])])
        np.save(os.path.join(dir, 'collision.dat'), np.array(positions))


# getZ() is just a dictionary of atoms to their atomic numbers (sometimes handy).
#   add to it as needed
def getZ(atomName):
    dict = { "B":5, "N":7, "O":8, "Mg":12, "Al":13, "Si":14, "Ar": 18, "Cr":24, "Mn":25, "Fe":26, "Ni":28, "Mo":42,"Cd":48, "Xe":54, "Au":79}
    return dict[atomName]
