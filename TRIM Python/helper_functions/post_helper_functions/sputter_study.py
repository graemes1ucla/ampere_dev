# -*- coding: utf-8 -*-
import math
import inspect
import sys
import os
from matplotlib.colors import Normalize
import time
import numpy as np
from numpy.core.function_base import linspace
import scipy as sp
import pandas as pd
from collections import defaultdict
import matplotlib.pyplot as plt
from datetime import datetime
import itertools
from numpy.core.fromnumeric import size
from scipy.stats.kde import gaussian_kde
from helper_functions.post_helper_functions.rotanimate import rotanimate 
from helper_functions.main_helper_functions.helper_functions import getZ

################################################################################
### Classes ###
################################################################################

# class SputterStudy defines the umbrella object for this module. Specifically,
#    it consists of 1 Ion() object and a dictionary of Target() objects, if simulating
#    ion bombardment of 1+ different materials).
class SputterStudy:
    def __init__( self, ionObject, TargObjList, user_inputs ):
        self._ionObject = ionObject
        self._dictTarget = self.setTargDict(TargObjList) #initialize dictionary of targets
        self.user_inputs = user_inputs
        self.line_index = 0

    # setTargDict() initializes the dictionary of Target objects within the
    #   SputterStudy(). The keys are the target name. Each Target() object also contains
    #   a dictionary of collisions, which is also initialized here.
    def setTargDict( self, TargObjList ):
        self._dictTarget = defaultdict(list)
        for target in TargObjList:
            target.initCollisionDict(self._ionObject)
            self._dictTarget[target._targName] = target
        return self._dictTarget

    def exportSputter(self, dest_fpath):
        open(dest_fpath, 'w').close() # clear file
        self._ionObject.toFile(dest_fpath)
        for tname, target in self._dictTarget.items():
            target.toFile(dest_fpath)

    # readSputterFiles() reads and unpacks a SPUTTER.txt file from TRIM, which
    #    has been renamed depending on the simulation.
    def readSputterFiles(self):
        # for every target object t within the dictionary of targets in Simulation()
        for tName, T in self._dictTarget.items():
            currentEnergy = 0 # the energy of the ion from the current simulation in the file
            currentAngle = 0 # the initial ion trajectory angle from the current sim in the file
            countSims = -1 # a count of the number of simulations run (ion energy/theta combos) in the file
            # open the file for the specific ion-target simulation
            with open( T._filepath + T._filename ) as fp:
                for line in fp: #gets next line
                    self.line_index += 1
                    lineArr = line.split()
                    # if the following string is read, signifies a new ion energy
                    if line == "============================== SRIM-2013.00 ==============================\n":
                        is_first_sim = 23
                    if line == "------------------------------------------------------------------------------\n":
                        is_first_sim = 21
                    if is_first_sim:    
                        countSims += 1 # inc the qty of simulations we've read before now
                        for _ in range(is_first_sim):
                            line = next(fp) # skip unecessary lines
                            self.line_index += 1
                        lineArr = line.split()
                        # get the energy of the ion from the current line in the file,
                        #    convert to eV (note this value may have been rounded by TRIM, bc it's a garbage program :)
                        currentEnergy = float( lineArr[2] )*1000 #*1000--<<< Add this back in when source srim files are fixed
                        currentAngle = float( lineArr[3] ) # get angle, in degrees
                        for _ in range(10):
                            next(fp) # skip unecessary lines, be at a good place after
                            self.line_index += 1
                        is_first_sim = 0

                    # if the line is empty, you've finished that energy, skip 2 lines
                    elif not lineArr:
                        print(self.line_index)
                        pass

                    # if line says "Resuming", skip it to get to where you need to be
                    elif lineArr[1] == "Resuming":
                        next(fp)
                        self.line_index =+ 1

                    # if the first char of the line is "S", then you have work to do
                    elif lineArr[0] == "S":
                        # get the ion ID from the file (was it the 2nd ion simulated? the 365th? etc)
                        ionIndex = (int(lineArr[1]) - 1) #+ countSims * self._ionObject._ionsPerSim # -1 b/c python is 0-indexed
                        #   increment number of atoms sputtered by this ion
                        T._dictCollisions[(currentEnergy, currentAngle)][ionIndex]._Natom += 1
                        #   append sputtered atom atomic number (Zatom)
                        T._dictCollisions[(currentEnergy, currentAngle)][ionIndex]._Zatom.append( int( lineArr[2] ) )
                        #   append sputtered atom energy (eV)
                        T._dictCollisions[(currentEnergy, currentAngle)][ionIndex]._Eatom.append( float( lineArr[3] ) )
                        #   append sputtering angles wrt surface normal (degrees)
                        T._dictCollisions[(currentEnergy, currentAngle)][ionIndex]._alphaAtom.append( 180/math.pi*( math.pi - math.acos(float( lineArr[7] )) ) )
                        T._dictCollisions[(currentEnergy, currentAngle)][ionIndex]._betaAtom.append( 180/math.pi*( math.acos(float( lineArr[8] )) ) )
                        T._dictCollisions[(currentEnergy, currentAngle)][ionIndex]._gammaAtom.append( 180/math.pi*( math.acos(float( lineArr[9] )) ) )


    # pltYvsE() outputs a single plot of sputtering yield as a function of ion energy
    #    for EACH Target() object in the SputterStudy(). It plots a sputtering line
    #    for each ion angle. The only input is boolean isDisplayed, for whether or not
    #    you want the plot to pop up.
    def pltYvsE(self, isDisplayed):
        percError = 0.3 # error of sputtering yield from TRIM (see manual)

        # for every target object T in the Simulation() object's target dictionary
        for tname, T in self._dictTarget.items():
            # initialize a list of ion energies, angles, sputtering yields
            e_list = []
            Y_list = []
            fig = plt.figure()
            ax = fig.add_subplot(111)

            for theta in self._ionObject._thetaList:
                # for every ion energy/angle in this Target() object's collision dictionary
                for (e,th), collisions in T._dictCollisions.items():
                    if th == theta:
                        e_list.append(e) #append the ion energy
                        # calculate the sputtering yield on this target for this ion energy e
                        Y_list.append( sum(collision._Natom for collision in collisions)/len(collisions) )

                err = np.array(Y_list)*percError #calc error bars
                ax.errorbar(e_list,Y_list,yerr=err, fmt='-o',markersize=3,capsize=3,label= r"$\theta_i$ = " + str(theta) + "$\degree$") #plot
                e_list.clear()
                Y_list.clear()

            plt.legend(loc="upper left")
            plt.xlabel("Ion Energy (eV)")
            # plt.xscale('log')
            plt.ylabel("Sputter Yield (atoms/ion)")
            plt.title("Sputtering of " + tname + " with " + self._ionObject._ionName + "$^+$ Ions")
            # save image, comment out if desired
            now = datetime.now()
            plotName = r"\\" + self._ionObject._ionName + r"_" + tname + "_" + now.strftime("%m-%d-%Y")+ r"_YvsE.png"
            # plt.savefig(T._filepath+ r"\Figures" + plotName,bbox_inches='tight', dpi=300)
            if isDisplayed:
                pass
                plt.show(block=True)
            plt.close()



    # pltYvsTheta() outputs a single plot of sputtering yield as a function of ion angle
    #    for EACH Target() object in the SputterStudy(). It plots a sputtering line
    #    for each energy. The only input is boolean isDisplayed, for whether or not
    #    you want the plot to pop up.
    def pltYvsTheta(self, isDisplayed):
        percError = 0.3 # error of sputtering yield from TRIM (see manual)

        # for every target object T in the Simulation() object's target dictionary
        for tname, T in self._dictTarget.items():
            # initialize a list of ion energies, angles, sputtering yields
            theta_list = []
            Y_list = []
            fig = plt.figure()
            ax = fig.add_subplot(111)

            for energy in self._ionObject._energyList:
                # for every ion energy/angle in this Target() object's collision dictionary
                for (e,th), collisions in T._dictCollisions.items():
                    if e == energy:
                        theta_list.append(th) #append the ion angle
                        # calculate the sputtering yield on this target for this ion energy e
                        Y_list.append( sum(collision._Natom for collision in collisions)/len(collisions) )

                err = np.array(Y_list)*percError #calc error bars
                ax.errorbar(theta_list,Y_list,yerr=err, fmt='-o',markersize=3,capsize=3,label= r"$E_i$ = " + str(energy/1000) + " keV") #plot
                theta_list.clear()
                Y_list.clear()

            plt.legend(loc="upper left")
            plt.xlabel("Ion Angle (deg)")
            # plt.xscale('log')
            plt.ylabel("Sputter Yield (atoms/ion)")
            plt.title("Sputtering of " + tname + " with " + self._ionObject._ionName + "$^+$ Ions")
            # save image, comment out if desired
            now = datetime.now()
            plotName = r"\\" + self._ionObject._ionName + r"_" + tname + "_" + now.strftime("%m-%d-%Y")+ r"_YvsTheta.png"
            # plt.savefig(T._filepath+ r"\Figures" + plotName,bbox_inches='tight', dpi=300)
            if isDisplayed:
                pass
                plt.show(block=True)
            plt.close()


    # pltYcompVsE() plots the individual sputtering yields of a Target() object's
    #    component atoms on a single plot. A plot is generated for each target
    #    in the Simulation() object's target dictionary. Boolean isDisplayed
    #    displays the plot image onscreen if true. theta_in is the input for which ion angle
    #    is desired.
    def pltYcompVsE(self, isDisplayed, theta_in):
        # for every target object T in the Simulation object's target dictionary
        for Tname, T in self._dictTarget.items():
            # initialize lists of ion energy, sputtering yield, and atomic number
            e_list = []
            Y_list = []
            Z_list = []
            # for every element name in the Target() object's list of elements
            for name in T._elementList:
                Y_list.append([]) # initialize a list of sputtering yields
                Z_list.append(getZ(name)) # add the atomic number of that element

            percError = .3 # error of sputtering yield from TRIM (see manual)

            # for every ion energy in the Target() object's collision dictionary
            for (e,th), collisions in T._dictCollisions.items():
                if th==theta_in:
                    e_list.append(e) #append the ion energy
                    # for every target element component
                    for i,Z in enumerate(Z_list):
                        # calculate the sputtering yield for this target element Z at this ion energy e
                        Y_list[i].append( sum(collision._Zatom.count(Z_list[i]) for collision in collisions)/len(collisions) )

            err_list = np.array(Y_list)*percError # calc % error bars

            # for every target element whose yields we've computed
            for i,y in enumerate(Y_list):
                # plot the yield, with errorbars
                plt.errorbar(e_list,y,yerr=err_list[i], fmt='-o',markersize=3,capsize=3,label=T._elementList[i])

            # compute the total yield, then plot it for comparison
            Ytotal = sum(np.array(Y_list),0)
            plt.errorbar(e_list,Ytotal,yerr=percError*(Ytotal), fmt='-o',markersize=3,capsize=3,label="Total")
            plt.legend(loc="upper left")
            plt.xlabel("Ion Energy (eV)")
            plt.ylabel("Sputtering Yield (atoms/ion)")
            plt.title("Sputter Yield of {} with {}{}, {}={}{}".format(Tname,self._ionObject._ionName,"$^+$ Ions",r'$\theta_i$',theta_in, r'$\degree$'))
            # save figure if desired
            now = datetime.now()
            plotFileName = r"\\" + self._ionObject._ionName + r"_" + Tname + r"_" + now.strftime("%m-%d-%Y") + r"_YcompVsE" + "_Th" + str(theta_in) + r".png"
            # plt.savefig(T._filepath+ r"\Figures" + plotFileName,bbox_inches='tight', dpi=300)
            if isDisplayed:
                plt.show(block=True)
            plt.close()


    # pltPdfEnergy plots a probability density function of the sputtering yield
    #    for a SINGLE target on a single plot, as a function of sputtered atom energy.
    #    The target surface is held constant for a single plot.
    #    Boolean isDisplayed is true when the plot is to be displayed onscreen.
    def pltPdfEnergy(self, isDisplayed):

        # for each target object T in the Simulation() object's dictionary
        for tname, T in self._dictTarget.items():
            modvalue = 1 # modulus value, to avoid plotting too many energies
            E_Theta_List = list(T._dictCollisions.keys())# init list of sputtering ion energies for each target
            Ea_list = [] # init list of sputtered atom energies for each target

            # create color palette for the number of energies to be displayed on the plot
            inputList = [tuple[0] for tuple in E_Theta_List if tuple[0]%modvalue==0] # list of energies to be displayed
            evenly_spaced_interval = np.linspace(0, 1, len( inputList ) )
            colors = itertools.cycle([plt.cm.rainbow(x) for x in evenly_spaced_interval])

            # for every ion energy in the Target() object's dictionary of collisions
            for i, ((e,th), collisions) in enumerate(T._dictCollisions.items()):
                Ea_list.append([]) #append a new blank list to the sputtered atom list

                # for every collision at this ion energy
                for collision in collisions:
                    Ea_list[i].extend(collision._Eatom)

            # make the list of lists Ea_arr a numpy array of numpy arrays
            Ea_arr = np.array([np.array(x) for x in Ea_list],dtype="object")

            # for each ion energy in the ion energy list Ei_list
            for i,tuple in enumerate(E_Theta_List):
                # if there's enough sputtered atoms and this is an energy to be displayed
                if(len(Ea_arr[i]) > 3 and tuple[0]%modvalue==0):
                    # plot a PDF for this ion energy
                    kde = gaussian_kde( Ea_arr[i] )
                    dist_space = np.linspace( min(Ea_arr[i]), max(Ea_arr[i]), 100 )
                    plt.plot( dist_space, kde(dist_space), color = next(colors), label= r"$E_i = {}~eV, \theta_i = {}\degree$".format(tuple[0],tuple[1]) )

            plt.legend(loc="upper right")
            plt.xlabel("Sputtered Atom Energy (eV)")
            #plt.xlim(right=500) # cutoff tail if too large
            plt.ylabel("Probability Density")
            plt.title("Energy Distribution of Sputterants for {}{} Impact on {}".format(self._ionObject._ionName, r'$^+$', tname))
            # save figure if desired
            now = datetime.now()
            plotFileName = r"\\" + self._ionObject._ionName + r"_" + tname + r"_"+ now.strftime("%m-%d-%Y") + r"_PdfE"+r".png"
            # plt.savefig(T._filepath+ r"\Figures" + plotFileName, dpi=300, bbox_inches='tight')
            if isDisplayed:
                plt.show(block=True)
            plt.close()


    # pltPdfAngle plots a probability density function of the sputtering yield
    #    for a SINGLE target on a single plot, as a function of sputtered atom angle wrt surface normal.
    #    The target surface is held constant for a single plot.
    #    Boolean isDisplayed is true when the plot is to be displayed onscreen.
    def pltPdfSingleAngle(self, isDisplayed):

        # for each target object T in the Simulation() object's dictionary
        for tname, T in self._dictTarget.items():
            modvalue = 1 # modulus value, to avoid plotting too many energies
            E_Theta_List = list(T._dictCollisions.keys())# init list of sputtering ion energies for each target
            Theta_list = [] # init list of sputtered atom directions for each target

            # create color palette for the number of energies to be displayed on the plot
            inputList = [tuple[0] for tuple in E_Theta_List if tuple[0]%modvalue==0] # list of energies to be displayed
            evenly_spaced_interval = np.linspace(0, 1, len( inputList ) )
            colors = itertools.cycle([plt.cm.rainbow(x) for x in evenly_spaced_interval])

            # for every ion energy in the Target() object's dictionary of collisions
            for i, ((e,th), collisions) in enumerate(T._dictCollisions.items()):
                Theta_list.append([]) #append a new blank list to the sputtered atom list

                # for every collision at this ion energy
                for collision in collisions:
                    Theta_list[i].extend(collision._alphaAtom)

            # make the list of lists Theta_arr a numpy array of numpy arrays
            Theta_arr = np.array([np.array(x) for x in Theta_list],dtype="object")

            # for each ion energy in the ion energy list Ei_list
            for i,tuple in enumerate(E_Theta_List):
                # if there's enough sputtered atoms and this is an energy to be displayed
                if(len(Theta_arr[i]) > 3 and tuple[0]%modvalue==0):
                    # plot a PDF for this ion energy
                    kde = gaussian_kde( Theta_arr[i] )
                    dist_space = np.linspace( min(Theta_arr[i]), max(Theta_arr[i]), 100 )
                    plt.plot( dist_space, kde(dist_space), color = next(colors), label= r"$E_i = {}~eV, \theta_i = {}\degree$".format(tuple[0],tuple[1]) )

            plt.legend(loc="upper right")
            plt.xlabel("Sputtering Angle (degrees)")
            #plt.xlim(right=500) # cutoff tail if too large
            plt.ylabel("Probability Density")
            plt.title("Angular Distribution of Sputterants for {}{} Impact on {}".format(self._ionObject._ionName, r'$^+$', tname))
            # save figure if desired
            now = datetime.now()
            plotFileName = r"\\" + self._ionObject._ionName + r"_" + tname + r"_" + now.strftime("%m-%d-%Y") + r"_PdfAng"+r".png"
            # plt.savefig(T._filepath+ r"\Figures" + plotFileName, dpi=300, bbox_inches='tight')
            if isDisplayed:
                plt.show(block=True)
            plt.close()
    
    # pltPdfAngle3D plots a three dimensional probability density function of the sputtering yield
    #    for a SINGLE target on a single plot.
    #    The target surface is held constant for a single plot.
    #    Boolean isDisplayed is true when the plot is to be displayed onscreen.
    def pltPdfAngle3D(self, isDisplayed):

        # for each target object T in the Simulation() object's dictionary
        for tname, T in self._dictTarget.items():
            modvalue = 1 # modulus value, to avoid plotting too many energies
            E_List = list(T._dictCollisions.keys())# init list of sputtering ion energies for each target
            Alpha_list = [] # init list of sputtered atom directions for each target
            Beta_list = [] # init list of sputtered atom directions for each target
            Gamma_list = [] # init list of sputtered atom directions for each target

            # create color palette for the number of energies to be displayed on the plot
            inputList = [tuple[0] for tuple in E_List if tuple[0]%modvalue==0] # list of energies to be displayed
            evenly_spaced_interval = np.linspace(0, 1, len( inputList ) )
            colors = itertools.cycle([plt.cm.rainbow(x) for x in evenly_spaced_interval])

            # for every ion energy in the Target() object's dictionary of collisions
            for i, ((e,th), collisions) in enumerate(T._dictCollisions.items()):
                Alpha_list.append([]) #append a new blank list to the sputtered atom list
                Beta_list.append([]) #append a new blank list to the sputtered atom list
                Gamma_list.append([]) #append a new blank list to the sputtered atom list

                # for every collision at this ion energy
                for collision in collisions:
                    Alpha_list[i].extend(collision._alphaAtom)
                    Beta_list[i].extend(collision._betaAtom)
                    Gamma_list[i].extend(collision._gammaAtom)

            # make the list of lists Alpha_arr a numpy array of numpy arrays
            Alpha_arr = np.array([np.array(x) for x in Alpha_list],dtype="object")
            Beta_arr = np.array([np.array(x) for x in Beta_list],dtype="object")
            Gamma_arr = np.array([np.array(x) for x in Gamma_list],dtype="object")

            # for each ion energy in the ion energy list Ei_list
            for i,tuple in enumerate(E_List):
                # if there's enough sputtered atoms and this is an energy to be displayed
                if(len(Alpha_arr[i]) > 3 and tuple[0]%modvalue==0):
                    # Plot a PDF for this ion energy
                    # Convert euler angles to x, y and z components of sputtered atom vector
                    Alpha_arr_comp = np.cos((Alpha_arr[i])*np.pi/180)
                    Beta_arr_comp = np.cos(Beta_arr[i]*np.pi/180)
                    Gamma_arr_comp = np.cos(Gamma_arr[i]*np.pi/180)

                    # Create PDFs of vectors in spherical coordinates (physics convention) 
                    counts_phi = np.arctan(Beta_arr_comp / Alpha_arr_comp) #Spans -pi/2 to pi/2 (measured from azimuth, +x)
                    counts_theta = np.arccos(Gamma_arr_comp) #Spans 0 to pi (measured from zenith, +z)
                    assert(size(counts_phi) == size(counts_theta)),"different number of phi/theta entries"

                    # Create linspaces that span the limits of the dataset
                    div = 100
                    phi_lower_limit = -np.pi/2
                    phi_upper_limit = np.pi/2
                    theta_lower_limit = 0
                    theta_upper_limit = np.pi                    
                    dist_space_phi = np.linspace(phi_lower_limit, phi_upper_limit, div)
                    dist_space_theta = np.linspace(theta_lower_limit, theta_upper_limit, div)
                    threshold = np.pi/div

                    PHI, THETA = np.meshgrid(dist_space_phi, dist_space_theta)

                    hist = np.zeros((size(dist_space_theta),size(dist_space_phi)))
                    np.set_printoptions(threshold=sys.maxsize)

                    for t in range(size(dist_space_theta)):
                        for p in range(size(dist_space_phi)):
                            for i in range(size(counts_phi)):
                                if counts_phi[i] < dist_space_phi[p] + threshold/2 and \
                                    counts_phi[i] > dist_space_phi[p] - threshold/2  and \
                                    counts_theta[i] < dist_space_theta[t] + threshold/2 and \
                                    counts_theta[i] >  dist_space_theta[t] - threshold/2:
                                    hist[t][p] = hist[t][p] + 1
                                else:
                                    print
                    
                    # Apply gaussian filter
                    sigma_x = 5
                    sigma_y = 5
                    sigma = [sigma_y, sigma_x]
                    hist = sp.ndimage.filters.gaussian_filter(hist, sigma, mode='constant')
                    print(hist)

                    # #Convert back to Cartesian
                    X = (hist * np.sin(THETA) * np.cos(PHI))/sum(sum(hist))
                    Y = (hist * np.sin(THETA) * np.sin(PHI))/sum(sum(hist))
                    Z = (hist * np.cos(THETA))/sum(sum(hist))                              

                    fig = plt.figure()

                    # # Plot the surface.
                    ax = fig.add_subplot(projection='3d')
                    
                    # Create cubic bounding box to simulate equal aspect ratio
                    max_range = np.array([X.max()-0, Y.max()-Y.min(), Z.max()-Z.min()]).max()
                    Xb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][0].flatten() + 0.5*(X.max()+0)
                    Yb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][1].flatten() + 0.5*(Y.max()+Y.min())
                    Zb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][2].flatten() + 0.5*(Z.max()+Z.min())
                    
                    # Comment or uncomment following both lines to test the fake bounding box:
                    for xb, yb, zb in zip(Xb, Yb, Zb):
                        ax.plot([yb], [zb], [xb], 'w')
                    s = ax.plot_surface(Y, Z, X, cmap=plt.get_cmap('jet'))
                    ax.set_xlabel(r'Y')
                    ax.set_ylabel(r'Z')
                    ax.set_zlabel(r'X')

                    angles = np.linspace(0,360, 180)[:-1] # Take 20 angles between 0 and 360
                    
                    name = str(i) + "-" +str(th) + ".mp4"
                    rotanimate(ax, angles, name, fps=20, bitrate=1000)

    def NormFunction(self, var_1):
        return abs(np.trapz(self.phi * 180/np.pi, self.hist/var_1)-1);                            

# pltPdfAngle2D plots a slice of the 3D probability density function of the sputtering yield
    #    for a SINGLE target on a single plot.
    #    Viewed on the plane formed by the incident ion and the surface normal vectors. 
    #    The target surface is held constant for a single plot.
    #    Boolean isDisplayed is true when the plot is to be displayed onscreen.
    def pltPdfAngle2D(self, isDisplayed):

        # for each target object T in the Simulation() object's dictionary
        for tname, T in self._dictTarget.items():
            modvalue = 1 # modulus value, to avoid plotting too many energies
            E_List = list(T._dictCollisions.keys())# init list of sputtering ion energies for each target
            Alpha_list = [] # init list of sputtered atom directions for each target
            Beta_list = [] # init list of sputtered atom directions for each target
            Gamma_list = [] # init list of sputtered atom directions for each target
            polar_plotting = False

            # create color palette for the number of energies to be displayed on the plot
            inputList = [tuple[0] for tuple in E_List if tuple[0]%modvalue==0] # list of energies to be displayed
            evenly_spaced_interval = np.linspace(0, 1, len( inputList ) )
            colors = itertools.cycle([plt.cm.rainbow(x) for x in evenly_spaced_interval])

            # for every ion energy in the Target() object's dictionary of collisions
            for i, ((e,th), collisions) in enumerate(T._dictCollisions.items()):
                Alpha_list.append([]) #append a new blank list to the sputtered atom list
                Beta_list.append([]) #append a new blank list to the sputtered atom list
                Gamma_list.append([]) #append a new blank list to the sputtered atom list

                # for every collision at this ion energy
                for collision in collisions:
                    Alpha_list[i].extend(collision._alphaAtom)
                    Beta_list[i].extend(collision._betaAtom)
                    Gamma_list[i].extend(collision._gammaAtom)

            # make the list of lists Alpha_arr a numpy array of numpy arrays
            Alpha_arr = np.array([np.array(x) for x in Alpha_list],dtype="object")
            Beta_arr = np.array([np.array(x) for x in Beta_list],dtype="object")
            Gamma_arr = np.array([np.array(x) for x in Gamma_list],dtype="object")

            prev_nrg = []
            # for each ion energy in the ion energy list Ei_list
            for i,tuple in enumerate(E_List):
                if prev_nrg != tuple[0]:
                    fig = plt.figure()
                    if polar_plotting:
                        ax = fig.add_subplot(111, projection='polar', xlim=(-90, 90))
                    elif not polar_plotting:
                        ax = fig.add_subplot(111)
                        
                    
                # if there's enough sputtered atoms and this is an energy to be displayed
                if(len(Alpha_arr[i]) > 3 and tuple[0]%modvalue==0):
                    # plot a PDF for this ion energy
                    # convert euler angles to x, y and z components of sputtered atom vector
                    Alpha_arr_comp = np.cos((Alpha_arr[i])*np.pi/180)
                    Beta_arr_comp = np.cos(Beta_arr[i]*np.pi/180)
                    Gamma_arr_comp = np.cos(Gamma_arr[i]*np.pi/180)
                    
                    # Create PDFs of vectors in spherical coordinates (physics convention) 
                    counts_phi = np.arctan(Beta_arr_comp / Alpha_arr_comp) #Spans -pi/2 to pi/2 (measured from azimuth, +x)
                    counts_theta = np.arccos(Gamma_arr_comp) #Spans 0 to pi (measured from zenith, +z)
                    assert(size(counts_phi) == size(counts_theta)), "different number of phi/theta entries"

                    # fig = plt.figure()
                    # ax = fig.add_subplot(111, projection='polar', xlim=(-90, 90))

                    # Create linspaces that span the limits of the dataset
                    div = 361
                    phi_lower_limit = -np.pi/2
                    phi_upper_limit = np.pi/2
                    theta_target = np.pi/2
                    theta_threshold = (np.pi/180)                   
                    dist_space_phi = np.linspace(phi_lower_limit, phi_upper_limit, div)
                    
                    points =  np.vstack((counts_phi,counts_theta)).T
                    points_reduced = np.empty([1,2])
                    
                    #Approach 2
                    app_2_tic = time.perf_counter()
                    points_reduced = points[(points[:,1] > theta_target-theta_threshold) & (points[:,1] < theta_target+theta_threshold)]
                    print(points_reduced)
                    app_2_toc = time.perf_counter()
                    app_2_time = time.strftime('%H:%M:%S', time.gmtime(app_2_toc-app_2_tic))
                    print("App 2 time: ", app_2_time)    
                    print("Total points in reduced dataset: ", np.shape(points_reduced)[0]) 
                    
                    points_reduced = np.sort(points_reduced[:,0], axis = 0)
                    
                    app_post_1_tic = time.perf_counter()
                    #Approach 1
                    hist, bin_edges = np.histogram(points_reduced, bins=div, range=(phi_lower_limit, phi_upper_limit))
                    app_post_1_toc = time.perf_counter()
                    app_post_1_time = time.strftime('%H:%M:%S', time.gmtime(app_post_1_toc-app_post_1_tic))
                    print("App post 1 time: ", app_post_1_time)                        
                                
                    # Apply gaussian filter
                    sigma = 1
                    hist = sp.ndimage.filters.gaussian_filter(hist, sigma, mode='constant')
                                
                    #Normalize
                    norm_start = 1
                    f = lambda var_1: abs(np.trapz(hist/var_1, x=dist_space_phi)-1)
                    norm_result = sp.optimize.minimize(f, norm_start, tol=1e-10)
                    hist_norm = norm_result['x'][0]   
                    
                    # text_file_name = str(tuple[0]) + r"_" + str(tuple[1]) + r"_" + tname + r"_" + str(time.time()) + "r_SMOOTHED_SIGMA_1" + r".csv"
                    # np.savetxt(text_file_name, hist, delimiter=",")            

                    # Polar Plotting 
                    if polar_plotting:
                        ax.set_thetamin(-90) # set the limits
                        ax.set_thetamax(90)
                        ax.set_theta_direction(-1)
                        ax.set_theta_offset(.5*np.pi) # point the origin towards the top
                        ax.set_thetagrids(range(-90, 91, 10)) # set the gridlines
                        ax.set_title('2D Sputterant Probability Distribution', pad=-50) # add title and relocate negative value lowers the location
                        ax.plot(dist_space_phi, hist/hist_norm, color = next(colors), label= r"$E_i = {}~eV, \theta_i = {}\degree$".format(tuple[0],tuple[1]))
                        ax.set_rlabel_position(-22.5)  # Move radial labels away from plotted line
                        ax.grid(True)
                        plt.legend(loc="upper right", bbox_to_anchor=(1.5, 0.75))
                        plot_file_name = str(tuple[0]) + r"_" + str(tuple[1]) + r"_" + tname + r"_" + str(time.time()) + r".png"
                        plt.savefig(self.user_inputs.output_dir + r"//" + plot_file_name, dpi=300, bbox_inches='tight')
                    
                    # Cartesian Plotting
                    elif not polar_plotting:
                        ax.set_title('2D Sputterant Probability Distribution', pad=-50) # add title and relocate negative value lowers the location
                        ax.plot(dist_space_phi, hist/hist_norm, color = next(colors), label= r"$E_i = {}~eV, \theta_i = {}\degree$".format(tuple[0],tuple[1]))
                        ax.grid(True)
                        plt.legend(loc="upper right", bbox_to_anchor=(1.5, 0.75))
                        plot_file_name = str(tuple[0]) + r"_" + str(tuple[1]) + r"_" + tname + r"_" + str(time.time()) + r".png"
                        # plt.savefig(self.user_inputs.output_dir + r"//" + plot_file_name, dpi=300, bbox_inches='tight')
                        

                    # Update previous energy from angle/energy simulation combination 
                    prev_nrg = tuple[0]
                    # plt.show()

            else:
                pass

        plt.show()
            